package com.jagex.web;


import com.jagex.game.Player;
import com.jagex.game.Skills;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;

public class CMSHandler extends Thread {

    private static Connection connection;

    static {
        createConnection();
    }

    public static void createConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost/jweb", "admin", "password");
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    public static boolean characterExists(String username) {
        try {
            PreparedStatement loginStatement = connection.prepareStatement("SELECT username FROM characters WHERE username = ? LIMIT 1;");
            loginStatement.setString(1, username);
            try (ResultSet set = loginStatement.executeQuery()) {
                if (set.next()) {
                    return true;
                }
            }
            return false;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void saveAccount(Player player) {
        if (player == null) {
            return;
        }
        String ip = "";
        if (player.getSession() != null) {
            ip = player.getSession().getIP();
        }

        String username = player.getUsername();
        if (!characterExists(username)) {
            createAccount(username, player.getPassword());
            return;
        }
        try {
            StringBuilder b = new StringBuilder();
            b.append("UPDATE characters SET ");
            String[] update = new String[]{
                    "username = '" + username + "'",
                    //"password ='" + pw + "'",
                    "last_signin = '" + new java.sql.Timestamp(System.currentTimeMillis()) + "'",
                    "last_ip = '" + ip + "'",
                    "online = '0'",

            };
            for (String d : update) {
                b.append(d).append(",");
            }
            b.deleteCharAt(b.length() - 1);
            b.append(" WHERE username = '" + username + "';");
            PreparedStatement statement = connection.prepareStatement(b.toString());
            statement.execute();
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    public static void removeDonated(String user) {
        try {
            createConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM donation WHERE username = ? LIMIT 1;");
            statement.setString(1, user);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static int checkDonated(String username) {
        try {
            createConnection();
            PreparedStatement loginStatement = connection.prepareStatement("SELECT * FROM donation WHERE username = ? LIMIT 1;");
            loginStatement.setString(1, username);
            try (ResultSet set = loginStatement.executeQuery()) {
                if (set.next()) {
                    String user = set.getString("username");
                    boolean claimed = set.getInt("claimed") == 1;
                    if (!user.equalsIgnoreCase(username)) {
                        System.out.println("USer error");
                        return -1;
                    }
                    return set.getInt("productid");
                }
            }
            return -1;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public static int getDatabaseId(String username) {
        try {
            PreparedStatement loginStatement = connection.prepareStatement("SELECT id FROM characters WHERE username = ? LIMIT 1;");
            loginStatement.setString(1, username);
            try (ResultSet set = loginStatement.executeQuery()) {
                if (set.next()) {
                    return set.getInt("id");
                }
            }
            return -1;
        } catch (SQLException ex) {
            return -1;
        }
    }

    public static String getPassword(String username) {
        try {
            PreparedStatement loginStatement = connection.prepareStatement("SELECT password FROM characters WHERE username = ? LIMIT 1;");
            loginStatement.setString(1, username);
            try (ResultSet set = loginStatement.executeQuery()) {
                if (set.next()) {
                    return set.getString("password");
                }
            }
            return null;
        } catch (SQLException ex) {
            return null;
        }
    }

    public static void main(String[] args) {
        //logger.info("Saving..");
        // saveHighscores("RSPSr");
//        System.out.println(java.sql.Date.valueOf(LocalDate.now().plusWeeks(3)));
//        insertBan("swag", "swag", 1);
        //logger.info("Saved");
    }

    public static void insertBan(String mod, String user, int type, boolean delete) {
        try {
            if (delete) {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM offences WHERE character_id = '" + getDatabaseId(user) + "';");
                statement.execute();
                connection.commit();
                return;
            }
            PreparedStatement statement = connection.prepareStatement("INSERT INTO offences (character_id, type, date, expire_date, moderator_id, reason, appeal_status, appeal_data, expired)  VALUES (?, ?, CURRENT_TIMESTAMP, ?, '" + getDatabaseId(mod) + "', 'in-game ban', '0', '','0');");
            statement.setInt(1, getDatabaseId(user));
            statement.setInt(2, type);
            Calendar c = Calendar.getInstance();
            java.sql.Date startDate = new java.sql.Date(c.getTimeInMillis());
            statement.setDate(3, startDate);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isBlacklisted(String ip) {
        try {
            PreparedStatement blacklist = connection.prepareStatement("SELECT ip_address FROM blacklist WHERE ip_address = ? LIMIT 1;");
            blacklist.setString(1, ip);
            try (ResultSet set = blacklist.executeQuery()) {
                if (set.next()) {
                    return set.getString("last_ip").equalsIgnoreCase(ip);
                }
            }
            return false;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void saveHighscores(Player player) {
        if (player == null) {
            return;
        }

        String user = player.getUsername();

        if (!characterExists(user)) {
            return;
        }
        try {
            if (getDatabaseId(user) == 0) {
                return;
            }
            StringBuilder b = new StringBuilder();
            b.append("REPLACE INTO characters_statistics (master_id, attack_level, attack_exp, defence_level," +
                    " defence_exp, strength_level, strength_exp, constitution_level, constitution_exp, range_level, " +
                    "range_exp, prayer_level, prayer_exp, magic_level, magic_exp, cooking_level, cooking_exp, " +
                    "woodcutting_level, woodcutting_exp, fletching_level, fletching_exp, fishing_level," +
                    " fishing_exp, firemaking_level, firemaking_exp, crafting_level, crafting_exp," +
                    " smithing_level, smithing_exp, mining_level, mining_exp, herblore_level, herblore_exp," +
                    " agility_level, agility_exp, thieving_level, thieving_exp, slayer_level, slayer_exp, " +
                    "farming_level, farming_exp, runecrafting_level, runecrafting_exp, construction_level, " +
                    "construction_exp, hunter_level, hunter_exp, summoning_level, summoning_exp," +
                    " dungeoneering_level, dungeoneering_exp," +
                    " constitution_points, prayer_points, run_energy, exp_counter, special_energy, poison_amount, total_level, total_exp, loyalty_points, username, combat_level, playerRights) ");
            b.append("VALUE (");

            for (int i : new int[63]) {
                b.append("?, ");
            }
            b.deleteCharAt(b.length() - 2).append(");");
            PreparedStatement statement = connection.prepareStatement(b.toString());
            statement.setInt(1, getDatabaseId(user));
            statement.setInt(2, player.getSkills().getLevel(Skills.ATTACK));//attack
            statement.setInt(3, (int) player.getSkills().getXp(Skills.ATTACK));
            statement.setInt(4, player.getSkills().getLevel(Skills.DEFENCE));//defence
            statement.setInt(5, (int) player.getSkills().getXp(Skills.DEFENCE));
            statement.setInt(6, player.getSkills().getLevel(Skills.STRENGTH));//strength
            statement.setInt(7, (int) player.getSkills().getXp(Skills.STRENGTH));
            statement.setInt(8, player.getSkills().getLevel(Skills.HITPOINTS));//constitution
            statement.setInt(9, (int) player.getSkills().getXp(Skills.HITPOINTS));
            statement.setInt(10, player.getSkills().getLevel(Skills.RANGE));//range
            statement.setInt(11, (int) player.getSkills().getXp(Skills.RANGE));
            statement.setInt(12, player.getSkills().getLevel(Skills.PRAYER));//prayer
            statement.setInt(13, (int) player.getSkills().getXp(Skills.PRAYER));
            statement.setInt(14, player.getSkills().getLevel(Skills.MAGIC));//magic
            statement.setInt(15, (int) player.getSkills().getXp(Skills.MAGIC));
            statement.setInt(16, player.getSkills().getLevel(Skills.COOKING));//cooking
            statement.setInt(17, (int) player.getSkills().getXp(Skills.COOKING));
            statement.setInt(18, player.getSkills().getLevel(Skills.WOODCUTTING));//woodcutting
            statement.setInt(19, (int) player.getSkills().getXp(Skills.WOODCUTTING));
            statement.setInt(20, player.getSkills().getLevel(Skills.FLETCHING));//fletching
            statement.setInt(21, (int) player.getSkills().getXp(Skills.FLETCHING));
            statement.setInt(22, player.getSkills().getLevel(Skills.FISHING));//fishing
            statement.setInt(23, (int) player.getSkills().getXp(Skills.FISHING));
            statement.setInt(24, player.getSkills().getLevel(Skills.FIREMAKING));//firemaking
            statement.setInt(25, (int) player.getSkills().getXp(Skills.FIREMAKING));
            statement.setInt(26, player.getSkills().getLevel(Skills.CRAFTING));//crafting
            statement.setInt(27, (int) player.getSkills().getXp(Skills.CRAFTING));
            statement.setInt(28, player.getSkills().getLevel(Skills.SMITHING));//smithing
            statement.setInt(29, (int) player.getSkills().getXp(Skills.SMITHING));
            statement.setInt(30, player.getSkills().getLevel(Skills.MINING));//mining
            statement.setInt(31, (int) player.getSkills().getXp(Skills.MINING));
            statement.setInt(32, player.getSkills().getLevel(Skills.HERBLORE));//herblore
            statement.setInt(33, (int) player.getSkills().getXp(Skills.HERBLORE));
            statement.setInt(34, player.getSkills().getLevel(Skills.AGILITY));//agility
            statement.setInt(35, (int) player.getSkills().getXp(Skills.AGILITY));
            statement.setInt(36, player.getSkills().getLevel(Skills.THIEVING));//thieving
            statement.setInt(37, (int) player.getSkills().getXp(Skills.THIEVING));
            statement.setInt(38, player.getSkills().getLevel(Skills.SLAYER));//slayer
            statement.setInt(39, (int) player.getSkills().getXp(Skills.SLAYER));
            statement.setInt(40, player.getSkills().getLevel(Skills.FARMING));//farming
            statement.setInt(41, (int) player.getSkills().getXp(Skills.FARMING));
            statement.setInt(42, player.getSkills().getLevel(Skills.RUNECRAFTING));//runecraft
            statement.setInt(43, (int) player.getSkills().getXp(Skills.RUNECRAFTING));
            statement.setInt(44, player.getSkills().getLevel(Skills.CONSTRUCTION));//construction
            statement.setInt(45, (int) player.getSkills().getXp(Skills.CONSTRUCTION));
            statement.setInt(46, player.getSkills().getLevel(Skills.HUNTER));//hunter
            statement.setInt(47, (int) player.getSkills().getXp(Skills.HUNTER));
            statement.setInt(48, player.getSkills().getLevel(Skills.SUMMONING));//summoning
            statement.setInt(49, (int) player.getSkills().getXp(Skills.SUMMONING));
            statement.setInt(50, player.getSkills().getLevel(Skills.DUNGEONEERING));//dungeoneering
            statement.setInt(51, (int) player.getSkills().getXp(Skills.DUNGEONEERING));
            statement.setInt(52, player.getMaxHitpoints());//constitution_points
            statement.setInt(53, player.getPrayer().getPrayerpoints());//prayer
            statement.setInt(54, player.getRunEnergy());//run
            statement.setInt(55, player.getSkills().getTotalExp());//exp_counter
            statement.setInt(56, player.getCombatDefinitions().getSpecialAttackPercentage());//special energy
            statement.setInt(57, 0);//poison amount
            statement.setInt(58, player.getSkills().getTotalLevel());//total level
            statement.setInt(59, player.getSkills().getTotalExp());//total exp
            statement.setInt(60, 0);//player.getLoyaltyPoints());//loyalty points
            
            statement.setString(61, player.getDisplayName());//username
            statement.setInt(62, player.getSkills().getTotalExp());//combat level
            statement.setInt(63, player.getRights());//player.getLoyaltyPoints());//player rights
            statement.execute();
            connection.commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static PunishType getPunishmentType(String player) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM offences WHERE character_id = ?");
            statement.setInt(1, getDatabaseId(player));

            try (ResultSet set = statement.executeQuery()) {
                if (set.next()) {
                    int appealStatus = set.getInt("appeal_status"); //0 - active  1-  not active
                    int type = set.getInt("type"); //0 - ban  1-  not mute
                    Date start_date = set.getDate("date");
                    Date end_date = set.getDate("expire_date");
                    int expired = set.getInt("expired");
                    boolean banned = start_date.getTime() < end_date.getTime();
                    if (banned && expired == 0) {
                        switch (type) {
                            case 2:
                                return PunishType.BAN;
                            case 1:
                                return PunishType.MUTE;
                        }
                    } else {
                        return PunishType.NONE;
                    }

                }
            }
            return PunishType.NONE;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return PunishType.NONE;
    }

    public static void writeConnectionLog(String ip, int port) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO connection_logs (ip, time, port) VALUES(?, CURRENT_TIMESTAMP(), ?);");
            statement.setString(1, ip);
            statement.setInt(2, port);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void adventureLog(String user, String shortDesc, String fullDesc) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO activity_logs (master_id, username, date, short_desc, full_desc) VALUES(?, ?, CURRENT_TIMESTAMP(), ?, ?);");
            statement.setInt(1, getDatabaseId(user));
            statement.setString(2, user);
            statement.setString(3, shortDesc);
            statement.setString(4, fullDesc);
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateOnline(String user, boolean online) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE characters SET online = '" + (online ? 1 : 0) + "' WHERE username = '" + user + "';");
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createAccount(String user, String pass) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO characters (email, password, username, register_date) VALUES('" + user + "', '" + pass + "', '" + user + "', CURRENT_TIMESTAMP);");
            statement.execute();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (connection == null) {
                    createConnection();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public enum PunishType {
        BAN, MUTE, NONE
    }
}
