-- phpMyAdmin SQL Dump
-- version 4.4.15.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2016 at 09:32 PM
-- Server version: 5.6.28
-- PHP Version: 5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rs3cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE IF NOT EXISTS `activity_logs` (
  `id` int(10) unsigned NOT NULL,
  `master_id` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `short_desc` tinytext NOT NULL,
  `full_desc` tinytext NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `master_id`, `username`, `date`, `short_desc`, `full_desc`) VALUES
(1, 1, 'Bobby', '2012-06-03 16:05:42', 'Registered', 'I joined RuneScape.'),
(2, 1, 'Bobby', '2012-06-03 16:05:42', 'Murdered', 'Killed a cat'),
(3, 1, 'Bobby', '2012-06-03 16:05:42', 'Looted', 'Looted 50000gp!!!'),
(4, 1, 'Bobby', '2012-06-03 16:05:42', 'Died', 'Got killed by a level 3 Goblin.'),
(5, 1, 'Bobby', '2012-06-03 16:05:42', 'Died', 'Died by a level 99 Paladin.'),
(6, 1, 'Bobby', '2012-06-03 16:05:42', 'Died', 'Unknown cause'),
(7, 1, 'Bobby', '2012-06-03 16:05:42', 'Won a Duel', 'Won 4000gp'),
(8, 0, 'Bobby', '2012-06-03 16:05:42', 'GGGGGG', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `blacklist`
--

CREATE TABLE IF NOT EXISTS `blacklist` (
  `id` int(10) unsigned NOT NULL,
  `ip_address` varchar(40) NOT NULL DEFAULT '',
  `date` datetime DEFAULT '0000-00-00 00:00:00',
  `moderator` varchar(12) DEFAULT 'undefined',
  `reason` text,
  `appeal_status` enum('0','1') DEFAULT NULL,
  `appeal_data` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blacklist`
--

INSERT INTO `blacklist` (`id`, `ip_address`, `date`, `moderator`, `reason`, `appeal_status`, `appeal_data`) VALUES
(1, '89.102.9.211', '2010-10-11 17:31:12', 'undefined', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(11) unsigned NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(40) NOT NULL DEFAULT '',
  `username` varchar(12) NOT NULL DEFAULT '',
  `online` tinyint(4) NOT NULL DEFAULT '0',
  `register_ip` varchar(40) DEFAULT ' ',
  `register_date` datetime DEFAULT NULL,
  `last_ip` varchar(40) NOT NULL DEFAULT '::0',
  `last_signin` datetime DEFAULT NULL,
  `birthyear` smallint(6) NOT NULL DEFAULT '1995',
  `coord_x` smallint(6) NOT NULL DEFAULT '3222',
  `coord_y` smallint(6) NOT NULL DEFAULT '3219',
  `coord_z` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `username_last_changed` datetime DEFAULT NULL,
  `previous_username` varchar(12) DEFAULT '',
  `account_file` varchar(12) DEFAULT '',
  `rights` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `email`, `password`, `username`, `online`, `register_ip`, `register_date`, `last_ip`, `last_signin`, `birthyear`, `coord_x`, `coord_y`, `coord_z`, `username_last_changed`, `previous_username`, `account_file`, `rights`) VALUES
(1, 'admin@localhost.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 0, '127.0.0.1', '2012-06-03 16:05:42', '192.168.1.100', '2013-12-24 14:45:49', 1993, 3221, 3218, 0, NULL, '', '', 0),
(2, 'admin@locablhost.com', 'sfg', 'noobhunter22', 0, '127.0.0.1', '2012-06-03 16:05:42', '192.168.1.100', '2013-12-24 14:45:49', 1993, 3221, 3218, 0, NULL, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `characters_contacts`
--

CREATE TABLE IF NOT EXISTS `characters_contacts` (
  `id` int(11) unsigned NOT NULL,
  `master_id` int(10) unsigned NOT NULL DEFAULT '0',
  `contact_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(4) unsigned NOT NULL,
  `fc_rank` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `characters_permissions`
--

CREATE TABLE IF NOT EXISTS `characters_permissions` (
  `master_id` int(11) unsigned NOT NULL,
  `permission` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters_permissions`
--

INSERT INTO `characters_permissions` (`master_id`, `permission`) VALUES
(1, 'client_admin'),
(1, 'system_admin'),
(1, 'web_admin');

-- --------------------------------------------------------

--
-- Table structure for table `characters_statistics`
--

CREATE TABLE IF NOT EXISTS `characters_statistics` (
  `master_id` int(10) unsigned NOT NULL,
  `attack_level` tinyint(3) unsigned DEFAULT '1',
  `attack_exp` int(11) DEFAULT '0',
  `defence_level` tinyint(3) unsigned DEFAULT '1',
  `defence_exp` int(11) DEFAULT '0',
  `strength_level` tinyint(3) unsigned DEFAULT '1',
  `strength_exp` int(11) DEFAULT '0',
  `constitution_level` tinyint(3) unsigned DEFAULT '10',
  `constitution_exp` int(11) DEFAULT '1154',
  `range_level` tinyint(3) unsigned DEFAULT '1',
  `range_exp` int(11) DEFAULT '0',
  `prayer_level` tinyint(3) unsigned DEFAULT '1',
  `prayer_exp` int(11) DEFAULT '0',
  `magic_level` tinyint(3) unsigned DEFAULT '1',
  `magic_exp` int(11) DEFAULT '0',
  `cooking_level` tinyint(3) unsigned DEFAULT '1',
  `cooking_exp` int(11) DEFAULT '0',
  `woodcutting_level` tinyint(3) unsigned DEFAULT '1',
  `woodcutting_exp` int(11) DEFAULT '0',
  `fletching_level` tinyint(3) unsigned DEFAULT '1',
  `fletching_exp` int(11) DEFAULT '0',
  `fishing_level` tinyint(3) unsigned DEFAULT '1',
  `fishing_exp` int(11) DEFAULT '0',
  `firemaking_level` tinyint(3) unsigned DEFAULT '1',
  `firemaking_exp` int(11) DEFAULT '0',
  `crafting_level` tinyint(3) unsigned DEFAULT '1',
  `crafting_exp` int(11) DEFAULT '0',
  `smithing_level` tinyint(3) unsigned DEFAULT '1',
  `smithing_exp` int(11) DEFAULT '0',
  `mining_level` tinyint(3) unsigned DEFAULT '1',
  `mining_exp` int(11) DEFAULT '0',
  `herblore_level` tinyint(3) unsigned DEFAULT '1',
  `herblore_exp` int(11) DEFAULT '0',
  `agility_level` tinyint(3) unsigned DEFAULT '1',
  `agility_exp` int(11) DEFAULT '0',
  `thieving_level` tinyint(3) unsigned DEFAULT '1',
  `thieving_exp` int(11) DEFAULT '0',
  `slayer_level` tinyint(3) unsigned DEFAULT '1',
  `slayer_exp` int(11) DEFAULT '0',
  `farming_level` tinyint(3) unsigned DEFAULT '1',
  `farming_exp` int(11) DEFAULT '0',
  `runecrafting_level` tinyint(3) unsigned DEFAULT '1',
  `runecrafting_exp` int(11) DEFAULT '0',
  `construction_level` tinyint(3) unsigned DEFAULT '1',
  `construction_exp` int(11) DEFAULT '0',
  `hunter_level` tinyint(3) unsigned DEFAULT '1',
  `hunter_exp` int(11) DEFAULT '0',
  `summoning_level` tinyint(3) unsigned DEFAULT '1',
  `summoning_exp` int(11) DEFAULT '0',
  `dungeoneering_level` tinyint(3) unsigned DEFAULT '1',
  `dungeoneering_exp` int(11) DEFAULT '0',
  `constitution_points` smallint(6) DEFAULT '100',
  `prayer_points` smallint(6) DEFAULT '100',
  `run_energy` tinyint(6) unsigned DEFAULT '100',
  `exp_counter` int(11) DEFAULT '0',
  `special_energy` smallint(6) DEFAULT '1000',
  `poison_amount` smallint(6) DEFAULT '0',
  `total_level` smallint(6) DEFAULT NULL,
  `total_exp` bigint(20) DEFAULT NULL,
  `loyalty_points` bigint(40) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `combat_level` int(20) DEFAULT NULL,
  `playerRights` tinyint(3) unsigned DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=511 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters_statistics`
--

INSERT INTO `characters_statistics` (`master_id`, `attack_level`, `attack_exp`, `defence_level`, `defence_exp`, `strength_level`, `strength_exp`, `constitution_level`, `constitution_exp`, `range_level`, `range_exp`, `prayer_level`, `prayer_exp`, `magic_level`, `magic_exp`, `cooking_level`, `cooking_exp`, `woodcutting_level`, `woodcutting_exp`, `fletching_level`, `fletching_exp`, `fishing_level`, `fishing_exp`, `firemaking_level`, `firemaking_exp`, `crafting_level`, `crafting_exp`, `smithing_level`, `smithing_exp`, `mining_level`, `mining_exp`, `herblore_level`, `herblore_exp`, `agility_level`, `agility_exp`, `thieving_level`, `thieving_exp`, `slayer_level`, `slayer_exp`, `farming_level`, `farming_exp`, `runecrafting_level`, `runecrafting_exp`, `construction_level`, `construction_exp`, `hunter_level`, `hunter_exp`, `summoning_level`, `summoning_exp`, `dungeoneering_level`, `dungeoneering_exp`, `constitution_points`, `prayer_points`, `run_energy`, `exp_counter`, `special_energy`, `poison_amount`, `total_level`, `total_exp`, `loyalty_points`, `username`, `combat_level`, `playerRights`) VALUES
(1, 99, 13047696, 99, 13209024, 99, 13823964, 99, 30610052, 99, 41666544, 99, 13344350, 99, 14643000, 74, 1206980, 55, 177500, 99, 13034431, 86, 3685950, 48, 87500, 63, 397500, 55, 175000, 61, 307500, 63, 388137, 99, 13034431, 92, 6608825, 99, 13166931, 99, 200000000, 71, 846399, 99, 200000000, 99, 13034431, 70, 13331790, 120, 200000000, 990, 798, 100, 819827935, 100, 0, 2174, 819827935, 6800, 'Bobby', 84, 2),
(2, 118, 16896764, 118, 13098644, 118, 71565212, 99, 47709840, 113, 28888488, 95, 9514770, 104, 16499790, 70, 744120, 60, 274950, 1, 0, 70, 765960, 60, 293020, 70, 737750, 52, 130000, 52, 130000, 63, 390250, 60, 274216, 80, 2155920, 99, 14060440, 99, 200000000, 52, 130000, 99, 200000000, 1, 0, 98, 14806753, 120, 200000000, 990, 642, 100, 839066887, 40, 0, 1896, 839066887, 15050, 'Grim', 17, 2);

-- --------------------------------------------------------

--
-- Table structure for table `chat_logs`
--

CREATE TABLE IF NOT EXISTS `chat_logs` (
  `id` int(10) unsigned NOT NULL,
  `master_id` int(11) unsigned NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `receiver_id` int(11) unsigned NOT NULL DEFAULT '0',
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_logs`
--

INSERT INTO `chat_logs` (`id`, `master_id`, `date`, `type`, `receiver_id`, `message`) VALUES
(1, 1, '2012-06-03 16:09:28', 0, 0, 'hello world!'),
(2, 1, '2013-12-24 14:43:38', 0, 0, 'Hello!'),
(3, 1, '2013-12-24 14:46:04', 0, 0, 'Hello!');

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE IF NOT EXISTS `configurations` (
  `name` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(7) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`name`, `type`, `value`) VALUES
('debug_mode', 'boolean', 'true'),
('email', 'string', 'support@localhost.com'),
('new_theme', 'string', 'eoc'),
('paypal_currency', 'string', 'USD'),
('paypal_email', 'string', 'test@live.com'),
('recaptcha_key', 'string', '0'),
('recaptcha_secret', 'string', '0'),
('remote_ip', 'string', '127.0.0.1'),
('remote_port', 'uint16', '43594'),
('server_desc', 'string', 'Play for free with hundreds of other players!'),
('server_motto', 'string', 'Free to play!'),
('server_name', 'string', 'GlobalScape'),
('session_check', 'boolean', 'false'),
('site_address', 'string', 'http://localhost/rs3cms/'),
('twitter_url', 'string', '/'),
('twitter_user', 'string', '0'),
('twitter_widget', 'string', '829924492'),
('url_donate', 'string', '/donate'),
('url_forums', 'string', '/forums'),
('url_support', 'string', '/support'),
('whitelist_ip', 'string', '127.0.0.1,127.0.0.3'),
('youtube_channel', 'string', '/'),
('youtube_video', 'string', 'https://www.youtube.com/embed/_odadIT9XhU');

-- --------------------------------------------------------

--
-- Table structure for table `connection_logs`
--

CREATE TABLE IF NOT EXISTS `connection_logs` (
  `id` int(11) NOT NULL,
  `ip` varchar(40) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `port` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donation`
--

CREATE TABLE IF NOT EXISTS `donation` (
  `username` varchar(32) NOT NULL,
  `productid` varchar(100) NOT NULL,
  `claimed` int(11) NOT NULL,
  `index` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL,
  `master_id` int(10) unsigned NOT NULL,
  `date` datetime DEFAULT NULL,
  `ip` varchar(40) NOT NULL,
  `attempt` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `master_id`, `date`, `ip`, `attempt`, `type`) VALUES
(1, 1, '2012-06-03 16:05:46', '127.0.0.1', 2, 0),
(1, 471, '2015-04-27 01:06:25', '127.0.0.1', 1, -1),
(2, 1, '2012-06-03 16:07:22', '127.0.0.1', 2, 1),
(2, 472, '2015-04-27 01:15:13', '127.0.0.1', 1, -1),
(3, 1, '2012-06-03 16:09:19', '127.0.0.1', 2, 1),
(4, 1, '2012-06-03 20:42:12', '127.0.0.1', 2, 0),
(5, 1, '2012-06-03 20:44:03', '127.0.0.1', 2, 1),
(6, 1, '2013-12-24 14:41:19', '192.168.1.100', 2, 0),
(7, 1, '2013-12-24 14:41:28', '192.168.1.100', 2, 1),
(8, 1, '2013-12-24 14:45:40', '192.168.1.100', 2, 0),
(9, 1, '2013-12-24 14:45:49', '192.168.1.100', 2, 1),
(10, 1, '2013-12-24 15:06:40', '::1', 1, -1),
(11, 472, '2015-04-28 09:58:51', '127.0.0.1', 1, -1),
(12, 472, '2015-04-28 10:02:25', '127.0.0.1', 1, -1),
(13, 471, '2015-04-28 10:22:56', '127.0.0.1', 1, -1),
(14, 472, '2015-04-28 10:26:45', '127.0.0.1', 1, -1),
(15, 471, '2015-04-28 22:43:35', '127.0.0.1', 1, -1),
(17, 1, '2015-05-02 00:26:19', '66.230.106.99', 1, -1),
(18, 1, '2015-05-02 10:06:49', '79.141.173.53', 1, -1),
(19, 1, '2015-05-03 09:22:59', '194.187.250.202', 1, -1),
(20, 1, '2015-05-04 08:12:15', '66.230.106.99', 1, -1),
(21, 1, '2015-05-04 08:29:02', '66.230.106.99', 1, -1),
(22, 1, '2015-05-04 08:45:47', '66.230.106.99', 1, -1),
(23, 1, '2015-05-04 08:47:09', '66.230.106.99', 1, -1),
(24, 1, '2015-05-04 20:06:44', '66.230.106.99', 1, -1),
(25, 1, '2015-05-05 07:42:32', '209.37.65.195', 1, -1),
(26, 1, '2015-05-06 18:45:19', '72.220.15.102', 1, -1),
(27, 1, '2015-05-08 13:27:39', '154.5.234.175', 1, -1),
(28, 1, '2015-05-08 18:49:05', '154.5.234.175', 1, -1),
(29, 1, '2015-05-10 13:21:40', '154.5.234.175', 1, -1),
(30, 1, '2015-05-10 13:23:31', '154.5.234.175', 1, -1),
(31, 1, '2016-02-27 02:04:36', '127.0.0.1', 1, -1),
(32, 1, '2016-02-27 03:32:31', '127.0.0.1', 1, -1),
(33, 1, '2016-02-27 04:54:42', '127.0.0.1', 1, -1),
(34, 1, '2016-02-27 08:43:08', '127.0.0.1', 1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `name_change_logs`
--

CREATE TABLE IF NOT EXISTS `name_change_logs` (
  `id` int(11) NOT NULL,
  `master_id` int(10) unsigned NOT NULL DEFAULT '0',
  `previous_name` varchar(12) DEFAULT NULL,
  `date_changed` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `name_change_logs`
--

INSERT INTO `name_change_logs` (`id`, `master_id`, `previous_name`, `date_changed`) VALUES
(1, 1, 'aj123', '2012-01-13 10:03:56'),
(2, 1, 'aj321', '2012-01-14 10:36:13');

-- --------------------------------------------------------

--
-- Table structure for table `offences`
--

CREATE TABLE IF NOT EXISTS `offences` (
  `id` int(11) NOT NULL,
  `character_id` int(11) unsigned NOT NULL,
  `type` tinyint(4) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `moderator_id` int(10) unsigned NOT NULL,
  `reason` text NOT NULL,
  `appeal_status` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `appeal_data` text NOT NULL,
  `expired` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offences`
--

INSERT INTO `offences` (`id`, `character_id`, `type`, `date`, `expire_date`, `moderator_id`, `reason`, `appeal_status`, `appeal_data`, `expired`) VALUES
(1, 473, 1, '2015-04-27 03:33:08', '2015-04-28 03:33:08', 472, '1', 0, '', 0),
(2, 473, 1, '2015-04-27 03:41:24', '2015-04-28 03:41:24', 472, '<?php echo "hirrrrrrr"; ?>', 0, '', 0),
(3, 473, 1, '2015-04-27 03:42:06', '2015-04-28 03:42:06', 472, '0', 0, '', 0),
(4, 473, 1, '2015-04-27 03:43:04', '2015-04-28 03:43:04', 472, '0', 0, '', 0),
(5, 472, 1, '2015-04-27 03:43:42', '2015-04-28 03:43:42', 472, '', 0, '', 0),
(6, 472, 1, '2015-04-27 03:43:49', '2015-04-28 03:43:49', 472, '', 0, '', 0),
(7, 472, 1, '2015-04-27 03:44:15', '2015-04-28 03:44:15', 472, '', 0, '', 0),
(8, 472, 1, '2015-04-27 03:44:45', '2015-04-28 03:44:45', 472, '', 0, '', 0),
(9, 472, 1, '2015-04-27 03:45:00', '2015-04-28 03:45:00', 472, '', 0, '', 0),
(10, 473, 1, '2015-04-27 03:45:05', '2015-04-28 03:45:05', 472, '', 0, '', 0),
(11, 473, 1, '2015-04-27 03:45:14', '2015-04-28 03:45:14', 472, '', 0, '', 0),
(12, 473, 1, '2015-04-27 03:46:31', '2015-04-28 03:46:31', 472, '', 0, '', 0),
(34, 1, 2, '2011-08-18 15:39:17', '2011-12-31 15:45:25', 1, 'for teh lulz', 0, 'lol', 1),
(35, 1, 2, '2012-02-08 17:20:17', '2012-02-13 17:20:17', 0, 'fsdfsdf', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_logs`
--

CREATE TABLE IF NOT EXISTS `password_logs` (
  `id` int(10) unsigned NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `log_time` datetime DEFAULT NULL,
  `password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `versioning`
--

CREATE TABLE IF NOT EXISTS `versioning` (
  `server_version` text,
  `website_version` text,
  `database_version` double DEFAULT NULL,
  `client_version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_acp_logs`
--

CREATE TABLE IF NOT EXISTS `web_acp_logs` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_ip` tinytext,
  `log_time` datetime DEFAULT NULL,
  `log_message` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_acp_logs`
--

INSERT INTO `web_acp_logs` (`id`, `user_id`, `user_ip`, `log_time`, `log_message`) VALUES
(1, 1, '::1', '2013-12-24 15:08:09', 'Added a note.'),
(1, 472, '127.0.0.1', '2015-04-27 02:09:10', 'Added a note.'),
(2, 472, '127.0.0.1', '2015-04-27 02:09:12', 'Deleted a note.'),
(3, 472, '127.0.0.1', '2015-04-27 02:33:08', 'Added new ban.<br />Â» snoop007e'),
(4, 472, '127.0.0.1', '2015-04-27 02:41:24', 'Added new ban.<br />Â» snoop007e'),
(5, 472, '127.0.0.1', '2015-04-27 02:42:06', 'Added new ban.<br />Â» snoop007e'),
(6, 472, '127.0.0.1', '2015-04-27 02:43:04', 'Added new ban.<br />Â» snoop007e'),
(7, 472, '127.0.0.1', '2015-04-28 10:10:00', 'Added a new article [Hello]'),
(8, 472, '127.0.0.1', '2015-04-28 10:27:13', 'Deleted a note.');

-- --------------------------------------------------------

--
-- Table structure for table `web_acp_notes`
--

CREATE TABLE IF NOT EXISTS `web_acp_notes` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `note_date` datetime DEFAULT NULL,
  `note_message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `web_news`
--

CREATE TABLE IF NOT EXISTS `web_news` (
  `id` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL,
  `category_id` tinyint(4) NOT NULL,
  `title` tinytext NOT NULL,
  `desc` text NOT NULL,
  `story` text NOT NULL,
  `seo_url` tinytext NOT NULL,
  `youtube_url` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_news`
--

INSERT INTO `web_news` (`id`, `date`, `author_id`, `category_id`, `title`, `desc`, `story`, `seo_url`, `youtube_url`, `image_url`) VALUES
(5, 1430244600, 472, 2, 'Happy 4th', 'Test Description\n', 'May the 4th be with you', 'hello', NULL, 'http://starwarshq.com/uploads/monthly_2015_05/star-wars-battlefront.jpg.edc75b98294878ca17ee98c062c928f2.jpg'),
(6, 1430224600, 472, 2, 'Test ARticle', 'Test Description.... Hello random ertc e,etcxnsfdonsdf dsfonsdfnofdson', 'Story here, etc test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test ', 'hello', 'OBcspJ8R44I', NULL),
(7, 1430243600, 472, 2, 'No type', '002002020202020', 'blahadshas hl sadhlads hlsa hlas hlsa d', 'hello', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_sessions`
--

CREATE TABLE IF NOT EXISTS `web_sessions` (
  `session_id` varchar(16) NOT NULL DEFAULT '',
  `session_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `session_start` int(11) NOT NULL,
  `last_visit` int(11) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `current_page` varchar(255) NOT NULL,
  `browser` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_sessions`
--

INSERT INTO `web_sessions` (`session_id`, `session_user_id`, `session_start`, `last_visit`, `ip_address`, `current_page`, `browser`) VALUES
('tC0XVbCM0SdnsPUf', 1, 1456594988, 1456607053, '127.0.0.1', '/rs3cms/create.php', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:44.0) Gecko/20100101 Firefox/44.0');

-- --------------------------------------------------------

--
-- Table structure for table `worlds`
--

CREATE TABLE IF NOT EXISTS `worlds` (
  `id` smallint(6) NOT NULL DEFAULT '0',
  `name` tinytext,
  `ip` varchar(16) DEFAULT '127.0.0.1',
  `flag` smallint(6) DEFAULT '1',
  `region` varchar(5) DEFAULT 'PT',
  `country` smallint(6) DEFAULT '12',
  `welcome_message` tinytext,
  `spawn_point` tinytext,
  `motw` tinytext,
  `exp_rate` int(4) DEFAULT NULL,
  `startup_time` datetime DEFAULT NULL,
  `online` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`,`master_id`);

--
-- Indexes for table `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`,`ip_address`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`,`email`);

--
-- Indexes for table `characters_contacts`
--
ALTER TABLE `characters_contacts`
  ADD PRIMARY KEY (`id`,`master_id`,`contact_id`);

--
-- Indexes for table `characters_statistics`
--
ALTER TABLE `characters_statistics`
  ADD PRIMARY KEY (`master_id`);

--
-- Indexes for table `chat_logs`
--
ALTER TABLE `chat_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `connection_logs`
--
ALTER TABLE `connection_logs`
  ADD PRIMARY KEY (`id`,`port`);

--
-- Indexes for table `donation`
--
ALTER TABLE `donation`
  ADD PRIMARY KEY (`index`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`,`master_id`);

--
-- Indexes for table `name_change_logs`
--
ALTER TABLE `name_change_logs`
  ADD PRIMARY KEY (`id`,`master_id`);

--
-- Indexes for table `offences`
--
ALTER TABLE `offences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_logs`
--
ALTER TABLE `password_logs`
  ADD PRIMARY KEY (`id`,`username`);

--
-- Indexes for table `web_acp_logs`
--
ALTER TABLE `web_acp_logs`
  ADD PRIMARY KEY (`id`,`user_id`);

--
-- Indexes for table `web_acp_notes`
--
ALTER TABLE `web_acp_notes`
  ADD PRIMARY KEY (`id`,`user_id`);

--
-- Indexes for table `web_news`
--
ALTER TABLE `web_news`
  ADD PRIMARY KEY (`id`,`author_id`);

--
-- Indexes for table `web_sessions`
--
ALTER TABLE `web_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `worlds`
--
ALTER TABLE `worlds`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `characters_contacts`
--
ALTER TABLE `characters_contacts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `characters_statistics`
--
ALTER TABLE `characters_statistics`
  MODIFY `master_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=511;
--
-- AUTO_INCREMENT for table `chat_logs`
--
ALTER TABLE `chat_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `connection_logs`
--
ALTER TABLE `connection_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `donation`
--
ALTER TABLE `donation`
  MODIFY `index` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `name_change_logs`
--
ALTER TABLE `name_change_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `offences`
--
ALTER TABLE `offences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `password_logs`
--
ALTER TABLE `password_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_acp_logs`
--
ALTER TABLE `web_acp_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `web_acp_notes`
--
ALTER TABLE `web_acp_notes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `web_news`
--
ALTER TABLE `web_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
