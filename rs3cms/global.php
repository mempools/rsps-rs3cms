<?php
ob_start();
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define("REMOTE_ADDR", $_SERVER['REMOTE_ADDR']);
define('DS', DIRECTORY_SEPARATOR);
define('LB', chr(13));
define('CWD', str_replace('manage' . DS, '', dirname(__FILE__) . DS));

error_reporting(E_ALL);

require_once("includes/database_core.php");
require_once("includes/functions_core.php");
require_once("includes/functions_database.php");
require_once("includes/functions_utils.php");
require_once("includes/functions_config.php");
require_once("includes/functions_remote.php");
require_once("includes/functions_manage.php");
require_once("includes/class_agent.php");

dbinit(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

$config = get_configs();

define("DEBUG_MODE", filter_var(get_config("debug_mode"), FILTER_VALIDATE_BOOLEAN));
define("DEMO_MODE", filter_var(get_config("debug_mode"), FILTER_VALIDATE_BOOLEAN));
define("NEW_THEME", get_config("new_theme"));
if (filter_var(get_config("session_check"), FILTER_VALIDATE_BOOLEAN)) {
    define("SESSION_CHECK", true);
}
define('PAYPAL_EMAIL', get_config("paypal_email"));
define("SANDBOX", false);//paypal sandbox
define("CURRENCY", get_config("paypal_currency"));
define("MASTER_SERVER_IP", get_config("remote_ip"));
define("REMOTE_SERVER_PORT", get_config("remote_port"));
define("SITE_ADDRESS", get_config("site_address"));
define("SITE_NAME", "" . get_config("server_name"));
define("SITE_MOTTO", get_config("server_motto"));
define("SITE_DESC", get_config("server_desc"));
define("FORUMS_URL", get_config("url_forums"));
define("DONATE_URL", get_config("url_donate"));
define("BUGREPORT_URL", get_config("url_support"));
define("EMAIL", get_config("email"));
define("RECAPTCHA_SITEKEY", get_config("recaptcha_key"));
define("RECAPTCHA_SECRET", get_config("recaptcha_secret"));
define("YOUTUBE_URL", get_config("youtube_url"));
define("FEATURED_VIDEO", get_config("youtube_video"));
define("FACEBOOK_URL", "");
define("FACEBOOK_FEED", "");
define("TWITTER_URL", get_config("twitter_url"));
define("TWITTER_USERNAME", get_config("twitter_user"));
define("TWITTER_WIDGET", get_config("twitter_widget"));
define("PAGE_TITLE", SITE_DESC);

$white_list = explode(",", get_config("whitelist_ip"));
if (DEBUG_MODE && !in_array(REMOTE_ADDR, $white_list)) {
    die(system_error("DEBUG MODE", "Down for maintanence." . $ip));
}


// Create agent session.
if (!defined("SESSION_CHECK")) {
    $agent = new agent();
    agent::delete_expired_sessions();
    agent::init_session($agent);
}