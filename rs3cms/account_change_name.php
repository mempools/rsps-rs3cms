<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}

$confirm = false;

$iron_mode = startsWith(strtolower($agent->username), "iron");

if ($agent->online <= 0) {
    if (!$iron_mode) {
        if (!DEMO_MODE && isset($_POST['submit'])) {
            $new_username = $_POST['name'];

            if (!startsWith($new_username, "iron") && preg_match("/^.{1,12}$/", $new_username) && preg_match("/(^[A-Za-z0-9]{1,12}$)|(^[A-Za-z0-9]+[\-\s][A-Za-z0-9]+[\-\s]{0,1}[A-Za-z0-9]+$)/", $new_username)) {
                $q = dbquery("SELECT id FROM characters WHERE username = '$new_username';");
                if (strcasecmp($new_username, $agent->username) == 0 || mysql_num_rows($q) == 0) {
                    $datetime = dbevaluate("SELECT UNIX_TIMESTAMP(username_last_changed) FROM characters WHERE id='$agent->master_id';");
                    if ((time() - $datetime) > 2592000) {
                        $confirm = true;
                    } else { // hasn't been 30 days since last changed.
                        $error = "You can only change your username once a month. You will be able to change your name again on " . simpledate($datetime + 2592000) . ".";
                    }
                } else {
                    $error = "That name has already been taken.";
                }
            } else {
                //$error = "That username is invalid.";
            }
        } else if (!DEMO_MODE && isset($_POST['confirm'])) {
            $new_username = $_POST['name'];

            if (!startsWith($new_username, "iron") && preg_match("/^.{1,12}$/", $new_username) && preg_match("/(^[A-Za-z0-9]{1,12}$)|(^[A-Za-z0-9]+[\-\s][A-Za-z0-9]+[\-\s]{0,1}[A-Za-z0-9]+$)/", $new_username)) {
                $q = dbquery("SELECT username FROM characters WHERE characters.username = '$new_username';");
                if (strcasecmp($new_username, $agent->username) == 0 || mysql_num_rows($q) == 0) {
                    $datetime = dbevaluate("SELECT UNIX_TIMESTAMP(username_last_changed) FROM characters WHERE id='$agent->master_id';");
                    if ((time() - $datetime) > 2592000) {
                        dbquery("UPDATE characters SET username='$new_username',username_last_changed=NOW(),previous_username='$agent->username' WHERE id='$agent->master_id';");
                        $agent->username = $new_username;
                        $finished = true;
                    } else { // hasn't been 30 days since last changed.
                        $error = "You can only change your username once a month. You will be able to change your name again on " . simpledate($datetime + 2592000) . ".";
                    }
                } else {
                    $error = "That name has already been tooken.";
                }
            } else {
                $error = "That username is invalid.";
            }
        }
    } else {
        $error = "You cannot change your name in ironman mode.";
    }
} else {
    $error = "You must be offline to change your name.";
}
?>


<!doctype html>
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/account_settings_frame-50.css" rel="stylesheet">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script type="text/javascript" src="https://www.jagex.com/js/jquery/jquery_effects_core_0.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript"
            src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script>
        if (window == window.top) {
            window.location.href = "http://google.com/";
        }
    </script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/account_settings_frame-50.js"></script>
    <title><?php printf(SITE_NAME); ?></title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java">
    <meta name="description" content="<?php printf(SITE_DESC); ?>">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">
    <meta property="og:title" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:type" content="game">
    <meta property="og:url" content="<?php printf(SITE_ADDRESS); ?>">
    <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:description" content="<?php printf(SITE_DESC); ?>">
    <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png">
    <style type="text/css">
        .js .helpBox {
            display: none;
        }

        .Button29 {
            margin-top: 2px;
        }

        .inputLabel {
            font-size: 15px;
        }

        .status {
            clear: both;
            float: none;
        }

        #help {
            display: none;
        }

        .js #help {
            display: block;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").addClass("js");
            $("#help").click(function (ev) {
                ev.preventDefault();
                $(".helpBox").slideToggle('fast', function () {
                    resize();
                });
            });
        });
    </script>
    <script>$(function () {

            var timer;
            var $status = $('.status');
            var $suggestions = $('.suggestions');

            // Handles setting the status of the message beneath the input
            function setStatus(newStatus, newClass) {
                $status
                    .text(newStatus)
                    .removeClass('success error')
                    .addClass(newClass)
                ;
            }

            // Handling making a check if user types in the box, but with a delay so we don't fire until they pause typing
            var $name = $('#name')
                .keypress(function () {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        var val = $name.val();
                        if (val.length > 0) {
                            checkUsername(val);
                        }
                        setStatus("Checking name...", "");
                    }, 500);
                });

            // Making calls to check script as needed
            function checkUsername(username) {
                var ajaxURL = "<?php printf(SITE_ADDRESS); ?>name_check.php?username=" + username;

                $.ajax({
                    url: ajaxURL,
                    success: handleResult,
                    error: function () {
                        handleResult('');
                    }
                });
            }

            // Handling return from ajax request
            function handleResult(response) {

                // Available
                if (response.match('1')) {
                    setStatus('That name is available ', 'success');
                }
                // Unavailable
                else if (response.match('2')) {
                    setStatus('That name is not available.', 'error');
                }
                // Check script wasn't happy with the name (empty or malformed)
                else if (response.match('3')) {
                    setStatus("Not a valid name", "error");
                } else if (response.match('4')) {
                    setStatus('Name cannot start with iron ', 'error');
                }

                // Something unexpected happened, e.g. transport error or failure at the script's end
                else {
                    setStatus("Couldn't check name: " + response, "error");
                }
                resize();

            }

            // Choosing an alternate username from the list
            $suggestions.delegate('<li>', 'click', function () {
                var $this = $(this);

                $name.val($this.text());

            });
        });
    </script>
</head>
<body>
<div class="container">
    <a href="#" class="Button Button31Help" id="help"><span><span><span class=""><b>Help</b></span></span></span></a>

    <div class="helpBox" style="padding-top: 35px;">
        <p>A character name may be up to 12 characters and can include numbers, letters, spaces and dashes (-). Unlike
            usernames, character names are case sensitive, so you may capitalise them as you wish. Your name must start
            and end with a letter or number, and may not contain more than one consecutive punctuation character or
            space.</p>

        <p>Your character name may not be offensive or deceptive, and your name will be removed if it is found to break
            these rules.</p>

        <p>If you repeatedly break our naming guidelines you will lose the right to change your character name.</p>
    </div>

    <?php
    if (isset($error)) {
        printf("<p class=\"intro error\">$error</p>");
    }

    if (!$iron_mode) {
        ?>

        <p>Your character name is: <b class="blue"><?php printf($agent->username); ?></b></p>

        <?php if ($confirm == true) { ?>
            <p class="intro">Your name will be changed to <b class="blue"><?php printf($new_username); ?></b></p>
            <p>You will not be able to change your name again for 30 days. Are you sure?</p>
            <form method="post" action="<?php printf(SITE_ADDRESS); ?>account_change_name.php" style="display:inline">
                <input type="hidden" name="name" value="<?php printf($new_username); ?>" id="name">
            <span class="Button Button29" id="confirm"><span><span><span class=""><b>Yes</b></span><input value="Yes"
                                                                                                          name="confirm"
                                                                                                          type="submit"
                                                                                                          title="Yes"/></span></span></span>
                <a href="<?php printf(SITE_ADDRESS); ?>account_change_name.php"><span class="Button Button29"
                                                                                      id="confirm"><span><span><span
                                    class=""><b>No</b></span></span></span></span></a>
            </form>
        <?php } else if (isset($finished)) { ?>
            <p class="intro">Your name has been successfully changed to <b
                    class="blue"><?php printf($new_username); ?></b>.
            </p>
            <?php
        } else {
            $datetime = dbevaluate("SELECT UNIX_TIMESTAMP(username_last_changed) FROM characters WHERE id='$agent->master_id';");
            if ((time() - $datetime) < 2592000) {
                $sdate = simpledate($datetime + 2592000);
                printf("<p class=\"intro\">You are not able to change your name again until $sdate.</p>");
            } else {
                ?>
                <form method="post" action="<?php printf(SITE_ADDRESS); ?>account_change_name.php"
                      style="padding-top:10px">
                    <label class="formInput">
                        <div class="inputLabel">New name:</div>
                        <div class="DarkInputBoxWrapper">
                            <div class="InputBoxLeft"><input type="text" title="" name="name" maxlength="12" size="12"
                                                             value="<?php printf($agent->username); ?>" id="name"></div>
                            <div class="InputBoxRight"></div>
                        </div>
                    </label>
                <span class="Button Button29" id="submit"><span><span><span class=""><b>Set Name</b></span><input
                                value="Set Name" name="submit" type="submit" title="Set Name"/></span></span></span>
                </form>
            <?php }
        }
    } ?>
    <div class="status"></div>
    <ul class="suggestions"></ul>
</div>
</body>
</html>