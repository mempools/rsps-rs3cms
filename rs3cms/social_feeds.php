<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<div id="rightColumn">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_twitter2_0_1-0.js"></script>
    <div id="SNcontainer">
        <div id="YouTubePanel">

            <h3 class="Gradient">
                <span class="G0">YouTube</span>
                <span class="G1" aria-hidden="true">YouTube</span>
                <span class="G2" aria-hidden="true">YouTube</span>
                <span class="G3" aria-hidden="true">YouTube</span>
                <span class="G4" aria-hidden="true">YouTube</span>
                <span class="G5" aria-hidden="true">YouTube</span>
                <span class="G6" aria-hidden="true">YouTube</span>
                                    <span class="mask"><span class="spacing">YouTube</span>
                                        <span class="middleUnderscore"><span class="spacing"
                                                                             aria-hidden="true">YouTube</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">YouTube</span>
                                    </span>
                <span class="leftUnderscore"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                        class="left" alt=""/></span>
            </h3>
            <iframe id="YouTubeEmbed" width="246" height="159" src="<?php printf(FEATURED_VIDEO); ?>" frameborder="0"
                    allowfullscreen></iframe>
            <a target="_blank" href="<?php printf(YOUTUBE_URL); ?>" class="Button Button29"
               id="YouTubeLink"><span><span><span class=""><b>View Youtube Channel</b></span></span></span></a>
        </div>
        <!--        <div id="FacebookPanel">-->
        <!---->
        <!--            <h3 class="Gradient">-->
        <!--                <span class="G0">Facebook</span>-->
        <!--                <span class="G1" aria-hidden="true">Facebook</span>-->
        <!--                <span class="G2" aria-hidden="true">Facebook</span>-->
        <!--                <span class="G3" aria-hidden="true">Facebook</span>-->
        <!--                <span class="G4" aria-hidden="true">Facebook</span>-->
        <!--                <span class="G5" aria-hidden="true">Facebook</span>-->
        <!--                <span class="G6" aria-hidden="true">Facebook</span>-->
        <!--                                    <span class="mask"><span class="spacing">Facebook</span>-->
        <!--                                        <span class="middleUnderscore"><span class="spacing"-->
        <!--                                                                             aria-hidden="true">Facebook</span></span>-->
        <!--                                    </span>-->
        <!--                                    <span class="rightUnderscore">-->
        <!--                                        <img-->
        <!--                                            src="-->
        <?php //printf(SITE_ADDRESS); ?><!--img/global/gradient_header/underscore_right.png"-->
        <!--                                            class="right" alt=""/><span class="spacing"-->
        <!--                                                                        aria-hidden="true">Facebook</span>-->
        <!--                                    </span>-->
        <!--                <span class="leftUnderscore"><img-->
        <!--                        src="-->
        <?php //printf(SITE_ADDRESS); ?><!--img/global/gradient_header/underscore_flourish_left.png"-->
        <!--                        class="left" alt=""/></span>-->
        <!--            </h3>-->
        <!--            <a target="_blank" href="--><?php //printf(FACEBOOK_URL); ?><!--" class="Button Button29"-->
        <!--               id="FacebookWallLink"><span><span><span class=""><b>View Facebook Wall</b></span></span></span></a>-->
        <!--            <script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>-->
        <!--            <div id="FacebookFeedHolder" class="scroll-pane">-->
        <!--                <p class='error'>Sorry, The Facebook feed is currently unavailable.</p>-->
        <!--            </div>-->
        <!--            <script>-->
        <!--                // Ready function-->
        <!--                $(function () {-->
        <!--                    //-->
        <!--                    var FBpane;-->
        <!--                    var FBapi;-->
        <!--                    if ($.browser.webkit) {-->
        <!--                        //alert("Webkit!");-->
        <!--                    } else {-->
        <!--                        FBpane = $("#FacebookFeedHolder.scroll-pane");-->
        <!--                        FBapi = FBpane.data('jsp');-->
        <!--                    }-->
        <!--                    $.getJSON("--><?php //printf(FACEBOOK_FEED); ?><!--//", function (response) {-->
        <!--//                        var oFacebookFeedHolder = $("#FacebookFeedHolder");//document.getElementById("FacebookFeedHolder");-->
        <!--//                        //-->
        <!--//                        var data = response.data;-->
        <!--//                        var out = "";-->
        <!--//                        nPosts = Math.min(data.length, 5)-->
        <!--//                        for (var i = 0; i < nPosts; i++) {-->
        <!--//                            out += "<div class='article'>";-->
        <!--//                            if (data[i].link != undefined) {-->
        <!--//                                out += "<a href='" + data[i].link + "' target='_blank'>";-->
        <!--//                            }-->
        <!--//                            out += "<img src='https://graph.facebook.com/" + data[i].from.id + "/picture' alt='' title='" + data[i].from.name + "' class='avatar'/>";-->
        <!--//                            //out += " <img src='./picture.jpg' alt='' title='" + data[i].from.name + "' class='avatar'/>";-->
        <!--//                            out += " <div class='AuthorAndTime'><h6 class='author'>" + data[i].from.name + "</h6>";-->
        <!--//                            //-->
        <!--//                            strD = data[i].created_time.split("+")[0];-->
        <!--//                            //-->
        <!--//                            // Format date as days and hours Ago.-->
        <!--//                            d = new Date(strD);-->
        <!--//                            n = new Date();-->
        <!--//                            a = new Date(n - d);-->
        <!--//                            days = a.getDate();-->
        <!--//                            strPast = "";-->
        <!--//                            if (days > 0)strPast += days + " day";-->
        <!--//                            if (days > 1)strPast += "s";-->
        <!--//                            if (days > 0)strPast += " ";-->
        <!--//                            h = a.getHours();-->
        <!--//                            if (h > 0)strPast += h + " hour";-->
        <!--//                            if (h > 1)strPast += "s";-->
        <!--//                            //m=a.getMinutes();-->
        <!--//                            //-->
        <!--//                            out += " <span class='date'>" + strPast + " ago</span></div>";-->
        <!--//                            if (data[i].picture != undefined)-->
        <!--//                                out += "<img src=" + data[i].picture + " class='picture'/>";-->
        <!--//                            if (data[i].message != undefined)-->
        <!--//                                out += " <p>" + data[i].message + "</p>";-->
        <!--//                            if (data[i].story != undefined)-->
        <!--//                                out += " <p>" + data[i].story + "</p>";-->
        <!--//                            //out += " <img src="+data[i].picture + "/>"-->
        <!--//                            //out += " <a href="+data[i].link + " target='_blank'>LINK</a>";-->
        <!--//                            //out += " <img src="+data[i].icon + "/>";-->
        <!--//                            if (data[i].link != undefined) {-->
        <!--//                                out += "</a>";-->
        <!--//                            }-->
        <!--//                            out += "</div>";-->
        <!--//                            if (i < nPosts - 1)out += "<div class='HrOneThird'></div>";-->
        <!--//                        }-->
        <!--//                        //var $repoppedHolder = oFacebookFeedHolder.find(".jspPane").empty().append(out);-->
        <!--//                        if ($.browser.webkit) {-->
        <!--//                            //alert("Webkit!");-->
        <!--//                            var $repoppedHolder = oFacebookFeedHolder.empty().append(out);-->
        <!--//                        } else {-->
        <!--//                            FBapi.getContentPane().empty().append(out);-->
        <!--//                            FBapi.reinitialise();-->
        <!--//                        }-->
        <!--//                        //$('#FacebookFeedHolder').tinyscrollbar();-->
        <!--//                    })-->
        <!--//                });-->
        <!--//            </script>-->
        <!--//        </div>-->
        <div id="TwitterPanel">

            <h3 class="Gradient">
                <span class="G0">Twitter</span>
                <span class="G1" aria-hidden="true">Twitter</span>
                <span class="G2" aria-hidden="true">Twitter</span>
                <span class="G3" aria-hidden="true">Twitter</span>
                <span class="G4" aria-hidden="true">Twitter</span>
                <span class="G5" aria-hidden="true">Twitter</span>
                <span class="G6" aria-hidden="true">Twitter</span>
                                    <span class="mask"><span class="spacing">Twitter</span>
                                        <span class="middleUnderscore"><span class="spacing"
                                                                             aria-hidden="true">Twitter</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Twitter</span>
                                    </span>
                <span class="leftUnderscore"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                        class="left" alt=""/></span>
            </h3>
            <a target="_blank" href="<?php printf(TWITTER_URL); ?>" class="Button Button29"
               id="TwitterFeedLink"><span><span><span class=""><b>View Twitter Feed</b></span></span></span></a>

            <div id="TwitterFeedHolder" class="scroll-pane">
                <!--                <p class='error'>Sorry, the Twitter feed is currently unavailable.</p>-->
                <a class="twitter-timeline" href="https://twitter.com/<?php echo TWITTER_USERNAME; ?>"
                   data-widget-id="<?php echo TWITTER_WIDGET; ?>">Tweets
                    by @<?php echo TWITTER_USERNAME; ?></a>
                <script>!function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                        if (!d.getElementById(id)) {
                            js = d.createElement(s);
                            js.id = id;
                            js.src = p + "://platform.twitter.com/widgets.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }
                    }(document, "script", "twitter-wjs");</script>
            </div>

        </div>
    </div>
</div>
