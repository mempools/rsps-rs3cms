<?php
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}

$result = mysql_query("SELECT * FROM messages WHERE uid = '$agent->master_id'") or die(mysql_error());

?>
<html
    class=" svg websockets sharedworkers localstorage video progressbar meter csspointerevents inlinesvg canvas datalistelem fileinput webgl webworkers no-touchevents no-details indexeddb indexeddb-deletedatabase boxsizing no-backgroundcliptext borderimage flexboxlegacy flexbox appearance blobworkers dataworkers"
    style="overflow: hidden; height: auto;">
<head>
    <link type="text/css" rel="stylesheet"
          href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/account_settings_frame-50.css" rel="stylesheet">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script type="text/javascript" src="https://www.jagex.com/js/jquery/jquery_effects_core_0.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript"
            src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script>
        if (window == window.top) {
            window.location.href = "http://google.com/";
        }
    </script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/account_settings_frame-50.js"></script>
    <title><?php printf(SITE_NAME); ?></title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java">
    <meta name="description" content="<?php printf(SITE_DESC); ?>">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">
    <meta property="og:title" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:type" content="game">
    <meta property="og:url" content="<?php printf(SITE_ADDRESS); ?>">
    <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:description" content="<?php printf(SITE_DESC); ?>">
    <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png">
    <style type="text/css">
        body {
            padding-left: 27px;
        }

        .container {
            overflow: hidden;
        }

        #heading {
            height: 45px;
            position: relative;
            z-index: 5;
        }

        #heading .accountSettingsTitle,
        #heading #refreshButton {
            float: left;
        }

        #filters {
            right: 0;
            z-index: 5;
        }

        .DropListWrapper li span {
            z-index: -1;
        }

        #refreshButton {
            margin-left: 10px;
        }

        .tableInnerContainer .accountSettingsTitleGrey {
            border-bottom: 1px solid #635035;
            width: 100%;
        }

        .columnIcon,
        .columnSubject,
        .columnCount,
        .columnLastUpdate {
            border-right: 1px dotted #635035;
            padding-top: 7px;
            padding-bottom: 7px;
        }

        .columnLastUpdate,
        .columnActions,
        .columnCount {
            text-align: center;
        }

        .columnIcon {
            width: 70px;
            padding-bottom: 0;
            padding-top: 5px;
        }

        .columnSubject {
            width: 150px;
            padding-left: 5px;
        }

        .columnCount {
            width: 20px;
        }

        .columnLastUpdate {
            width: 120px;
        }

        .columnActions {
            width: 50px;
            padding-top: 5px;
        }

        .columnSubject .dialogue_link {
            display: block;
            overflow: hidden;
        }

        .js .columnSubject .dialogue_link {
            overflow: visible;
            width: 125px;
        }

        .columnSubject .dialogue_link span {
            color: #F3B13F;
        }

        .columnSubject .dialogue_link:hover span {
            text-decoration: underline;
        }

        .columnActions a {
            display: block;
        }

        #refreshButton {
            margin: 20px 0 0 !important;
        }

        .BubbleContainer {
            overflow: hidden;
            position: relative;
        }

        .Bubble {
            background-position: center top;
            background-repeat: repeat-y;
            font-size: 12px;
            margin: 0;
            width: 325px;
        }

        .Bubble, .BubbleHeader, .BubbleFooter {
            background-color: transparent;
            position: relative;
        }

        .BubbleHeader, .BubbleFooter {
            margin: 0;
        }

        .BubbleContainer .BubbleContents {
            margin: -30px 0;
            padding: 15px 13px 22px;
            position: relative;
            width: auto;
            z-index: 2;
        }

        .BubbleHeader, .BubbleFooter {
            background-repeat: no-repeat;
            font-size: 0 !important;
            min-height: 49px;
            overflow: hidden;
            z-index: 1;
        }

        .BubbleHeader {
            background-position: left top;
        }

        .BubbleFooter {
            background-position: right bottom;
        }

        .BubbleCreation {
            font-size: 11px;
        }

        .BubbleCreation {
            float: left;
            height: 35px;
            line-height: 35px;
            margin-left: 39px;
        }

        .BubbleCreation span {
            color: #F3B13F;
            line-height: 35px;
        }

        .msgcontents {
            clear: both;
            font-size: 12px;
            line-height: 1.5em;
            margin: 0 18px 0 39px;
            min-height: 47px;
            overflow: hidden;
            padding-bottom: 10px;
            padding-top: 13px;
            position: relative;
            text-align: left;
            word-wrap: break-word;
        }

        .Button29 {
            float: right;
        }

        #notabene {
            clear: both;
            padding: 10px 0 10px;
        }

        .bottom-spacer {
            clear: both;
            position: relative;
            overflow: hidden;
            padding: 20px 0 0;
        }

        .Bubble, .BubbleHeader, .BubbleFooter {
            background-image: url("https://www.runescape.com/img/ticketing/bubbleGrey.png");
        }

        .replyinstructions {
            width: 100%;
            clear: both;
        }

        #replyBox {
            float: right;
            margin-right: 10px;
        }

        #sendReply {
            float: right;
            margin-top: 10px;
        }

        #charlimit_text_msg {
            overflow: auto;
            width: 250px;
            height: 180px;
            border: none;
            padding: 5px;
        }

        .accountSettingsTitle {
            float: left;
        }

        #backButton {
            float: right;
        }

        #backButton2 {
            float: left;
        }

        #heading {
            height: 40px;
        }

        .message {
            padding: 0 0 10px;
        }

        .ticket_leftbox img {
            border: 2px solid black;
        }

        .jmod .ticket_leftbox img {
            border-color: #C29F43;
        }

        .jx .ticket_leftbox img {
            border-color: #453F33;
        }

        .rs .ticket_leftbox img {
            border-color: #81633E;
        }

        .fo .ticket_leftbox img {
            border-color: #3361AA;
        }

        .ticket {
            margin: 15px 0 0 -10px;
            overflow: hidden;
        }

        .ticket_leftbox {
            float: left;
            width: 100px;
            text-align: center;
        }

        .ticket_author {
            font-family: KingthingsPetrockRegular, Times New Roman, serif;
            font-size: 15px;
            margin-bottom: 10px;
            margin-top: 10px;
            color: #f3b13f;
        }

        .ticket_topbar,
        .ticket_contentbox {
            text-align: center;
        }

        .ticket_contentbox {
            padding-top: 20px;
        }

        .ticket_rightbox {
            float: right;
            margin-right: 10px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").addClass("js");
            $(".dialogue_link").elide();
        });
    </script>
</head>
<body style="overflow: hidden; height: auto;" class="accountSettingsFrame js">
<?php
if (isset($_GET['deleteid'])) {
    $dId = $_GET['deleteid'];
    mysql_query("DELETE FROM messages WHERE id = '$dId'") or die(mysql_error());
}
if (isset($_GET['viewid'])) {
    $vid = $_GET['viewid'];

    $mresult = mysql_query("SELECT * FROM messages WHERE id = '$vid'") or die(mysql_error());
    $mrow = mysql_fetch_array($result);
    $vtitle = $mrow['title'];
    $vsender = $mrow['sender'];
    $vmessage = $mrow['message'];
    ?>
    <div class="container">
        <div class="heading">
            <h2 class="accountSettingsTitle">Viewing Message Exchange</h2>
            <a href="account_messages.php" class="Button Button29 icon" id="backButton"><span><span><img
                            src="https://www.runescape.com/img/forum/cmdicons/backtoforum.gif" alt=""><span
                            class=""><b><?php echo "Back to Messages"; ?></b></span></span></span></a>
        </div>
        <div class="message">

            <h5 class="Gradient">
                <span class="G0"><?php echo $vtitle; ?></span>
                <span class="G1" aria-hidden="true"><?php echo $vtitle; ?></span>
                <span class="G2" aria-hidden="true"><?php echo $vtitle; ?></span>
                <span class="G3" aria-hidden="true"><?php echo $vtitle; ?></span>
                <span class="G4" aria-hidden="true"><?php echo $vtitle; ?></span>
                <span class="G5" aria-hidden="true"><?php echo $vtitle; ?></span>
                <span class="G6" aria-hidden="true"><?php echo $vtitle; ?></span>

                                                    <span class="mask"><span
                                                            class="spacing"><?php echo $vtitle; ?></span>
                                        <span class="middleUnderscore"><span class="spacing"
                                                                             aria-hidden="true"><?php echo $vtitle; ?></span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true"><?php echo $vtitle; ?></span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
            </h5>
            <div class="ticket jmod">
                <div class="ticket_leftbox">
                    <h5 class="ticket_author">
                        <?php echo $vsender; ?>
                    </h5>
                    <img src="<?php printf(SITE_ADDRESS); ?>img/avatars/default.png" width="70px" class="center">
                </div>
                <div class="ticket_rightbox jmod">
                    <div class="BubbleContainer Grey">
                        <div class="Bubble">
                            <div class="BubbleHeader">
                                <div class="BubbleFooter">
                                    <div class="BubbleCreation">
<span>
09-Feb-2016 04:47
</span>
                                    </div>
                                    <div class="msgcontents">
                                        <?php echo $vmessage; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-spacer">
            </div>
        </div>
        <div id="notabene">
            <!--            <i>Messages sent to you by Jagex are strictly confidential and are intended only for the owner of the-->
            <!--                account to which they are sent. The contents of these messages must not be disclosed to any other person-->
            <!--                or copies taken.</i>-->
        </div>
    </div>
    <?php
    mysql_query("UPDATE messages SET status = '1' WHERE id = '$vid'");
} else {
    ?>
    <div class="container">
    <div id="heading">
        <h2 class="accountSettingsTitle">Your Message Centre</h2>

        <style>
            #filters.InPageDropDownNew.NoJS:hover .DropListWrapper,
            #filters.InPageDropDownNew.CSS3:hover .DropListWrapper {
                top: -8px;
            }

            #filters.InPageDropDownNew.NoJS .DropListWrapper,
            #filters.InPageDropDownNew.CSS3 .DropListWrapper {
                top: -82px;
            }

            #filters:hover {
                height: 110px;
            }
        </style>
        <!--        <div class="InPageDropDownNew NoJS CSS3" id="filters" data-expand="110px">-->
        <!--            <div class="Mask"></div>-->
        <!--            <div class="DropListWrapper" data-retract="-82px">-->
        <!--                <img src="https://www.runescape.com/img/global/header/nav/DropDownFlourish.png" class="Flourish" alt="">-->
        <!--                <span class="firstValue">All</span>-->
        <!--                <ul>-->
        <!--                    <form action="inbox.ws" name="gameform" id="gameform">-->
        <!--                        <li><span>All</span><input name="game" value="" type="submit"></li>-->
        <!--                        <li><span>RuneScape</span><input name="game" value="runescape" type="submit"></li>-->
        <!--                        <li><span>Jagex</span><input name="game" value="jagex" type="submit"></li>-->
        <!--                        <li><span>FunOrb</span><input name="game" value="funorb" type="submit"></li>-->
        <!--                    </form>-->
        <!--                </ul>-->
        <!--            </div>-->
        <!--        </div>-->
    </div>

    <div class="accountSettingsTable">
        <div class="tableBoxTopLeft"></div>
        <div class="tableBoxTopRight"></div>
        <div class="tableInnerContainer">
            <h2 class="accountSettingsTitleGrey">
                Received Messages
            </h2>
            <?php
            while ($row = mysql_fetch_array($result)) {
                $status = $row['status'];
                $title = $row['title'];
                $id = $row['id'];
                if ($status == 0) {
                    ?>
                    <div class="row last-row">
                    <span class="columnSubject">
<a href="account_messages.php?viewid=<?php echo $id; ?>" class="dialogue_link"><span
        style="display: block; width: 200%;"><span>
<?php echo mb_strimwidth($title, 0, 22, "..."); ?></span></span></a>
</span>

                        <span class="columnCount">1</span>
                        <span class="columnLastUpdate">09-Feb-2016 04:47</span>
<span class="columnActions">
<a href="account_messages.php?viewid=<?php echo $id; ?>"><img
        src="https://www.runescape.com/img/ticketing/dlg_view2.gif" alt="View"
        title="View"></a>
</span>
                    </div>
                <?php }
            } ?>
        </div>
        <div class="tableBoxBottomLeft"></div>
        <div class="tableBoxBottomRight"></div>
    </div>
    <div class="accountSettingsTable">
        <div class="tableBoxTopLeft"></div>
        <div class="tableBoxTopRight"></div>
        <div class="tableInnerContainer">
            <h2 class="accountSettingsTitleGrey">

                Sent Messages
            </h2>
            <!--            <div class="row last-row" style="padding: 10px 0 5px">No conversations in this section.</div>-->
        </div>
        <div class="tableBoxBottomLeft"></div>
        <div class="tableBoxBottomRight"></div>
    </div>
    <div class="accountSettingsTable">
        <div class="tableBoxTopLeft"></div>
        <div class="tableBoxTopRight"></div>
        <div class="tableInnerContainer">
            <h2 class="accountSettingsTitleGrey">

                Read Messages
            </h2>
            <?php
            $result2 = mysql_query("SELECT * FROM messages WHERE uid = '$agent->master_id' and `status` = 1") or die(mysql_error());
            while ($rrow = mysql_fetch_array($result2)) {
                $rstatus = $rrow['status'];
                $rtitle = $rrow['title'];
                $rid = $rrow['id'];
                $rcount = 0;
                if ($rstatus == 1) {
                    $rcount++;
                    ?>
                    <div class="row">
<span class="columnSubject">
<a href="account_messages.php?viewid=<?php echo $rid; ?>" class="dialogue_link"><span
        style="display: block; width: 200%;"><span>
<?php echo mb_strimwidth($rtitle, 0, 22, "..."); ?></span></span></a>
</span>
                        <span class="columnCount">1</span>
                        <span class="columnLastUpdate">09-Feb-2016 04:47</span>
<span class="columnActions">
<a href="account_messages.php?deleteid=<?php echo $rid; ?>" style="padding-left:3px;float: right"><img
        src="https://www.runescape.com/img/ticketing/dlg_delete.gif" alt="Delete" title="Delete"></a>
<a href="account_messages.php?viewid=<?php echo $rid; ?>" style="float: right"><img
        src="https://www.runescape.com/img/ticketing/dlg_view2.gif" alt="View" title="View"></a>
</span>
                    </div>
                <?php }
            } ?>
        </div>
        <div class="tableBoxBottomLeft"></div>
        <div class="tableBoxBottomRight"></div>
    </div>
    <a href="account_messages.php" class="Button Button29 icon" id="refreshButton"><span><span><img
                    src="https://www.runescape.com/img/forum/cmdicons/refresh.gif" alt=""><span class=""><b>Refresh</b></span></span></span></a>
    </div><?php } ?>

</body>
</html>