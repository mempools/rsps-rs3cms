<?php
require_once("global.php");

if ($agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "home"));
}

$failed = false;
$script_name = "forgot_password.php";
$server_name = SITE_NAME;
$from_email = EMAIL;
$subject = $server_name . " - Password Reset";
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: ' . $from_email . "\r\n";

function email_message($username, $password)
{
    global $server_name;
    $img = "<img src=" . SITE_ADDRESS . "img/global/logos/runescape.png>";
    $message = '<html><body>';
    $message .= $img . '</br></br>';
    $message .= 'Hello ' . $username . ',</br></br>
    We\'ve reset your password to <b>' . $password . '</b>.</br>
    Please set a new password under <b><a href="<?php printf(SITE_ADDRESS); ?>account_settings/">Account Settings</b>.</br></br></br>' . $server_name;
    $message .= '</body></html>';
    return $message;
}

function rand_password($length = 7)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

function update_password($username)
{
    $password_gen = rand_password();
    dbquery("UPDATE characters SET password = '" . filter_for_input(sha1($password_gen)) . "' WHERE username = '$username';");
    return $password_gen;
}

$message = "";
if (!DEMO_MODE && isset($_POST['username']) && isset($_POST['email'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $check = agent::check_email_and_user($email, $username);
    if ($check == 1) {
        $results = mysql_fetch_array($query);
        mail($email, $subject, email_message($username, update_password($username)), $headers);
        $failed = false;
        die(header("Location: " . SITE_ADDRESS . "forgot_password?success"));
    } else {
        $failed = true;
        die(header("Location: " . SITE_ADDRESS . "forgot_password?failed"));
    }
}
require_once("header.php");
?>
    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/error-50.css"
          xmlns="http://www.w3.org/1999/html"/>
    <!--    <link rel="stylesheet" type="text/css" href="--><?php //printf(SITE_ADDRESS); ?><!--css/email.css"/>-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing" aria-hidden="true">Forgotten Password</span>
                                    <span class="G0">Forgotten Password</span>
                                    <span class="G1" aria-hidden="true">Forgotten Password</span>
                                    <span class="G2" aria-hidden="true">Forgotten Password</span>
                                    <span class="G3" aria-hidden="true">Forgotten Password</span>
                                    <span class="G4" aria-hidden="true">Forgotten Password</span>
                                    <span class="G5" aria-hidden="true">Forgotten Password</span>
                                    <span class="G6" aria-hidden="true">Forgotten Password</span>
                                    <span class="mask"><span class="spacing"
                                                             aria-hidden="true">Forgotten Password</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Forgotten Password</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <?php if (!isset($_GET['failed']) and !isset($_GET['success'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">

                                <h2 class="Gradient">
                                    <span class="G0">Password Reset</span>
                                    <span class="G1" aria-hidden="true">Password Reset</span>
                                    <span class="G2" aria-hidden="true">Password Reset</span>
                                    <span class="G3" aria-hidden="true">Password Reset</span>
                                    <span class="G4" aria-hidden="true">Password Reset</span>
                                    <span class="G5" aria-hidden="true">Password Reset</span>
                                    <span class="G6" aria-hidden="true">Password Reset</span>
                                    <span class="mask"><span class="spacing">Password Reset</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Password Reset</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Password Reset</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">

                                    <form action="<?php printf(SITE_ADDRESS); ?>forgot_password" method="post"
                                          autocomplete="off">
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Email:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login" name='email'
                                                                                 maxlength='200' size='30' value=""
                                                                                 id='email' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Username:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login"
                                                                                 name='username'
                                                                                 maxlength='20' size='30' value=""
                                                                                 id='username' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>

                                        <a class="password" target="_parent"><?php
                                            if ($failed) {
                                                echo "Please make sure all forms are filled.";
                                            }
                                            ?></a> <br class="clear"/>

                                        <a class="Button Button29 errorPageButton w160"><input
                                                type="submit"><span><span><span
                                                        class=""><b><?php echo "Submit" ?></b></span></span></span></a>

                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } else if (isset($_GET['success'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Password Reset</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Email Sent</span>
                                    <span class="G1" aria-hidden="true">Email Sent</span>
                                    <span class="G2" aria-hidden="true">Email Sent</span>
                                    <span class="G3" aria-hidden="true">Email Sent</span>
                                    <span class="G4" aria-hidden="true">Email Sent</span>
                                    <span class="G5" aria-hidden="true">Email Sent</span>
                                    <span class="G6" aria-hidden="true">Email Sent</span>
                                    <span class="mask"><span class="spacing">Email Sent</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Email sent</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Email Sent</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>Email successfully sent. If you did not receive one, please check your junk/spam
                                        folder.</p>
                                    <br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>login?confirm"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b>Login</b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } else { ?>
                        <!-- Account not found -->
                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Reset Password</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Invalid Account</span>
                                    <span class="G1" aria-hidden="true">Invalid Account</span>
                                    <span class="G2" aria-hidden="true">Invalid Account</span>
                                    <span class="G3" aria-hidden="true">Invalid Account</span>
                                    <span class="G4" aria-hidden="true">Invalid Account</span>
                                    <span class="G5" aria-hidden="true">Invalid Account</span>
                                    <span class="G6" aria-hidden="true">Invalid Account</span>
                                    <span class="mask"><span class="spacing">Invalid Account</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Invalid Account</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Invalid Account</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>No account cannot be found with that username or email.</p><br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>forgot_password"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Try Again" ?></b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

<?php require_once("footer.php"); ?>