<?php
require_once("../global.php");
require_once("../header.php");
?>

    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/news-50.css"
          xmlns="http://www.w3.org/1999/html"/>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Donate</span>
                                    <span class="G0">Donate</span>
                                    <span class="G1" aria-hidden="true">Donate</span>
                                    <span class="G2" aria-hidden="true">Donate</span>
                                    <span class="G3" aria-hidden="true">Donate</span>
                                    <span class="G4" aria-hidden="true">Donate</span>
                                    <span class="G5" aria-hidden="true">Donate</span>
                                    <span class="G6" aria-hidden="true">Donate</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">Donate</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Donate</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="FilterControl">
                        <div id="newsFilterVisible" class="FilterControlVisible">
                            <div id="newsFiltersBg" class="FilterControlSemaphore"></div>
                            <ul>
                                <li <?php if (!isset($cat)) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">All</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/all_categories.png"
                                            alt="All" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 1) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Game Update</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=1"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/game_updates.png"
                                            alt="Game Update" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 2) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Website</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=2"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/website.png"
                                            alt="Website" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 3) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Customer Support</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=3"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/customer_support.png"
                                            alt="Customer Support" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 4) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Technical</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=4"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/technical.png"
                                            alt="Technical" title=""></a></li>
                            </ul>
                        </div>
                        <div id="newsFilterMask" class="FilterControlMask"></div>
                    </div>


                    <div id="NewsArticles">


                        <div class="Article">
                            <div>
                                <div class="NewsHeaderImage">
                                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg" alt=""
                                         title=""/>

                                    <div></div>
                                </div>

                                <div class="Title">
                                    <a href="#">
                                        <h4 class="FlatHeader">Donate</h4></a>
                                    <br style="clear:both"/>
                                    <span></span>
                                </div>
                                <div class="Content">
                                    <p>Test</p>

                                    <!--                                    --><?php ///*BEGIN OF DONATION*/
                                    //                                    include("donates/config.php");
                                    //                                    $products = new Products();
                                    //
                                    //                                    ?>
                                    <!--                                    <form action="-->
                                    <?php //printf(SITE_ADDRESS); ?><!--donates/paypal.php" method="post">-->
                                    <!--                                        <input type="hidden" name="action" value="0"/>-->
                                    <!---->
                                    <!--                                        <div class="form-group">-->
                                    <!--                                            <label for="product">Username:</label>-->
                                    <!--                                            <input class="form-control" id="product" type="text" name="username">-->
                                    <!--                                        </div>-->
                                    <!--                                        <div class="form-group">-->
                                    <!--                                            <label for="product">Reward:</label>-->
                                    <!--                                            <select class="form-control" id="product" name="product_id" width="100">-->
                                    <!--                                                --><?php
                                    //                                                foreach ($products->get_products() as $product) {
                                    //                                                    echo '<option value="' . $product[0] . '">' . $product[1] . '</option>';
                                    //                                                }
                                    //                                                ?>
                                    <!--                                            </select>-->
                                    <!--                                        </div>-->
                                    <!---->
                                    <!--                                        <button type="submit" value="Submit" class="btn btn-success btn-block">Donate-->
                                    <!--                                        </button>-->
                                    <!--                                    </form>-->


                                    <!--end-->
                                    </br>
                                    </br>
                                    </br>
                                    </br>
                                    </br>
                                    </br>
                                    </br>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php
require_once("../footer.php");