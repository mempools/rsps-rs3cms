<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("global.php");
require_once("header.php");
?>

    <link href="<?php printf(SITE_ADDRESS); ?>css/community-51.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <script>window.swfobject || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/swfobject/swfobject_2_2.js'>\x3C/script>")</script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_arturoSlider_0_9_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_form-0.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/community-50.js" type="text/javascript"></script>

    <div id="banner_Container">
        <div id="headerMask"></div>
        <div id="banner">
            <div id="bannerReel">
                <a href="<?php printf(SITE_ADDRESS); ?>forums/"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/banners/firemaker_en.jpg" alt=""
                        title=""></a>
                <a target="_blank" href="/donate"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/banners/loyalty.jpg" alt="" title=""></a>
            </div>

            <div id="PlayNowWrapper">
                <a id="PlayNowWrapperPlayFree" href="<?php printf(SITE_ADDRESS); ?>play" class="GameLink HoverImgJs">
                </a>

                <div id="SN_links" style="display: block; ">

                </div>
            </div>
        </div>
        <div id="bannerBorder"></div>
        <div id="ctaOuterContainer">
            <div id="bannerSwitcher">
                <a id="bannerPrevious" class="HoverImg" href="?banner=4">
                    <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/prev-button.png" alt="" title=""/>
                </a>
                <a id="bannerNext" class="HoverImg" href="?banner=1">
                    <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/next-button.png" alt="" title=""/>
                </a>
            </div>
            <div id="ctaInnerContainer">
                <a class="ctaHover" id="ctaMembers" href="/donate"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-members-hover.jpg"/></a>
                <a class="ctaHover" id="ctaClans" href="<?php printf(SITE_ADDRESS); ?>hiscores"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-clans-hover.jpg"/></a>
                <a class="ctaHover" id="ctaGrandExchange" href="/webclient"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-grand-exchange-hover.jpg"/></a>
                <a class="ctaHover" id="ctaMembership" href="/forum"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-membership-hover.jpg"/></a>
            </div>
        </div>
    </div>

    <div id="MainContentOuter">
        <div class="MainContentBg">
            <div class="MainContentTopBg">
                <div class="MainContentBottomBg">
                    <div id="MainContent">

                        <div id="leftColumn">
                            <h3 class="Gradient" id="mainTitle">
                                <span class="G0">Latest News</span>
                                <span class="G1" aria-hidden="true">Latest News</span>
                                <span class="G2" aria-hidden="true">Latest News</span>
                                <span class="G3" aria-hidden="true">Latest News</span>
                                <span class="G4" aria-hidden="true">Latest News</span>
                                <span class="G5" aria-hidden="true">Latest News</span>
                                <span class="G6" aria-hidden="true">Latest News</span>
                            <span class="mask"><span class="spacing">Latest News</span>
                                <span class="middleUnderscore"><span class="spacing"
                                                                     aria-hidden="true">Latest News</span></span>
                            </span>
                            <span class="rightUnderscore">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                     class="right" alt=""/><span class="spacing" aria-hidden="true">Latest News</span>
                            </span>
                                <span class="leftUnderscore"><img
                                        src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                        class="left" alt=""/></span>
                            </h3>
                            <a href="<?php printf(SITE_ADDRESS); ?>news" class="Button Button29"><span><span><span
                                            class=""><b>View All News</b></span></span></span></a>

                            <div id="NewsArticles">
                                <?php
                                $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT 6;");

                                if (mysql_num_rows($news_qry) > 0) {
                                    $first = true;
                                    while ($news_item = mysql_fetch_assoc($news_qry)) {
                                        $news_id = $news_item['id'];
                                        $news_date = $news_item['date'];
                                        $news_author = agent::username_from_id($news_item['author_id']);
                                        $news_title = $news_item['title'];
                                        $news_desc = $news_item['desc'];
                                        $category_id = $news_item['category_id'];
                                        $seo_url = $news_item['seo_url'];

                                        ?>
                                        <div class="Article <?php if (!$first) {
                                            printf("ArticleCollapsed");
                                        } else {
                                            $first = false;
                                        } ?>">
                                            <div>
                                                <div class="NewsHeaderImage">
                                                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg"
                                                         alt="" title=""/>

                                                    <div></div>
                                                </div>
                                                <a class="ArticleControl HoverImg ControlMinus">
                                                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/Minus.png"
                                                         alt="Close" title="Close"/>
                                                </a>
                                                <a class="ArticleControl HoverImg ControlPlus">
                                                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/Plus.png"
                                                         alt="Open" title="Open"/>
                                                </a>

                                                <div class="CollapsedMask"></div>
                                                <?php
                                                $category_name = news_category($category_id);
                                                printf("<img src=\"" . SITE_ADDRESS . "img/global/news/categories/Cat_" . $category_id . ".png\" alt=\"" . $category_name . "\" title=\"" . $category_name . "\" class=\"Category\"/>");
                                                ?>
                                                <div class="Title">
                                                    <a href="<?php printf(SITE_ADDRESS . "viewnews.php?article=" . $news_id); ?>">
                                                        <h4 class="FlatHeader"><?php printf($news_title); ?></h4></a>
                                                    <br style="clear:both"/>
                                                    <span><?php printf(compare_dates($news_date, time())); ?></span>
                                                </div>
                                                <div class="Content">
                                                    <?php printf($news_desc); ?>
                                                </div>
                                    <span class="SNbutton">
                                        <span class="left"></span>
                                        <span class="middle">
                                            <span class="contentHolder">
                                                    <a class="facebook SB_link"
                                                       href="http://facebook.com/sharer.php?u=<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"
                                                       target="_blank" title="Add this page to Facebook">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNfacebook.png"
                                                            alt="Facebook" title="Facebook"/>
                                                        <span class="SB_label">Facebook</span>
                                                    </a>
                                                    <a href="http://twitter.com/share?text=<?php printf($news_title); ?>&url=<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"
                                                       target="_blank" class="twitter SB_link">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNtwitter.png"
                                                            alt="Twitter" title="Twitter"/>
                                                        <span class="SB_label">Twitter</span>
                                                    </a>
                                            </span>
                                            <span class="right">Share</span>
                                        </span> 
                                    </span>
                                                <a class="HoverImg More"
                                                   href="<?php printf(SITE_ADDRESS . "viewnews.php?article=" . $news_id . "-" . $seo_url); ?>"><img
                                                        src="<?php printf(SITE_ADDRESS); ?>img/global/news/ReadMore.png"
                                                        alt="" title=""/></a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div id="newsArticleFooter">
                                    <a href="<?php printf(SITE_ADDRESS); ?>news"
                                       class="Button Button29"><span><span><span class=""><b>View All
                                                        News</b></span></span></span></a>
                                </div>
                            </div>
                        </div>
                        <div id="rightColumn">
                            <div id="advert">
                                <iframe src="http://www.runescape.com/g=runescape/advert/advert.ws?placement=200"
                                        allowtransparency="true" width="300" height="269" scrolling="no"
                                        frameborder="0"></iframe>
                                <span>Advert</span>
                            </div>
                            <div id="social">
                                <div id="socialInner">
                                    <a href="<?php printf(FACEBOOK_URL); ?>" title="Follow us on Facebook"
                                       target="_blank" class="HoverImg NoFade AddButtons"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/MediumFacebook.png"
                                            alt="Facebook"/></a>
                                    <a href="<?php printf(TWITTER_URL); ?>" title="Follow us on Twitter" target="_blank"
                                       class="HoverImg NoFade AddButtons"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/MediumTwitter.png"
                                            alt="Twitter"/></a>
                                    <a href="<?php printf(YOUTUBE_URL); ?>" title="Follow us on YouTube" target="_blank"
                                       class="HoverImg NoFade AddButtons"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/MediumYoutube.png"
                                            alt="YouTube"/></a>

                                    <div id="bottomNavigationFlag">

                                        <div class="fb-like" data-href="http://www.facebook.com/CX"
                                             data-send="false" data-layout="button_count" data-width="45"
                                             data-show-faces="false" data-colorscheme="dark"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="events">
                                <div id="eventsInner">
                                    <h3 class="Gradient">
                                        <span class="G0">Events</span>
                                        <span class="G1" aria-hidden="true">Events</span>
                                        <span class="G2" aria-hidden="true">Events</span>
                                        <span class="G3" aria-hidden="true">Events</span>
                                        <span class="G4" aria-hidden="true">Events</span>
                                        <span class="G5" aria-hidden="true">Events</span>
                                        <span class="G6" aria-hidden="true">Events</span>
                                    <span class="mask"><span class="spacing">Events</span>
                                        <span class="middleUnderscore"><span class="spacing"
                                                                             aria-hidden="true">Events</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Events</span>
                                    </span>
                                        <span class="leftUnderscore"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                                class="left" alt=""/></span>
                                    </h3>

                                    <div id="eventsContent">
                                        <a href="http://www.runescape.com/g=runescape/events.ws"
                                           class="Button Button29"><span><span><span class=""><b>View All
                                                            Events</b></span></span></span></a>

                                        <div id="eventImage">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/event.jpg" alt=""
                                                 title=""/>
                                        </div>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">The RuneZone New Years Party</h4>
                                            Location: <span>Varrock</span><br/>
                                            When: <span>06/01/2012 - 9pm</span>
                                        </a>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">Wilderness Battle (F2P)</h4>
                                            Location: <span>North Varrock</span><br/>
                                            When: <span>21-01-2012 - 9pm</span>
                                        </a>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">RuneScape Rugby Premiership</h4>
                                            Location: <span>Clan Citadel</span><br/>
                                            When: <span>26-01-2012 to 11-03-2012</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php require_once("footer.php"); ?>