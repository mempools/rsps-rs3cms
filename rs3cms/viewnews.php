<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");

if (isset($_GET['article'])) {
    $article = $_GET['article'];

    if (!strpos($article, "-")) {
//        exit(header("Location: " . SITE_ADDRESS . "news"));
    }

    $bits = explode("-", $article);
    $id = mysql_real_escape_string($bits[0]);

    $q = dbquery("SELECT * FROM web_news WHERE id = '$id' LIMIT 1;");
    if (mysql_num_rows($q) > 0) {
        $news = mysql_fetch_array($q);

        $news_date = $news['date'];
        $news_author = agent::username_from_id($news['author_id']);
        $news_title = $news['title'];
        $news_desc = $news['desc'];
        $news_story = $news['story'];
        $category_id = $news['category_id'];
        $seo_url = $news['seo_url'];
        $avail = true;
    } else {
        exit(header("Location: " . SITE_ADDRESS . "news"));
    }
} else {
    exit(header("Location: " . SITE_ADDRESS . "news"));
}

define("PAGE_TITLE", "News - " . $news_title);
require_once("header.php");
?>

<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/news-50.css"/>

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing" aria-hidden="true">News</span>
                                    <span class="G0">News</span>
                                    <span class="G1" aria-hidden="true">News</span>
                                    <span class="G2" aria-hidden="true">News</span>
                                    <span class="G3" aria-hidden="true">News</span>
                                    <span class="G4" aria-hidden="true">News</span>
                                    <span class="G5" aria-hidden="true">News</span>
                                    <span class="G6" aria-hidden="true">News</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">News</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">News</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <?php require_once("social_feeds.php"); ?>

                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/deviceTL.png" alt="" class="deviceTL"/>

                    <div id="NewsArticles" class="singleArticle">
                        <div class="Article">
                            <div class="NewsHeaderImage">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg" alt="" title=""/>

                                <div></div>
                            </div>
                            <?php
                            $category_name = news_category($category_id);
                            printf("<img src=\"" . SITE_ADDRESS . "img/global/news/categories/Cat_" . $category_id . ".png\" alt=\"" . $category_name . "\" title=\"" . $category_name . "\" class=\"Category\"/>");
                            ?>
                            <div class="Title">
                                <h4 class="FlatHeader"><?php printf($news_title) ?></h4>
                                <br style="clear:both"/>
                                <span><?php printf(compare_dates($news_date, time())); ?></span>
                            </div>
                            <div class="Content">
                                <?php printf($news_story); ?>
                                <br/>
                                <br/>
                                <hr/>
                                Article written by <b><?php printf($news_author); ?></b>
                            </div>
                            <span class="SNbutton">
                                <span class="left"></span>
                                <span class="middle">
                                    <span class="contentHolder">
                                        <a class="facebook SB_link"
                                           href="http://facebook.com/sharer.php?u=<?php printf(SITE_ADDRESS . "news/article/" . $id . "-" . $seo_url); ?>"
                                           target="_blank" title="Add this page to Facebook">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNfacebook.png"
                                                 alt="Facebook" title="Facebook"/>
                                            <span class="SB_label">Facebook</span>
                                        </a>
                                        <a href="http://twitter.com/share?text=<?php printf($news_title); ?>&url=<?php printf(SITE_ADDRESS . "news/article/" . $id . "-" . $seo_url); ?>"
                                           target="_blank" class="twitter SB_link">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNtwitter.png"
                                                 alt="Twitter" title="Twitter"/>
                                            <span class="SB_label">Twitter</span>
                                        </a>
                                    </span>
                                    <span class="right">Share</span>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php require_once("footer.php"); ?>
