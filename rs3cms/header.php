<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

    <!doctype html>
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#"
          xmlns:fb="http://www.facebook.com/2008/fbml">
    <head>
        <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet"/>
        <link type="text/css" rel="stylesheet"
              href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
        <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/shadowbox/shadowbox-0.css"/>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
        <script
            type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
        <script>window.jQuery.ui || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_ui_1_8_6.js'>\x3C/script>")</script>
        <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
        <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
        <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
        <style type="text/css">/*\*/
            @import url(<?php echo WWW; ?> /css/home-28.css); /**/</style>
        <script type="text/javascript"
                src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
        <script>window.JSON || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/json2.js'>\x3C/script>")</script>
        <script type="text/javascript"
                src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
        <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/shadowbox/shadowbox-0.js"></script>
        <style type="text/css">/*\*/
            @import url(<?php echo SITE_ADDRESS; ?> /css/global-30.css); /**/</style>
        <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
        <title><?php printf(SITE_NAME);
            if (defined('PAGE_TITLE'))
                printf(" - " . PAGE_TITLE); ?></title>

        <meta name="keywords"
              content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, <?php printf(SITE_NAME); ?>"/>
        <meta name="description" content="<?php printf(SITE_DESC); ?>"/>

        <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>favicon.ico"/>
        <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>favicon.ico"/>
        <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">

        <meta property="fb:app_id" content="174823375888281"/>
        <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png"/>
        <meta property="og:title" content="<?php printf(SITE_NAME);
        if (defined('PAGE_TITLE'))
            printf(" - " . PAGE_TITLE); ?>"/>
        <meta property="og:type" content="game"/>
        <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>"/>
        <meta property="og:description" content="<?php printf(SITE_DESC); ?>"/>

        <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_thickbox_3.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_thickbox_3-0.css"/>
    </head>
<body>
    <a style="display:none" id="SNbuttonLink" class="thickbox"></a>


<div id="Background">
    <div id="TopBackground">
<?php if (empty($_GET['eoc'])) { ?>
    <div id="BottomBackground">
    <div id="HeaderBg">
        <div id="HeaderLeft">
            <div id="HeaderLeftInner"></div>
        </div>
        <div id="HeaderCentre"></div>
        <div id="HeaderRight">
            <div id="HeaderRightInner"></div>
        </div>
    </div>

    <div id="Header">
        <a href="<?php printf(SITE_ADDRESS); ?>" id="HeaderLogo"><img
                src="<?php printf(SITE_ADDRESS); ?>img/global/logos/runescape.png" alt="RuneScape"
                title=""/></a>

        <div id="Nav">
            <nav>
                <ul class="MainMenu">
                    <li class="News">
                        <a class="HoverImg" href="<?php printf(SITE_ADDRESS); ?>news">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/News.jpg" alt="News"
                                 title=""/>
                        </a>
                    </li>
                    <li class="TheGame CSS3">
                        <a class="HoverImg" href="#">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/GameInfo.jpg"
                                 alt="Game Info" title=""/>
                        </a>

                        <div class="Mask"></div>
                        <div class="DropDown Kingthings">
                            <div>
                                <ul>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>webclient/webclient.php">Webclient</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>webclient/client.jar">Desktop Client</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="Community CSS3">
                        <a class="HoverImg" href="#">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/Community.jpg"
                                 alt="Community" title=""/>
                        </a>

                        <div class="Mask"></div>
                        <div class="DropDown Kingthings">
                            <div>
                                <ul>
                                    <li><a href="<?php printf(FORUMS_URL); ?>">Forums</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>profile">Adventurer's Log</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores">Hiscores</a></li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="Forum">
                        <a class="HoverImg" href="<?php printf(FORUMS_URL); ?>">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/Forum.jpg" alt="Forum"
                                 title=""/>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <?php
        if ($agent->logged_in) {
            ?>

            <div id="LoggedIn" class='noshowupgradetopro'>
                <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/SubNavMask.png" class="SubNavMask"
                     alt=""/>

                <div id="UsernameDisplay">
                    <div class="Centre1">
                        <div class="Centre2">

                            <h5 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                         aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G0"><?php printf($agent->username); ?></span>
                                <span class="G1" aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G2" aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G3" aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G4" aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G5" aria-hidden="true"><?php printf($agent->username); ?></span>
                                <span class="G6" aria-hidden="true"><?php printf($agent->username); ?></span>
                            </h5>
                        </div>
                    </div>
                </div>
                <div id="OverflowMask">
                    <div id="SubNav">
                        <nav>
                            <ul>
                                <li class="Support CSS3">
                                    <a href="#" class="support">Support</a>

                                    <div class="Mask M1"></div>
                                    <div class="Mask M2"></div>
                                    <div class="Mask M3"></div>
                                    <div class="DropDown">
                                        <div>
                                            <ul>
                                                <li><a href="<?php printf(BUGREPORT_URL); ?>">Submit a Bug
                                                        Report</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="Account CSS3">
                                    <a href="<?php printf(SITE_ADDRESS); ?>account_settings" class="account">Account</a>

                                    <div class="Mask M2"></div>
                                    <div class="Mask M3"></div>
                                    <div class="Mask M4"></div>
                                    <div class="Mask M5"></div>
                                    <div class="Mask M6"></div>
                                    <div class="Mask M7"></div>
                                    <div class="Mask M8"></div>
                                    <div class="Mask M9"></div>
                                    <div class="Mask M10"></div>
                                    <div class="DropDown">
                                        <div>
                                            <ul>
                                                <li><a href="<?php printf(SITE_ADDRESS); ?>account_settings">Account
                                                        Settings</a></li>
                                                <?php if ($agent->has_permission("web_admin")) {
                                                    echo '<li><a href=' . SITE_ADDRESS . 'manage/index.php>Moderator CP</a></li>';
                                                } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li class="Logout CSS3">
                                    <a href="logout" class="logout">Logout</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <a href="<?php printf(SITE_ADDRESS); ?>play" id="PlayNow" class="HoverImgCss3 GameLink"><img
                            src="<?php printf(SITE_ADDRESS); ?>img/global/header/login/PlayNow.png"
                            alt="Play Now"/></a>
                </div>
                play
                <div id="Avatar">
                    <img src='<?php printf(SITE_ADDRESS); ?>img/avatars/default.png' class='avatar' alt=''/>

                    <div class="mask"></div>
                </div>
            </div>

            <?php
        } else {
            ?>

            <!-- Login Box begin -->
            <div id="Login" class="NoJS">
                <a class="LoginX LoginXtop"></a>

                <div class="LoginXbottom"></div>
                <a href="#" class="Button Button29" id="LoginButton1"><span><span><span
                                class=""><b>Login</b></span></span></span></a>
                <a href="register?confirm" class="Button Button29" id="RegisterButton"><span><span><span class=""><b>Register</b></span></span></span></a>

                <div id="LoginMask"></div>
                <div id="LoginPanel">
                    <form class="LoginForm" action="<?php printf(SITE_ADDRESS); ?>login?c" method="post"
                          autocomplete="off">
                        <label for="Username" class="username Kingthing">Email:
                            <input type="text" id="Username" class="Username" title="Email" name="email"
                                   value="" tabindex="1"/>
                        </label>
                        <label for="Password" class="password Kingthing">Password:<input type="password"
                                                                                         id="Password"
                                                                                         class="Password"
                                                                                         name="password"
                                                                                         title="Password"
                                                                                         tabindex="1"
                                                                                         maxlength="20"/></label>
                        <a href="forgot_password" class="forgot">Forgot Password?</a>
                                <span class="Button Button29" id="LoginButton2"><span><span><span class=""><b>Login</b></span><input
                                                value="login" name="submit" type="submit" title="Login"/></span></span></span>
                        <!-- <input type="hidden" value="news" name="mod">
                        <input type="hidden" value="0" name="ssl">
                        <input type="hidden" value="" name="dest"> -->
                    </form>
                    <p id="Or1">or&nbsp;</p>

                    <p id="GetAccess">Get access and start playing free!</p>

                    <p id="Or2">or</p>

                    <div id="SocialNetworkingLogin">
                        <a id="facebooklogin" class="Facebooklogin HoverImg"
                           href="<?php printf(SITE_ADDRESS); ?>register?confirm">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/login/modalLoginFB.png"
                                 alt="Facebook" title="Facebook"/>
                            <span>Register a new account</span>
                        </a>
                    </div>
                </div>
                <a href="play" id="PlayNowFree" class="HoverImgCss3Js GameLink"><img
                        src="<?php printf(SITE_ADDRESS); ?>img/global/header/login/PlayNowFree.png"
                        alt="Play Now Free"/></a>
            </div>
            <!-- Login box end -->

            <?php
        }
        ?>

        <div id="SearchPanel">
            <form action="<?php printf(SITE_ADDRESS); ?>profile" method="get">
                        <span class="Button Button29" id="SearchButton"><span><span><span
                                        class=""><b><?php echo "Find User"; ?></b></span><input value="Search"
                                                                                                name="search"
                                                                                                type="submit"
                                                                                                title="Find"/></span></span></span>
                <input type="text" id="Search" maxlength="12" name="searchName" title="Search users"/>
            </form>
        </div>
        <?php
        $users_online = dbevaluate("SELECT COUNT(id) FROM characters WHERE online > '0';");
        ?>
        <div id="PlayerCount"><p class="top"><span><?php printf($users_online); ?></span> people currently
                online</p></div>
        <div id="CrumbTrail">
            <div id="Trail">
                <!--                        <span>--><?php //if (defined("PAGE_TITLE")) {
                //                                printf(PAGE_TITLE);
                //                            } ?><!--</span>-->
            </div>
        </div>
    </div>
<? } else {
    ?>

    <?php
    include_once("header.php");
}
?>