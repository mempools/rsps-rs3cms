<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("global.php");

$show = 5;
$page = 1;

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}

$start_from = ($page - 1) * $show;

$cat2 = filter_for_input($_GET['cat']);

if (isset($_GET['cat'])) {
    $cat = filter_for_input($_GET['cat']);
    $total_results = dbevaluate("SELECT COUNT(*) FROM web_news WHERE category_id='$cat';");
    $news_qry = dbquery("SELECT * FROM web_news WHERE category_id='$cat' ORDER BY id DESC LIMIT $start_from, $show;");
} else {
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_news;");
    $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT $start_from, $show;");
}

$total_pages = ceil($total_results / $show);

if ($page < 1 || ($total_pages > 0 && $page > $total_pages)) {
    $url = SITE_ADDRESS . "news";
    exit(header("Location: $url"));
}

require_once("header.php");
?>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/news-50.css"/>

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing" aria-hidden="true">News</span>
                                    <span class="G0">News</span>
                                    <span class="G1" aria-hidden="true">News</span>
                                    <span class="G2" aria-hidden="true">News</span>
                                    <span class="G3" aria-hidden="true">News</span>
                                    <span class="G4" aria-hidden="true">News</span>
                                    <span class="G5" aria-hidden="true">News</span>
                                    <span class="G6" aria-hidden="true">News</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">News</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">News</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="FilterControl">
                        <div id="newsFilterVisible" class="FilterControlVisible">
                            <div id="newsFiltersBg" class="FilterControlSemaphore"></div>
                            <ul>
                                <li <?php if (!isset($cat)) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">All</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/all_categories.png"
                                            alt="All" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 1) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Game Update</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=1"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/game_updates.png"
                                            alt="Game Update" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 2) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Website</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=2"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/website.png"
                                            alt="Website" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 3) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Customer Support</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=3"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/customer_support.png"
                                            alt="Customer Support" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 4) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Technical</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=4"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/technical.png"
                                            alt="Technical" title=""></a></li>
                            </ul>
                        </div>
                        <div id="newsFilterMask" class="FilterControlMask"></div>
                    </div>
                    <div id="NewsFilterDropdown">
                        <h4>Filter By:</h4>

                        <style>
                            #categoryDropdown.InPageDropDownNew.NoJS:hover .DropListWrapper,
                            #categoryDropdown.InPageDropDownNew.CSS3:hover .DropListWrapper {
                                top: -8px;
                            }

                            #categoryDropdown.InPageDropDownNew.NoJS .DropListWrapper,
                            #categoryDropdown.InPageDropDownNew.CSS3 .DropListWrapper {
                                top: -82px;
                            }

                            #categoryDropdown:hover {
                                height: 110px;
                            }
                        </style>
                        <div class="InPageDropDownNew NoJS CSS3 width150" id="categoryDropdown" data-expand="110px">
                            <div class="Mask"></div>
                            <div class="DropListWrapper" data-retract="-82px">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/DropDownFlourish.png"
                                     class="Flourish" alt="">
                                <span class="firstValue">
                                <?php isset($cat) ? printf(news_category($cat)) : printf("All"); ?>
                                </span>
                                <ul>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news">All</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=1">Game Update</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=2">Website</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=3">Customer Support</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=4">Technical</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php require_once("social_feeds.php"); ?>

                    <div id="NewsArticles">

                        <?php
                        if (mysql_num_rows($news_qry) > 0) {
                            while ($news = mysql_fetch_assoc($news_qry)) {
                                $news_id = $news['id'];
                                $news_date = $news['date'];
                                $news_author = agent::username_from_id($news['author_id']);
                                $news_title = $news['title'];
                                $news_desc = $news['desc'];
                                $news_story = $news['story'];
                                $category_id = $news['category_id'];
                                $seo_url = $news['seo_url'];
                                ?>
                                <div class="Article">
                                    <div>
                                        <div class="NewsHeaderImage">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg" alt=""
                                                 title=""/>

                                            <div></div>
                                        </div>
                                        <?php
                                        $category_name = news_category($category_id);
                                        printf("<img src=\"" . SITE_ADDRESS . "img/global/news/categories/Cat_" . $category_id . ".png\" alt=\"" . $category_name . "\" title=\"" . $category_name . "\" class=\"Category\"/>");
                                        ?>
                                        <div class="Title">
                                            <a href="<?php printf(SITE_ADDRESS . "viewnews.php?article=" . $news_id); ?>">
                                                <h4 class="FlatHeader"><?php printf($news_title); ?></h4></a>
                                            <br style="clear:both"/>
                                            <span><?php printf(compare_dates($news_date, time())); ?></span>
                                        </div>
                                        <div class="Content">
                                            <?php printf($news_story); ?>
                                        </div>
                                        <span class="SNbutton">
                                            <span class="left"></span>
                                            <span class="middle">
                                                <span class="contentHolder">
                                                    <a class="facebook SB_link"
                                                       href="http://facebook.com/sharer.php?u=<?php printf(SITE_ADDRESS . "viewnews.php?article=" . $news_id); ?>"
                                                       target="_blank" title="Add this page to Facebook">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNfacebook.png"
                                                            alt="Facebook" title="Facebook"/>
                                                        <span class="SB_label">Facebook</span>
                                                    </a>
                                                    <a href="http://twitter.com/share?text=<?php printf($news_title); ?>&url=<?php printf(SITE_ADDRESS . "viewnews.php?article=" . $news_id); ?>"
                                                       target="_blank" class="twitter SB_link">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNtwitter.png"
                                                            alt="Twitter" title="Twitter"/>
                                                        <span class="SB_label">Twitter</span>
                                                    </a>
                                                </span>
                                                <span class="right">Share</span>
                                            </span>
                                        </span>
                                        <a class="HoverImg More"
                                           href="<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/news/ReadMore.png" alt=""
                                                title=""/></a>
                                    </div>
                                </div>

                                <?php
                            }
                        } else {
                            echo "<center><h3>Sorry, there are no news articles available at this moment.</h3></center>";
                        }
                        ?>
                    </div>
                    <div class="PageControl">
                        <?php
                        $base_url = SITE_ADDRESS . "news/";
                        if (isset($cat)) {
                            $base_url .= news_category($cat) . "/";
                        }

                        if ($page > 1) {
                            printf("<a href=\"" . $base_url . "p" . ($page - 1) . "\" class=\"Button Button29 Prev\"><span><span><span class=\"\"><b>Prev</b></span></span></span></a>");
                        } else {
                            printf("<span class=\"Button Button29disabled Prev\"><span><span><span class=\"\"><b>Prev</b></span></span></span></span>");
                        }

                        if ($page < $total_pages) {
                            printf("<a href=\"" . $base_url . "p" . ($page + 1) . "\" class=\"Button Button29 Next\"><span><span><span class=\"\"><b>Next</b></span></span></span></a>");
                        } else {
                            printf("<span class=\"Button Button29disabled Next\"><span><span><span class=\"\"><b>Next</b></span></span></span></span>");
                        }

                        $adjacents = 1;

                        // start on pagignition
                        printf("<ul>");
                        if ($total_pages < (7 + ($adjacents * 2))) {
                            for ($i = 1; $i <= $total_pages; $i++) {
                                if ($i == $page) {
                                    printf("<li class=\"current\">" . $page . "</li>");
                                } else {
                                    printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                }
                            }
                        } elseif ($total_pages > 5 + ($adjacents * 2)) {
                            if ($page < 1 + ($adjacents * 2)) {
                                for ($i = 1; $i < 4 + ($adjacents * 2); $i++) {
                                    if ($i == $page) {
                                        printf("<li class=\"current\">" . $page . "</li>");
                                    } else {
                                        printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                    }
                                }
                                printf("<li class=\"ellipses\">...</li>");
                                printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                            } elseif ($total_pages - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                                printf("<li><a href=\"" . $base_url . "p1\">1</a></li>");
                                printf("<li class=\"ellipses\">...</li>");
                                for ($i = $page - $adjacents; $i <= $page + $adjacents; $i++) {
                                    if ($i == $page) {
                                        printf("<li class=\"current\">" . $page . "</li>");
                                    } else {
                                        printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                    }
                                }
                                printf("<li class=\"ellipses\">...</li>");
                                printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                            } else {
                                printf("<li><a href=\"" . $base_url . "p1\">1</a></li>");
                                printf("<li class=\"ellipses\">...</li>");
                                for ($i = $total_pages - (2 + ($adjacents * 2)); $i <= $total_pages; $i++) {
                                    if ($i == $page) {
                                        printf("<li class=\"current\">" . $page . "</li>");
                                    } else {
                                        printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                    }
                                }
                            }
                        }
                        printf("</ul>");
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("footer.php"); ?>
