<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}

$confirm = false;

if (!DEMO_MODE && isset($_POST['submit'])) {
    $new_email = $_POST['name'];

    if (filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
        $q = dbquery("SELECT id FROM characters WHERE characters.email = '$new_email';");
        if (strcasecmp($new_email, $agent->email) == 0 || mysql_num_rows($q) == 0) {
            $confirm = true;
        } else {
            $error = "That email is already in use.";
        }
    } else {
        $error = "Please enter a valid email address!";
    }
} else if (!DEMO_MODE && isset($_POST['confirm'])) {
    $new_email = $_POST['name'];

    if (filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
        $q = dbquery("SELECT email FROM characters WHERE characters.email = '$new_email';");
        if (strcasecmp($new_email, $agent->email) == 0 || mysql_num_rows($q) == 0) {
            dbquery("UPDATE characters SET email = '$new_email' WHERE id = '$agent->master_id';");
            $agent->email = $new_email;
            $finished = true;
        } else {
            $error = "That email is already in use.";
        }
    } else {
        $error = "Please enter a valid email address!";
    }
}

?>


<!doctype html>
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/account_settings_frame-50.css" rel="stylesheet">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script type="text/javascript" src="https://www.jagex.com/js/jquery/jquery_effects_core_0.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript"
            src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script>
        if (window == window.top) {
            window.location.href = "http://google.com/";
        }
    </script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/account_settings_frame-50.js"></script>
    <title><?php printf(SITE_NAME); ?></title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java">
    <meta name="description" content="<?php printf(SITE_DESC); ?>">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">
    <meta property="og:title" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:type" content="game">
    <meta property="og:url" content="<?php printf(SITE_ADDRESS); ?>">
    <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:description" content="<?php printf(SITE_DESC); ?>">
    <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png">
    <style type="text/css">
        .js .helpBox {
            display: none;
        }

        .Button29 {
            margin-top: 2px;
        }

        .inputLabel {
            font-size: 15px;
        }

        .status {
            clear: both;
            float: none;
        }

        #help {
            display: none;
        }

        .js #help {
            display: block;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").addClass("js");
            $("#help").click(function (ev) {
                ev.preventDefault();
                $(".helpBox").slideToggle('fast', function () {
                    resize();
                });
            });
        });
    </script>
    <script>$(function () {

            var timer;
            var $status = $('.status');
            var $suggestions = $('.suggestions');

            // Handles setting the status of the message beneath the input
            function setStatus(newStatus, newClass) {
                $status
                    .text(newStatus)
                    .removeClass('success error')
                    .addClass(newClass)
                ;
            }

            // Handling making a check if user types in the box, but with a delay so we don't fire until they pause typing
            var $name = $('#name')
                .keypress(function () {
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        var val = $name.val();
                        if (val.length > 0) {
                            checkEmail(val);
                        }
                        setStatus("Checking email...", "");
                    }, 500);
                });

            // Making calls to check script as needed
            function checkEmail(email) {
                var ajaxURL = "<?php printf(SITE_ADDRESS); ?>email_check.php?email=" + email;

                $.ajax({
                    url: ajaxURL,
                    success: handleResult,
                    error: function () {
                        handleResult('');
                    }
                });
            }

            // Handling return from ajax request
            function handleResult(response) {

                // Available
                if (response.match(/^OK/)) {
                    setStatus('That email is available', 'success');
                }
                // Unavailable
                else if (response.match(/^NOK/)) {
                    setStatus('That email is not available.', 'error');
                }
                // Check script wasn't happy with the name (empty or malformed)
                else if (response.match(/^NONAME/)) {
                    setStatus("Not a valid email address", "error");
                }
                // Something unexpected happened, e.g. transport error or failure at the script's end
                else {
                    setStatus("Couldn't check", "error");
                }
                resize();

            }

            $suggestions.delegate('<li>', 'click', function () {
                var $this = $(this);

                $name.val($this.text());

            });
        });
    </script>
</head>
<body>
<div class="container">
    <a href="#" class="Button Button31Help" id="help"><span><span><span class=""><b>Help</b></span></span></span></a>

    <div class="helpBox" style="padding-top: 35px;">
        <p>Your email is solely used for us to send you your forgotten password.</p>
    </div>

    <?php
    if (isset($error)) {
        printf("<p class=\"intro error\">$error</p>");
    }
    ?>

    <p>Your current email is: <b class="blue"><?php printf($agent->email); ?></b></p>

    <?php if ($confirm == true) { ?>
        <p class="intro">Your email will be changed to <b class="blue"><?php printf($new_email); ?></b></p>
        <form method="post" action="<?php printf(SITE_ADDRESS); ?>account_change_email.php" style="display:inline">
            <input type="hidden" name="name" value="<?php printf($new_email); ?>" id="name">
            <span class="Button Button29" id="confirm"><span><span><span class=""><b>Yes</b></span><input value="Yes"
                                                                                                          name="confirm"
                                                                                                          type="submit"
                                                                                                          title="Yes"/></span></span></span>
            <a href="<?php printf(SITE_ADDRESS); ?>account_change_email.php"><span class="Button Button29"
                                                                                   id="confirm"><span><span><span
                                class=""><b>No</b></span></span></span></span></a>
        </form>
    <?php } else if (isset($finished)) { ?>
        <p class="intro">Your email has been successfully changed to <b class="blue"><?php printf($new_email); ?></b>.
        </p>
        <?php
    } else {
        ?>
        <form method="post" action="<?php printf(SITE_ADDRESS); ?>account_change_email.php" style="padding-top:10px">
            <label class="formInput">
                <div class="inputLabel">New Email:</div>
                <div class="DarkInputBoxWrapper">
                    <div class="InputBoxLeft"><input type="text" title="" name="name" maxlength="40" size="20"
                                                     value="<?php printf($agent->email); ?>" id="name"></div>
                    <div class="InputBoxRight"></div>
                </div>
            </label>
                <span class="Button Button29" id="submit"><span><span><span class=""><b>Set Email</b></span><input
                                value="Set Name" name="submit" type="submit" title="Set Email"/></span></span></span>
        </form>
        <?php
    } ?>
    <div class="status"></div>
    <ul class="suggestions"></ul>
</div>
</body>
</html>