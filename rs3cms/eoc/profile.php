<?php
require_once("global.php");
require_once("../includes/class_character_statistics.php");

$user_found = false;
if (isset($_GET['searchName'])) {
    $username = filter_for_input($_GET['searchName']);
    if (preg_match("/^.{1,12}$/", $username) && preg_match("/(^[A-Za-z0-9]{1,12}$)|(^[A-Za-z0-9]+[\-\s][A-Za-z0-9]+[\-\s]{0,1}[A-Za-z0-9]+$)/", $username)) {
        $q2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.username = '$username' LIMIT 1;");
        $cdata = mysql_fetch_array($q2);
        if (mysql_num_rows($q2) > 0) {
            $stats_data = mysql_fetch_array($q2);
            $statistics = new character_statistics($stats_data);
            $user_found = true;
            define("PAGE_TITLE", "Viewing profile: " . $cdata['username']);
            $user_name = $cdata['username'];

            // determine overall ranking
            $rank = 0;
            dbquery("SET @rankcount = 0;");
            $rqry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank, total_level, total_exp, master_id FROM characters_statistics ORDER BY total_level DESC, total_exp DESC ) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id='$cid';");

            if (mysql_num_rows($rqry) > 0) {
                $rdata = mysql_fetch_array($rqry);
                $rank = $rdata['rank'];
            }
        }
    }
}


$page_stats = [$_GET['stats'], "advprofile"];
$page_skills = [$_GET['skills'], "advskills"];
$page_activity = [$_GET['activity'], "advactivity"];

function getRank($user, $order)
{
    $count = 1;
    $res = dbquery("SELECT username FROM characters_statistics ORDER BY $order;");

    if (mysql_num_rows($res) > 0) {
        while ($row = mysql_fetch_array($res)) {
            $userid = $row['username'];
            if ($userid == $user) {
                return $count;
            }
            $count++;
        }
    }
    return -1;
}

?>

<html>
<head>
    <title>Player Profile: <?php echo $username; ?></title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!--    <script type="text/javascript" src="--><?php //echo SITE_ADDRESS; ?><!--eoc/js/adventurers_log-3.js">-->
    <!--        function updateHTML(elmId, value) {-->
    <!--            document.getElementById(elmId).innerHTML = value;-->
    <!--        }-->
    <!--        updateHTML("combat-chart__canvas", '<canvas style="width: 160px; height: 220px;" class="combat-chart__canvas" id="combat-chart__canvas" width="160" height="220"></canvas>');-->
    <!---->
    <!---->
    <!--    </script>-->

    <script type='text/javascript'>
        var _vis_opt_cookieDays = 1,
            _vwo_code = (function () {
                var account_id = 54596,
                    settings_tolerance = 2000,
                    library_tolerance = 2500,
                    use_existing_jquery = false,
                    f = false, d = document;
                return {
                    use_existing_jquery: function () {
                        return use_existing_jquery;
                    }, library_tolerance: function () {
                        return library_tolerance;
                    }, finish: function () {
                        if (!f) {
                            f = true;
                            var a = d.getElementById('_vis_opt_path_hides');
                            if (a)a.parentNode.removeChild(a);
                        }
                    }, finished: function () {
                        return f;
                    }, load: function (a) {
                        var b = d.createElement('script');
                        b.src = a;
                        b.type = 'text/javascript';
                        b.innerText;
                        b.onerror = function () {
                            _vwo_code.finish();
                        };
                        d.getElementsByTagName('head')[0].appendChild(b);
                    }, init: function () {
                        settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                        this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                        var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
                        a.setAttribute('id', '_vis_opt_path_hides');
                        a.setAttribute('type', 'text/css');
                        if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                        h.appendChild(a);
                        return settings_timer;
                    }
                };
            }());
        _vwo_settings_timer = _vwo_code.init();
    </script>
</head>


<?php if (isset($page_stats[0])){ ?>
<body id="advprofile" class="advprofile en"
      onload="donut_set(<?php echo $cdata['magic_exp']; ?>,<?php echo $cdata['attack_exp']; ?>,<?php echo $cdata['range_exp']; ?>)"
      itemscope itemtype="http://schema.org/WebPage">
<?php } else if (isset($page_skills[0])){ ?>
<body id="advskills" class="advskills en" itemscope itemtype="http://schema.org/WebPage">
<?php } else if (isset($page_activity[0])){ ?>
<body id="advactivity" class="advactivity en" itemscope itemtype="http://schema.org/WebPage">
<?php } else { ?>
<body id="advprofile" class="advprofile en" itemscope itemtype="http://schema.org/WebPage">
<?php } ?>
<div class="stickyWrap">

    <?php include_once("header.php") ?>
    <div class="contents">
        <main class="player-log">
            <?php
            include_once("plog-header.php") ?>

            <?php if (!$user_found) { ?>
                <div class="error-details error-eyes">
                    <section class="error-details__copy">
                        <h1 class="double-title"><span>player</span>not found</h1>

                        <p class="error-details__meta">The player profile you tried to view cannot be found. This player
                            may not exist or may not have logged in to RuneScape recently.</p>

                        <form class="advlog-search" action="<?php echo SITE_ADDRESS; ?>eoc/profile.php"
                              method="get">
                            <input class="advlog-search__text" placeholder="Search for a player" name="searchName"
                                   maxlength="12" id="searchName" title="Player Name"
                                   pattern="^[a-zA-Z0-9]+([ _-&amp;nbsp;]{0,10}[0-9a-zA-Z]+)*$" required="" type="text">
                            <input type="hidden" id="stats" value="" name="stats">
                            <input class="advlog-search__submit" value="Go" type="submit">
                        </form>
                    </section>
                </div>
            <?php } else if (isset($page_stats[0])) { ?>
                <div class="profile-stats">
                    <div class="profile-stats__right profile-stats__right--show" id="profile-stats__right">
                        <!--                    <section class="stats-box stats-box--clan" id="stats-box--clan">-->
                        <!--                        <a id="clanLink"-->
                        <!--                           href="http://services.runescape.com/m=clan-home/c=YAFMFdU6vJ4/clan/Nex and SS">-->
                        <!--                            <h2 class="stats-box__title stats-box__title--clans">Clan</h2>-->
                        <!---->
                        <!--                            <div class="stats-box__clanflag">-->
                        <!--                                <img-->
                        <!--                                    src="http://services.runescape.com/m=avatar-rs/c=YAFMFdU6vJ4/Drumgun/ownclan.png?w=274"-->
                        <!--                                    alt="Clan Flag" title="Clan Flag">-->
                        <!--                            </div>-->
                        <!--                            <p class="stats-box__clan-name" id="clanName">Nex and SS</p>-->
                        <!--                            <span class="stats-box__footlink" href="#">Clan Page</span>-->
                        <!--                        </a>-->
                        <!--                    </section>-->
                        <section class="stats-box stats-box--activity stats-box--activity-small"
                                 id="stats-box--activity">
                            <a href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name; ?>&activity">
                                <h2 class="stats-box__title stats-box__title--activity">Activity</h2>
                                <?php
                                $activities = dbquery("SELECT * FROM activity_logs WHERE username = '$user_name' LIMIT 4;");
                                if (mysql_num_rows($activities) > 0) {
                                    while ($activity = mysql_fetch_assoc($activities)) {
                                        $activity_date = $activity['date'];
                                        $short_desc = $activity['short_desc'];
                                        $full_desc = $activity['full_desc'];
                                        ?>
                                        <article class="event">
                                            <h3 class="event__title"><?php echo $full_desc; ?></h3>
                                            <time class="event__time"><?php echo $activity_date; ?></time>
                                        </article>
                                    <?php }
                                } ?>

                                <!--                            <span class="stats-box__footlink">all activity</span>-->
                            </a>
                        </section>
                    </div>
                    <!--                <section class="stats-box stats-box--quests">-->
                    <!--                    <a href="http://services.runescape.com/m=adventurers-log/c=YAFMFdU6vJ4/quests?searchName=Drumgun">-->
                    <!--                        <h2 class="stats-box__title">Quests</h2>-->
                    <!--                        <span class="stats-box__totals">167 / 215</span>-->
                    <!--                        <span class="stats-box__label">Completed</span>-->
                    <!---->
                    <!--                        <div class="bar-graph" title="The number of quests you have completed.">-->
                    <!--                            <span class="bar-graph__value">167</span>-->
                    <!---->
                    <!--                            <div class="bar-graph__fill bar-graph__fill--animate bar-graph__fill--comp"-->
                    <!--                                 style="width: 78%;"></div>-->
                    <!--                        </div>-->
                    <!--                        <span class="stats-box__label">In Progress</span>-->
                    <!---->
                    <!--                        <div class="bar-graph"-->
                    <!--                             title="The total number of quests in progress out of the total number available.">-->
                    <!--                            <span class="bar-graph__value">5</span>-->
                    <!---->
                    <!--                            <div class="bar-graph__fill bar-graph__fill--animate bar-graph__fill--prog"-->
                    <!--                                 style="width: 10%;"></div>-->
                    <!--                        </div>-->
                    <!--                        <span class="stats-box__label">Not Started</span>-->
                    <!---->
                    <!--                        <div class="bar-graph" title="The number of quests you have yet to start.">-->
                    <!--                            <span class="bar-graph__value">43</span>-->
                    <!---->
                    <!--                            <div class="bar-graph__fill bar-graph__fill--animate bar-graph__fill--non"-->
                    <!--                                 style="width: 20%;"></div>-->
                    <!--                        </div>-->
                    <!--                        <span class="stats-box__footlink">all quests</span>-->
                    <!--                    </a>-->
                    <!--                </section>-->
                    <section class="stats-box stats-box--skills">
                        <a href="#">
                            <h2 class="stats-box__title">Skills</h2></a>
                        <article class="skill-top">
                            <a href="#">
                                <figure class="skill-top__figure">
                                    <img class="skill-top__icon"
                                         src="<?php echo SITE_ADDRESS; ?>eoc/img/adventurers-log/skills/hitpoints.png"
                                         title="Constitution">
                                </figure>
                            </a>

                            <div class="skill-top__stats">
                                <h3 class="skill-top__title">Constitution</h3>

                                <p class="skill-top__level">
                                    <span>Level <?php echo $cdata['constitution_level']; ?></span>
                                    | <?php echo $cdata['constitution_exp']; ?> XP</p>
                            </div>
                        </article>
                        <ul class="stats-box__top-skills">
                            <li class="stats-box__top-skill">
                                <a href="#">
                                    <img class="stats-box__small-icon"
                                         src="http://www.runescape.com/img/eoc/adventurers-log/skills/strength.png">

                                    <p class="stats-box__small-text">Strength</p>

                                    <p class="stats-box__small-text stats-box__small-text--level">
                                        Level <?php echo $cdata['strength_level']; ?></p>
                                </a>
                            </li>
                            <li class="stats-box__top-skill">
                                <a href="#">
                                    <img class="stats-box__small-icon"
                                         src="http://www.runescape.com/img/eoc/adventurers-log/skills/defence.png">

                                    <p class="stats-box__small-text">Defence</p>

                                    <p class="stats-box__small-text stats-box__small-text--level">
                                        Level <?php echo $cdata['defence_level']; ?></p>
                                </a>
                            </li>
                            <li class="stats-box__top-skill">
                                <a href="#">
                                    <img class="stats-box__small-icon"
                                         src="http://www.runescape.com/img/eoc/adventurers-log/skills/prayer.png"
                                         title="Construction" alt="Construction">

                                    <p class="stats-box__small-text">Prayer</p>

                                    <p class="stats-box__small-text stats-box__small-text--level">
                                        Level <?php echo $cdata['prayer_level']; ?></p>
                                </a>
                            </li>
                        </ul>
                        <a href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name; ?>&stats"
                           class="stats-box__footlink">all skills</a>
                    </section>
                    <section class="stats-box stats-box--combat">
                        <a href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name; ?>&stats">
                            <h2 class="stats-box__title">Combat</h2>

                            <div class="combat-chart">
                                <canvas style="width: 160px; height: 220px;" class="combat-chart__canvas"
                                        id="combat-chart__canvas" width="160" height="220"></canvas>
                            <span
                                class="combat-chart__favourite combat-chart__favourite--attack">Combat Skills</span>
                                <ul class="combat-chart__legend">
                                    <li class="combat-chart__legend-value combat-chart__legend-value--magic">Magic</li>
                                    <li class="combat-chart__legend-value combat-chart__legend-value--attack">Melee</li>
                                    <li class="combat-chart__legend-value combat-chart__legend-value--ranged">Ranged
                                    </li>
                                </ul>
                            </div>
                            <span class="stats-box__footlink">all skills</span>
                        </a>
                    </section>
                    <div class="stats-box stats-box--current">
                        <section class="stats-box stats-box--small">
                            <h2 class="stats-box__score-title">Current Level</h2>

                            <div class="stats-box__score-parent">
                                <p class="stats-box__score-value">
                                    <?php echo $cdata['total_level']; ?> <span
                                        class="stats-box__score-subtext">/ 2,595</span></p>
                            </div>
                        </section>
                        <section class="stats-box stats-box--small stats-box--combat-level">
                            <h2 class="stats-box__score-title">Combat Level</h2>

                            <div class="stats-box__score-parent">
                                <p class="stats-box__score-value"><?php echo $cdata['combat_level']; ?> <span
                                        class="stats-box__score-subtext">/ 138</span>
                                </p>
                            </div>
                        </section>
                        <section class="stats-box stats-box--small">
                            <a href="#">
                                <h2 class="stats-box__score-title">Current Rank</h2>

                                <p class="stats-box__score-value stats-box__score-value--rank"><?php echo getRank($user_name, "total_level DESC") ?></p>
                            </a>
                        </section>
                        <div class="stats-box stats-box--small stats-box--avatar">
                            <img class="stats-box__chat-head"
                                 src="<?php echo SITE_ADDRESS; ?>eoc/img/default_chat.png">
                        </div>
                    </div>
                </div>
            <?php } else if (isset($page_skills[0])) { ?>
                <section class="skills">
                    <header class="skills__header">
                        <h1 class="single-title">Skills</h1>
                    </header>
                    <h2 class="skills__level-title skills__level-title--first">Goal</h2>

                    <h2 class="skills__level-title">Current Level</h2>
                    <?php
                    for ($skillId = 0; $skillId <= 24; $skillId++) {
                        $skill_name = strtolower(character_statistics::get_skill_name($skillId));
                        if ($skill_name != "total") {
                            $skill_level = $cdata[$skill_name . '_level'];

                            ?>
                            <section class="skill">
                                <div class="skill__details item__expand">
                                    <figure class="item-plate">
                                        <img
                                            src="<?php echo SITE_ADDRESS; ?>eoc/img/adventurers-log/skills/<?php
                                            if ($skill_name == "constitution") {
                                                $skill_name = "hitpoints";
                                            } else if ($skill_name == "range") {
                                                $skill_name = "ranged";
                                            }
                                            echo $skill_name;
                                            ?>.png"
                                            title="<?php echo $skill_name; ?>" alt="<?php echo $skill_name; ?>"
                                            class="item-plate__img skill__icon">
                                    </figure>
                                    <h2 class="skill__title"><?php echo ucfirst($skill_name); ?></h2>

                                    <p class="skill__level"><?php echo $skill_level; ?></p>

                                    <div class="skill__bar">
                                        <div class="bar-graph">
                                        <span
                                            class="bar-graph__value bar-graph__value--full"><?php echo $skill_level; ?>
                                            %</span>

                                            <div class="bar-graph__fill bar-graph__fill--animate bar-graph__fill--comp"
                                                 style="width: <?php
                                                 if ($skill_level > 99) {
                                                     $skill_level = 100;
                                                 }
                                                 echo $skill_level;
                                                 ?>%;"></div>
                                        </div>
                                    </div>
                                    <p class="skill__level skill__level--next">
                                        120
                                    </p>

                                </div>
                            </section>

                            <?php
                        }
                    }
                    ?>
                </section>
            <?php } else if (isset($page_activity)) { ?>
                <section class="controls">
                    <h1 class="single-title">Activity</h1>
                    <span class="top-disk"><span>Most </span>Recent</span>
                </section>
                <div class="activities">
                    <?php
                    $result = dbquery("SELECT * FROM activity_logs WHERE username = '$user_name' LIMIT 10;");
                    $i = 0;
                    // First side.
                    while ($row = mysql_fetch_array($result)) {
                        $i++; ?>

                        <?php if ($i % 2) { ?>
                            <article class="activity activity--left">
                                <div class="activity__player-details">
                                    <a href="#">
                                        <h3 class="activity__player-title"><?php echo $user_name; ?></h3>
                                        <time class="activity__time"><?php echo $row['date']; ?></time>
                                    </a>
                                </div>
                                <h3 class="activity__title"><?php echo $row['short_desc']; ?></h3>

                                <p class="activity__meta"><?php echo $row['full_desc']; ?></p>
                            </article>
                            <?php ?>
                            <?php
                        } else { ?>
                            <article class="activity activity--right">
                                <div class="activity__player-details">
                                    <a href="#">
                                        <h3 class="activity__player-title"><?php echo $user_name; ?></h3>
                                        <time class="activity__time"><?php echo $row['date']; ?></time>
                                    </a>
                                </div>
                                <h3 class="activity__title"><?php echo $row['short_desc']; ?></h3>

                                <p class="activity__meta"><?php echo $row['full_desc']; ?></p>
                            </article>
                        <?php }
                    } ?>
                </div>
            <?php } ?>
        </main>
    </div>
</div>
<?php include_once("footer.php"); ?>
</body>

</html>

