<?php
?>
<div class="fancybox-overlay fancybox-overlay-fixed" style="display: block; width: auto; height: auto;">
    <div class="fancybox-wrap fancybox-desktop fancybox-type-inline loginFancyBox fancybox-opened" tabindex="-1"
         style="opacity: 1; overflow: visible; height: auto; width: 490px; position: absolute; top: 50px; left: 395px;">
        <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
            <div class="fancybox-outer">
                <div class="fancybox-inner" style="overflow: auto; width: 460px; height: auto;">
                    <div id="loginPanel" class="loginPanel">

                        <div class="messageBox">
                            <div class="topPart ">
                                <h1><img alt="Log In" src="http://www.runescape.com/img/eoc/titles/login.png"></h1>
                                <a href="http://www.runescape.com/" class="fauxClose">Close</a>
                            </div>
                            <div class="middlePart">

                                <form class="LoginForm" action="https://secure.runescape.com/m=weblogin/login.ws"
                                      method="post" autocomplete="off">
                                    <div class="login">
                                        <div class="floatright clear">
                                            <label for="remember">Remember username</label><input type="checkbox"
                                                                                                  name="rem"
                                                                                                  id="remember"
                                                                                                  checked="">
                                        </div>
                                        <input type="text" id="username" placeholder="Email/Username"
                                               class="Username placeholder NoPlaceholder text"
                                               title="Email/Username"
                                               name="username" value="" required="required" tabindex="1">
                                        <input type="password" id="password" placeholder="Password"
                                               class="Password placeholder NoPlaceholder password" name="password"
                                               title="Password" required="required" tabindex="2" maxlength="20">
                                        <a class="recover forgot" href="https://www.runescape.com/cant_log_in">Can't
                                            login?</a>
                                        <a class="create"
                                           href="https://secure.runescape.com/m=account-creation/create_account">Create
                                            an account</a>

                                        <div class="btnWrap " id="globalLoginFormLogIn">
                                            <div class="btn">
                                                <div class="btnRight">
                                                    <input type="submit" value="Log In" name="submit">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" value="news" name="mod">
                                        <input type="hidden" value="0" name="ssl">
                                        <input type="hidden" value="" name="dest">
                                    </div>
                                    <section class="alternative">
                                        <h4>Log in with:</h4>
                                        <ul class="alternativeLogin">
                                            <li><a class="facebookLogin" id="facebooklogin" title="Facebook" href=""
                                                   target="_parent">Facebook</a></li>
                                            <li><a class="googleLogin" id="googlelogin" title="Google+"
                                                   href="https://secure.runescape.com/m=sn-integration/google/gamelogin.ws?mod=news&amp;ssl=0&amp;dest="
                                                   target="_parent">Google Plus</a></li>
                                        </ul>
                                    </section>
                                </form>
                            </div>
                            <div class="bottomPart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a></div>
    </div>
</div>