<?php
require_once("global.php");

?>

<html>
<head>
    <title><?php echo SITE_NAME; ?> Profile Search</title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
</head>
<body id="advh" class="advhdome en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php
    include_once("header.php"); ?>

    <div class="contents">
        <main class="player-log adv-home">
            <h1 class="title"><span>Adventurer's Log</span></h1>
            <ul class="breadcrumb" itemprop="breadcrumb">
                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/" itemprop="url">Home</a></li>
                <li>Adventurer's Log</li>
            </ul>
            <section class="top-section">
                <div class="top-section__meta">
                    <h2 class="double-title"><span>Track your</span> adventure</h2>

                    <p class="top-section__copy">Every <strong><?php echo SITE_NAME; ?></strong> member has an
                        Adventurer’s Log- a
                        record of their <strong>character</strong> and <strong>achievements</strong>
                        in <?php echo SITE_NAME; ?>. Find
                        out a player’s <strong>progression</strong> through each quest and skill. See what your friends
                        have accomplished recently or just take a peek at their <strong>character</strong>.</p>

                    <p class="top-section__copy"><strong><a class="openLoginPanel" href="#loginPanel">Login
                                Now</a></strong> or Search for a Player.</p>

                    <form class="advlog-search" action="<?php echo SITE_ADDRESS; ?>eoc/profile.php"
                          method="get">
                        <input class="advlog-search__text advlog-search__text--small" type="text"
                               placeholder="Search for a player" name="searchName" maxlength="12" id="searchName"
                               title="Player Name" pattern="^[a-zA-Z0-9]+([ _-&amp;nbsp;]{0,10}[0-9a-zA-Z]+)*$"
                               required="">
                        <input type="hidden" id="stats" value="" name="stats">
                        <input class="advlog-search__submit" type="submit" value="Go">
                    </form>
                </div>
            </section>

        </main>
    </div>
</div>
<?php include_once("footer.php"); ?>

</body>

</html>