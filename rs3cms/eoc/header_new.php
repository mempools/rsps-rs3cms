<script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/functions-86.js"></script>
<link href="<?php printf(SITE_ADDRESS); ?>css/eoc/components-14.css" rel="stylesheet"/>
<!--[if lt IE 9]>
<link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
<![endif]-->
<!--[if gt IE 8]><!--><!-- x -->
<link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
<!--<![endif]-->
<link href="<?php printf(SITE_ADDRESS); ?>css/eoc/pages-14.css" rel="stylesheet"/>
<header class="header" role="banner">
    <div class="header__container">
        <div class="header-top">
            <div class="header-top__left">
                <span class="header-top__name"><?php if ($agent->logged_in) {
                        echo $agent->username;
                    } ?></span>
            </div>
            <ul class="header-top__right">
                <li class="header-top__right-option"><strong id="playerCount0"><?php
                        $users_online = dbevaluate("SELECT COUNT(id) FROM characters WHERE online > '0';");
                        echo $users_online; ?></strong>
                    Online
                </li>
                <li class="header-top__right-option">

                <li class="header-top__right-option"><a class="header-top__right-link"
                                                        href="<?php echo SITE_ADDRESS; ?>account_settings">Account</a>
                </li>
                <li class="header-top__right-option">
                    <?php if ($agent->logged_in) { ?>
                        <a class="header-top__right-link header-top__right-link--signin" id="inline"
                           href="<?php echo SITE_ADDRESS; ?>logout.php">Sign out</a>
                        <?php
                    } else { ?>
                        <a class="header-top__right-link header-top__right-link--signin" id="inline"
                           href="#"
                           onclick="document.getElementById('loginBox').style.display = 'block';">Sign in / Join</a>
                        <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="header__bg">
        <div class="header__container">
            <div class="header-second">
                <div class="header-table">
                    <a class="header-second__logo" href="<?php echo SITE_ADDRESS; ?>eoc/">
                        <img class="header-second__logo-image"
                             src="<?php echo SITE_ADDRESS; ?>img/global/header_logo.png"/>
                    </a>
                    <nav class="main-nav" id="main-nav">
                        <ul class="main-nav__list">
                            <li class="main-nav__item main-nav__item--group main-nav__item--game">

                            <li class="main-nav__item main-nav__item--news">
                                <a class="main-nav__heading" href="<?php echo SITE_ADDRESS; ?>eoc/news.php" style="
font-family: Trajan Pro, 'trajan-pro-3', serif;">News</a>
                            </li>
                            <li class="main-nav__item main-nav__item--group main-nav__item--community">
                                <a href="#"
                                   class="main-nav__heading" style="
font-family: Trajan Pro, 'trajan-pro-3', serif;">Community<span class="main-nav__expander"></span></a>
                                <ul class="main-nav__group">
                                    <li class="main-nav__group-item"><a class="main-nav__group-item-link"
                                                                        href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php">HiScores</a>
                                    </li>
                                    <li class="main-nav__group-item"><a class="main-nav__group-item-link"
                                                                        href="<?php echo SITE_ADDRESS; ?>eoc/search.php">Adventurer's
                                            Log</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="main-nav__item main-nav__item--forums">
                                <a class="main-nav__heading"
                                   href="<?php echo H_FORUMS; ?>" style="
font-family: Trajan Pro, 'trajan-pro-3', serif;">Forums</a>
                            </li>
                            <li class="main-nav__item main-nav__item--forums">
                                <a class="main-nav__heading"
                                   href="<?php echo H_VOTE; ?>" style="
font-family: Trajan Pro, 'trajan-pro-3', serif;">Vote</a>
                            </li>
                            <li class="main-nav__item main-nav__item--forums">
                                <a class="main-nav__heading"
                                   href="<?php echo H_DONATE; ?>" style="
font-family: Trajan Pro, 'trajan-pro-3', serif;">Donate</a>
                            </li>
                            <li class="main-nav__item main-nav__item--account">
                                <a class="main-nav__heading"
                                   href="<?php echo SITE_ADDRESS; ?>account_settings.php" style="">Account</a>
                            </li>
                            <?php if ($agent->has_permission("web_admin") || $agent->has_permission("web_admin")) { ?>
                                <li class="main-nav__item main-nav__item--account">
                                    <a class="main-nav__heading"
                                       href="<?php echo SITE_ADDRESS; ?>manage/" style="">Mod CP</a>
                                </li>
                            <?php } ?>
                            <li class="main-nav__item main-nav__item--account">
                                <?php if ($agent->logged_in) { ?>
                                    <a class="main-nav__heading"
                                       href="<?php echo SITE_ADDRESS; ?>logout.php">Sign out</a>
                                    <?php
                                } else { ?>
                                    <a class="main-nav__heading"
                                       href="#loginPanel">Sign
                                        in / Join</a>
                                    <?php
                                }
                                ?>
                            </li>
                            <li class="main-nav__item main-nav__item--play">
                                <a id="play" class="main-nav__heading"
                                   href="<?php echo H_PLAY; ?>"
                                   style="font-family: Trajan Pro, 'trajan-pro-3', serif;">Play
                                    Now</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="header__login">
    <div id="loginPanel" class="loginPanel">

        <div class="messageBox">
            <div class="topPart ">
                <h1><img alt="Log In" src="../img/titles/login.png"></h1>
                <a href="#" class="fauxClose">Close</a>
            </div>
            <div class="middlePart">

                <form class="LoginForm" action="<?php echo SITE_ADDRESS; ?>login.php" method="post"
                      autocomplete="off">
                    <div class="login">
                        <div class="floatright clear">
                            <label for="remember">Remember username</label><input type="checkbox" name="rem"
                                                                                  id="remember" checked/>
                        </div>
                        <input type="text" id="username" placeholder="Email/Username"
                               class="Username placeholder NoPlaceholder text" title="Email/Username"
                               name="username" value="" required="required" tabindex="1"/>
                        <input type="password" id="password" placeholder="Password"
                               class="Password placeholder NoPlaceholder password" name="password" title="Password"
                               required="required" tabindex="2" maxlength="20"/>
                        <a class="recover forgot" href="#"></a>
                        <a class="create" href="<?php echo SITE_ADDRESS; ?>register.php">Create
                            an account</a>

                        <div class="btnWrap " id="globalLoginFormLogIn">
                            <div class="btn">
                                <div class="btnRight">
                                    <input type="submit" value="Log In" name="submit"/>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="bottomPart"></div>
        </div>
    </div>
</div>

<div id="loginBox" class="fancybox-overlay fancybox-overlay-fixed" style="display: none; width: auto; height: auto;">
    <div class="fancybox-wrap fancybox-desktop fancybox-type-inline loginFancyBox fancybox-opened" tabindex="-1"
         style="opacity: 1; overflow: visible; height: auto; width: 490px; position: absolute; top: 50px; left: 395px;">
        <div class="fancybox-skin" style="padding: 15px; width: auto; height: auto;">
            <div class="fancybox-outer">
                <div class="fancybox-inner" style="overflow: auto; width: 460px; height: auto;">
                    <div id="loginPanel" class="loginPanel">

                        <div class="messageBox">
                            <div class="topPart ">
                                <h1><img alt="Log In" src="../img/titles/login.png"></h1>
                                <a href="<?php echo SITE_ADDRESS; ?>eoc/" class="fauxClose">Close</a>
                            </div>
                            <div class="middlePart">

                                <form class="LoginForm" action="login.php"
                                      method="post" autocomplete="off">
                                    <div class="login">
                                        <div class="floatright clear">
                                            <label for="remember">Remember username</label><input type="checkbox"
                                                                                                  name="rem"
                                                                                                  id="remember"
                                                                                                  checked="">
                                        </div>
                                        <input type="text" id="username" placeholder="Email/Username"
                                               class="Username placeholder NoPlaceholder text"
                                               title="Email/Username"
                                               name="username" value="" required="required" tabindex="1">
                                        <input type="password" id="password" placeholder="Password"
                                               class="Password placeholder NoPlaceholder password" name="password"
                                               title="Password" required="required" tabindex="2" maxlength="20">
                                        <a class="recover forgot" href="forgot_password.php">Can't
                                            login?</a>
                                        <a class="create"
                                           href="<?php echo SITE_ADDRESS; ?>create_account.php">Create
                                            an account</a>

                                        <div class="btnWrap " id="globalLoginFormLogIn">
                                            <div class="btn">
                                                <div class="btnRight">
                                                    <input type="submit" value="Log In" name="submit">
                                                </div>
                                            </div>
                                        </div>
                                        <!--                                        <input type="hidden" value="news" name="mod">-->
                                        <!--                                        <input type="hidden" value="0" name="ssl">-->
                                        <!--                                        <input type="hidden" value="" name="dest">-->
                                    </div>
                                </form>
                            </div>
                            <div class="bottomPart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <a title="Close" class="fancybox-item fancybox-close" href="#"
               onclick="document.getElementById('loginBox').style.display = 'none';"></a></div>
    </div>
</div>