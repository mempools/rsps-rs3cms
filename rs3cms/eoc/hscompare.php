<?php
require_once("global.php");
require_once("../includes/class_character_statistics.php");

// determines if information for user1 & user2 was successfully retrieved.
$user1_good = false;
$user2_good = false;

/* data for user1 */
if (isset($_GET['user1'])) {
    $user1 = filter_for_input($_GET['user1']);

    $user1_qry2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.username = '$user1' LIMIT 1;");
    if (mysql_num_rows($user1_qry2) > 0) {
        $user1_sdata = mysql_fetch_array($user1_qry2);
        $user1_stats = new character_statistics($user1_sdata);
        $user1_name = $user1;
        $user1_good = true;
    }

}

/* data for user2 */
if (isset($_GET['user2'])) {
    $user2 = filter_for_input($_GET['user2']);

    $user2_qry2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.username = '$user2' LIMIT 1;");
    if (mysql_num_rows($user2_qry2) > 0) {
        $user2_sdata = mysql_fetch_array($user2_qry2);
        $user2_stats = new character_statistics($user2_sdata);
        $user2_name = $user2;
        $user2_good = true;
    }
}

function getRank($username, $order)
{
    $count = 1;
    $res = dbquery("SELECT * FROM characters_statistics ORDER BY $order");
    if (mysql_num_rows($res) > 0) {
        while ($row = mysql_fetch_array($res)) {
            $userid = strtolower($row['username']);
            //  echo $count;
            if ($userid == strtolower($username)) {
                return $count;
            }
            $count++;
        }
    }
    return -1;
}

/* comparison can only process if user1 is available */
if (!$user1_good) {
//    exit(header("Location: " . SITE_ADDRESS . "hiscores_compare"));
}
?>
<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]>
<html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]>
<html class="no-js lt-ie10 lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]>
<html class="no-js lt-ie10" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><!-- x -->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Compare Players &amp; - Rankings - <?php echo SITE_NAME; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/><![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
    <script type='text/javascript'>
        var _vis_opt_cookieDays = 1,
            _vwo_code = (function () {
                var account_id = 54596,
                    settings_tolerance = 2000,
                    library_tolerance = 2500,
                    use_existing_jquery = false,
                    f = false, d = document;
                return {
                    use_existing_jquery: function () {
                        return use_existing_jquery;
                    }, library_tolerance: function () {
                        return library_tolerance;
                    }, finish: function () {
                        if (!f) {
                            f = true;
                            var a = d.getElementById('_vis_opt_path_hides');
                            if (a)a.parentNode.removeChild(a);
                        }
                    }, finished: function () {
                        return f;
                    }, load: function (a) {
                        var b = d.createElement('script');
                        b.src = a;
                        b.type = 'text/javascript';
                        b.innerText;
                        b.onerror = function () {
                            _vwo_code.finish();
                        };
                        d.getElementsByTagName('head')[0].appendChild(b);
                    }, init: function () {
                        settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                        this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                        var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
                        a.setAttribute('id', '_vis_opt_path_hides');
                        a.setAttribute('type', 'text/css');
                        if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                        h.appendChild(a);
                        return settings_timer;
                    }
                };
            }());
        _vwo_settings_timer = _vwo_code.init();
    </script>
</head>
<body id="hiscore" class="hiscore en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php
    $_GET['banner'] = "a";
    include_once("header.php") ?>

    <div class="banner">
        <div class="inner">
            <img src="../img/banners/generic.jpg"/>
        </div>
    </div>
    <div class="contents">
        <div class="main">

            <h1 class="title "><span>HiScores</span></h1>
            <ul class="breadcrumb" itemprop="breadcrumb">
                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/">Home</a></li>
                <li><a href="hiscores.php">HiScores</a></li>
                <li>Skills</li>
            </ul>

            <nav class="secondary">
                <ul>
                    <li><a href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><span>Skills</span></a></li>
                    <li class=active>
                        <a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php"><span>Compare</span></a>
                    </li>
                </ul>
            </nav>
            <div class="secondaryNavDivide"></div>
            <div class="content" style="width:100%;">
                <div class="categorySection">
                    <h3>Compare Players - Skills</h3>

                    <p>Compare your friends with each other or with yourself. You can also select a category to filter
                        the results.</p>
                </div>
                <div class="compareNameOuter clear">
                    <section class="compareNameLeft">
                        <div class="playerAvatar avatarLeft">
                            <div class="avatarFrame">
                                <img src='../img/img/default_chat.png' alt=''/>
                            </div>
                        </div>
                        <h4><?php if ($user1_good) {
                                echo $user1_name;
                            } else {
                                echo "--";
                            } ?></h4>
                    </section>
                    <section class="compareNameRight">
                        <div class="playerAvatar avatarRight">
                            <div class="avatarFrame">
                                <img src='../img/img/default_chat.png' alt=''/>
                            </div>
                        </div>
                        <h4><?php if ($user2_good) {
                                echo $user2_name;
                            } else {
                                echo "--";
                            } ?></h4>
                    </section>
                    <div class="compareVs"></div>
                </div>
                <div class="changeCharacterOuter clear">
                    <div class="changeCharacterForm">
                        <h4>Search For Players</h4>

                        <p>Type your friend’s player name in the input boxes below and hit enter or the button to
                            compare stats.</p>

                        <form action="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php" class="index">
                            <input type="hidden" name="category_type" value="-1"/>
                            <input type="text" placeholder="Player 1" name="user1" required="required"
                                   class="leftInput placeholder text" title="Player 1" value=""
                                   maxlength="12"/>
                            <input type="text" placeholder="Player 2" name="user2" class="rightInput placeholder text"
                                   title="Player 2" value="" maxlength="12"/>

                            <div class="btnWrap ">
                                <div class="btn">
                                    <div class="btnRight">
                                        <input type="submit" value="Compare" name=""/>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="changeCharacterTab">
                        <a href="javascript:compareFormToggle();" class="changeCharacterLink changeCharacterLeft">Change
                            Character</a>
                        <a href="javascript:compareFormToggle();" class="changeCharacterLink changeCharacterRight">Change
                            Character</a>
                    </div>
                </div>
                <div class="compareSkillsOuter clear">
                    <section class="playerStats">

                        <div class="tiledBlueFrame">
                            <div class="tl corner"></div>
                            <div class="tr corner"></div>
                            <div class="bl corner"></div>
                            <div class="br corner"></div>
                            <div class="tableWrap">
                                <table class="headerBgLeft">
                                    <thead>
                                    <tr>
                                        <th class="alignleft">Rank</th>
                                        <th class="alignleft">Total XP</th>
                                        <th class="alignleft">Level</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($user1_good) {
                                        $qry = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.username = '$user1_name' LIMIT 1;");
                                        if (mysql_num_rows($qry) > 0) {
                                            $qry_data = mysql_fetch_assoc($qry);
                                            ?>

                                            <tr>
                                                <td class="alignleft"><a
                                                        href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo getRank($user1_name, "total_level DESC"); ?></a>
                                                </td>
                                                <td class="alignleft"><a
                                                        href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo $qry_data['total_exp']; ?></a>
                                                </td>
                                                <td class="playerWinLeft alignleft">
                                                    <div class="relative"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo $qry_data['total_level']; ?></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php


                                            for ($skill = 0; $skill <= 24; $skill++) {
                                                $skill_name = strtolower(character_statistics::get_skill_name($skill));

                                                ?>
                                                <tr>
                                                    <td class="alignleft"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo getRank($user1_name, $skill_name . '_level DESC'); ?></a>
                                                    </td>
                                                    <td class="alignleft"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php printf(number_format($qry_data[$skill_name . '_exp'])); ?></a>
                                                    </td>
                                                    <td class="playerWinLeft alignleft">
                                                        <div class="relative"><a
                                                                href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php printf(number_format($qry_data[$skill_name . '_level'])); ?></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        for ($skill = 0; $skill <= 25; $skill++) {
                                            $skill_name = strtolower(character_statistics::get_skill_name($skill));

                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section class="playerStats">

                        <div class="tiledBlueFrame">
                            <div class="tl corner"></div>
                            <div class="tr corner"></div>
                            <div class="bl corner"></div>
                            <div class="br corner"></div>
                            <div class="tableWrap">
                                <table class="headerBgRight">
                                    <thead>
                                    <tr>
                                        <th>Level</th>
                                        <th>Total XP</th>
                                        <th>Rank</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($user2_good) {
                                        $qry = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.username = '$user2_name' LIMIT 1;");
                                        if (mysql_num_rows($qry) > 0) {
                                            $qry_data = mysql_fetch_assoc($qry);
                                            ?>

                                            <tr>
                                                <td class="alignleft"><a
                                                        href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo $qry_data['total_level']; ?></a>
                                                </td>
                                                <td class="alignleft"><a
                                                        href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo $qry_data['total_exp']; ?></a>
                                                </td>
                                                <td class="playerWinLeft alignleft">
                                                    <div class="relative"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><?php echo getRank($user2_name, "total_level DESC"); ?></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php


                                            for ($skill = 0; $skill <= 24; $skill++) {
                                                $skill_name = strtolower(character_statistics::get_skill_name($skill));

                                                ?>
                                                <tr>
                                                    <td class="alignleft"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php?skill=0">    <?php printf(number_format($qry_data[$skill_name . '_level'])); ?></a>
                                                    </td>
                                                    <td class="alignleft"><a
                                                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php?skill=0"><?php printf(number_format($qry_data[$skill_name . '_exp'])); ?></a>
                                                    </td>
                                                    <td class="playerWinLeft alignleft">
                                                        <div class="relative"><a
                                                                href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php?skill=0"><?php echo getRank($user2_name, "total_level DESC"); ?></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    } else {
                                        for ($skill = 0; $skill <= 25; $skill++) {
                                            $skill_name = strtolower(character_statistics::get_skill_name($skill));

                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                                <td>
                                                    <a href="#">--</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <section class="skillIcons">
                        <div class="hiscoreIcon iconOverall" title="Overall"></div>
                        <div class="hiscoreIcon iconAttack" title="Attack"></div>
                        <div class="hiscoreIcon iconDefence" title="Defence"></div>
                        <div class="hiscoreIcon iconStrength" title="Strength"></div>
                        <div class="hiscoreIcon iconHitpoints" title="Constitution"></div>
                        <div class="hiscoreIcon iconRanged" title="Ranged"></div>
                        <div class="hiscoreIcon iconPrayer" title="Prayer"></div>
                        <div class="hiscoreIcon iconMagic" title="Magic"></div>
                        <div class="hiscoreIcon iconCooking" title="Cooking"></div>
                        <div class="hiscoreIcon iconWoodcutting" title="Woodcutting"></div>
                        <div class="hiscoreIcon iconFletching" title="Fletching"></div>
                        <div class="hiscoreIcon iconFishing" title="Fishing"></div>
                        <div class="hiscoreIcon iconFiremaking" title="Firemaking"></div>
                        <div class="hiscoreIcon iconCrafting" title="Crafting"></div>
                        <div class="hiscoreIcon iconSmithing" title="Smithing"></div>
                        <div class="hiscoreIcon iconMining" title="Mining"></div>
                        <div class="hiscoreIcon iconHerblore" title="Herblore"></div>
                        <div class="hiscoreIcon iconAgility" title="Agility"></div>
                        <div class="hiscoreIcon iconThieving" title="Thieving"></div>
                        <div class="hiscoreIcon iconSlayer" title="Slayer"></div>
                        <div class="hiscoreIcon iconFarming" title="Farming"></div>
                        <div class="hiscoreIcon iconRunecraft" title="Runecrafting"></div>
                        <div class="hiscoreIcon iconHunter" title="Hunter"></div>
                        <div class="hiscoreIcon iconConstruction" title="Construction"></div>
                        <div class="hiscoreIcon iconSummoning" title="Summoning"></div>
                        <div class="hiscoreIcon iconDungeoneering" title="Dungeoneering"></div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once('footer.php'); ?>


<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="js/jquery_1_11_0-0.js"><\/script>')</script>
<script type="text/javascript" src="js/plugins-7.js"></script>
<script type='text/javascript' src='js/jagex_global-1.js'></script>
<!--<script type="text/javascript" src="js/functions-86.js"></script>-->
<script type="text/javascript" src="js/gtm-0.js"></script>
<script type="text/javascript">
    function compareFormToggle() {
        if ($('.changeCharacterLink').length && $('.changeCharacterForm').length) {
            $('a.changeCharacterLink').on('click', function (e) {
                e.preventDefault();
                $('a.changeCharacterLink').toggleClass('changeCharacterOpen');
                $('.changeCharacterForm').slideToggle();
            });
        }
    }
</script>

</body>
</html>

