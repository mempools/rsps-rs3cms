<?php
?>
<section class="plog-header">
    <figure class="plog-header__avatar"><img class="plog-header__image"
                                             src="<?php echo SITE_ADDRESS; ?>eoc/img/default_chat.png"
                                             alt="Avatar" title=""></figure>
    <h3 class="plog-header__title" id="js-title"><?php if ($user_found) {
            echo get_rank_name($cdata['playerRights']);
        } ?></h3>


    <h1 class="plog-header__name"><?php if ($user_found) {
            echo $user_name;
        } ?></h1>

    <div class="plog-stats">
        <?php if ($user_found) { ?>
            <div class="plog-stats__item plog-stats__item--level"><?php echo $cdata['total_level']; ?> <span
                    class="plog-stats__inner"></span></div>
            <div class="plog-stats__item plog-stats__item--xp"><span class="plog-stats__subtitle">XP</span>
                <?php echo $cdata['total_exp']; ?>
            </div>
        <?php } ?>
        <form method="GET"
              action="<?php echo SITE_ADDRESS; ?>eoc/profile.php"
              class="plog-stats__search">
            <input name="username" class="plog-stats__input" placeholder="Search for a player"
                   title="Player Name" maxlength="12&quot;"
                   pattern="^[a-zA-Z0-9]+([ _-&amp;nbsp;]{0,10}[0-9a-zA-Z]+)*$" required="" type="text">
            <input type="hidden" id="stats" value="" name="stats">
            <input value="Search" class="plog-stats__submit" type="submit">
        </form>
    </div>
</section>

<div class="tabbednav">
    <nav class="tabbednav__nav">
        <ul class="tabbednav__list">

            <li class="tabbednav__tab tabbednav__tab<?php if (isset($_GET['stats'])) {
                echo "--active";
            } ?>"><a class="tabbednav__link"
                     href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name ?>&stats"><span
                        class="tabbednav__link-text">Profile</span></a></li>

            <li class="tabbednav__tab tabbednav__tab<?php if (isset($_GET['skills'])) {
                echo "--active";
            } ?>"><a class="tabbednav__link"
                     href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name ?>&skills"><span
                        class="tabbednav__link-text">Skills</span></a></li>

            <li class="tabbednav__tab tabbednav__tab<?php if (isset($_GET['activity'])) {
                echo "--active";
            } ?>"><a class="tabbednav__link"
                     href="<?php echo SITE_ADDRESS; ?>eoc/profile.php?searchName=<?php echo $user_name ?>&activity"><span
                        class="tabbednav__link-text">Activity</span></a></li>

        </ul>
    </nav>
</div>