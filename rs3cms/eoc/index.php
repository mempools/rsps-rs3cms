<?php
require_once("global.php");

$slider = true;

$show = 5;
$page = 1;

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}

$start_from = ($page - 1) * $show;

if (isset($_GET['cat'])) {
    $cat = $_GET['cat'];
    $total_results = dbevaluate("SELECT COUNT(*) FROM web_news WHERE category_id='$cat';");
    $news_qry = dbquery("SELECT * FROM web_news WHERE category_id='$cat' ORDER BY id DESC LIMIT $start_from, $show;");
} else {
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_news;");
    $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT $start_from, $show;");
}

$total_pages = ceil($total_results / $show);

if ($page < 1 || ($total_pages > 0 && $page > $total_pages)) {
    $url = SITE_ADDRESS . "news";
    exit(header("Location: $url"));
}

?>

<html>
<head>
    <title><?php echo SITE_NAME; ?></title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>
<body id="home" class="home en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php include_once("header.php"); ?>
    <div class="banner">
        <div id="carousel" class="inner">
            <ul class="slides">
                <li class="flex-active-slide"
                    style="width: 100%; float: left; margin-right: -100%; position: relative; display: block;">
                    <a href="<?php echo $C_SLIDER['C_SLIDER1'][0]; ?>"><img
                            src="<?php echo $C_SLIDER['C_SLIDER1'][1]; ?>" alt=""></a>
                </li>
                <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;">
                    <a href="<?php echo $C_SLIDER['C_SLIDER2'][0]; ?>"><img
                            src="<?php echo $C_SLIDER['C_SLIDER2'][1]; ?>" alt=""></a>
                </li>
                <li style="width: 100%; float: left; margin-right: -100%; position: relative; display: none;" class="">
                    <a href="<?php echo $C_SLIDER['C_SLIDER3'][0]; ?>"><img
                            src="<?php echo $C_SLIDER['C_SLIDER3'][1]; ?>" alt=""></a>
                </li>
            </ul>
            <!--            <ol class="flex-control-nav flex-control-paging">-->
            <!--                <li><a class="flex-active">1</a></li>-->
            <!--                <li><a class="">2</a></li>-->
            <!--                <li><a class="">3</a></li>-->
            <!--            </ol>-->
            <ul class="flex-direction-nav">
                <li><a class="flex-prev" href="#">Previous</a></li>
                <li><a class="flex-next" href="#">Next</a></li>
            </ul>
        </div>
    </div>

    <div class="contents">
        <main class="main" role="main">
            <div class="promoBoxes" id="promos">
                <div class="inner">
                    <ul class="selection">
                        <li class="oldschool custom">
                            <h3><a href="<?php echo BUTTON_1; ?>">Old School</a></h3>
                        </li>

                        <li class="member">
                            <h3><a href="<?php echo BUTTON_2; ?>">Become a
                                    Member</a></h3>
                        </li>
                        <li class="runelabs">
                            <h3><a href="<?php echo BUTTON_3; ?>">RuneLabs</a>
                            </h3>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clear">
                <div class="tabbedElement" id="tabs">
                    <div class="inner">
                        <ul class="tabNav" id="tabNav">
                            <li class="active"><a href="<?php echo SITE_ADDRESS; ?>eoc/news.php"><span>News</span></a>
                            </li>
                        </ul>
                        <div class="tabbedContents">
                            <div id="news" class="tabbedContent">

                                <div class="categories">
                                    <h6>Categories</h6>
                                    <ul class="catList" id="catListNews">
                                        <li><a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="0" class="active">All</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=1&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="1">Game Updates</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=2&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="2">Website</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=3&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="3">Support</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=4&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="4">Technical</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=5&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="5">Community</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=6&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="6">Behind The Scenes</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=9&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="9">Shop</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=12&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="12">Future Updates</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=13&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="13">Solomon's Store</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=14&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="14">Treasure Hunter</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=15&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="15">Your Feedback</a></li>
                                        <li>
                                            <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=16&amp;page=<?php echo $page; ?>"
                                               data-type="news"
                                               data-id="16">Events</a></li>
                                    </ul>
                                </div>
                                <div class="index" id="newsSection">
                                    <?php
                                    if (mysql_num_rows($news_qry) > 0) {
                                        while ($news = mysql_fetch_assoc($news_qry)) {
                                            $news_id = $news['id'];
                                            $news_date = $news['date'];
                                            $news_author = agent::username_from_id($news['author_id']);
                                            $news_title = $news['title'];
                                            $news_desc = $news['desc'];
                                            $news_story = $news['story'];
                                            $category_id = $news['category_id'];
                                            $seo_url = $news['seo_url'];

                                            $image_url = $news['image_url'];
                                            $youtube_url = $news['youtube_url'];

                                            $pic_url = "http://cdn.runescape.com/assets/img/external/news/2015/04/news_thumbnail_BnL.jpg";

                                            if (!empty($image_url)) {
                                                $pic_url = $image_url;
                                            } else {
                                                if (!empty($youtube_url)) {
                                                    $pic_url = "http://img.youtube.com/vi/" . $youtube_url . "/1.jpg";
                                                }
                                            }
                                            $category_name = news_category($category_id);
                                            ?>


                                            <article <?php if (!empty($youtube_url)) {
                                                echo 'class="video"';
                                            } ?>>
                                                <figure>
                                                    <a href="viewnews.php?news=<?php echo $news_id; ?>"><img
                                                            src="<?php echo $pic_url; ?>" width="169" height="88""></a>
                                                </figure>
                                                <div class="copy">
                                                    <header>
                                                        <h4>
                                                            <a href="viewnews.php?news=<?php echo $news_id; ?>"><?php echo $news_title; ?></a>
                                                            <h5>
                                                                <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=<?php echo $category_id; ?>&amp;page=1"><?php echo $category_name; ?></a>
                                                            </h5>
                                                            <span><?php printf(compare_dates($news_date, time())); ?></span>
                                                    </header>
                                                    <p><?php echo $news_desc; ?><br><a class="readMore"
                                                                                       href="viewnews.php?news=<?php echo $news_id; ?>">read
                                                            more</a></p>
                                                </div>
                                            </article>

                                        <?php }
                                    } ?>
                                </div>
                                <div class="btnWrap more">
                                    <div class="btn">
                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php"><span>More News</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include_once("social_feeds.php"); ?>
            </div>
        </main>
    </div>
</div>
<?php include_once("footer.php"); ?>

</body>

</html>