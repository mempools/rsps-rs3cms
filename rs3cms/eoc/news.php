<?php
require_once("global.php");

$show = 5;
$page = 1;

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}

$start_from = ($page - 1) * $show;

if (isset($_GET['cat'])) {
    $cat = $_GET['cat'];
    $total_results = dbevaluate("SELECT COUNT(*) FROM web_news WHERE category_id='$cat';");
    $news_qry = dbquery("SELECT * FROM web_news WHERE category_id='$cat' ORDER BY id DESC LIMIT $start_from, $show;");
} else {
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_news;");
    $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT $start_from, $show;");
}

$total_pages = ceil($total_results / $show);

if ($page < 1 || ($total_pages > 0 && $page > $total_pages)) {
    $url = SITE_ADDRESS . "news";
    exit(header("Location: $url"));
}

?>

<html>
<head>
    <title><?php echo SITE_NAME; ?> News - Latest Game News &amp; Updates</title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
</head>
<body id="news" class="news en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php
    $_GET['banner'] = "a";
    include_once("header.php") ?>
    <div class="contents">
        <div class="main" role="main">
            <h1 class="title"><span>News</span></h1>
            <ul class="breadcrumb" itemprop="breadcrumb">
                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/" itemprop="url">Home</a></li>
                <li>News</li>
            </ul>
            <div class="NametabbedElement" id="tabs">
                <ul class="tabNav" id="tabNav">
                    <li class="active"><a href="#newsContent"><span>Latest</span></a></li>
                </ul>
                <div class="tabbedContents">
                    <div id="newsContent" class="tabbedContent">

                        <div class="categories">
                            <h6>Categories</h6>
                            <ul class="catList" id="catListNews">
                                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="0" class="active">All</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=1&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="1">Game Updates</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=2&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="2">Website</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=3&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="3">Support</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=4&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="4">Technical</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=5&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="5">Community</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=6&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="6">Behind The Scenes</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=9&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="9">Shop</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=12&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="12">Future Updates</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=13&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="13">Solomon's Store</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=14&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="14">Treasure Hunter</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=15&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="15">Your Feedback</a></li>
                                <li>
                                    <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=16&amp;page=<?php echo $page; ?>"
                                       data-type="news"
                                       data-id="16">Events</a></li>
                            </ul>
                        </div>
                        <div class="index" id="newsSection">
                            <?php
                            if (mysql_num_rows($news_qry) > 0) {
                                while ($news = mysql_fetch_assoc($news_qry)) {
                                    $news_id = $news['id'];
                                    $news_date = $news['date'];
                                    $news_author = agent::username_from_id($news['author_id']);
                                    $news_title = $news['title'];
                                    $news_desc = $news['desc'];
                                    $news_story = $news['story'];
                                    $category_id = $news['category_id'];
                                    $seo_url = $news['seo_url'];

                                    $image_url = $news['image_url'];
                                    $youtube_url = $news['youtube_url'];

                                    $pic_url = "http://cdn.runescape.com/assets/img/external/news/2015/04/news_thumbnail_BnL.jpg";

                                    if (!empty($image_url)) {
                                        $pic_url = $image_url;
                                    } else {
                                        if (!empty($youtube_url)) {
                                            $pic_url = "http://img.youtube.com/vi/" . $youtube_url . "/1.jpg";
                                        }
                                    }
                                    $category_name = news_category($category_id);
                                    ?>


                                    <article <?php if (!empty($youtube_url)) {
                                        echo 'class="video"';
                                    } ?>>
                                        <figure>
                                            <a href="viewnews.php?news=<?php echo $news_id; ?>"><img
                                                    src="<?php echo $pic_url; ?>" width="169" height="88""></a></figure>
                                        <div class="copy">
                                            <header>
                                                <h4>
                                                    <a href="viewnews.php?news=<?php echo $news_id; ?>"><?php echo $news_title; ?></a>
                                                    <h5>
                                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?cat=<?php echo $category_id; ?>&amp;page=1"><?php echo $category_name; ?></a>
                                                    </h5>
                                                    <span><?php printf(compare_dates($news_date, time())); ?></span>
                                            </header>
                                            <p><?php echo $news_desc; ?><br><a class="readMore"
                                                                               href="viewnews.php?news=<?php echo $news_id; ?>">read
                                                    more</a></p>
                                        </div>
                                    </article>

                                <?php }
                            } ?>
                        </div>
                        <div class="btnWrap showMore" id="moreNews">
                            <div class="btn">
                                <a href="<?php echo SITE_ADDRESS; ?>eoc/news.php?page=2"><span>Show More</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once("social_feeds.php"); ?>
        </div>
    </div>
</div>
<?php include_once("footer.php"); ?>

</body>

</html>