<?php
require_once("global.php");

if (isset($_GET['news'])) {
    $article = $_GET['news'];

    if (!strpos($article, "-")) {
//        exit(header("Location: " . SITE_ADDRESS . "news"));
    }

    $bits = explode("-", $article);
    $id = mysql_real_escape_string($bits[0]);

    $q = dbquery("SELECT * FROM web_news WHERE id = '$id' LIMIT 1;");
    if (mysql_num_rows($q) > 0) {
        $news = mysql_fetch_array($q);

        $news_date = $news['date'];
        $news_author = agent::username_from_id($news['author_id']);
        $news_title = $news['title'];
        $news_desc = $news['desc'];
        $news_story = $news['story'];
        $category_id = $news['category_id'];
        $seo_url = $news['seo_url'];
        $image_url = $news['image_url'];
        $youtube_url = $news['youtube_url'];
        $avail = true;
    } else {
        exit(header("Location: " . SITE_ADDRESS . "news.php"));
    }
} else {
    exit(header("Location: " . SITE_ADDRESS . "news.php"));
}

?>

<html>
<head>
    <title><?php echo SITE_NAME; ?> News - Latest Game News &amp; Updates</title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>
</head>
<body id="news" class="news en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php
    $_GET['banner'] = "a";
    include_once("header.php") ?>
    <div class="contents">
        <div class="main" role="main">
            <h1 class="title"><span>News</span></h1>
            <ul class="breadcrumb" itemprop="breadcrumb">
                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/" itemprop="url">Home</a></li>
                <li><a href="news.php" itemprop="url">News</a></li>
                <li><?php echo $news_title; ?></li>
            </ul>
            <article class="content">

                <?php if (!empty($youtube_url) || !empty($image_url)) { ?>
                    <div class="feature">
                        <div class="inner">
                            <?php if (isset($youtube_url)) { ?>
                                <iframe width="630" height="340"
                                        src="https://www.youtube.com/embed/<?php echo $youtube_url; ?>?rel=0&amp;wmode=opaque"
                                        frameborder="0" allowfullscreen=""></iframe>
                            <?php } else if (isset($image_url)) { ?>
                                <figure><img src="<?php echo $image_url; ?>" width="630" height="340"></figure>
                                <?php
                            } ?>
                        </div>
                    </div>

                    <?php
                } ?>
                <div class="articleContent">
                    <div class="articleMeta">
                        <time
                            datetime="<?php echo $news_date; ?>"><?php printf(compare_dates($news_date, time())); ?></time>
                        <span><a href="news.php?cat=2&amp;page=1">Website</a></span>
                        <span></span>
                    </div>
                    <h2><?php echo $news_title; ?></h2>

                    <div class="articleContentText">
                        <?php echo $news_story; ?>
                    </div>
                </div>
            </article>
            <?php include_once("social_feeds.php"); ?>
        </div>
    </div>
</div>
<?php include_once("footer.php"); ?>

</body>

</html>