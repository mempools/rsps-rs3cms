<aside class="sidebar" role="complementary">
    <section class="accountsCreated">
        <h3 id="numberAccountsd"><?php echo $users_online; ?></h3>
        <h4>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Players Online</h4>
    </section>
    <section>
        <h3 class="youtube"><a href="<?php echo YOUTUBE_URL; ?>" target="_blank" class="ext">YouTube</a>
        </h3>

        <div id="youtube" style="display: block;">
            <div class="index">
                <article class="video">
                    <figure class="greyFrame youtube">
                        <div class="tl corner"></div>
                        <div class="tr corner"></div>
                        <div class="bl corner"></div>
                        <div class="br corner"></div>
                        <div class="lightbox">
                            <iframe id="YouTubeEmbed" width="270" height="145"
                                    src="<?php printf(FEATURED_VIDEO); ?>" frameborder="0"
                                    allowfullscreen></iframe>
                        </div>
                    </figure>
                    <h4><a href="<?php echo FEATURED_VIDEO; ?>"
                           class="lightbox" style="font-size: 10px;"><?php echo YOUTUBE_URL; ?></a></h4>

                    <p></p>
                </article>
            </div>
            <div class="btnWrap more">
                <div class="btn"><a href="<?php echo YOUTUBE_URL; ?>" data-title="Visit Channel"
                                    class="ext">
                        <span>Visit Channel</span></a>
                </div>
            </div>
        </div>
    </section>
    <section>
        <h3 class="facebook"><a href="<?php echo FACEBOOK_URL; ?>" target="_blank" class="ext">Like Us</a>
        </h3>
    </section>

    <section>
        <h3>Follow Us</h3>

        <div class="social social--homepage">
            <ul id="footerSocialButtons" class="social__list">
                <li class="social__list__item social__list__item--facebook"><a
                        href="<?php echo FACEBOOK_URL; ?>" class="ext social__item__link"
                        target="_blank" data-title="footer_facebook">Facebook</a></li>
                <li class="social__list__item social__list__item--twitter"><a
                        href="<?php echo TWITTER_URL; ?>" class="ext social__item__link" target="_blank"
                        data-title="footer_twitter">Twitter</a></li>
                <li class="social__list__item social__list__item--youtube"><a
                        href="<?php echo YOUTUBE_URL; ?>" class=" ext social__item__link" target="_blank"
                        data-title="footer_youtube">YouTube</a></li>
            </ul>
        </div>
    </section>

</aside>