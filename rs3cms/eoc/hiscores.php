<?php
require_once("global.php");
require_once("../includes/class_character_statistics.php");

function getRank($id, $order)
{
    $count = 1;
    $res = dbquery("SELECT master_id FROM characters_statistics ORDER BY $order;");

    if (mysql_num_rows($res) > 0) {
        while ($row = mysql_fetch_array($res)) {
            $userid = $row['master_id'];
            if ($userid == $id) {
                return $count;
            }
            $count++;
        }
    }
    return -1;
}

if (isset($_GET['skill'])) {
    $skill_id = $_GET['skill'];
    switch ($skill_id) {
        case 0:
            $skill = "total";
            break;
        case 1:
            $skill = "attack";
            break;
        case 2:
            $skill = "defence";
            break;
        case 3:
            $skill = "strength";
            break;
        case 4:
            $skill = "constitution";
            break;
        case 5:
            $skill = "range";
            break;
        case 6:
            $skill = "prayer";
            break;
        case 7:
            $skill = "magic";
            break;
        case 8:
            $skill = "cooking";
            break;
        case 9:
            $skill = "woodcutting";
            break;
        case 10:
            $skill = "fletching";
            break;
        case 11:
            $skill = "fishing";
            break;
        case 12:
            $skill = "firemaking";
            break;
        case 13:
            $skill = "crafting";
            break;
        case 14:
            $skill = "smithing";
            break;
        case 15:
            $skill = "mining";
            break;
        case 16:
            $skill = "herblore";
            break;
        case 17:
            $skill = "agility";
            break;
        case 18:
            $skill = "thieving";
            break;
        case 19:
            $skill = "slayer";
            break;
        case 20:
            $skill = "farming";
            break;
        case 21:
            $skill = "runecrafting";
            break;
        case 22:
            $skill = "construction";
            break;
        case 23:
            $skill = "hunter";
            break;
        case 24:
            $skill = "summoning";
            break;
        case 25:
            $skill = "dungeoneering";
            break;
        default:
            $skill = "total";
            break;
    }
} else {
    $skill = "total";
}

?>

<html>
<head>
    <title>Top Player Rankings by Skill - <?php echo SITE_NAME; ?></title>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/font_fallback-0.css" type="text/css" media="all"/>
    <![endif]-->
    <!--[if gt IE 8]><!--><!-- x -->
    <link rel="stylesheet" href="../css/font_base64-0.css" type="text/css" media="all"/>
    <!--<![endif]-->
    <link href="../css/components-14.css" rel="stylesheet"/>
    <link href="../css/pages-14.css" rel="stylesheet"/>
    <script src="js/modernizr_2_8_3_min-6.js"></script>

    <script type='text/javascript'>
        var _vis_opt_cookieDays = 1,
            _vwo_code = (function () {
                var account_id = 54596,
                    settings_tolerance = 2000,
                    library_tolerance = 2500,
                    use_existing_jquery = false,
                    f = false, d = document;
                return {
                    use_existing_jquery: function () {
                        return use_existing_jquery;
                    }, library_tolerance: function () {
                        return library_tolerance;
                    }, finish: function () {
                        if (!f) {
                            f = true;
                            var a = d.getElementById('_vis_opt_path_hides');
                            if (a)a.parentNode.removeChild(a);
                        }
                    }, finished: function () {
                        return f;
                    }, load: function (a) {
                        var b = d.createElement('script');
                        b.src = a;
                        b.type = 'text/javascript';
                        b.innerText;
                        b.onerror = function () {
                            _vwo_code.finish();
                        };
                        d.getElementsByTagName('head')[0].appendChild(b);
                    }, init: function () {
                        settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                        this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                        var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
                        a.setAttribute('id', '_vis_opt_path_hides');
                        a.setAttribute('type', 'text/css');
                        if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                        h.appendChild(a);
                        return settings_timer;
                    }
                };
            }());
        _vwo_settings_timer = _vwo_code.init();
    </script>
</head>
<body id="hiscore" class="hiscore en" itemscope itemtype="http://schema.org/WebPage">
<div class="stickyWrap">

    <?php include_once("header.php") ?>
    <div class="contents">
        <div class="main">

            <h1 class="title "><span>HiScores</span></h1>
            <ul class="breadcrumb" itemprop="breadcrumb">
                <li><a href="<?php echo SITE_ADDRESS; ?>rs3">Home</a></li>
                <li><a href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php">HiScores</a></li>
                <li>Skills</li>
            </ul>

            <nav class="secondary">
                <ul>
                    <li><a href="<?php echo SITE_ADDRESS; ?>rs3"><span>Home</span></a></li>
                    <li class=active><a
                            href="<?php echo SITE_ADDRESS; ?>eoc/hiscores.php"><span>Skills</span></a>
                    </li>
                    <li><a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php"><span>Compare</span></a></li>
                    <li><a href="<?php echo SITE_ADDRESS; ?>eoc/search.php"><span>Adventurer's Log</span></a></li>
                </ul>
            </nav>
            <!--            <nav class="tertiaryNav">-->
            <!--                <ul>-->
            <!--                    <li>-->
            <!--                        <a href="http://services.runescape.com/m=hiscore/ranking?category_type=0&table=0&time_filter=0&page=1"-->
            <!--                           class="active"><span>All time</span></a>-->
            <!--                    </li>-->
            <!--                    <li>-->
            <!--                        <a href="http://services.runescape.com/m=hiscore/ranking?category_type=0&table=0&time_filter=1&page=1"><span>Weekly</span></a>-->
            <!--                    </li>-->
            <!--                    <li class="lastNavContainer">-->
            <!--                        <a href="http://services.runescape.com/m=hiscore/ranking?category_type=0&table=0&time_filter=2&page=1"><span>Monthly</span></a>-->
            <!--                    </li>-->
            <!--                </ul>-->
            <!--            </nav>-->
            <div class="ranking content">
                <div class="categorySection">
                    <div class="categories">
                        <h6 id="categorySelectorText">
                            Skills
                        </h6>

                        <div class="filterContainer">
                            <h3>Skills</h3>
                            <ul>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=1">
                                        <div class="hiscoreIcon iconAttack"></div>
                                        Attack</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=4">
                                        <div class="hiscoreIcon iconConstitution"></div>
                                        Constitution</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=15">
                                        <div class="hiscoreIcon iconMining"></div>
                                        Mining</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=3">
                                        <div class="hiscoreIcon iconStrength"></div>
                                        Strength</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=17">
                                        <div class="hiscoreIcon iconAgility"></div>
                                        Agility</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=14">
                                        <div class="hiscoreIcon iconSmithing"></div>
                                        Smithing</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=2">
                                        <div class="hiscoreIcon iconDefence"></div>
                                        Defence</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=16">
                                        <div class="hiscoreIcon iconHerblore"></div>
                                        Herblore</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=11">
                                        <div class="hiscoreIcon iconFishing"></div>
                                        Fishing</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=5">
                                        <div class="hiscoreIcon iconRanged"></div>
                                        Ranged</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=18">
                                        <div class="hiscoreIcon iconThieving"></div>
                                        Thieving</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=8">
                                        <div class="hiscoreIcon iconCooking"></div>
                                        Cooking</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=6">
                                        <div class="hiscoreIcon iconPrayer"></div>
                                        Prayer</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=13">
                                        <div class="hiscoreIcon iconCrafting"></div>
                                        Crafting</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=12">
                                        <div class="hiscoreIcon iconFiremaking"></div>
                                        Firemaking</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=7">
                                        <div class="hiscoreIcon iconMagic"></div>
                                        Magic</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=10">
                                        <div class="hiscoreIcon iconFletching"></div>
                                        Fletching</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=9">
                                        <div class="hiscoreIcon iconWoodcutting"></div>
                                        Woodcutting</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=21">
                                        <div class="hiscoreIcon iconRunecrafting"></div>
                                        Runecrafting</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=19">
                                        <div class="hiscoreIcon iconSlayer"></div>
                                        Slayer</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=20">
                                        <div class="hiscoreIcon iconFarming"></div>
                                        Farming</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=23">
                                        <div class="hiscoreIcon iconConstruction"></div>
                                        Construction</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=22">
                                        <div class="hiscoreIcon iconHunter"></div>
                                        Hunter</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=24">
                                        <div class="hiscoreIcon iconSummoning"></div>
                                        Summoning</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=25">
                                        <div class="hiscoreIcon iconDungeoneering"></div>
                                        Dungeoneering</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=26">
                                        <div class="hiscoreIcon iconDivination"></div>
                                        Divination</a>
                                </li>
                                <li>
                                    <a href="<?php printf(SITE_ADDRESS); ?>eoc/hiscores.php?skill=0">
                                        <div class="hiscoreIcon iconOverall"></div>
                                        Overall</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <h3 class="descTrigger">
                        <?php if (isset($_GET['skill'])) {
                            ?>
                            <?php echo ucfirst($skill); ?>
                            <?php
                        } else {
                            echo "Overall";
                        } ?>
                    </h3>

                    <p class="hiScoreDescription">Skills are the abilities of player characters, which may be
                        improved
                        by practising them. Performing actions using skills rewards the player with experience in
                        the
                        appropriate skill(s).</p>

                    <div class="filterDivide"></div>
                    <!--                    <div class="filter">-->
                    <!--                        <h6>View:</h6>-->
                    <!--                        <ul>-->
                    <!--                            <li class="active"><a-->
                    <!--                                    href="http://services.runescape.com/m=hiscore/ranking?category_type=0&table=0&date=1430557199015&time_filter=0&page=1">All</a>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <a href="http://services.runescape.com/m=hiscore/friends?category_type=0&table=0&date=1430557199015&time_filter=0&page=1">Friends</a>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <a href="http://services.runescape.com/m=hiscore/ranking?user=&findme=1&table=0&date=1430557199015&category_type=0&time_filter=0">Your-->
                    <!--                                    Rank</a></li>-->
                    <!--                        </ul>-->
                    <!--                    </div>-->
                </div>

                <div class="tiledBlueFrame">
                    <div class="tl corner"></div>
                    <div class="tr corner"></div>
                    <div class="bl corner"></div>
                    <div class="br corner"></div>
                    <div class="tableWrap">
                        <?php if (isset($_GET['skill'])) {
                            ?>
                            <div class="iconPlate">
                                <img
                                    src="<?php echo SITE_ADDRESS; ?>eoc/img/hiscores/large-icon-<?php echo ucfirst($skill); ?>.png"
                                    title="<?php echo ucfirst($skill); ?>">
                            </div>
                            <?php
                        } ?>
                        <table>
                            <thead>
                            <tr>
                                <th class="col1 align">Rank</th>
                                <th class="col2">Player&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th class="col3 align">Level</th>
                                <th class="col4 align">XP</th>
                            </tr>
                            </thead>
                        </table>
                        <table>
                            <tbody>
                            <?php # BEGIN OF HISCORES FOR PLAYERS

                            $show = 7;
                            $page = 1;

                            if (isset($_GET['page']) && is_numeric($_GET['page'])) {
                                $page = $_GET['page'];
                                $offset = $show * $page;
                            } else {
                                $page = 1;
                                $offset = 7;
                            }

                            if (isset($_GET['rank']) && is_numeric($_GET['rank'])) {
                                $rank_loc = $_GET['rank'];
                                $page = ceil(($rank_loc / $show));
                            }

                            $start_from = ($page - 1) * $show;

                            $total_results = dbevaluate("SELECT COUNT(*) FROM characters_statistics;");
                            $total_pages = ceil($total_results / $show);

                            dbquery("SET @rankcount = $start_from;");
                            $clevel = $skill . "_level";
                            $cexp = $skill . "_exp";

                            if ($skill == "total") {
                                $order = "total_level DESC, total_exp DESC";
                                $columns = "total_level, total_exp";
                            } else {
                                $order = "$cexp DESC";
                                $columns = "$cexp";
                            }

                            //         $qry = dbquery("SELECT characters_statistics.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank, $columns, username FROM characters_statistics ORDER BY $order LIMIT $offset,$show ) AS cstats INNER JOIN characters_statistics ON characters_statistics.username = characters_statistics.username");
                            $qry = dbquery("SELECT * FROM characters_statistics ORDER BY $order LIMIT $start_from, $show;");
                            while ($user = mysql_fetch_assoc($qry)) {
                                $hs_name = $user['username'];

                                $hs_rights = $user['playerRights'];


                                ?>

                                <tr class="">
                                    <td class="col1 align">
                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php?user1=<?php echo $hs_name ?>">
                                            <?php printf(number_format(getRank($user['master_id'], $order))); ?>
                                        </a>
                                    </td>
                                    <td class="col2">
                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php?user1=<?php echo $hs_name ?>">
                                            <?php echo $hs_name ?>
                                            <img class="memberIcon"
                                                 src="<?php echo get_player_icon($hs_rights); ?>">
                                        </a>
                                    </td>
                                    <td class="col3 align">
                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php?user1=<?php echo $hs_name ?>">
                                            <?php printf(number_format($user[$clevel])); ?>
                                        </a>
                                    </td>
                                    <td class="col4 align">
                                        <a href="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php?user1=<?php echo $hs_name ?>">
                                            <?php printf(number_format($user[$cexp])); ?>

                                        </a>
                                    </td>
                                </tr>
                                <?php
                            }

                            if (mysql_num_rows($qry) == 0) {
                                printf("<center>No results.</center>");
                            } else {
                            }

                            ?>

                            </tbody>
                        </table>

                        <!-- PAGINATION START -->
                        <div class="paginationWrap clear">
                            <div class="pagination">
                                <div class="paging">

                                    <?php
                                    $base_url = SITE_ADDRESS . "eoc/hiscores.php?";
                                    if (isset($skill_id)) {
                                        $base_url .= "skill=" . $skill_id . "/";
                                    }

                                    $adjacents = 1;

                                    // start on pagignition
                                    printf("<ul id=\"paginationUL78436\" class=\"pageNumbers $page\">");
                                    if ($total_pages < (7 + ($adjacents * 2))) {
                                        for ($i = 1; $i <= $total_pages; $i++) {
                                            if ($i == $page) {
                                                printf("<li id=\"current\">" . $page . "</li>");
                                            } else {
                                                printf("<li id=\"paginationListItem$page\"><a href=\"" . $base_url . "page=" . $i . "\">" . $i . "</a></li>");
                                            }
                                        }
                                    } elseif ($total_pages > 5 + ($adjacents * 2)) {
                                        if ($page < 1 + ($adjacents * 2)) {
                                            for ($i = 1; $i < 4 + ($adjacents * 2); $i++) {
                                                if ($i == $page) {
                                                    printf("<li id=\"current\">" . $page . "</li>");
                                                } else {
                                                    printf("<li id=\"paginationListItem$page\"><a href=\"" . $base_url . "page=" . $i . "\">" . $i . "</a></li>");
                                                }
                                            }
                                            printf("<li class=\"ellipses\">...</li>");
                                            printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                                        } elseif ($total_pages - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                                            printf("<li><a href=\"" . $base_url . "p1\">1</a></li>");
                                            printf("<li class=\"ellipses\">...</li>");
                                            for ($i = $page - $adjacents; $i <= $page + $adjacents; $i++) {
                                                if ($i == $page) {
                                                    printf("<li class=\"current\">" . $page . "</li>");
                                                } else {
                                                    printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                }
                                            }
                                            printf("<li class=\"ellipses\">...</li>");
                                            printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                                        } else {
                                            printf("<li id=\"current\"><a href=\"" . $base_url . "page=1\">1</a></li>");
                                            printf("<li class=\"ellipses\">...</li>");
                                            for ($i = $total_pages - (2 + ($adjacents * 2)); $i <= $total_pages; $i++) {
                                                if ($i == $page) {
                                                    printf("<li id=\"current\">" . $page . "</li>");
                                                } else {
                                                    printf("<li id=\"paginationListItem$page\"><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                }
                                            }
                                        }
                                    }
                                    printf("</ul>");
                                    ?>
                                    <a id="paginationNext" class="next"
                                       href="?page=2">
                                        &#8594;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <aside role="complementary" class="sidebar">

                <section class="input">
                    <h3>Compare Players</h3>

                    <form action="<?php echo SITE_ADDRESS; ?>eoc/hscompare.php" class="index">
                        <input type="text" placeholder="Player 1" class="placeholder text" title="Player 1"
                               maxlength="12" name="user1" required="required"/>
                        <input type="text" placeholder="Player 2" class="placeholder text" title="Player 2"
                               maxlength="12" name="user2"/>

                        <div class="btnWrap ">
                            <div class="btn">
                                <div class="btnRight">
                                    <input type="submit" value="Go" name=""/>
                                </div>
                            </div>
                        </div>
                    </form>
                </section>
                <!--                <section class="ironmanSide">-->
                <!--                    <h3><a class="ironmanSide__link" href="http://services.runescape.com/m=hiscores_ironman/ranking">Ironman-->
                <!--                            Challenge</a></h3>-->
                <!--                </section>-->
                <!--                <section class="ironmanSide">-->
                <!--                    <h3><a class="ironmanSide__link"-->
                <!--                           href="http://services.runescape.com/m=hiscores_hardcore_ironman/ranking">Hardcore Ironman</a>-->
                <!--                    </h3>-->
                <!--                </section>-->
            </aside>
        </div>
    </div>
</div>
<?php include_once("footer.php"); ?>

</body>

</html>