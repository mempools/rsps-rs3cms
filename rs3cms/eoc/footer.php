<?php
?>
<footer class="footer" role="contentinfo">
    <div class="footer__inner">
        <a href="http://www.jagex.com/g=runescape/" class="footer__jagex ext" data-title="footer_jagex">Jagex</a>
        <div class="footer__paragraph">
            <p>
                Images on this website are copyright &copy; 1999 - 2015 Jagex Ltd<br/>
                RuneScape content and materials are trademarks and copyrights of Jagex or its licensors. All rights
                reserved. This site is not affiliated with Jagex.<br/>
                You can play RuneScape <a href="http://runescape.com/">here.</a>.
            </p>
        </div>
    </div>
</footer>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>window.jQuery || document.write('<script type="text/javascript" src="<?php echo SITE_ADDRESS;?>eoc/js/jquery_1_11_0-0.js"><\/script>')</script>
<script type="text/javascript" src="<?php echo SITE_ADDRESS; ?>eoc/js/plugins-7.js"></script>
<script type='text/javascript' src='<?php echo SITE_ADDRESS; ?>eoc/js/jagex_global-1.js'></script>
<script type="text/javascript" src="<?php echo SITE_ADDRESS; ?>eoc/js/functions-86.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADDRESS; ?>eoc/js/gtm-0.js"></script>
<script type="text/javascript" src="<?php echo SITE_ADDRESS; ?>eoc/js/adventurers_log-3.js"></script>