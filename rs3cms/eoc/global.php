<?php
include_once("../global.php");

# promotion buttons
define("BUTTON_1", ""); # old sch00l
define("BUTTON_2", ""); # become a member
define("BUTTON_3", ""); # runelabs button

# Header links
define("H_FORUMS", "google.com#");
define("H_VOTE", "google.com#");
define("H_DONATE", "google.com#");
define("H_PLAY", "google.com#"); # play now button


# The big slider images & urls
$C_SLIDER = array
(
    "C_SLIDER1" => ["google.com#", "http://cdn.runescape.com/assets/img/external/rotators/2015/04/RS-Rotator-BnL-EN.jpg"],
    "C_SLIDER2" => ["google.com#", "http://cdn.runescape.com/assets/img/external/rotators/2015/04/RS-2015_Rotator_RS-Roadtrip_EN.jpg"],
    "C_SLIDER3" => ["google.com#", "http://cdn.runescape.com/assets/img/external/rotators/2015/04/RS_Rotator_Aquarium_EN.jpg"],
);
$C_SLIDER[''];

#
# define image ranks here
#
function get_player_icon($playerRights)
{
    switch ($playerRights) {
        case 1: # Mod
            return SITE_ADDRESS . "img/ranks/mod_silver.gif";
        case 2 :# Admin
            return SITE_ADDRESS . "img/ranks/mod_gold.gif";
        case 3: # Donator
            return SITE_ADDRESS . "img/ranks/donator.png";
    }
    return SITE_ADDRESS . "img/members_icon_small.png";
}