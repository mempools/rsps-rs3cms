<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("global.php");
require_once("includes/recaptchalib.php");

$login_failed = false;

/* If already logged in, redirect to homepage. */
$redir = $_GET['dest'];
if ($agent->logged_in) {
    die(header("Location: home"));
}

if (isset($_POST['submit'])) {
    $email_input = $_POST['email'];
    $password_input = $_POST['password'];

    $email = filter_for_input(strtolower($email_input));
    $hash = filter_for_input(sha1($password_input));

    $recap = get_recaptcha_response($_POST['g-recaptcha-response']);
    if (isset($recap)) {
        if (!$recap->success) {
            if (!isset($_GET['c'])) {
                die(header("Location: login?confirm"));
            }
        }
    }

    if (agent::validate($email, $hash)) {
        $_SESSION['USER_EMAIL'] = $email;
        $_SESSION['USER_HASH'] = $hash;

        unset($_SESSION['ID']);
        $uip = REMOTE_ADDR;
        dbquery("DELETE FROM web_sessions WHERE ip_address = '$uip';");

        $user_id = agent::id_from_email($email);
        $user_ip = REMOTE_ADDR;

        dbquery("INSERT INTO login_attempts (master_id,date,ip,attempt,type) VALUES('$user_id',NOW(),'$user_ip','1','-1');");

        $url = SITE_ADDRESS . "home";
        if (isset($_SESSION['RD'])) {
            $_SESSION['RD'] = '';
            die(header("Location: donate"));
        } else {
            die(header("Location: home"));
        }
    } else {
        $login_failed = true;
    }
}

require_once("header.php");
?>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/error-50.css"/>

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Login</span>
                                    <span class="G0">Login</span>
                                    <span class="G1" aria-hidden="true">Login</span>
                                    <span class="G2" aria-hidden="true">Login</span>
                                    <span class="G3" aria-hidden="true">Login</span>
                                    <span class="G4" aria-hidden="true">Login</span>
                                    <span class="G5" aria-hidden="true">Login</span>
                                    <span class="G6" aria-hidden="true">Login</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">Login</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Login</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <?php if (isset($_GET['confirm'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">

                                <h2 class="Gradient">
                                    <span class="G0">Confirm Your Login</span>
                                    <span class="G1" aria-hidden="true">Confirm Your Login</span>
                                    <span class="G2" aria-hidden="true">Confirm Your Login</span>
                                    <span class="G3" aria-hidden="true">Confirm Your Login</span>
                                    <span class="G4" aria-hidden="true">Confirm Your Login</span>
                                    <span class="G5" aria-hidden="true">Confirm Your Login</span>
                                    <span class="G6" aria-hidden="true">Confirm Your Login</span>
                                    <span class="mask"><span class="spacing">Confirm Your Login</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Confirm Your Login</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Confirm Your Login</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2></br>

                                <p><b>NOTE:</b></p>

                                <p>If you have an existing in-game account, please login using your IGN, otherwise
                                    login with your email.</p>

                                <div id="errorContent">

                                    <form action="<?php printf(SITE_ADDRESS); ?>login" method="post" autocomplete="off">
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Email:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login" name='email'
                                                                                 maxlength='20' size='35' value=""
                                                                                 id='username' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="password">
                                            <span class="inputLabel FlatHeader">Password:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='password' title=""
                                                                                 name='password' maxlength='20'
                                                                                 size='35' value="" id='password'
                                                                                 class="NoPlaceholder"></div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <a class="password" href="/forgot_password" target="_parent">Forgot
                                            Password?</a> <br
                                            class="clear"/>

                                        <div class="section_form" id="recaptcha">
                                            <div id="recaptcha_box">
                                                <div id="recaptcha_image"></div>
                                                <script src="https://www.google.com/recaptcha/api.js" async
                                                        defer></script>
                                                <div class="g-recaptcha"
                                                     data-sitekey=<?php printf(RECAPTCHA_SITEKEY); ?>></div>
                                                <div id="button">
                                                    <button class="HoverImg" type="submit" name="submit">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/weblogin/recaptcha_login.png"
                                                            alt="Login" title=""/>
                                                    </button>
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } else { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Login Failed</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Invalid Login or Password</span>
                                    <span class="G1" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="G2" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="G3" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="G4" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="G5" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="G6" aria-hidden="true">Invalid Login or Password</span>
                                    <span class="mask"><span class="spacing">Invalid Login or Password</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Invalid Login or Password</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Invalid Login or Password</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>The email or password you entered was incorrect.</p><br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>login?confirm"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Try Again"; ?></b></span></span></span></a>
                                    <a href="forgot_password"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Forgotten Password"; ?></b></span></span></span></a>
                                    <a href="register?confirm" class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Create FREE Account"; ?></b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <?php require_once("footer.php"); ?>

