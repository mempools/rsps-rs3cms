<?php
require_once("global.php");

$username = "";
if ($agent->logged_in) {
    // exit(header("Location: " . SITE_ADDRESS . "home"));
    $username = $agent->username;
}

$failed = false;
$server_name = SITE_NAME;
$script = "support.php";
$to_email = "support@odevain.com";
$from_email = "bug.report@odevain.com";
$subject = $server_name . " - Bug Report";
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: ' . $from_email . "\r\n";

function email_message($msg, $username)
{
    $img = "<img src=" . SITE_ADDRESS . "img/global/logos/runescape.png>";
    $message = '<html><body>';
    $message .= $img . '</br></br>';
    $message .= '
    <b>Bug Report by:</b> ' . $username . '</br>
    <b>Time:</b> ' . date('l jS \of F Y h:i:s A') . '
    </br>
    <b>Report:</b>
    </br>
    ' . $msg . '
    </br></br>';
    $message .= '</body></html>';
    return $message;
}


$message = "";
$content = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['content']) || empty($_POST['username'])) {
        die(header("Location: " . SITE_ADDRESS . "" . $script . "?2"));
    } else {
        $content = $_POST['content'];
        $username = $_POST['username'];
        mail($to_email, $subject, email_message($content, $username), $headers);
        die(header("Location: " . SITE_ADDRESS . "" . $script . "?1"));
    }
}
require_once("header.php");
?>
    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/error-50.css"
          xmlns="http://www.w3.org/1999/html"/>
    <!--    <link rel="stylesheet" type="text/css" href="--><?php //printf(SITE_ADDRESS); ?><!--css/email.css"/>-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Bug Report</span>
                                    <span class="G0">Bug Report</span>
                                    <span class="G1" aria-hidden="true">Bug Report</span>
                                    <span class="G2" aria-hidden="true">Bug Report</span>
                                    <span class="G3" aria-hidden="true">Bug Report</span>
                                    <span class="G4" aria-hidden="true">Bug Report</span>
                                    <span class="G5" aria-hidden="true">Bug Report</span>
                                    <span class="G6" aria-hidden="true">Bug Report</span>
                                    <span class="mask"><span class="spacing"
                                                             aria-hidden="true">Bug Report</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Bug Report</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <?php if (!isset($_GET['1']) and !isset($_GET['2'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">

                                <h2 class="Gradient">
                                    <span class="G0">Submit a Bug Report</span>
                                    <span class="G1" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="G2" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="G3" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="G4" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="G5" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="G6" aria-hidden="true">Submit a Bug Report</span>
                                    <span class="mask"><span class="spacing">Submit a Bug Report</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Submit a Bug Report</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Submit a Bug Report</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">

                                    <form action="<?php printf(SITE_ADDRESS) ?>support.php" method="post"
                                          autocomplete="off">
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Name:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login"
                                                                                 name='username'
                                                                                 maxlength='20' size='30' value=""
                                                                                 id='username' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="login">
                                            <div class="form-group">
                                                <span class="inputLabel FlatHeader">Report:</span>
                                                <textarea class="form-control" rows="3" name="content"></textarea>
                                            </div>
                                        </div>

                                        <a class="password" target="_parent"><?php
                                            if ($failed) {
                                                echo "Please make sure all forms are filled.";
                                            }
                                            ?></a> <br class="clear"/>

                                        <a class="Button Button29 errorPageButton w160"><input
                                                type="submit"><span><span><span
                                                        class=""><b><?php echo "Submit" ?></b></span></span></span></a>

                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } else if (isset($_GET['1'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Submit a Bug Report</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Report Sent</span>
                                    <span class="G1" aria-hidden="true">Report Sent</span>
                                    <span class="G2" aria-hidden="true">Report Sent</span>
                                    <span class="G3" aria-hidden="true">Report Sent</span>
                                    <span class="G4" aria-hidden="true">Report Sent</span>
                                    <span class="G5" aria-hidden="true">Report Sent</span>
                                    <span class="G6" aria-hidden="true">Report Sent</span>
                                    <span class="mask"><span class="spacing">Report Sent</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Report Sent</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Report Sent</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>Your bug report has successfully been sent!</p>
                                    <br/>
                                </div>
                            </div>
                        </div>

                    <?php } else if (isset($_GET['2'])) { ?>
                        <!-- Account not found -->
                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Submit a Bug Report</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Error Reporting</span>
                                    <span class="G1" aria-hidden="true">Error Reporting</span>
                                    <span class="G2" aria-hidden="true">Error Reporting</span>
                                    <span class="G3" aria-hidden="true">Error Reporting</span>
                                    <span class="G4" aria-hidden="true">Error Reporting</span>
                                    <span class="G5" aria-hidden="true">Error Reporting</span>
                                    <span class="G6" aria-hidden="true">Error Reporting</span>
                                    <span class="mask"><span class="spacing">Error Reporting</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Error Reporting</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Error Reporting</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>Please make sure you have filled out all forms!</p><br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>support"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Try Again" ?></b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

<?php require_once("footer.php"); ?>