<?php
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}
?>
<html
    class=" svg websockets sharedworkers localstorage video progressbar meter csspointerevents inlinesvg canvas datalistelem fileinput webgl webworkers no-touchevents no-details indexeddb indexeddb-deletedatabase boxsizing no-backgroundcliptext borderimage flexboxlegacy flexbox appearance blobworkers dataworkers"
    style="overflow: hidden; height: auto;">
<head>
    <link type="text/css" rel="stylesheet"
          href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/account_settings_frame-50.css" rel="stylesheet">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script type="text/javascript" src="https://www.jagex.com/js/jquery/jquery_effects_core_0.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript"
            src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script>
        if (window == window.top) {
            window.location.href = "http://google.com/";
        }
    </script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/account_settings_frame-50.js"></script>
    <title><?php printf(SITE_NAME); ?></title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java">
    <meta name="description" content="<?php printf(SITE_DESC); ?>">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">
    <meta property="og:title" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:type" content="game">
    <meta property="og:url" content="<?php printf(SITE_ADDRESS); ?>">
    <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:description" content="<?php printf(SITE_DESC); ?>">
    <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png">
    <style type="text/css">

        .accountSettingsTitle {
            clear: both;
        }

        .borderedTable {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 1em;
        }

        .borderedTable th {
            border-bottom: 1px solid #635035;
        }

        .borderedTable th,
        .borderedTable td {
            padding: 10px 2px;
            vertical-align: middle;
        }

        .borderedTable td {
            border-bottom: 1px dotted #635035;
        }

        .evidenceBox {
            display: block;
            background: #333;
            border: 1px solid #555;
            padding: 8px;
            word-wrap: break-word;
        }

        .evidenceBox textarea {
            width: 100%;
            background: none;
            border: 0;
        }

        .offenceInfo {
            width: 50%;
            float: left;
        }

        ul {
            margin-bottom: 1em;
        }

        .sn-pxg .pxg-set {
            user-select: none;
            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .sn-pxg span.pxg-source {
            position: relative;
            display: inline-block;
            z-index: 2;
        }

        .sn-pxg U.pxg-set, .sn-pxg U.pxg-set S, .sn-pxg U.pxg-set S B {
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            height: inherit;
            width: inherit;
            position: absolute;
            display: inline-block;
            text-decoration: none;
            font-weight: inherit;
        }

        .sn-pxg U.pxg-set S {
            overflow: hidden;
        }

        .sn-pxg U.pxg-set {
            text-decoration: none;
            z-index: 1;
            display: inline-block;
            position: relative;
        }

        #offencestatus {
            width: 255px;
            margin: 0;
            padding: 0;
        }

        #offencestatus .meter {
            width: 218px;
            margin: 0 10px 20px;
        }

        #offencestatus .bar, #offencestatus .heading {
            width: 100%;
        }

        #offencestatus .FlatHeader {
            font-size: 20px;
            margin: 10px 0 0 10px;
        }

        #offencestatus .bar {
            background: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/offence-appeal/barBG.png") no-repeat transparent;
            height: 41px;
            position: relative;
            margin-top: 5px;
        }

        #offencestatus .marker {
            display: block;
            position: absolute;
            top: 1px;
            height: 26px;
            width: 21px;
            left: 10px;
        }

        #offencestatus .statusBar {
            background: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/offence-appeal/statusBar.jpg") no-repeat scroll 18px 19px transparent;
            overflow: hidden;
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 22px;
        }

        #offencestatus .legend {
            height: 20px;
            margin: 20px 0 20px 11px;
        }

        #offencestatus .legend span {
            background: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/offence-appeal/legendGreen.jpg") no-repeat scroll 0 0 transparent;
            display: block;
            float: left;
            line-height: 12px;
            padding: 0 16px;
        }

        #offencestatus #orange {
            background-image: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/offence-appeal/legendOrange.jpg");
        }

        #offencestatus #red {
            background-image: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/offence-appeal/legendRed.jpg");
        }

        .error {
            color: #e00;
        }

        .padded, .error {
            padding: 0 10px 5px 0;
            text-align: center;
        }

        .offences td,
        .offences th {
            text-align: center;
        }

        .offences td.offenceDetail {
            text-align: left;
        }


    </style>

    <script type="text/javascript">
        var _vis_opt_cookieDays = 1,
            _vwo_code = (function () {
                var account_id = 54596,
                    settings_tolerance = 2000,
                    library_tolerance = 2500,
                    use_existing_jquery = false,
                    f = false, d = document;
                return {
                    use_existing_jquery: function () {
                        return use_existing_jquery;
                    }, library_tolerance: function () {
                        return library_tolerance;
                    }, finish: function () {
                        if (!f) {
                            f = true;
                            var a = d.getElementById('_vis_opt_path_hides');
                            if (a)a.parentNode.removeChild(a);
                        }
                    }, finished: function () {
                        return f;
                    }, load: function (a) {
                        var b = d.createElement('script');
                        b.src = a;
                        b.type = 'text/javascript';
                        b.innerText;
                        b.onerror = function () {
                            _vwo_code.finish();
                        };
                        d.getElementsByTagName('head')[0].appendChild(b);
                    }, init: function () {
                        settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
                        this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
                        var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
                        a.setAttribute('id', '_vis_opt_path_hides');
                        a.setAttribute('type', 'text/css');
                        if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
                        h.appendChild(a);
                        return settings_timer;
                    }
                };
            }());
        _vwo_settings_timer = _vwo_code.init();
    </script>
    <script type="text/javascript"
            src="//dev.visualwebsiteoptimizer.com/j.php?a=54596&amp;u=https%3A%2F%2Fwww.runescape.com%2Fc%3D7iizKUB*LOA%2Faccount_settings.ws&amp;r=0.2851882011597132"></script>
    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/account_settings-56.css">
    <script>$(function () {
            $('.RaggedBoxToggly')
                .addClass('RaggedBoxTogglyJs')

                .find('.RaggedBoxTitle')
                .hover(
                    function () {
                        $(this).prev('.RaggedBoxToggle').mouseover()
                    },
                    function () {
                        $(this).prev('.RaggedBoxToggle').mouseout()
                    }
                )
                .click(function (ev, do_not_animate) {
                    var $this = $(this);

                    var $box = $this.closest('.RaggedBox');
                    var $bg = $box.find('.RaggedBoxBg');
                    $bg.stop().animate(
                        {
                            marginTop: ($box.hasClass('RaggedBoxClosed') ? 47 + "px" : ((-$box.height() + 81) + "px"))
                        },
                        {duration: (do_not_animate ? 0 : 1000)}
                    );
                    $box
                        .toggleClass('RaggedBoxClosed')
                        .trigger('RaggedBoxToggle', [!$box.hasClass('RaggedBoxClosed')])
                    ;
                })
                .end()
                .filter('.RaggedBoxCloseMe')
                .removeClass('RaggedBoxCloseMe')
                .find('.RaggedBoxTitle')
                .trigger('click', [true]);

        });</script>
    <script>
        window.resizeRaggedBox = function (iframe, h) {
            var $this = $(iframe);
            var $box = $this.closest('.RaggedBox');
            $this.height(h);
            $box.trigger('RaggedBoxToggle', [!$box.hasClass('RaggedBoxClosed'), true]);
        }
        function getGameModeSettings() {

            var clientPref = JAGEX.cookie.get('client_html5_java');
            if (clientPref > 0) {
                var clientName = (clientPref == 1) ? "HTML5" : "Java";
                $('#preferredClient').text(clientName);
                $('#noPreferences').hide();
                $('#preferencesSet').show();
                if (clientPref == 2) {
                    $('.ifJava').show();
                }
            }
            else {
                $('#noPreferences').show();
                $('#preferencesSet').hide();
            }
        }
        $(document).ready(function () {
            var module = "";
            if (module === "recovery_questions") module = "recoveries";
            if (module === "sn-integration") module = "socnet";
            if (module === "password_history") module = "security";
            if (module === "displaynames") module = "charname";
            if (module === "offence-appeal") module = "offence";
            if (module === "email_registration") module = "email";
            if (module === "ticketing") module = "messages";
            if (module === "www") module = "java";
            if (module !== "") {
                $("#" + module).find('.RaggedBoxTitle').trigger("click", true);
            }
            getGameModeSettings();
            $("#clearPreferences").click(function (ev) {
                ev.preventDefault();
                JAGEX.cookie.set('client_html5_java', -1);
                RS3.global.setSplashCount(4);
                getGameModeSettings();
            });
        });

    </script>
    <style type="text/css">
        #membership .OrnamentalBoxTop {
            background-image: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/ornamentalBoxrsnopro_top.png");
        }

        .updateCard {
            display: block;
            line-height: 1.4em;
            margin: 10px auto;
            width: 180px;
        }
    </style>
    <!--    <script type="text/javascript">-->
    <!--        $(document).ready(function () {-->
    <!--            $("body").addClass("js");-->
    <!--            $(".dialogue_link").elide();-->
    <!--        });-->
    <!--    </script>-->
    <script type="text/javascript" src="<?php echo SITE_ADDRESS; ?>js/jagex/jagex_form-7.js"></script>
    <script>
        $(document).ready(function () {
            JAGEX.form.pritify.run();
        });
        /* Ready function */
    </script>


</head>
<body style="overflow: hidden; height: auto;" class="accountSettingsFrame">
<?php
if (isset($_GET['viewid']) || isset($_GET['step'])) {
    $viewid = $_GET['viewid'];
    $step = $_GET['step'];
    ?>
    <?php
    if (!isset($_GET['step'])) {
        $step = 1;
    } else {
        $step = $_GET['step'];
    }
    if ($step == 1) {
        ?>
        <div class="container">
            <p class="intro"><a href="account_history.php">Back to your offences</a></p>
            <h2 class="accountSettingsTitle">Disclaimer</h2>
            <p>We cannot guarantee that the evidence you are requesting is not offensive.</p>
            <p>By demanding to view this evidence you are agreeing to view possibly offensive material, which Jagex
                cannot be held accountable for.</p>
            <form method="get" action="account_history.php?step=2">
                <input value="<?php echo $viewid; ?>" name="viewid" type="hidden">
                <input value="2" name="step" type="hidden">
                <div class="formSubmit"><span class="Button Button29" id="submit"><span><span
                                class=""><b>Continue</b></span><input value="1" name="submit" title="submit"
                                                                      type="submit"></span></span></div>
            </form>
        </div>
    <?php } elseif ($step == 2) {
        $voffences = dbquery("SELECT * FROM offences WHERE id = $viewid;");
        $voffence = mysql_fetch_assoc($voffences);
        $vid = $voffence['id'];
        $vreason = $voffence['reason'];

        $vtype = $voffence['type'] == 1 ? "Mute" : "Ban";
        $vdate = date('m-M-y H:i:s', strtotime($voffence['date']));
        $vedate = date('m-M-y H:i:s', strtotime($voffence['expire_date']));

        ?>

        <div class="container">
            <p class="intro"><a href="account_history.php">Back to your offences</a></p>
            <h2 class="accountSettingsTitle">Offence Information</h2>
            <p class="offenceInfo"><b>Offence:</b>
                <?php echo mb_strimwidth($vreason, 0, 25, "..."); ?></p>
            <p class="offenceInfo"><b>Type:</b>
                <span class="redtext"><?php echo $vtype; ?></span>
            </p>
            <p class="offenceInfo"><b>Offence Date:</b><?php echo $vdate; ?></p>
            <p class="offenceInfo"><b>Offence Expiry:</b>
                <?php echo $vedate; ?></p>
            <!--            <p class="offenceInfo"><b>Game:</b> RuneScape</p>-->
            <h2 class="accountSettingsTitle">Offence Reason</h2>
            <p class="error"><b><?php echo $vreason; ?></b></p>
        </div>

    <?php }
} else {
    ?>
    <div class="container">
        <?php
        $offences = dbquery("SELECT * FROM offences WHERE character_id = '$agent->master_id' ORDER BY id DESC;");

        $blackmarks = 0;
        $ban_count = 0;
        $mute_count = 0;
        $expired_mutes = 0;
        $expired_bans = 0;
        $is_banned = false;
        $is_muted = false;

        /*

         expired bans:
        1 - green
        2 - yellow
        3 - red
         */
        if (mysql_num_rows($offences) > 0) {
            while ($offence = mysql_fetch_assoc($offences)) {
                $expired = $offence['expired'];
                $type = $offence['type'];
                if ($type == 2) {
                    $is_banned = true;
                    $ban_count++;
                    if ($expired == 1) {
                        $expired_bans++;
                    }
                } elseif ($type == 1) {
                    $is_muted = true;
                    $mute_count++;
                    if ($expired == 1) {
                        $expired_mutes++;
                    }
                }
            }
        }
        //    echo "BAN C: " . $ban_count . " E: " . $expired_bans;
        //    echo "</br>";
        //    echo "MUTE C: " . $mute_count . " E: " . $expired_mutes;
        ?>
        <?php
        if ($is_banned || $is_muted) {
            ?>
            <?php if ($is_banned) { ?>
                <p class="error intro">
                    <b>Banned:</b> Due to the nature of the offence(s) you have committed, your account has been
                    banned.
                </p>
            <?php } elseif ($is_muted) { ?>
                <p class="error intro">
                    <b>Muted:</b> Due to the nature of the offence(s) you have committed, you've been muted.
                </p>
            <?php } elseif ($is_banned && $is_muted) { ?>
                <p class="error intro">
                    <b>Muted/Banned:</b> Due to the nature of the offence(s) you have committed, your account has
                    been
                    permanently banned.
                </p>
            <?php } ?>
            <?php
        }

        $ban_meter = 0;
        $mute_meter = 0;
        $bm_GREEN = 112;
        $bm_MED = 155;
        $bm_RED = 200;

        if ($is_banned) {
            $ban_meter = $bm_RED;
        } elseif ($expired_bans >= 1) {
            $ban_meter = $bm_GREEN;
        } elseif ($expired_bans >= 2) {
            $ban_meter = $bm_MED;
        } elseif ($expired_bans >= 3) {
            $ban_meter = $bm_RED;
        } else {
            $ban_marker = 10;
        }

        if ($ban_count == 0) {
            $ban_marker = 9;
        } else {
            $ban_marker = $ban_meter - 10;
        }

        if ($expired_mutes >= 1) {
            $mute_meter = $bm_GREEN;
        } elseif ($expired_mutes >= 2) {
            $mute_meter = $bm_MED;
        } elseif ($expired_mutes >= 3) {
            $mute_meter = $bm_RED;
        } else {
            $mute_marker = 10;
        }

        if ($mute_count == 0) {
            $mute_marker = 9;
        } else {
            $mute_marker = $mute_meter - 10;
        }
        ?>
        <div id="offencestatus">
            <div id="bans" class="meter">
                <h4 class="FlatHeader">Ban Meter</h4>
                <div class="bar">
                    <div class="statusBar" style="width:<?php echo $ban_meter; ?>px"></div>
                    <img class="marker"
                         src="<?php echo SITE_ADDRESS; ?>img/account_settings/offence-appeal/marker.png"
                         alt="Marker" style="left:<?php echo $ban_marker; ?>px">
                </div>
            </div>
            <div id="mutes" class="meter">
                <h4 class="FlatHeader">Mute Meter</h4>
                <div class="bar">
                    <div class="statusBar" style="width:<?php echo $mute_meter; ?>px"></div>
                    <img class="marker"
                         src="<?php echo SITE_ADDRESS; ?>img/account_settings/offence-appeal/marker.png"
                         alt="Marker" style="left:<?php echo $mute_marker; ?>px">
                </div>
            </div>
            <div class="legend">
                <span id="green">Safe</span>
                <span id="orange">Warning</span>
                <span id="red">Banned/Muted</span>
            </div>
        </div>
        <p class="">Your account has the following offences recorded:</p>
        <table class="offences borderedTable">
            <thead>
            <tr>
                <th>Date</th>
                <th>Offence</th>

                <th colspan="2">Evidence</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $lists = dbquery("SELECT * FROM offences WHERE character_id = '$agent->master_id';");
            if (mysql_num_rows($lists) > 0) {
                while ($list = mysql_fetch_assoc($lists)) {
                    $oid = $list['id'];
                    $oreason = $list['reason'];
                    $date = $list['date']; //2016-02-14 03:10:30
                    $timestamp = date('m-M-Y', strtotime($date));

                    ?>
                    <tr>
                        <td><?php echo $timestamp; ?></td>
                        <td class="offenceDetail">
                            <?php echo mb_strimwidth($oreason, 0, 25, "..."); ?>
                        </td>
                        <td colspan="2">
                            <a href="account_history.php?viewid=<?php echo $oid; ?>&step=1">View</a>

                        </td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
        <a target="_parent" href="http://services.runescape.com/m=rswiki/en/Rules%20of%20RuneScape"
           class="Button Button29"><span><span><span
                        class=""><b>View Code of Conduct</b></span></span></span></a>
    </div>
    <?php
} ?>

</body>