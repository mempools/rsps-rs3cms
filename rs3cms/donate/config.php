<?php
define("LOG_FILE", 'log.log');


function write_log($text)
{
    $fp = fopen(LOG_FILE, 'a');
    fwrite($fp, $text . "\n");
    fclose($fp);
}

class Products
{
    private $PRODUCTS = array(
        array(0, "$25. Custom Donator", 25.00),
        array(1, "$20. Extreme Donator", 20.00),
        array(2, "$10. Donator", 10.00),
        array(3, "$3. Contributor (Yell Only)", 3.00),
        array(4, "$10. Dicing Rights", 10.00),
        array(5, "$50. Reward Tokens (10,000)", 50.00),
        array(6, "$30. Reward Tokens (5,000)", 30.00),
        array(7, "$15. Reward Tokens (2,000)", 15.00),
        array(8, "$15. Loyalty Points (5,000)", 15.00),
        array(9, "$10. Slayer Points (1,000)", 10.00),
        array(10, "$30. Torva set (3 Pieces)", 30.00),
        array(11, "$30. Pernix Set (3 Pieces)", 30.00),
        array(12, "$30. Virtus Set (3 Pieces)", 30.00),
        array(13, "$10. Bandos set (2 Pieces)", 10.00),
        array(14, "$25. Armadyl Godsword", 25.00),
        array(15, "$20. Saradomin Godsword", 20.00),
        array(16, "$15. Zamorak Godsword", 15.00),
        array(17, "$10. Bandos Godsword", 10.00),
        array(18, "$22. Black h'ween", 22.00),
        array(19, "$22. White h'ween", 22.00),
        array(20, "$42. Lava h'ween", 42.00),
        array(21, "$45. Dominion crossbow", 45.00),
        array(22, "$100. 'the Ultimate package' includes: 1 set of Torva, Pernix, Virtus,
 2K Reward tokens, 10K Loyalty Points, Extreme donator. | Total separate Value: $160", 100.00),
        array(23, "Testing Donation", 0.00)
    );

    public function get_products()
    {
        return $this->PRODUCTS;
    }

    public function get_product_id($id)
    {
        return $this->PRODUCTS[$id][0];
    }

    public function get_name($id)
    {
        return $this->PRODUCTS[$id][1];
    }

    public function get_price($id)
    {
        return $this->PRODUCTS[$id][2];
    }

}