<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("../global.php");

$show = 5;
$page = 1;

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}

$start_from = ($page - 1) * $show;

if (isset($_GET['cat'])) {
    $cat = $_GET['cat'];
    $total_results = dbevaluate("SELECT COUNT(*) FROM web_news WHERE category_id='$cat';");
    $news_qry = dbquery("SELECT * FROM web_news WHERE category_id='$cat' ORDER BY id DESC LIMIT $start_from, $show;");
} else {
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_news;");
    $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT $start_from, $show;");
}

$total_pages = ceil($total_results / $show);

if ($page < 1 || ($total_pages > 0 && $page > $total_pages)) {
    $url = SITE_ADDRESS . "news";
    exit(header("Location: $url"));
}

require_once("../header.php");

if (!$agent->logged_in) {
    $_SESSION['RD'] = "<?php printf(SITE_ADDRESS); ?>donate";
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}
?>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/news-50.css"/>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Donate</span>
                                    <span class="G0">Donate</span>
                                    <span class="G1" aria-hidden="true">Donate</span>
                                    <span class="G2" aria-hidden="true">Donate</span>
                                    <span class="G3" aria-hidden="true">Donate</span>
                                    <span class="G4" aria-hidden="true">Donate</span>
                                    <span class="G5" aria-hidden="true">Donate</span>
                                    <span class="G6" aria-hidden="true">Donate</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">Donate</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Donate</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <div class="FilterControl">
                        <div id="newsFilterVisible" class="FilterControlVisible">
                            <div id="newsFiltersBg" class="FilterControlSemaphore"></div>
                            <ul>
                                <li <?php if (!isset($cat)) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">All</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/all_categories.png"
                                            alt="All" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 1) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Game Update</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=1"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/game_updates.png"
                                            alt="Game Update" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 2) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Website</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=2"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/website.png"
                                            alt="Website" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 3) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Customer Support</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=3"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/customer_support.png"
                                            alt="Customer Support" title=""></a></li>
                                <li <?php if (isset($cat) && $cat == 4) {
                                    printf("class=\"selected\"");
                                } ?>><span class="ToolTip" style="z-index: 30;">Technical</span><a
                                        href="<?php printf(SITE_ADDRESS); ?>news.php?cat=4"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/technical.png"
                                            alt="Technical" title=""></a></li>
                            </ul>
                        </div>
                        <div id="newsFilterMask" class="FilterControlMask"></div>
                    </div>
                    <div id="NewsFilterDropdown">
                        <h4>Filter By:</h4>

                        <style>
                            #categoryDropdown.InPageDropDownNew.NoJS:hover .DropListWrapper,
                            #categoryDropdown.InPageDropDownNew.CSS3:hover .DropListWrapper {
                                top: -8px;
                            }

                            #categoryDropdown.InPageDropDownNew.NoJS .DropListWrapper,
                            #categoryDropdown.InPageDropDownNew.CSS3 .DropListWrapper {
                                top: -82px;
                            }

                            #categoryDropdown:hover {
                                height: 110px;
                            }
                        </style>
                        <div class="InPageDropDownNew NoJS CSS3 width150" id="categoryDropdown" data-expand="110px">
                            <div class="Mask"></div>
                            <div class="DropListWrapper" data-retract="-82px">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/DropDownFlourish.png"
                                     class="Flourish" alt="">
                                <span class="firstValue">
                                <?php isset($cat) ? printf(news_category($cat)) : printf("All"); ?>
                                </span>
                                <ul>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news">All</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=1">Game Update</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=2">Website</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=3">Customer Support</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>news.php?cat=4">Technical</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <?php require_once("social_feeds.php"); ?>

                    <div id="NewsArticles">


                        <div class="Article">
                            <div>
                                <div class="NewsHeaderImage">
                                    <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg" alt=""
                                         title=""/>

                                    <div></div>
                                </div>

                                <div class="Title">
                                    <a href="#">
                                        <h4 class="FlatHeader">Donate</h4></a>
                                    <br style="clear:both"/>
                                    <span></span>
                                </div>
                                <div class="Content">
                                    <p>Test</p>

                                    <?php /*BEGIN OF DONATION*/
                                    include("donates/config.php");
                                    $products = new Products();

                                    ?>
                                    <form action="<?php printf(SITE_ADDRESS); ?>donates/paypal.php" method="post">
                                        <input type="hidden" name="action" value="0"/>

                                        <div class="form-group">
                                            <label for="product">Username:</label>
                                            <input class="form-control" id="product" type="text" name="username">
                                        </div>
                                        <div class="form-group">
                                            <label for="product">Reward:</label>
                                            <select class="form-control" id="product" name="product_id" width="100">
                                                <?php
                                                foreach ($products->get_products() as $product) {
                                                    echo '<option value="' . $product[0] . '">' . $product[1] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <button type="submit" value="Submit" class="btn btn-success btn-block">Donate
                                        </button>
                                    </form>


                                    <!--end-->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php require_once("footer.php"); ?>
