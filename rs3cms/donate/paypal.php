<?php
require_once("../global.php");
require_once("config.php");
require_once("paypal_class.php");

define("STAGE_URL", 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
define('PAYPAL_EMAIL', 'fuzzy_fizzy@hotmail.com');
define('CURRENCY', 'USD');

$paypal = new paypal_class();
$paypal->paypal_mail = PAYPAL_EMAIL;
$action = $_REQUEST["action"];

function connect()
{
    $connection = mysql_connect(DATABASE_HOST, DATABASE_USER, DATABASE_PASS);
    mysql_select_db(DATABASE_NAME, $connection);
}


switch ($action) {
    case 0:
        $paypal->add_field('business', PAYPAL_EMAIL);
        $paypal->add_field('cmd', '_donations');
        $paypal->add_field('upload', '1');
        $paypal->add_field('return', STAGE_URL . '?action=1');
        $paypal->add_field('notify_url', STAGE_URL . '?action=2');
        $paypal->add_field('cancel_return', STAGE_URL . '?action=3');
        $paypal->add_field('currency_code', CURRENCY);

        $products = new Products();
        $product_id = $_POST["product_id"];
        $username = $_POST["username"];

        $paypal->add_field('item_number', $product_id);
        $paypal->add_field('item_name', $products->get_name($product_id));
        $paypal->add_field('amount', $products->get_price($product_id));
        $paypal->add_field('custom', $product_id . '|' . $username);
        $paypal->submit_paypal_post();
        //$paypal->dump_fields();
        break;

//        echo '<html>';
//        echo $product_id.'<br/>';
//        foreach ($products->get_products() as $product) {
//            echo $product[0] . ' : ' . $product[1] . '<br/>';
//        }
//        break;
//        echo '</html>';

    case 1:
        echo "<h1>Transaction Completed</h1>";

        connect();
        mysql_query("INSERT INTO donation (username, productid, claimed) VALUES ('tet', '33', 0);");
        break;

    case 3:
        echo "<h1>Transaction Cancelled";
        break;

    case 2:
        $payment_status = strtolower($_POST["payment_status"]);
        $custom = explode('|', $_POST['custom']);
        $product_id = $custom[0];
        $username = $custom[1];

        if ($paypal->validate_ipn()) {
            write_log("Successful! " . $username . " : " . $product_id);
            connect();
            mysql_query("INSERT INTO donation (username, productid, claimed) VALUES ('$username', '$product_id', 0);");
        } else {
            write_log("Failed" . $username . " : " . $product_id);
        }
        break;
}