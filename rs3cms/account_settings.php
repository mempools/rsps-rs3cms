<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}

require_once("header.php");
?>

    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/account_settings-51.css"/>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_form-0.js"></script>
    <script>$(function () {
            $('.RaggedBoxToggly')
                .addClass('RaggedBoxTogglyJs')

                .find('.RaggedBoxTitle')
                .hover(
                    function () {
                        $(this).prev('.RaggedBoxToggle').mouseover()
                    },
                    function () {
                        $(this).prev('.RaggedBoxToggle').mouseout()
                    }
                )
                .click(function (ev, do_not_animate) {
                    var $this = $(this);

                    var $box = $this.closest('.RaggedBox');
                    var $bg = $box.find('.RaggedBoxBg');
                    $bg.stop().animate(
                        {
                            marginTop: ($box.hasClass('RaggedBoxClosed') ? 47 + "px" : ((-$box.height() + 81) + "px"))
                        },
                        {duration: (do_not_animate ? 0 : 1000)}
                    );
                    $box
                        .toggleClass('RaggedBoxClosed')
                        .trigger('RaggedBoxToggle', [!$box.hasClass('RaggedBoxClosed')])
                    ;
                })
                .end()
                .filter('.RaggedBoxCloseMe')
                .removeClass('RaggedBoxCloseMe')
                .find('.RaggedBoxTitle')
                .trigger('click', [true]);
            ;
        });</script>
    <script>
        window.resizeRaggedBox = function (iframe, h) {
            var $this = $(iframe);
            var $box = $this.closest('.RaggedBox');
            $this.height(h);
            $box.trigger('RaggedBoxToggle', [!$box.hasClass('RaggedBoxClosed'), true]);
        }
        $(document).ready(function () {
            var module = "";
            if (module === "recovery_questions") module = "recoveries";
            if (module === "password_history") module = "security";
            if (module === "displaynames") module = "charname";
            if (module === "offence-appeal") module = "offence";
            if (module === "email_registration") module = "email";
            if (module === "ticketing") module = "messages";
            if (module === "www") module = "java";
            if (module !== "") {
                $("#" + module).find('.RaggedBoxTitle').trigger("click", true);
            }
        });
    </script>
    <style type="text/css">
        #membership .OrnamentalBoxTop {
            background-image: url("<?php printf(SITE_ADDRESS); ?>img/account_settings/ornamentalBoxrspro_top.png");
        }
    </style>

    <div id="MainContentOuter" class="MyProfileOuter">
        <div class="MyProfile">
            <div class="MainContentBg">
                <div class="MainContentTopBg">
                    <div class="MainContentBottomBg">
                        <div id="MainContent">
                            <div id="MainTitle">
                                <div class="Centre1">
                                    <div class="Centre2">

                                        <h3 class="Gradient DoubleFlourish"><span class="spacing" aria-hidden="true">Account Settings</span>
                                            <span class="G0">Account Settings</span>
                                            <span class="G1" aria-hidden="true">Account Settings</span>
                                            <span class="G2" aria-hidden="true">Account Settings</span>
                                            <span class="G3" aria-hidden="true">Account Settings</span>
                                            <span class="G4" aria-hidden="true">Account Settings</span>
                                            <span class="G5" aria-hidden="true">Account Settings</span>
                                            <span class="G6" aria-hidden="true">Account Settings</span>
                                        <span class="mask"><span class="spacing"
                                                                 aria-hidden="true">Account Settings</span>
                                            <span class="leftInnerFlourish"></span><span
                                                class="centreFlourish"></span><span class="rightInnerFlourish"></span>
                                        </span>
                                        <span class="rightUnderscore">
                                            <img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                                class="right" alt=""/><span class="spacing" aria-hidden="true">Account Settings</span>
                                        </span>
                                            <span class="leftUnderscore"><img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                                    class="left" alt=""/></span>
                                        </h3>
                                    </div>
                                </div>
                            </div>

                            <nav>
                                <div id="MyProfileNavi" class="SubNavigation">
                                </div>
                            </nav>
                            <div class="FilterControl">
                                <div id="newsFilterVisible" class="FilterControlVisible">
                                    <div id="newsFiltersBg" class="FilterControlSemaphore"></div>
                                    <ul>
                                        <li><span class="ToolTip" style="z-index: 30;">Profile</span><a
                                                href="<?php printf(SITE_ADDRESS); ?>profile/<?php echo $agent->username ?>"><img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/adventurers-log.png"
                                                    alt="Profile" title=""></a></li>
                                        <li class="selected"><span class="ToolTip"
                                                                   style="z-index: 30;">Account Settings</span><a
                                                href="<?php printf(SITE_ADDRESS); ?>account_settings"><img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/global/FilterControl/account-settings.png"
                                                    alt="Account Settings" title=""></a></li>
                                    </ul>
                                </div>
                                <div id="newsFilterMask" class="FilterControlMask"></div>
                            </div>
                            <div id="MyName">
                                <b class="FlatHeader" id="playerTitle"><span class="PlayerInfoTitle">Player</span></b>

                                <h2 class="Gradient NoFlourish">
                                    <span class="G0"><?php printf($agent->username); ?></span>
                                    <span class="G1" aria-hidden="true"><?php printf($agent->username); ?></span>
                                    <span class="G2" aria-hidden="true"><?php printf($agent->username); ?></span>
                                    <span class="G3" aria-hidden="true"><?php printf($agent->username); ?></span>
                                    <span class="G4" aria-hidden="true"><?php printf($agent->username); ?></span>
                                    <span class="G5" aria-hidden="true"><?php printf($agent->username); ?></span>
                                    <span class="G6" aria-hidden="true"><?php printf($agent->username); ?></span>
                                </h2>
                            </div>
                            <div id="settingsLeft">

                                <div class="RaggedBox TwoThirds RaggedBoxToggly RaggedBoxCloseMe" id="namechange">
                                    <div class="RaggedBoxHeader">
                                        <div class="RaggedBoxToggle HoverImg"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png"
                                                alt="Toggle visibility" title=""></div>
                                        <h3 class="RaggedBoxTitle HoverText">Change Character Name</h3>
                                    </div>
                                    <div class="RaggedBoxContainer">
                                        <div class="RaggedBoxBg">
                                            <div class="RaggedBoxTop"></div>
                                            <div class="RaggedBoxContent">
                                                <iframe src="account_change_name.php" allowtransparency="true"
                                                        frameborder="0"></iframe>
                                            </div>
                                            <div class="RaggedBoxBottom"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="RaggedBox TwoThirds RaggedBoxToggly RaggedBoxCloseMe" id="namechange">
                                    <div class="RaggedBoxHeader">
                                        <div class="RaggedBoxToggle HoverImg"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png"
                                                alt="Toggle visibility" title=""></div>
                                        <h3 class="RaggedBoxTitle HoverText">Change Password</h3>
                                    </div>
                                    <div class="RaggedBoxContainer">
                                        <div class="RaggedBoxBg">
                                            <div class="RaggedBoxTop"></div>
                                            <div class="RaggedBoxContent">
                                                <iframe src="account_change_password.php" allowtransparency="true"
                                                        frameborder="0"></iframe>
                                            </div>
                                            <div class="RaggedBoxBottom"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="RaggedBox TwoThirds RaggedBoxToggly RaggedBoxCloseMe" id="namechange">
                                    <div class="RaggedBoxHeader">
                                        <div class="RaggedBoxToggle HoverImg"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png"
                                                alt="Toggle visibility" title=""></div>
                                        <h3 class="RaggedBoxTitle HoverText">Change Email</h3>
                                    </div>
                                    <div class="RaggedBoxContainer">
                                        <div class="RaggedBoxBg">
                                            <div class="RaggedBoxTop"></div>
                                            <div class="RaggedBoxContent">
                                                <iframe src="account_change_email.php" allowtransparency="true"
                                                        frameborder="0"></iframe>
                                            </div>
                                            <div class="RaggedBoxBottom"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="RaggedBox TwoThirds RaggedBoxToggly RaggedBoxCloseMe" id="messages">
                                    <div class="RaggedBoxHeader">
                                        <div class="RaggedBoxToggle HoverImgJs"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png"
                                                alt="Toggle visibility" title=""><img style="" class="HoverImgJsFg"
                                                                                      src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png">
                                        </div>
                                        <h3 class="RaggedBoxTitle HoverText">
                                            Messages
                                        </h3>
                                    </div>
                                    <div class="RaggedBoxContainer">
                                        <div style="margin-top: 47px;" class="RaggedBoxBg">
                                            <div class="RaggedBoxTop"></div>
                                            <div class="RaggedBoxContent">
                                                <iframe style="height: 424px;" src="account_messages.php"
                                                        allowtransparency="true" frameborder="0"></iframe>
                                            </div>
                                            <div class="RaggedBoxBottom"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="RaggedBox TwoThirds RaggedBoxToggly RaggedBoxCloseMe" id="offence">
                                    <div class="RaggedBoxHeader">
                                        <div class="RaggedBoxToggle HoverImgJs"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png"
                                                alt="Toggle visibility" title=""><img style="" class="HoverImgJsFg"
                                                                                      src="<?php printf(SITE_ADDRESS); ?>img/global/boxes/raggedBoxToggle.png">
                                        </div>
                                        <h3 class="RaggedBoxTitle HoverText">
                                            Account Status
                                        </h3>
                                    </div>
                                    <div class="RaggedBoxContainer">
                                        <div style="margin-top: 47px;" class="RaggedBoxBg">
                                            <div class="RaggedBoxTop"></div>
                                            <div class="RaggedBoxContent">
                                                <iframe style="height: 424px;" src="account_history.php"
                                                        allowtransparency="true" frameborder="0"></iframe>
                                            </div>
                                            <div class="RaggedBoxBottom"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div id="settingsRight">
                                <div class="OrnamentalBox Width311 SegmentOne" id="membership">
                                    <div class="OrnamentalBoxTop"><h3 class="FlatHeader">Member until 08-Feb-2012
                                            16:37</h3></div>
                                    <div class="OrnamentalBoxTitle">
                                        <div class="Centre1">
                                            <div class="Centre2">
                                                <h3 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                                             aria-hidden="true">Membership</span>
                                                    <span class="G0">Membership</span>
                                                    <span class="G1" aria-hidden="true">Membership</span>
                                                    <span class="G2" aria-hidden="true">Membership</span>
                                                    <span class="G3" aria-hidden="true">Membership</span>
                                                    <span class="G4" aria-hidden="true">Membership</span>
                                                    <span class="G5" aria-hidden="true">Membership</span>
                                                    <span class="G6" aria-hidden="true">Membership</span>
                                                </h3></div>
                                        </div>
                                    </div>
                                    <div class="OrnamentalBoxBg">
                                        <div class="OrnamentalBoxContent">
                                            <a href="https://secure.runescape.com/m=billing_core/g=runescape/c=UHDQd3hNfuk/paymentoptions.ws"
                                               class="Button Button29"><span><span><span class=""><b>Extend
                                                                Membership</b></span></span></span></a><br/>
                                            <a href="https://secure.runescape.com/m=billing_core/g=runescape/c=UHDQd3hNfuk/selectpackage.ws"
                                               class="Button Button29"><span><span><span class=""><b>Purchase Other
                                                                Packages</b></span></span></span></a><br/>
                                            <a href="http://services.runescape.com/m=rswiki/en/Billing_Advice"
                                               class="Button Button29"><span><span><span class=""><b>Billing Support</b></span></span></span></a><br/>
                                        </div>
                                    </div>
                                    <div class="OrnamentalBoxBottom"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once("footer.php"); ?>