<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    // exit(header("Location: home"));
}

if (isset($_GET['username'])) {
    $new_username = $_GET['username'];

    if (strcasecmp($new_username, $agent->username) == 0) {
        exit("1");
    }

    if (startsWith(strtolower($new_username), "iron")) {
        exit("4");
    }

    if (preg_match("/^.{1,12}$/", $new_username) && preg_match("/(^[A-Za-z0-9]{1,12}$)|(^[A-Za-z0-9]+[\-\s][A-Za-z0-9]+[\-\s]{0,1}[A-Za-z0-9]+$)/", $new_username)) {
        $q = dbquery("SELECT username FROM characters WHERE characters.username = '$new_username';");
        if (mysql_num_rows($q) > 0) {
            exit("2");
        }
    } else {
        exit("3");
    }
    exit("1");
}

