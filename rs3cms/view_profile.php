<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
require_once("includes/class_character_statistics.php");

$user_found = false;
if (isset($_GET['searchName'])) {
    $username = filter_for_input($_GET['searchName']);
    $url = SITE_ADDRESS . "profile/" . $username;
    exit(header("Location: $url"));
} else if (isset($_GET['username'])) {
    $username = filter_for_input($_GET['username']);
    if (preg_match("/^.{1,12}$/", $username) && preg_match("/(^[A-Za-z0-9]{1,12}$)|(^[A-Za-z0-9]+[\-\s][A-Za-z0-9]+[\-\s]{0,1}[A-Za-z0-9]+$)/", $username)) {
        $q = dbquery("SELECT * FROM characters WHERE characters.username = '$username' LIMIT 1;");
        if (mysql_num_rows($q) > 0) {
            $cdata = mysql_fetch_array($q);
            $cid = $cdata['id'];

            $q2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.master_id = '$cid' LIMIT 1;");
            if (mysql_num_rows($q2) > 0) {
                $stats_data = mysql_fetch_array($q2);
                $statistics = new character_statistics($stats_data);
                $user_found = true;
                define("PAGE_TITLE", "Viewing profile: " . $cdata['username']);

                // determine overall ranking
                $rank = 0;
                dbquery("SET @rankcount = 0;");
                $rqry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank, total_level, total_exp, master_id FROM characters_statistics ORDER BY total_level DESC, total_exp DESC ) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id='$cid';");

                if (mysql_num_rows($rqry) > 0) {
                    $rdata = mysql_fetch_array($rqry);
                    $rank = $rdata['rank'];
                }
            }
        }
    }
}

require_once("header.php");
?>
<?php if ($user_found) { ?>
<link rel="stylesheet" href="<?php printf(SITE_ADDRESS); ?>css/adventurers_log-52.css"/>
<script src="<?php printf(SITE_ADDRESS); ?>js/adventurers_log/excanvas-51.js"></script>
<script src="<?php printf(SITE_ADDRESS); ?>js/adventurers_log/jquery_visualize_RED_edit-51.js"></script>
<script src="<?php printf(SITE_ADDRESS); ?>js/adventurers_log/adventurers_log-51.js"></script>

<div id="MainContentOuter" class="MyProfileOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Profile</span>
                                    <span class="G0">Profile</span>
                                    <span class="G1" aria-hidden="true">Profile</span>
                                    <span class="G2" aria-hidden="true">Profile</span>
                                    <span class="G3" aria-hidden="true">Profile</span>
                                    <span class="G4" aria-hidden="true">Profile</span>
                                    <span class="G5" aria-hidden="true">Profile</span>
                                    <span class="G6" aria-hidden="true">Profile</span>
                                        <span class="mask"><span class="spacing" aria-hidden="true">Profile</span>
                                            <span class="leftInnerFlourish"></span><span
                                                class="centreFlourish"></span><span class="rightInnerFlourish"></span>
                                        </span>
                                        <span class="rightUnderscore">
                                            <img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                                class="right" alt=""/><span class="spacing"
                                                                            aria-hidden="true">Profile</span>
                                        </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <nav>
                        <div id="MyProfileNavi" class="SubNavigation">
                        </div>
                    </nav>
                    <div id="MyName">
                        <b class="FlatHeader" id="playerTitle"><span
                                class="PlayerInfoTitle"><?php printf($cdata['online'] > 0 ? "Online" : "Offline"); ?></span></b>

                        <h2 class="Gradient NoFlourish">
                            <span class="G0"><?php printf($cdata['username']); ?></span>
                            <span class="G1" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                            <span class="G2" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                            <span class="G3" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                            <span class="G4" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                            <span class="G5" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                            <span class="G6" aria-hidden="true"><?php printf($cdata['username']); ?></span>
                        </h2>
                    </div>
                    <form class="search" action="<?php printf(SITE_ADDRESS); ?>profile" method="get">

                        <div class="EncrustedInputBoxWrapper">
                            <div class="InputBoxLeft"><input type='text' title="Search for a player" name='searchName'
                                                             maxlength='12' size='41' value="" id='searchName'></div>
                            <div class="InputBoxRight"></div>
                        </div>
                        <span class="Button Button29"><span><span><span class=""><b>Find</b></span><input type="submit"
                                                                                                          title="Find"/></span></span></span>
                    </form>
                    <div class="adventurerslog">
                        <div class="primaryContent overall">
                            <ul>
                                <li><h5>Level:</h5> <strong
                                        class="stat level"><?php printf(number_format($statistics->get_total_levels())); ?></strong>
                                </li>
                                <li><h5>Rank:</h5> <strong><?php printf($rank); ?></strong></li>
                                <li><h5>Total XP:</h5>
                                    <strong><?php printf(number_format($statistics->get_total_exp())); ?></strong></li>
                            </ul>
                            <h4 class="FlatHeader">Stats:</h4>
                            <ul>
                                <li class="stat attack"><h5>Attack Level:</h5>
                                    <strong><?php printf($statistics->get_skill_level(character_statistics::attack)); ?></strong>
                                </li>
                                <li class="stat defence"><h5>Defence Level:</h5>
                                    <strong><?php printf($statistics->get_skill_level(character_statistics::defence)); ?></strong>
                                </li>
                                <li class="stat strength"><h5>Strength Level:</h5>
                                    <strong><?php printf($statistics->get_skill_level(character_statistics::strength)); ?></strong>
                                </li>
                                <li class="stat constitution"><h5>Constitution Level:</h5>
                                    <strong><?php printf($statistics->get_skill_level(character_statistics::constitution)); ?></strong>
                                </li>
                            </ul>
                            <h4 class="FlatHeader loyaltyHeader">Loyalty:</h4> <a
                                href="http://services.runescape.com/m=loyalty_points/g=runescape/c=yMVR0ggBiQo/"
                                class="Button Button29 loyaltyMore"><span><span><span
                                            class=""><b>?</b></span></span></span></a>
                            <ul>
                                <li>
                                    <h5>Points:</h5>
                                    <?php
                                    $username = filter_for_input($_GET['username']);
                                    $i = agent::id_from_name($username);
                                    $q = dbquery("SELECT characters_statistics.`loyalty_points` FROM characters_statistics WHERE characters_statistics.`master_id` = '$i' LIMIT 1;");
                                    if (mysql_num_rows($q) > 0) {
                                        $cdata = mysql_fetch_array($q);
                                        $loylty = $cdata['loyalty_points'];
                                    }

                                    ?>
                                    <strong><?php echo $loylty; ?></strong>
                                </li>
                                <li>
                                    <h5>Equipped Aura:</h5>
                                    <strong>None</strong>
                                </li>
                            </ul>
                        </div>

                        <div class="OrnamentalBox Width311 SegmentOne secondaryContent" id="skills">
                            <div class="OrnamentalBoxTop"></div>
                            <div class="OrnamentalBoxTitle">
                                <div class="Centre1">
                                    <div class="Centre2">
                                        <h3 class="Gradient NoFlourish Centre"><span class="spacing" aria-hidden="true">Top Skills</span>
                                            <span class="G0">Top Skills</span>
                                            <span class="G1" aria-hidden="true">Top Skills</span>
                                            <span class="G2" aria-hidden="true">Top Skills</span>
                                            <span class="G3" aria-hidden="true">Top Skills</span>
                                            <span class="G4" aria-hidden="true">Top Skills</span>
                                            <span class="G5" aria-hidden="true">Top Skills</span>
                                            <span class="G6" aria-hidden="true">Top Skills</span>
                                        </h3></div>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBg">
                                <div class="OrnamentalBoxContent">
                                    <div class="FilterControl" style="display: block; height: 448px;">
                                        <div id="newsFilterVisible" class="FilterControlVisible">
                                            <div id="newsFiltersBg" class="FilterControlSemaphore"></div>
                                            <ul>

                                                <li class="skill-group-tab selected"><span class="ToolTip"
                                                                                           style="z-index: 30;">Top Skills</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/top-skills.png"
                                                            alt="Top Skills" title=""></a></li>
                                                <li class="skill-group-tab"><span class="ToolTip" style="z-index: 30;">Gathering</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/gathering.png"
                                                            alt="Gathering" title=""></a></li>
                                                <li class="skill-group-tab"><span class="ToolTip" style="z-index: 30;">Combat</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/combat.png"
                                                            alt="Combat" title=""></a></li>
                                                <li class="skill-group-tab"><span class="ToolTip" style="z-index: 30;">Support</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/support.png"
                                                            alt="Support" title=""></a></li>
                                                <li class="skill-group-tab"><span class="ToolTip" style="z-index: 30;">Artisan</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/artisan.png"
                                                            alt="Artisan" title=""></a></li>
                                                <li class="skill-group-tab"><span class="ToolTip" style="z-index: 30;">Members' Skills</span><a><img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/members-only.png"
                                                            alt="Members' Skills" title=""></a></li>
                                            </ul>
                                        </div>
                                        <div id="newsFilterMask" class="FilterControlMask"></div>
                                    </div>

                                    <ul class="skills-matrix">
                                        <?php
                                        $top_skills = $statistics->get_top_skills();

                                        for ($i = 0; $i < 9; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($top_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($top_skills[$i]->id);
                                            $skill_level = $top_skills[$i]->level;
                                            $skill_experience = $top_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                    <ul class="skills-matrix selected">
                                        <?php
                                        $gathering_skills = $statistics->get_gathering_skills();

                                        for ($i = 0; $i < 5; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($gathering_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($gathering_skills[$i]->id);
                                            $skill_level = $gathering_skills[$i]->level;
                                            $skill_experience = $gathering_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                    <ul class="skills-matrix selected">
                                        <?php
                                        $comnbat_skills = $statistics->get_combat_skills();

                                        for ($i = 0; $i < 8; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($comnbat_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($comnbat_skills[$i]->id);
                                            $skill_level = $comnbat_skills[$i]->level;
                                            $skill_experience = $comnbat_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                    <ul class="skills-matrix selected">
                                        <?php
                                        $support_skills = $statistics->get_support_skills();

                                        for ($i = 0; $i < 4; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($support_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($support_skills[$i]->id);
                                            $skill_level = $support_skills[$i]->level;
                                            $skill_experience = $support_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                    <ul class="skills-matrix selected">
                                        <?php
                                        $artisan_skills = $statistics->get_artisan_skills();

                                        for ($i = 0; $i < 8; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($artisan_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($artisan_skills[$i]->id);
                                            $skill_level = $artisan_skills[$i]->level;
                                            $skill_experience = $artisan_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>

                                    <ul class="skills-matrix selected">
                                        <?php
                                        $member_skills = $statistics->get_member_skills();

                                        for ($i = 0; $i < 9; $i++) {
                                            $img_url = SITE_ADDRESS . "img/adventurers-log/skills/" . strtolower(character_statistics::get_skill_name($member_skills[$i]->id)) . ".png?2";
                                            $skill_name = character_statistics::get_skill_name($member_skills[$i]->id);
                                            $skill_level = $member_skills[$i]->level;
                                            $skill_experience = $member_skills[$i]->experience;
                                            ?>
                                            <li class="TipBubble-Parent">
                                                <img src="<?php printf($img_url); ?>"
                                                     alt="<?php printf($skill_name); ?>">

                                                <div
                                                    class="progress<?php printf($skill_level == 99 ? " complete" : ""); ?>"
                                                    data-value="<?php printf($skill_level == 99 ? "100" : $skill_level); ?>"><?php printf($skill_level); ?>
                                                    % Complete
                                                </div>
                                                <div class="TipBubble">
                                                    <h6><?php printf($skill_name); ?></h6>

                                                    <p>Level <?php printf($skill_level); ?></p>

                                                    <p><?php printf(number_format($skill_experience)); ?> XP</p>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBottom"></div>
                        </div>
                        <img src="<?php printf(SITE_ADDRESS); ?>img/adventurers-log/character-placeholder.png" alt=""
                             class="character">

                        <div class="OrnamentalBox Width923 SegmentTwo game-progress tertiaryContent" id="">
                            <div class="OrnamentalBoxTop"></div>
                            <div class="OrnamentalBoxTitle">
                                <div class="Centre1">
                                    <div class="Centre2">
                                        <h3 class="Gradient NoFlourish Centre"><span class="spacing" aria-hidden="true">Information</span>
                                            <span class="G0">Information</span>
                                            <span class="G1" aria-hidden="true">Information</span>
                                            <span class="G2" aria-hidden="true">Information</span>
                                            <span class="G3" aria-hidden="true">Information</span>
                                            <span class="G4" aria-hidden="true">Information</span>
                                            <span class="G5" aria-hidden="true">Information</span>
                                            <span class="G6" aria-hidden="true">Information</span>
                                        </h3></div>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBg">
                                <div class="OrnamentalBoxContent">
                                    <div class="block recent-activity">
                                        <table>
                                            <caption>Recent Activity</caption>
                                            <thead>
                                            <tr>
                                                <th class="description" scope="col">Description</th>
                                                <th scope="col">Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $activities = dbquery("SELECT * FROM activity_logs WHERE master_id = '$cid' LIMIT 4;");
                                            if (mysql_num_rows($activities) > 0) {
                                                while ($activity = mysql_fetch_assoc($activities)) {
                                                    $activity_date = $activity['date'];
                                                    $short_desc = $activity['short_desc'];
                                                    $full_desc = $activity['full_desc'];
                                                    ?>
                                                    <tr class="triggerMoreInfo">
                                                        <th scope="row">
                                                            <div class="eventsWrapper">
    <span
        class="eventTitle"><?php printf($short_desc); ?></span>

                                                                <div class="eventsInfoBubble">
                                                                    <div class="top"></div>
                                                                    <div
                                                                        class="middle"><?php printf($full_desc); ?></div>
                                                                    <div class="bottom"></div>
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <td class="date"><?php printf($activity_date); ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                        <a class="HoverImg NoFade" target="_blank" title="RSS"
                                           href="http://services.runescape.com/m=adventurers-log/g=runescape/c=yMVR0ggBiQo/rssfeed?searchName=darksteve100"><img
                                                alt="RSS" src="http://www.runescape.com/img/global/logos/MediumRss.png"></a>
                                    </div>
                                    <div class="friendListTable block block-right friends-list">
                                        <div class="viewport scroll-pane">
                                            <h6 class="caption">Friends List</h6>
                                            <table>
                                                <thead>
                                                <tr>
                                                    <th class="description" scope="col">Name</th>
                                                    <th scope="col">Donator</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                                </thead>
                                                <tbody class="adventurerslog">
                                                <?php
                                                define("AVATAR_IMG", SITE_ADDRESS . "img/avatars/default.png");
                                                define("MEMBER_IMG", SITE_ADDRESS . "img/adventurers-log/member.png");
                                                define("FREE_IMG", SITE_ADDRESS . "img/adventurers-log/free.png");
                                                define("OFFLINE_IMG", SITE_ADDRESS . "img/global/myprofile/icon-status-red.png");
                                                define("ONLINE_IMG", SITE_ADDRESS . "img/global/myprofile/icon-status-green.png");

                                                $friends = dbquery("SELECT characters.`username`, characters.`online` FROM characters_contacts LEFT JOIN characters ON characters_contacts.contact_id = characters.id WHERE characters_contacts.master_id = '$cid';");
                                                if (mysql_num_rows($friends) > 0) {
                                                    while ($friend = mysql_fetch_assoc($friends)) {
                                                        $fname = $friend['username'];
                                                        $fonline = $friend['online'];
                                                        $furl = SITE_ADDRESS . "profile/" . $fname;
                                                        ?>
                                                        <tr>
                                                            <th scope="row">
                                                                <a href="<?php printf($furl); ?>"><?php printf($fname); ?></a>
                                                            </th>
                                                            <td>
            <span class="icon">
                <img src="<?php printf(MEMBER_IMG); ?>"
                     alt="Donator" title="Donator">
            </span>
                                                            </td>
                                                            <td>
            <span class="PlayerInfoOnline">
                <img
                    src="<?php printf($fonline > 0 ? ONLINE_IMG : OFFLINE_IMG); ?>"
                    title="<?php printf($fonline > 0 ? ($fonline > 0 ? ("World " . $fonline) : "Lobby") : "Not Online"); ?>"
                    alt="<?php printf($fonline > 0 ? "Online" : "Offline"); ?>"/>
            </span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBottom"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } else { ?>
    <link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/error-50.css"/>
    <div id="MainContentOuter">
        <div class="MainContentBg">
            <div class="MainContentTopBg">
                <div class="MainContentBottomBg">
                    <div id="MainContent">
                        <div id="MainTitle">
                            <div class="Centre1">
                                <div class="Centre2">

                                    <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                              aria-hidden="true">Error</span>
                                        <span class="G0">Error</span>
                                        <span class="G1" aria-hidden="true">Error</span>
                                        <span class="G2" aria-hidden="true">Error</span>
                                        <span class="G3" aria-hidden="true">Error</span>
                                        <span class="G4" aria-hidden="true">Error</span>
                                        <span class="G5" aria-hidden="true">Error</span>
                                        <span class="G6" aria-hidden="true">Error</span>
                                            <span class="mask"><span class="spacing" aria-hidden="true">Error</span>
                                                <span class="leftInnerFlourish"></span><span
                                                    class="centreFlourish"></span><span
                                                    class="rightInnerFlourish"></span>
                                            </span>
                                            <span class="rightUnderscore">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                                    class="right" alt=""/><span class="spacing"
                                                                                aria-hidden="true">Error</span>
                                            </span>
                                        <span class="leftUnderscore"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                                class="left" alt=""/></span>
                                    </h3>
                                </div>
                            </div>
                        </div>


                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h2 class="Gradient">
                                    <span class="G0">Account Not Found</span>
                                    <span class="G1" aria-hidden="true">Account Not Found</span>
                                    <span class="G2" aria-hidden="true">Account Not Found</span>
                                    <span class="G3" aria-hidden="true">Account Not Found</span>
                                    <span class="G4" aria-hidden="true">Account Not Found</span>
                                    <span class="G5" aria-hidden="true">Account Not Found</span>
                                    <span class="G6" aria-hidden="true">Account Not Found</span>
                                        <span class="mask"><span class="spacing">Account Not Found</span>
                                            <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Account Not Found</span></span>
                                        </span>
                                        <span class="rightUnderscore">
                                            <img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                                class="right" alt=""/><span class="spacing" aria-hidden="true">Account Not Found</span>
                                        </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>This account either does not exist or has set their profile to private.</p>

                                    <p>Please check the account name and try again.</p>

                                    <form action="<?php printf(SITE_ADDRESS); ?>profile" method="get"
                                          style="margin-top: 20px;">

                                        <div class="EncrustedInputBoxWrapper">
                                            <div class="InputBoxLeft"><input type='text' title="Search for a player"
                                                                             name='searchName' maxlength='12' size='56'
                                                                             value="" id='searchName'></div>
                                            <div class="InputBoxRight"></div>
                                        </div>
                                        <span style="margin: 9px 0 0 10px; float: left;"><span
                                                class="Button Button29"><span><span><span
                                                            class=""><b>Find</b></span><input type="submit"
                                                                                              title="Find"/></span></span></span></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        }
        require_once("footer.php");
        ?>
