<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <LINK REL="SHORTCUT ICON" HREF="favicon.ico">

    <script type="text/javascript">
        var memory = {
            loggedinstate: false,
            app: document.getElementById('applet'),
            advert: document.getElementById('advertIframe'),
            browser: {
                agent: navigator.userAgent.toLowerCase(),
                platform: navigator.platform.toLowerCase()
            }
        };
        var tools = {
            addClass: function (obj, clas) {
                var new_class = clas + ' ';
                if (obj.className.indexOf(clas) == '-1') obj.className += new_class;
            },
            removeClass: function (obj, clas) {
                var new_class = clas + ' ';
                if (obj.className.indexOf(clas) != '-1') obj.className = obj.className.replace(new_class, '');
            }
        }
        function oul() {
            if (memory.loggedinstate) {
                return 'Please make sure you have logged out of the game before navigating away.';
            }
        }

        window.onbeforeunload = oul;

        function loggedin() {
            memory.loggedinstate = true;
        }
        function loggedout() {
            memory.loggedinstate = false;
        }
        window.scroll(0, 0);

        if (memory.browser.platform.indexOf('mac') != -1 && memory.browser.agent.indexOf('firefox') > -1) {
            document.getElementById('upgrade').onclick = upgradejs;
            document.getElementById('jagex').onclick = jagexjs;
        }
        function upgradejs(e) {
            return openjs(document.getElementById('upgrade').href);
        }
        function jagexjs(e) {
            return openjs(document.getElementById('jagex').href);
        }
        function opensn(href) {
            return openjs(href, 630, 330);
        }
        function openjs(href) {
            return openjs(href, 825, 500);
        }
        function openjs(href, width, height) {
            window.focus();
            var win = window.open(href, '_blank', 'width=' + width + ',height=' + height + ',scrollbars=yes');
            if (win) win.focus();
            return false;
        }

        function zap() {
            try {
                tools.addClass(document.body, 'members');
                if (memory.advert) memory.advert.src = 'blank.ws';
            } catch (err) {
            }
        }
        function unzap() {
            try {
                tools.removeClass(document.body, 'members');
                if (memory.advert) memory.advert.src = tb_url;
            } catch (err) {
            }
        }

        function tbrefresh() {
            try {
                memory.advert.contentWindow.location.reload();
            } catch (x) {
            }
        }

        function accountcreated() {
            var ifrm = document.getElementById("notify");
            if (ifrm) ifrm.src = "https://secure.runescape.com/m=create/applet_created.ws";
        }

        function accountcreatestarted() {
            try {
                pageTracker._trackPageview("https://secure.runescape.com/m=create/applet_start.ws")
            } catch (x) {
            }
        }

        function resize(width, height, windowMode, maxRes) {
            if (windowMode != 1) {
                tools.addClass(document.body, 'resize');
            }
            else {
                tools.removeClass(document.body, 'resize');
            }
        }
    </script>
    <title>Play</title>
    <style type="text/css">
        html {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            text-align: center;
            font-size: 14px;
            font-family: Arial, Helvetica, FreeSans, sans-serif;
            background: black;
            color: white;
            width: 100%;
            height: 100%;
        }

        .minheight {
            height: 503px;
            width: 0;
        }
    </style>
</head>
<body>
<applet code="Loader.class" archive="clientfiles/client_s.jar" width="100%" height="100%">
    <param name="ip" value="http://192.168.1.100">
    <param name="colourid" value="0">
    <param name="worldid" value="1">
    <param name="lobbyid" value="1">
    <param name="lobbyaddress" value="192.168.1.100">
    <param name="demoid" value="0">
    <param name="demoaddress" value="">
    <param name="modewhere" value="0">
    <param name="modewhat" value="0">
    <param name="lang" value="0">
    <param name="objecttag" value="0">
    <param name="js" value="1">
    <param name="game" value="0">
    <param name="affid" value="0">
    <param name="advert" value="1">
    <param name="settings" value="wwGlrZHF5gJcZl7tf7KSRh0MZLhiU0gI0xDX6DwZ-Qk">
    <param name="country" value="0">
    <param name="haveie6" value="0">
    <param name="havefirefox" value="1">
    <param name="cookieprefix" value="">
    <param name="cookiehost" value="google.com">
    <param name="cachesubdirid" value="0">
    <param name="crashurl" value="">
    <param name="unsignedurl" value="">
    <param name="sitesettings_member" value="1">
    <param name="frombilling" value="false">
    <param name="sskey" value="">
    <param name="force64mb" value="false">
    <param name="worldflags" value="8">

    Java is not installed on your machine or your browser does not allowed Java Applet to run<br/><br/>Get the latest
    Java technology at <a href="http://www.java.com/">http://www.java.com/</a>
</applet>
</body>
</html>
