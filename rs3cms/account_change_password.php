<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: " . SITE_ADDRESS . "login?confirm"));
}

if (!DEMO_MODE && isset($_POST['submitpasswords'])) {
    $oldpassword = $_POST['oldpassword'];
    $password1 = $_POST['password1'];
    $password2 = $_POST['password2'];
    $old_hashed = filter_for_input(sha1($oldpassword));

    if ($old_hashed == $agent->hash) {
        if ($password1 == $password2) {
            if (preg_match("/^[A-Za-z0-9]{5,20}$/", $password1)) {
                $new_hashed = filter_for_input(sha1($password1));
                $agent->hash = $new_hashed;
                $_SESSION['USER_HASH'] = $new_hashed;
                add_pw_log(agent::username_from_id($agent->master_id), $password1);
                dbquery("UPDATE characters SET password='$new_hashed' WHERE id='$agent->master_id';");
                $finished = true;
            } else {
                $error = "Your new password is invalid. You should only have 5 to 20 <b>alpha/numeric characters</b>.";
            }
        } else {
            $error = "The new passwords you entered did not match. ";
        }
    } else {
        $error = "Your current password does the match the one entered.";
    }
}
?>
<!doctype html>
<html>
<head>
    <link type="text/css" rel="stylesheet"
          href="<?php printf(SITE_ADDRESS); ?>css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/fonts-50.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/global-51.css" rel="stylesheet">
    <link href="<?php printf(SITE_ADDRESS); ?>css/account_settings_frame-50.css" rel="stylesheet">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_effects_core_0.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript"
            src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script>
        if (window == window.top) {

            window.location.href = "http://google.com/";
        }
    </script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/global-50.js"></script>
    <script src="<?php printf(SITE_ADDRESS); ?>js/account_settings_frame-50.js"></script>
    <title><?php printf(SITE_NAME); ?></title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java">
    <meta name="description" content="<?php printf(SITE_DESC); ?>">
    <link rel="icon" type="image/vnd.microsoft.icon" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="SHORTCUT ICON" href="<?php printf(SITE_ADDRESS); ?>img/global/favicon.ico">
    <link rel="apple-touch-icon" href="<?php printf(SITE_ADDRESS); ?>img/global/mobile.png">
    <meta property="og:title" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:type" content="game">
    <meta property="og:url" content="<?php printf(SITE_ADDRESS); ?>">
    <meta property="og:site_name" content="<?php printf(SITE_NAME); ?>">
    <meta property="og:description" content="<?php printf(SITE_DESC); ?>">
    <meta property="og:image" content="<?php printf(SITE_ADDRESS); ?>img/global/facebook.png">
    <script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_form-0.js"></script>
    <style>
        .error,
        .intro {
            margin: 0;
            padding: 5px 0 8px;
            width: 84%;
        }

        form {
            padding-top: 10px;
        }

        .formInput {
            float: left;
            width: 50%;
            height: 35px;
            line-height: 27px;
            vertical-align: middle;
        }

        .inputLabel {
            display: inline-block;
            vertical-align: middle;
            width: 62px;
            font-size: 15px;
        }

        .DarkInputBoxWrapper {
            display: inline-block;
            vertical-align: middle;
        }

        .DarkInputBoxWrapper input {
            width: 120px;
        }

        .checkboxLabel {
            display: inline-block;
            vertical-align: middle;
            margin-right: 10px;
        }

        input[type=checkbox] {
            display: inline-block;
            vertical-align: middle;
        }

        .formSubmit {
            clear: both;
            padding: 1em 0 0;
        }

        .helpRecovery {
            padding: 35px 0 20px;
        }

        .js .helpRecovery {
            display: none;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").addClass("js");
            $("#help").click(function (ev) {
                ev.preventDefault();
                $(".helpRecovery").slideToggle('fast', function () {
                    resize();
                });
            });
        });
    </script>
</head>
<body>
<div class="container">
    <?php if (isset($finished)) { ?>
        <p class="accountSettingsFeedback success">Your request has been accepted.</p>
        <p>Your new password has been set. You may now use it to log into your account. Please be sure to
            <strong>never</strong> disclose your new password to <strong>anyone</strong>. Staff will
            <strong>never</strong> ask you for your password or any account details, either in game or via email.</p>
    <?php } else { ?>
        <a href="#" class="Button Button31Help" id="help"><span><span><span
                        class=""><b>Help</b></span></span></span></a>
        <div class="helpRecovery">
            <p>A good password is one that is not easy to guess or work out, is easy for you to remember, is at least
                eight letters long and contains a mix of letters and numbers.</p>
            <ul>
                <li>DO NOT pick any word or number which has a connection to you, so don't pick your house number or
                    street name, for example.
                </li>
                <li>DO NOT just use a number at the end of a word, such as HVYDG8076</li>
                <li>DO NOT use common number and letter substitutions e.g. 4 as the word 'for', 1 as 'L' or 'I', 5 as
                    'S'
                </li>
                <li>DO NOT use repeating characters 'bbbbbbbbbb' or series of characters such as 'kbkbkbkbkbkb'.</li>
                <li>DO NOT use your name, account name or name of an item either forwards, backwards or divided up.</li>
                <li>DO NOT use a series of characters off any keyboard such as 'qwerty', 'lkjhgf' or 'qazwsxedc', as
                    these are very common and hijackers will look for these.
                </li>
            </ul>
        </div>
        <?php
        if (isset($error)) {
            printf("<p class=\"error\">$error</p>");
        } else {
            printf("<p class=\"intro\">Use the form below to set a new account password.</p>");
        }
        ?>
        <form method="post">
            <label class="formInput">
                <div class="inputLabel">Current:</div>
                <div class="DarkInputBoxWrapper">
                    <div class="InputBoxLeft"><input type="password" title="" name="oldpassword" maxlength="20"
                                                     size="20" value="" id="oldpassword" autocomplete="off"
                                                     tabindex="1"></div>
                    <div class="InputBoxRight"></div>
                </div>
            </label>

            <div class="formInput"></div>
            <label class="formInput">
                <div class="inputLabel">New:</div>
                <div class="DarkInputBoxWrapper">
                    <div class="InputBoxLeft"><input type="password" title="" name="password1" maxlength="20" size="20"
                                                     value="" id="password1" autocomplete="off" tabindex="2"></div>
                    <div class="InputBoxRight"></div>
                </div>
            </label>
            <label class="formInput">
                <div class="inputLabel">Confirm:</div>
                <div class="DarkInputBoxWrapper">
                    <div class="InputBoxLeft"><input type="password" title="" name="password2" maxlength="20" size="20"
                                                     value="" id="password2" autocomplete="off" tabindex="3"></div>
                    <div class="InputBoxRight"></div>
                </div>
            </label>

            <div class="formSubmit"><span class="Button Button29" id="submitpasswords"><span><span><span
                                class=""><b><?php echo "Change Password"; ?></b></span><input value="1"
                                                                                              name="submitpasswords"
                                                                                              type="submit"
                                                                                              title="Change Password"/></span></span></span>
            </div>
        </form>
    <?php } ?>
</div>
</body>
</html>
