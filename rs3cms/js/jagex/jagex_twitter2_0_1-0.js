var JAGEX = JAGEX || {};
JAGEX.Twitter2 = function (options) {

    var
        _this = this,

        options = $.extend({
            rts: true,
            replies: false,
            show: 3,
            timeout: 10000
        }, options)
        ;
    if (typeof(options.failed) !== 'function') {
        options.failed = null;
    }


    if (!options.user) {
        return "No user specified";
    }
    if (typeof(options.success) !== 'function') {
        return "No output function";
    }


    var init = function () {
        _this.update();
    }


    var displayTweets = function (tweets) {

        var out = [];

        var now = new Date();

        for (var i = 0; i < tweets.length && i < options.show; i++) {

            var tweet = tweets[i];

            var
                text_raw = tweet.text,
                user = tweet.user
                ;

            if (tweet.retweeted_status) {
                text_raw = tweet.retweeted_status.text;
                user = tweet.retweeted_status.user;
            }


            text = parseTweet(text_raw);

            var created = new Date(tweet.created_at);
            var ago = timeAgo(now, created);


            out.push({
                user: {
                    name: user.name,
                    image: user.profile_image_url,
                    image_secure: user.profile_image_url_https,
                    description: user.description
                },
                time: created,
                time_ago: ago,
                tweet: text,
                tweet_raw: text_raw
            });

        }

        options.success(out);

    }


    var parseTweet = function (tweet) {

        /* Many thanks to Simon Whatley for this code, http://www.simonwhatley.co.uk/parsing-twitter-usernames-hashtags-and-urls-with-javascript */


        tweet = tweet.replace(/[A-Za-z]+:\/\/[A-Za-z0-9-_]+\.[A-Za-z0-9-_:%&~\?\/.=]+/g, function (url) {
            return url.link(url);
        });


        tweet = tweet.replace(/[@]+[A-Za-z0-9-_]+/g, function (u) {
            var username = u.replace("@", "")
            return u.link("http://twitter.com/" + username);
        });


        tweet = tweet.replace(/[@]+[A-Za-z0-9-_]+/g, function (u) {
            var username = u.replace("@", "")
            return u.link("http://twitter.com/" + username);
        });


        tweet = tweet.replace(/[#]+[A-Za-z0-9-_]+/g, function (t) {
            var tag = t.replace("#", "%23")
            return t.link("http://search.twitter.com/search?q=" + tag);
        });

        return tweet;
    }


    var timeAgo = function (now, then) {
        var
            periods = ["second", "minute", "hour", "day", "week", "month", "year", "decade"],
            lengths = [60, 60, 24, 7, 4.35, 12, 10]
            ;

        var delta = (now - then) / 1000;

        var i;
        for (i = 0; delta >= lengths[i] && i < lengths.length - 1; i++) {
            delta /= lengths[i];
        }

        delta = Math.round(delta);

        return "" + delta + " " + periods[i] + (delta != 1 ? 's' : '');
    }

    var failed = function (reason) {
        if (options.error) options.error(reason);
    }


    this.update = function () {
        $.ajax({
            url: "https://api.twitter.com/1/statuses/user_timeline.json?screen_name=" + options.user + "&count=" + options.show + (options.rts ? "&include_rts=1" : "") + (!options.replies ? "&exclude_replies=true" : "") + "&callback=?",
            dataType: 'jsonp',
            success: function (tweets) {
                if (tweets.length > 0) {

                    displayTweets(tweets);
                }
                else {
                    failed('No tweets');
                }
            },
            error: function (jqXHR, error) {
                failed(error);
            },
            timeout: options.timeout
        })
    }


    this.options = function (_options) {
        options = $.extend(_options, options);
    }


    init();

};