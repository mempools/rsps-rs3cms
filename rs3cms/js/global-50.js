var RSGLOBAL = {};

RSGLOBAL.hoverImageReplace = function (domObject) {

    if (!domObject.find("img").attr("src")) {
        var originalColor = domObject.css("color");
        domObject
            .hover(function (event) {
                event.preventDefault();
            })
            .mouseover(function (event) {
                event.preventDefault();
                $(this).stop().animate({
                    color: "#FFFFFF"
                }, 300);
            })
            .mouseout(function () {
                $(this).stop().animate({
                    color: originalColor
                }, 300);
            });
    }
    else {


        var imgObj = new Image();
        imgObj.src = domObject.find("img").attr("src");
        var myDiv;
        if (imgObj.src.toUpperCase().split(".PNG").length > 0) {
            myDiv = RSGLOBAL.pngOpacityFix(imgObj);
        }
        else {
            myDiv = imgObj;
        }

        $(myDiv).addClass("HoverImgJsFg");
        domObject.find("img").after(myDiv);


        domObject
            .removeClass("HoverImg")
            .addClass("HoverImgJs");
    }
}

RSGLOBAL.pngOpacityFix = function (pngImage) {
    var myObject;


    if ($.browser.msie && $.browser.version < 9) {
        myObject = $("<div></div>").css({
            width: pngImage.width,
            height: pngImage.height,
            'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,sizingMethod=crop,src=' + pngImage.src + ')'
        });
    } else {
        myObject = pngImage;
    }

    return myObject;
}

RSGLOBAL.pngOpacityBgSwap = function (pngImageDiv) {
    if ($.browser.msie && $.browser.version < 9) {
        var backgroundImage = pngImageDiv.css("background-image").match(/\"(.*?)\"/)[1];
        var backPos = pngImageDiv.css("background-position");
        pngImageDiv.css({
            'background-image': 'none',
            'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,sizingMethod=crop,src=' + backgroundImage + ')'
        });
    }
}

RSGLOBAL.slowClassSelecter = ($.browser.msie && $.browser.version < 9 );

RSGLOBAL.playerInfoAjax = function () {


    var playernames = [];
    var span = RSGLOBAL.slowClassSelector ? 'span' : '';

    var titleElements = $(span + ".PlayerInfoTitle");
    var memberElements = $(span + ".PlayerInfoMember");
    var onlineElements = $(span + ".PlayerInfoOnline");
    var inclanElements = $(".PlayerInfoInClan");

    if (titleElements.length <= 0 && memberElements.length <= 0 && onlineElements.length <= 0) {
        return false;
    }

    titleElements.each(function () {
        playernames.push($(this).data("displayname"));
    });
    memberElements.each(function () {
        playernames.push($(this).data("displayname"));
    });
    onlineElements.each(function () {
        playernames.push($(this).data("displayname"));
    });
    inclanElements.each(function () {
        playernames.push($(this).data("displayname"));
    });

    function process(data) {
        var counter = 0;
        titleElements.each(function () {
            var that = $(this);
            if (data[counter].title != "") {
                that.text(data[counter].title);
            }
            counter++;
        });
        memberElements.each(function () {
            var that = $(this);
            if (data[counter].member === true) {
                that.html('<img src="http://www.runescape.com/img/global/myprofile/icon-pro.png" title="" alt="Member">');
            }
            else {
                that.html('<img src="http://www.runescape.com/img/global/myprofile/icon-nopro.png" title="" alt="No Member">');
            }
            counter++;
        });
        onlineElements.each(function () {
            var that = $(this);
            if (data[counter].online === true) {
                that.html('<img src="http://www.runescape.com/img/global/myprofile/icon-status-green.png" title="' + data[counter].world + '" alt="Online">');
            }
            else {
                that.html('<img src="http://www.runescape.com/img/global/myprofile/icon-status-red.png" title="Not Online" alt="Not Online">');
            }
            counter++;
        });
        inclanElements.each(function () {
            var that = $(this);
            if (data[counter].clan !== undefined) {
                $("#CrestWrapper").css("display", "block");
            }
            counter++;
        });
    }


    $.ajax({
        url: "http://services.runescape.com/m=website-data/g=runescape/playerDetails.ws?names=" + JSON.stringify(playernames),
        dataType: "jsonp",
        success: function (response) {
            process(response);
        }
    });
}

RSGLOBAL.jsGameLinks = function () {
    $((RSGLOBAL.slowClassSelector ? 'a' : '') + '.GameLink').each(function () {
        if (this.tagName == "A") this.href += (this.href.indexOf('?') != -1 ? '&' : '?') + 'j=1';
        else if (this.tagName == "FORM") $(this).prepend('<input name="j" value="1" type="hidden">');
    });
}

RSGLOBAL.jScrollPane = function () {
    if ($.browser.webkit) {
        $('.scroll-pane').css("overflow-y", "auto");
    }
    else {
        $('.scroll-pane').jScrollPane({
            verticalDragMaxHeight: 50,
            verticalDragMinHeight: 10,
            stickToBottom: true,
            animateScroll: true
        });
    }
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

$(function () {

    $(".HoverImg:not(.NoFade)").each(function () {
        RSGLOBAL.hoverImageReplace($(this));
    });

    RSGLOBAL.playerInfoAjax();
    RSGLOBAL.jsGameLinks();

    $(".HoverImgJs").live('mouseover', function () {
        var domObject = $(this);
        if (!domObject.hasClass("Special")) {
            domObject.find(".HoverImgJsFg").stop().animate({
                opacity: 0
            }, 700);
        }
    })
        .live('mouseout', function () {
            var domObject = $(this);
            if (!domObject.hasClass("selected") && !domObject.hasClass("Special")) {
                domObject.find(".HoverImgJsFg").stop().animate({
                    opacity: 1
                }, 700, function () {
                    domObject.find(".HoverImgJsFg").css({
                        opacity: ''
                    });
                });
            }
        })
    ;

    if (!Modernizr.csstransitions) {

        $(".MainMenu > li")
            .removeClass("CSS3")
            .each(function () {
                var $this = $(this);
                var $menu = $this.find('.DropDown div');
                if ($menu.length < 1) return;
                var h = $menu.outerHeight();
                $this.hover(
                    function () {
                        $menu.stop().animate({
                            marginTop: 0
                        }, 600);
                    },
                    function () {
                        $menu.stop().animate({
                            marginTop: -h
                        }, 600);
                    }
                );
            })
        ;

        var $lis = $("#SubNav").find("nav > ul > li").filter('.Support, .Account');
        var $masks = $lis.find('.Mask');
        var $dropdowns = $lis.find('.DropDown > div');
        var dropDownDuration = 600;

        $lis
            .removeClass("CSS3")
            .mouseenter(
            function () {
                var $this = $(this);
                $this.find("div.DropDown > div").stop().animate({
                    marginTop: "0"
                }, {
                    duration: dropDownDuration
                });
                $masks.css({
                    opacity: 0,
                    visibility: "hidden"
                });
                $this.find($masks).css({
                    opacity: 1,
                    visibility: "visible"
                });
            }
        )
            .mouseleave(
            function () {
                var $this = $(this);
                $this.find($dropdowns).stop().animate({
                    marginTop: "-160px"
                }, {
                    duration: dropDownDuration,
                    complete: function () {
                        $this.find($masks).css({
                            opacity: 0,
                            visibility: "hidden"
                        });
                    }
                });
            }
        );

    }


    $("html.no-csstransition .HoverImageCss3").mouseover(function () {
        var $this = $(this);
        $this.find("img").fadeOut();
    });
    $("html.no-csstransition .HoverImageCss3").mouseout(function () {
        var $this = $(this);
        $this.find("img").fadeIn();
    });


    $(".no-csstransitions .HoverGradient *:not(img)").each(function () {
        var domObject = $(this);
        if (typeof $.effects !== 'object') return false;
        var originalColor = domObject.css("color");
        domObject.parents("a")
            .live('hover', function (event) {
                event.preventDefault();
            })
            .live('mouseover', function (event) {
                event.preventDefault();
                domObject.stop().animate({
                    color: "#FFFFFF"
                }, 300);
            })
            .live('mouseout', function (event) {
                domObject.stop().animate({
                    color: originalColor
                }, 300);
            });
    });

    $(".csstransitions #LoginButton1").click(function () {
        var $this = $(this);
        $this.parent().addClass("open");
        $("#username").focus();
        $(".no-csstransitions #Login.open a.LoginX").show().mouseover(function () {
            var $this = $(this);
            $this.stop().show().animate({
                opacity: '1'
            }, 200);
        });
        $(".no-csstransitions #Login.open a.LoginX").mouseout(function () {
            var $this = $(this);
            $this.stop().animate({
                opacity: '0'
            }, 200);
        });


        $(".no-csstransitions #LoginButton1").click(function () {
            var $this = $(this);
            var $LoginPanel = $("#LoginPanel").stop().animate({
                'top': '53px'
            }, 1000);
            $this.fadeOut(1000);
            $("#Login #RegisterButton,#LoginPanel #GetAccess").stop().fadeOut(1000);
            $("#LoginPanel .Facebooklogin").stop().animate({
                'opacity': '1',
                'display': 'block',
                'z-index': '3'
            }, 1000);
            $("#Login .LoginX,#Login .LoginXbottom").stop().fadeIn(1000);
        });
        $(".no-csstransitions #Login .LoginXtop").mouseenter(function () {
            var $this = $(this);
            $this.stop().fadeIn(1000);
        });
        $(".no-csstransitions #Login .LoginXtop").mouseleave(function () {
            var $this = $(this);
            $this.stop().fadeOut(1000);
        });
        $(".no-csstransitions #Login .LoginXtop").click(function () {
            var $this = $(this);
            var $LoginPanel = $("#LoginPanel").stop().animate({
                'top': '-155px'
            }, 1000);
            $this.fadeOut(1000);
            $("this,#LoginButton1,#Login #RegisterButton,#LoginPanel #GetAccess").stop().fadeIn(1000);
            $("#LoginPanel .Facebooklogin").stop().animate({
                'opacity': '0'
            }, 1000);
            $("#Login .LoginX,#Login .LoginXbottom").stop().fadeOut(1000);
            $("#Login a.LoginX").unbind();
        });
        $(".csstransitions a.LoginX").click(function () {
            var $this = $(this);
            $this.parent().removeClass("open");
            $("#Login a.LoginX").unbind();
        });
    });


    $(".no-csstransitions #LoginButton1").click(function () {
        var $this = $(this);
        var $LoginPanel = $("#LoginPanel").stop().animate({
            'top': '53px'
        }, 1000);
        $this.fadeOut(1000);
        $("#Login #RegisterButton,#LoginPanel #GetAccess").stop().fadeOut(1000);
        $("#LoginPanel .Facebooklogin").stop().animate({
            'opacity': '1',
            'display': 'block',
            'z-index': '3'
        }, 1000);
        $("#Login .LoginX,#Login .LoginXbottom").stop().fadeIn(1000);
        $(".no-csstransitions #Login .LoginXtop").mouseenter(function () {
            var $this = $(this);
            $this.stop().fadeIn(1000);
        });
        $(".no-csstransitions #Login .LoginXtop").mouseleave(function () {
            var $this = $(this);
            $this.stop().fadeOut(1000);
        });
        $(".no-csstransitions #Login .LoginXtop").click(function () {
            var $this = $(this);
            var $LoginPanel = $("#LoginPanel").stop().animate({
                'top': '-155px'
            }, 1000);
            $this.fadeOut(1000);
            $("this,#LoginButton1,#Login #RegisterButton,#LoginPanel #GetAccess").stop().fadeIn(1000);
            $("#LoginPanel .Facebooklogin,#Login .LoginX,#Login .LoginXbottom").stop().fadeOut(1000);
            $("#Login a.LoginX").unbind();
        });
    });

    $(".no-csstransitions .InPageDropDownNew").css({
        'height': '45px'
    });

    $(".no-csstransitions .InPageDropDownNew").removeClass("CSS3")
        .mouseenter(function () {
            var $this = $(this);
            var $dropListWrapper = $this.find(".DropListWrapper");
            var expand = $this.data("expand");
            $this.css({
                'height': expand
            });
            $dropListWrapper.stop().animate({
                'top': '-8px'
            }, 400);
        })
        .mouseleave(function () {
            var $this = $(this);
            var $dropListWrapper = $this.find(".DropListWrapper");
            var retract = $dropListWrapper.data("retract");
            $dropListWrapper.stop().animate({
                'top': retract
            }, 400);
            setTimeout(function () {
                $dropListWrapper.parent().stop().css({
                    'height': '45px'
                });
            }, 500);
            /*Shrink the parent shortly after the dropdown retracts */
        });


    $("#Login").removeClass("NoJS");
    $("#PlayerCount").show();

    /*if($("#PlayerCount").length > 0){
     PollPlayerCount();
     setInterval( PollPlayerCount, 20000)
     }

     function PollPlayerCount(){
     $.ajax({
     url: 'player_count.php',
     success: function(iPlayerCount){
     $("#PlayerCount").show();
     if ( $("#PlayerCount p.top").css("display")=='block' ){
     $("#PlayerCount p.bottom span").html(addCommas(iPlayerCount));
     $("#PlayerCount p").fadeToggle(1000);
     }else{
     $("#PlayerCount p.top span").html(addCommas(iPlayerCount));
     $("#PlayerCount p").fadeToggle(1000);
     }
     }
     });
     }*/


    if ($.fn.placeholder) {
        $("textarea:not(.NoPlaceholder),input:text:not(.NoPlaceholder),input:password:not(.NoPlaceholder)").placeholder({
            attribute: "title"
        });
        $("textarea:not(.NoPlaceHolder)").addClass("placeholderSubtle");
    }

    $("span.FacebookLike,span.TwitterTweet").show();


    RSGLOBAL.jScrollPane();


});




