(function ($) {
    $.fn.placeholder = function (options) {


// configuration properties
        options = $.extend({
                // defaults
                holderText: null,
                attribute: 'placeholder',
                addclass: 'placeholder'
            },
            options);


        return this.each(function () {
            var $this = $(this);

            // set a property to avoid placeholdering the same element twice if we're called again

            if ($this.data('hasPlaceholder') === true) return;
            if ($this.attr(options.attribute)) $this.data({'hasPlaceholder': true});


            // if no placeholder text is defined, give up
            var placeholder = options.holderText || $this.attr(options.attribute);
            if (!placeholder || placeholder.length < 1) return;

            // focus handler/function - if placeholder text is there, remove it, remove placeholder class
            $this.focus(function () {
                clearFunction($this);
                if (options.addclass) $this.removeClass(options.addclass);
            });

            $this.parents('form').submit(function () {
                clearFunction($this.parents('form').find('.placeholder'));
                if (options.addclass) $this.removeClass(options.addclass);
            });

            // focus handler - blur function separate so we can call it on page load
            $this.blur(blurFunction);

            // blur function - if placeholder text is there, remove it, remove placeholder class
            function blurFunction() {
                var val = $this.val();
                if (val == '' || val == placeholder && options.addclass) {
                    $this.addClass(options.addclass);
                }
                if (val == '') {
                    $this.val(placeholder)
                }
            }

            function clearFunction($obj) {
                $obj.each(function () {
                    var val = $(this).val();
                    if (val == placeholder) {
                        $(this).val('');
                    }
                });
            }

            // Is this a password field? If so, make a duplicate text field to show the placeholder.
            // This is not perfect, as it relies on the styles being applied to the class and NOT the ID.
            // ID is not duplicated.
            if ($this.attr("type") == "password") {
                var $dummyInput = $('<input type="text" title="' + $this.attr('title') + '"  class="' + $this.attr('class') + '" value="' + placeholder + '"/>').css({
                    'position': 'absolute',
                    'top': $this.position().top,
                    'left': $this.position().left
                });
                $this.parent().append($dummyInput);
                $dummyInput.focus(function () {
                        $(this).hide();
                        /* Hide the fake input */
                        clearFunction($this);
                        $this.focus();
                        /* Give focus to the real password field */
                    }
                );
                $this.unbind('blur').blur(function () {
                        /* Remove the old blur function and replace with this new one,
                         which simply masks with the dummy input */
                        if ($this.val().length == 0)$dummyInput.show();
                    }
                ).unbind('focus').focus(function () {
                        /* Remove the old blur function and replace with this new one,
                         which also hides the dummy input */
                        $dummyInput.hide();
                        clearFunction($this);
                        if (options.addclass) $this.removeClass(options.addclass);
                    });
            }

            // call the blur function when the page is loaded to set up placeholder text
            blurFunction();

        });
        /*this.each()*/


    }
})(jQuery)