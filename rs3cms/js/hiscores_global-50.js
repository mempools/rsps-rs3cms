$(function () {

    $('.categorySelect').css('display', 'block');

    $('#skillStats .row').mousemove(function (ev) {
        $('.row .skillDifference').css('display', 'none');

        if (typeof($(ev.target).attr('class')) != 'undefined' && $(ev.target).attr('class').indexOf('column', 0) > -1) {
            $(this).find('.' + $(ev.target).attr('class') + ' .skillDifference').css('display', 'block');
        }
    });

    $('.categorySelect a').click(function (ev) {
        ev.preventDefault();
        $('.categorySelect .firstValue').text($(this).text());
        changeSkill($(this).attr('href').replace('#', ''), $(this).parents('.OrnamentalBox'));
    });
})


function changeSkill(_skill, _parent) {
    _parent.find('.OrnamentalBoxContent').fadeOut(function () {
        $(this).find('.row').css('display', 'block');
        $(this).find('.row').removeClass('last-row');

        if (_skill != 'all') {
            if (_skill != 'members') {
                $(this).find('.row:not([data-skill="' + _skill + '"])').css('display', 'none');
                $(this).find('.row[data-skill="' + _skill + '"]').last().addClass('last-row');
            }
            else {
                $(this).find('.row[data-member="false"]').css('display', 'none');
                $(this).find('.row[data-member="true"]').last().addClass('last-row');
            }
        }
        else {
            $(this).find('.row').last().addClass('last-row');
        }

        $(this).fadeIn();
    });
}  