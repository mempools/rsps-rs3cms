$(function () {


    var $loyaltyMore = $('.loyaltyMore');
    var original_text = $loyaltyMore.find('b:first').text();

    $loyaltyMore.hover(
        function () {
            $(this).find('b').text('Learn more');
        },
        function () {
            $(this).find('b').text(original_text);
        }
    );


    var
        $skillsBox = $('#skills'),
        $skillsTitle = $skillsBox.find('h3.Gradient').find('span'),
        $skillsMatrices = $('.skills-matrix'),
        $skillsFilters = $('.secondaryContent .FilterControl'),
        $skillsTabs = $('.skill-group-tab')
        ;

    $skillsFilters.show();
    $skillsMatrices.removeClass('selected').eq(0).addClass('selected');

    $skillsTabs
        .find('a')
        .click(function (ev) {
            var $this = $(this);

            var
                $li = $this.closest('li'),
                $tooltip = $li.find('.ToolTip'),
                ind = $skillsTabs.index($li)
                ;

            $skillsTitle.text($tooltip.text());

            $skillsTabs
                .removeClass('selected')
                .eq(ind)
                .addClass('selected')
            ;

            $skillsMatrices
                .removeClass('selected')
                .eq(ind)
                .addClass('selected')
            ;

            ev.preventDefault();

        });
    ;


    $skillsMatrices.find('.progress, .complete').each(function () {
        var $this = $(this);

        $this
            .wrapInner('<div><span style="width: ' + $this.attr('data-value') + '%;"></span></div>')
            .addClass('active')
        ;

    });


    $(".graph table").visualize({
        type: "pie",
        pieMargin: 3,
        sliceStrokeColor: Modernizr.canvas ? ["#000", "#555"] : "#272422",
        sliceStrokeWidth: 2,
        pieStrokeColor: Modernizr.canvas ? ["#e3821a", "#611804"] : "#97470e",
        pieStrokeWidth: 4,
        width: 163,
        height: 163,
        appendKey: false,
        colors: [["#63cb39", "#317217"], ["#f1dfd5", "#d59c82"], ["#183e7f", "#040c17"]],
        colFilter: ':first-child',
        rowFilter: 'tbody tr'
    });


    var
        $questStatus = $(".quest-status"),
        $questLog = $('.quest-log'),
        $questLogSelect = $questLog.find('.DropListWrapper'),
        $questLogSelected = $questLog.find('.firstValue'),
        $questLogOptions = $questLogSelect.find('a'),
        $questLogViewport = $questLog.find('.viewport'),
        $questLogViews = $questLog.find('.quest-log-view')
        ;


    $(".quest-toggle")
        .show()
        .click(function (ev) {
            var $this = $(this);
            $questStatus.toggleClass('log');
            $questLogViewport.data('jsp').reinitialise();
            ev.preventDefault();
        })
    ;


    $questLogOptions.click(function (ev) {
        var $this = $(this);

        var ind = $questLogOptions.index($this);

        $questLogSelected.text($this.text());

        $questLogViews
            .removeClass('selected')
            .eq(ind)
            .addClass('selected')
        ;

        $questLogViewport.data('jsp').reinitialise();

        ev.preventDefault();

    });

    $('.triggerMoreInfo').hover(function () {
            $('.eventsInfoBubble').hide();
            var that = $(this);
            that.find('.eventsInfoBubble').stop(true, true).fadeIn().addClass("open");
        },
        function () {
            $('.eventsInfoBubble').stop(true, true).fadeOut().removeClass('open');
        });
    $('.eventsInfoBubble .middle').mouseenter(function () {
        var parentDiv = $(this).parent();
        if (parentDiv.hasClass("open")) {
            parentDiv.hide().removeClass('open');
        }
    });

});
