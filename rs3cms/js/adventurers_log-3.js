var global = {
        banner: function () {//Used when only the title is needed
            $.ajax({
                url: websiteDataUrl + "playerDetails.ws?names=[" + JSON.stringify(plname) + "]",
                dataType: "jsonp",
                timeout: 3000,
                success: function (response) {
                    if (response[0].title) {
                        $('#js-title').html(response[0].title);
                    }
                }
            });
        }
    },
    profile = {
        chart: function () {
            var ctx = document.getElementById("combat-chart__canvas").getContext("2d"), options = {
                segmentShowStroke: true,
                segmentStrokeColor: "#000",
                segmentStrokeWidth: 1,
                percentageInnerCutout: 70
            };
            var data = [
                {value: 200000000, color: "#37cad2"},
                {value: 200000000, color: "#5ab826"},
                {value: 200000000, color: "#ff8a00"}
            ];
            new Chart(ctx).Doughnut(data, options);
        },
        clan: function () {//used to populate clan and title data
            $.ajax({
                url: websiteDataUrl + "playerDetails.ws?names=[" + JSON.stringify(plname) + "]",
                dataType: "jsonp",
                timeout: 3000,
                success: function (response) {
                    if (response[0].title) {
                        $('#js-title').html(response[0].title);
                    }
                    if (response[0].clan) {
                        $('#stats-box--activity').addClass('stats-box--activity-small');
                        $('#clanName').html(response[0].clan);
                        $('#clanLink').attr('href', clanUrl + '/clan/' + response[0].clan);
                        var events = $('.event');
                        if (events.length > 3) {
                            events.slice(events.length - 4).remove();
                        }
                    }
                    else {
                        $('#stats-box--clan').remove();
                    }
                    $('#profile-stats__right').addClass('profile-stats__right--show');
                },
                fail: function () {
                    $('#stats-box--clan').remove();
                    $('#profile-stats__right').addClass('profile-stats__right--show');
                },
                error: function () {
                    $('#stats-box--clan').remove();
                    $('#profile-stats__right').addClass('profile-stats__right--show');
                }
            });
        },
        init: function () {
            //this.clan();
            this.chart();
        }
    },
    filters = {
        dropDown: function () {
            $('.drop-filter').on('click', function (ev) {
                $(this).toggleClass('drop-filter--dropped');
            });
        }
    },
    friends = {
        results: {},
        type: 'all',
        loadContent: function (pageNumber) {
            if ($('.friend').length) {// check player has any friends from WS
                var resultsPage = 24;
                if (pageNumber == 'undefined') {
                    var pageNumber = 1;
                }
                else {
                    var pageNumber = parseInt(pageNumber);
                }
                //build the cache name for the key of the results object
                var cacheName = 'page_' + pageNumber + '_size_' + resultsPage + '_display_' + friends.type;
                // empty results ready for caching
                if (friends.results[cacheName]) {
                    $('#friendsTableContent').hide();
                    friends.parseContent(friends.results[cacheName], pageNumber);
                }
                else {
                    $('#friends').hide();
                    // build the url depending on user selections
                    var buildURL = websiteDataUrl + "playerFriendsDetails.json?resultsPerPage=" + resultsPage + "&currentPage=" + pageNumber;
                    if (friends.type == 'online') {
                        buildURL += '&display=online';
                    }
                    else if (friends.type == 'offline') {
                        buildURL += '&display=offline';
                    }
                    var ajaxURL = RS3.global.addCToLink(buildURL + '&callback=?');
                    var friendList = $.getJSON(ajaxURL, function (data) {
                        if (data.friends != null) {
                            if (data.friends.length > 0) {
                                friends.results[cacheName] = data; // cache the results
                                friends.parseContent(data, pageNumber);
                            }
                            else {
                                $('#friends').html("<p class='friends__error'>" + noFriendsStr + "</p>").show();
                            }
                        }
                        else {
                            $('#friends').html("<p class='friends__error'>" + noFriendsStr + "</p>").show();
                        }
                    }).fail(function () {
                        $('#friends').show();
                    }).always(function () {
                    });
                }
            }
        },
        parseContent: function (data, pageNumber) {
            var tableContent = [], nextPage = (pageNumber + 1), prevPage = (pageNumber - 1), upperPagination = [], lowerPagination = [], friendsEle = $('#friends');
            if (data.pageFriends > 0) {
                $('#onlineFriends').html(parseInt(data.online) + " / " + parseInt(data.allFriends) + " " + onlineStr);//Set online friends
                //pagination
                if (parseInt(data.totalPages) > 1) {
                    //Upper
                    upperPagination.push('<div id="pageNumberFinder" class="paginationOuter" data-pagenumber="' + pageNumber + '">');
                    upperPagination.push('<div class="pagination simple');
                    if (pageNumber == 1) {
                        upperPagination.push(' noFirst">');
                    }
                    else if (pageNumber == parseInt(data.totalPages)) {
                        upperPagination.push(' noLast">');
                    }
                    else {
                        upperPagination.push('">');
                    }
                    if (pageNumber != 1) {
                        upperPagination.push('<a id="topPaginationPrev" data-page="' + prevPage + '" class="previous ajaxClick" href="?page=' + prevPage + '">' + previousStr + '</a>');
                    }
                    upperPagination.push('<span>' + pageStr + ' ' + pageNumber + '/' + data.totalPages + '</span>');
                    if (pageNumber != parseInt(data.totalPages)) {
                        upperPagination.push('<a id="topPaginationNext" data-page="' + nextPage + '" class="next ajaxClick" href="?page=' + nextPage + '">' + nextStr + '</a>');
                    }
                    upperPagination.push('</div></div>');
                    //Lower
                    lowerPagination.push('<div class="paginationWrap clear">');
                    lowerPagination.push('<div class="pagination">');
                    lowerPagination.push('<div class="paging">');
                    lowerPagination.push('<ul id="ajaxPaginationBot" class="pageNumbers ' + pageNumber + '">');
                    for (var i = 1; i <= data.totalPages; i++) {
                        var className = '';
                        if (i == pageNumber) {
                            var className = ' class="current"';
                        }
                        lowerPagination.push('<li' + className + '><a class="ajaxClick" data-page="' + i + '" id="paginationListItem' + i + '" href="?page=' + i + '">' + i + '</a></li>');
                    }
                    lowerPagination.push('</ul></div></div></div>');
                }
                //draw all friends
                $.each(data.friends, function (index, item) {
                    if (item.name != '') {
                        var avatarClass = '';
                        if (item.status == 'offline') {
                            avatarClass = ' friend__avatar--offline';
                        }
                        tableContent.push('<article class="friend"><figure class="friend__figure">');
                        tableContent.push('<a href="' + advlogUrl + 'profile?searchName=' + item.name + '"><img class="friend__avatar' + avatarClass + '" alt="' + item.name + '" src="http://services.runescape.com/m=avatar-rs/' + escape($.trim(item.name)) + '/chat.png?w=60&h=60"/></a></figure>');
                        tableContent.push('<div class="friend__meta"><a href="' + advlogUrl + 'profile?searchName=' + item.name + '"><h2 class="friend__name">' + item.name + '</h2></a>');
                        if (item.status == 'offline') {
                            tableContent.push('<div class="friend__player-state"><span class="friend__location">' + offlineStr + '</span></div>');
                        }
                        else {
                            tableContent.push('<div class="friend__player-state"><img class="friend__state-icon" src="' + imgRootStr + '/adventurers-log/friend_online.png" alt="' + onlineStr + '" title="' + onlineStr + '"/><span class="friend__location">' + onlineStr + ' | ' + item.world + '</span></div>');
                        }
                        tableContent.push('<a class="friend__compare" href="' + compareUrl + 'compare?user1=' + plname + '&user2=' + item.name + '">' + compareStr + '</a></div>');
                        tableContent.push('</article>');
                    }
                });
            }
            else {
                $('#upperPaginationBox').remove();
                $('#onlineFriends').remove();
            }
            $('#upperPaginationBox').html(upperPagination.join(''));
            $('#lowerPaginationBox').html(lowerPagination.join(''));
            friendsEle.html(tableContent.join(''));
            friendsEle.fadeIn(500);
        },
        events: function () {
            $('.friends-pagination').on('click', 'a.ajaxClick', function () {
                var $ele = $(this);
                var page = parseInt($ele.data('page'));
                var currentPage = $('#pageNumberFinder').data('pagenumber');
                if (page != currentPage) {
                    friends.loadContent(page);
                }
                return false;
            });
        },
        init: function () {
            this.loadContent(1);
            this.events();
        }
    },
    expand = {
        init: function () {
            $('.item__expand').on('click', function (ev) {
                $(this).toggleClass('item--expanded');
            });
        }
    },
    onLoad = {
        init: function () {
            profile.init();
            //var id = document.getElementsByTagName('body')[0].id;
            //switch (id) {
            //    case "advprofile" :
            //        profile.init();
            //        break;
            //    case "advquests":
            //    case "advskills" :
            //        global.banner();
            //        filters.dropDown();
            //        expand.init();
            //        break;
            //    case "advactivity":
            //        global.banner();
            //        break;
            //    case "advfriends":
            //        global.banner();
            //        friends.init();
            //        break;
            //    case "advfriendsact":
            //        global.banner();
            //        break;
            //}
        }
    };
function d() {
    onLoad.init()
}
d();

function donut() {
    var ctx = document.getElementById("combat-chart__canvas").getContext("2d"), options = {
        segmentShowStroke: true,
        segmentStrokeColor: "#000",
        segmentStrokeWidth: 1,
        percentageInnerCutout: 70
    };
    var data = [
        {value: 200000000, color: "#37cad2"},
        {value: 200000000, color: "#5ab826"},
        {value: 200000000, color: "#ff8a00"}
    ];
    new Chart(ctx).Doughnut(data, options);
}

function donut_set(magic, melee, range) {
    var ctx = document.getElementById("combat-chart__canvas").getContext("2d"), options = {
        segmentShowStroke: true,
        segmentStrokeColor: "#000",
        segmentStrokeWidth: 1,
        percentageInnerCutout: 70
    };
    var data = [
        {value: magic, color: "#37cad2"},
        {value: melee, color: "#5ab826"},
        {value: range, color: "#ff8a00"}
    ];
    new Chart(ctx).Doughnut(data, options);
}