document.domain = 'dev.spacegh0st.net/eoc';
var RS3 = RS3 || {};
RS3.global = {
    viewportDimensions: function () {
        var client = {};
        client.cHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        client.cWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        return client;
    },
    external: function () {
        var $body = $('body');
        $body.on('click', '.ext', function (e) {
            var $self = $(this), title = $self.data('title'), href = $self.attr('href');
            if (title) {
                e.preventDefault();
                window.open(href);
                RS3.tracking.pushTrackEvent('external_link', 'click', title);
            }
        });
    },
    internal: function () {
        var $body = $('body');
        $body.on('click', '.int', function (e) {
            var $self = $(this);
            RS3.tracking.pushTrackEvent('internal_link', 'click', $self.attr('title'));
        });
    },
    addCToLink: function (linkHref) {
        function retrieveC(url) {
            var param = url.split('/'), session;
            for (var i in param) {
                var curr = param[i];
                if (curr.split('c=').length > 1) {
                    session = curr;
                }
            }
            return session;
        }

        function removeC(url) {
            var param = url.split('/'), session, newArr = [];
            for (var i in param) {
                var curr = param[i];
                if (curr.split('c=').length <= 1) {
                    newArr.push(curr);
                }
            }
            return newArr.join('/');
        }

        var sessionId = retrieveC(pageLocation);
        linkHref = removeC(linkHref);
        return linkHref.split('.com').join('.com/' + sessionId);
    },
    logIn: function () {
        $("a#inline, a.openLoginPanel, div.openLoginPanel a, .loginPanelBtn a").attr('href', '#loginPanel').each(function () {
            var that = $(this);
            that.fancybox({
                'wrapCSS': 'loginFancyBox',
                afterShow: function () {

                    var redirectParams = that.data('redirect'),
                        currentFancyBox = $('.fancybox-opened');
                    if (redirectParams) {
                        redirectParams = redirectParams.split('/');
                        currentFancyBox.find('input[name=mod]').val(redirectParams[0]);
                        currentFancyBox.find('input[name=ssl]').val(redirectParams[1]);
                        currentFancyBox.find('input[name=dest]').val(redirectParams[2]);
                        currentFancyBox.find('#facebooklogin').unbind().click(function (ev) {
                            ev.preventDefault();
                            var qs = 'mod=' + redirectParams[0] + '&ssl=' + redirectParams[1] + '&dest=' + redirectParams[2];
                            RS3.global.facebook.fbButtonClickHandler(qs, '');
                        });
                    }

                    if ($('#username').length) {
                        $('#username').focus();
                    }
                },
                beforeShow: function () {

                    if ($('applet').length) {
                        $('applet').css('visibility', 'hidden');
                    }
                },
                afterClose: function () {

                    if ($('applet').length) {
                        $('applet').css('visibility', 'visible');
                    }
                }
            });
        });
    },
    responsiveMenu: function () {
        $('#main-nav').on('click', function () {
            $(this).toggleClass('main-nav--show');
        });
        $('.main-nav__expander').on('click', function (ev) {
            ev.stopPropagation();
            ev.preventDefault();
            $(this).parent().parent().toggleClass('main-nav__item--expand');
        });
    },
    playerCount: function () {
        var $playerCount = $("#playerCounts");
        if ($playerCount.length > 0) {

            var currentCount = parseInt($playerCount.html().replace(',', ''));
            if (currentCount < 1 || isNaN(currentCount)) {
                RS3.global.pollPlayerCount($playerCount);
            }
        }
    },
    pollPlayerCount: function ($playerCount) {
        $.ajax({
            url: 'http://www.runescape.com/player_count.js?varname=iPlayerCount',
            dataType: 'jsonp',
            success: function (iPlayerCount) {
                if (iPlayerCount > 0) {
                    $playerCount.html(RS3.global.playerCountCommas(iPlayerCount));
                }
            }
        });
    },
    playerCountCommas: function (iPlayerCount) {
        iPlayerCount += '';
        x = iPlayerCount.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    },
    lightbox: {
        wrap: '<div class="fancybox-wrap" tabIndex="-1">'
        + '<div class="fancybox-skin"><div class="fancybox-outer">'
        + '<div class="tl outerCorner"></div><div class="tr outerCorner"></div><div class="bl outerCorner"></div><div class="br outerCorner"></div>'
        + '<div class="fancybox-inner greyFrame"><div class="tl corner">'
        + '</div><div class="tr corner"></div><div class="bl corner"></div><div class="br corner">'
        + '</div></div></div></div></div>',
        onUpdate: function () {

            var $greyFrame = $('.fancybox-inner.greyFrame');
            if ($('html').hasClass('no-borderimage')) {
                $greyFrame.find('iframe').width($greyFrame.width() - 10).height($greyFrame.height() - 10);
            }
        },
        init: function () {
            'use strict';
            var $lightbox = $('.lightbox');
            $lightbox.fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                tpl: {
                    wrap: RS3.global.lightbox.wrap
                },
                scrolling: false,
                helpers: {
                    media: {}
                },
                onUpdate: RS3.global.lightbox.onUpdate
            });
        }
    },
    tabbedElement: function () {
        'use strict';
        var $tabs = $('#tabs'), $tabNav = $('#tabNav'), hash = window.location.hash;
        if (hash === '#videos') {
            $tabs.find('.tabbedContent').hide().end().find(hash).show();
            $tabNav.find('a[href="' + hash + '"]').parent().addClass('active');
        } else {
            $tabs.find('.tabbedContent:not(:first)').hide();
            $tabNav.find('li:first').addClass('active');
        }
        $tabNav.on('click', 'a', function (e) {
            e.preventDefault();
            var $self = $(this), activeTab = $self.attr('href');
            $tabNav.find('.active').removeClass('active');
            $self.parent().addClass('active');
            if ($('.lt-ie9').length) {
                $tabs.find('.tabbedContent').hide().end().find(activeTab).show();
            } else {
                $tabs.find('.tabbedContent').hide().end().find(activeTab).fadeIn(250);
            }
        });
    },
    headerDescriptionToggle: function () {
        if ($('.descTrigger').length && $('.hiScoreDescription').length) {
            $('.descTrigger').on('click', function (e) {
                e.preventDefault();
                var $ele = $(this);
                $ele.toggleClass('descTriggerOpen');
                $ele.next('.hiScoreDescription').slideToggle(500, function () {
                    //if inside a fancybox then position the fancybox
                    if ($('.fancybox-wrap').is(':visible') && $ele.hasClass('descTriggerOpen')) {
                        $.fancybox.update();
                    }
                });
            });
        }
    },
    categoryDropDown: {
        categories: function () {


            var categories = [];
            categories[0] = "All";
            categories[1] = "Game Updates";
            categories[2] = "Website";
            categories[3] = "Support";
            categories[4] = "Technical";
            categories[5] = "Community";
            categories[6] = "Behind The Scenes";
            categories[7] = '';
            categories[8] = "Minor Customer Support";
            categories[9] = "Shop";
            categories[10] = '';
            categories[11] = '';
            categories[12] = "Future Updates";
            categories[13] = "Solomon's Store";
            categories[14] = "Treasure Hunter";
            categories[15] = "Your Feedback";
            categories[16] = "Events";
            return categories;
        },
        fallbackImages: function (category) {
            switch (category) {
                case 1:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-gameupdates.jpg';
                    break;
                case 2:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-website.jpg';
                    break;
                case 3:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-support.jpg';
                    break;
                case 4:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-technical.jpg';
                    break;
                case 5:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-community.jpg';
                    break;
                case 6:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-behindthescenes.jpg';
                    break;
                case 8:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-support.jpg';
                    break;
                case 9:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-shop.jpg';
                    break;
                case 12:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-futureupdates.jpg';
                    break;
                case 13:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-solomons.jpg';
                    break;
                case 14:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-treasurehunter.jpg';
                    break;
                case 15:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-yourfeedback.jpg';
                    break;
                case 16:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-events.jpg';
                    break;
                default:
                    fallbackImage = 'http://www.runescape.com/img/eoc/news/fallback-unspecified.jpg';
            }
            return fallbackImage;
        },
        getNewsCats: function () {
            'use strict';
            var $links = $('.catList');
            $links.on('click', 'a', function (e) {
                e.preventDefault();


                var $self = $(this), type = $self.data('type'), catId = $self.data('id'), langId = '_0', $section, sessionName, $parent;


                if ($self.hasClass('active') || isNaN(catId)) {
                    return false;
                }

                if (type === 'news') {
                    $section = $('#newsSection');
                    sessionName = 'jsonNews_';
                    $parent = $('#catListNews');
                } else {
                    $section = $('#videoSection');
                    sessionName = 'jsonVideo_';
                    $parent = $('#catListVideo');
                }

                // if archive page remove active cat class
                $('#catListArchive').find('.active').removeClass('active');
                $parent.find('.active').removeClass('active');
                $self.addClass('active');
                $section.hide().empty();


                var ajaxUrl = ( type === 'news' ? 'latestNews' : 'latestNewsWithVideoContent' );

                $.ajax({
                    url: 'http://services.runescape.com/m=news/' + ajaxUrl + '.json?cat=' + catId,
                    dataType: 'jsonp',
                    success: function (newsJson) {
                        RS3.global.categoryDropDown.parseNewsCats(newsJson, type, $section);
                    },
                    error: function (request, status, error) {
                        var newsJson = [];
                        RS3.global.categoryDropDown.parseNewsCats(newsJson, type);
                    }
                });

            });
        },
        parseNewsCats: function (newsJson, type, $section, $page) {
            var found = 0, limit = 7, news = [], showMessage = true, categoryId = newsJson.categoryId, categories = RS3.global.categoryDropDown.categories(), tracking;

            switch (archiveOrNewsPage) {
                case "archive":
                    tracking = 'jptg=ia&jptv=news_archive';
                    break;
                case "news":
                    tracking = 'jptg=ia&jptv=news_list';
                    break;
                case "home":
                    tracking = 'jptg=ia&jptv=community_news';
                    break;
            }
            ;
            if (newsJson !== null && newsJson.newsItems.length) {
                $.each(newsJson.newsItems, function (index, newsItem) {

                    var category = categoryId === 0 ? newsItem.categoryId : categoryId, categoryText = categories[category];
                    RS3.global.categoryDropDown.fallbackImages(category);

                    if (type === 'video' && newsItem.largeMediaType != 3) {
                        return true;
                    }

                    if (found > limit) {
                        return false;
                    }

                    found += 1;


                    news.push('<article ' + (newsItem.largeMediaType == 3 ? 'class="video"' : '') + '>');
                    news.push('<figure><a href="' + newsItem.link + '?' + tracking + '">');
                    news.push("<img src=\"" + (newsItem.summaryImageLink != '' ? newsItem.summaryImageLink : fallbackImage) + "\" alt=\"" + newsItem.title + "Teaser Image\" title=\"" + newsItem.title + "Teaser Image\" />");
                    news.push('</a></figure>');
                    news.push('<div class="copy"><header>');
                    news.push('<h4><a href="' + newsItem.link + (newsItem.largeMediaType == 3 ? '?play=true' : '') + '&' + tracking + '">' + newsItem.title + '</a></h4>');
                    news.push('<h5><a href="http://services.runescape.com/m=news/list?cat=' + category + '&page=1' + '">');
                    news.push(categoryText);
                    news.push('</a></h5>');
                    news.push('<time>' + newsItem.formattedDate + '</time></header>');
                    if (archiveOrNewsPage != 'home') {
                        news.push('<p>' + newsItem.summary + ' <a class="readMore" href="' + newsItem.link + '?' + tracking + '">read more</a></p>');
                    }
                    ;
                    news.push('</div>');
                    news.push('</article>');
                });


                if (found != 0) {
                    showMessage = false;
                }
            }

            if (showMessage) {


                news.push("<p class=\"error\">Sorry, there are no articles for this category. Please try another category.</p>");
            }


            $section.html(news.join(''));
            $section.fadeIn(500);
        }
    },
    facebook: {

        loadFacebookAPI: function (initCallback) {
            'use strict';
            if (typeof (FB) == 'undefined') {
                if ($("#fb-root").length === 0) {
                    $("body").prepend('<div id="fb-root"></div>');
                }
                window.fbAsyncInit = function () {
                    FB.init({
                        appId: PAGEGLOBALS.FB.appId,
                        status: false,
                        cookie: true,
                        xfbml: true,
                        version: 'v2.2',
                        oauth: true
                    });

                    if (initCallback) {
                        initCallback();
                    }
                };
                (function (d, debug) {
                    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement('script');
                    js.id = id;
                    js.async = true;
                    js.src = "//connect.facebook.net/en_US/sdk" + (debug ? "/debug" : "") + ".js";
                    ref.parentNode.insertBefore(js, ref);
                }(document, /*debug*/ false));
            }
        },

        fbLoginRedirect: function (destStr, target) {
            'use strict';
            var fbUrl = PAGEGLOBALS.FB.fbURL,
                checkLoginUrl = PAGEGLOBALS.FB.checkLoginURL,
                loginUrl = PAGEGLOBALS.FB.loginURL,
                frameHeight = 500;
            $.getJSON(checkLoginUrl + '?json=?', {
                    "tps": 0,
                    "token": FB.getAuthResponse().accessToken,
                    "expiry": FB.getAuthResponse().expiresIn,
                    "signed": FB.getAuthResponse().signedRequest
                },
                function (data) {
                    var popupTarget = '', popupTargetURL;
                    if (data['sso'] < 0) {


                        alert("There was a problem verifying your login. Please try again.");
                        return false;
                    }
                    else if (data['sso'].length > 1) {

                        if (data['sna'] > -1) {

                            popupTargetURL = fbUrl + '?key=' + data['sso'] + '&tps=0&' + destStr;
                            if (target === '_parent') {
                                window.parent.location = popupTargetURL;
                            }


                            frameHeight = 620;

                        }
                        else {

                            if (typeof(target) === 'undefined') target = '';
                            popupTargetURL = loginUrl + '?key=' + data['sso'] + '?' + destStr;
                            if (target === '_parent') {
                                window.parent.location = popupTargetURL;
                            }
                            else if (target === '_top') {
                                window.top.location = popupTargetURL;
                            }
                            else if (target === '_blank') {
                                window.open(popupTargetURL, '_blank');
                            }
                            else {
                                window.location = popupTargetURL;
                            }
                            return true;
                        }
                    }
                    else {

                        popupTargetURL = fbUrl + '?' + destStr;
                        if (target === '_parent') {
                            window.parent.location = popupTargetURL;
                        }
                    }
                    if (target !== '_parent') {
                        $.fancybox({
                            href: popupTargetURL,
                            type: 'iframe',
                            width: 462,
                            height: frameHeight,
                            autoSize: false,
                            wrapCSS: 'loginFancyBoxFB'
                        });
                    }
                });
            return false;
        },

        fbButtonClickHandler: function (destination, target) {
            'use strict';
            var destinationURL = destination || PAGEGLOBALS.FB.queryString;
            if (typeof(FB) == 'undefined' || typeof(FB.getAuthResponse) != 'function') {

                alert("Couldn't contact Facebook. Please try again later.");
            }

            else if (FB.getAuthResponse() == null) {
                FB.login(function (response) {
                        if (response.authResponse) {

                            RS3.global.facebook.fbLoginRedirect(destinationURL, target);
                        }
                        else {


                            alert("Authentication with Facebook failed. Please try again.");
                        }
                    },
                    {scope: PAGEGLOBALS.FB.scope});
            }
            else {

                RS3.global.facebook.fbLoginRedirect(destinationURL, target);
            }
        },

        setUpEvents: function () {
            'use strict';
            $('.facebookLogin').click(function (ev) {
                ev.preventDefault();
                var that = $(this), href = that.data('redirecturl'), target = that.attr("target");
                RS3.global.facebook.fbButtonClickHandler(href, target);
            });

            if ($('#weblogin .fbLogout').length) {
                FB.getLoginStatus(function (response) {
                    if (response.authResponse) {
                        document.getElementById('socialnetworks').style.display = "block";
                        FB.XFBML.parse(document.getElementById("fb_logout"));
                        document.getElementById('fb_logout').style.display = "block";
                    }
                });

            }
            if ($('#weblogin .googleLogout').length) {
                document.getElementById('socialnetworks').style.display = "block";
                document.getElementById('google_logout').style.display = "block";

            }
        }
    },

    galleryLightBox: function () {
        var $lightboxTrigger = $('.galleryLightBox'), $carousel = $('.carouselLightboxNav'), lastChange = new Date();
        $body = $("body"), galleries = {};

        if ($lightboxTrigger.length === 0 || $carousel.length === 0) return;

        $carousel.on('mouseenter', function () {
            var $this = $(this);
            if ($this.width() > availWidthHeight().width || $this.height() > availWidthHeight().height) {
                $carousel.find('.galleryLightBox').addClass('inactive');
            }
            else {
                $carousel.find('.galleryLightBox').removeClass('inactive');
            }
        });
        function getDuration() {
            var lastMillis, currentTime, currentMillis, duration, bannerSrc, bannerHref;
            lastMillis = lastChange.getTime();
            currentTime = new Date();
            currentMillis = currentTime.getTime();
            duration = currentMillis - lastMillis;
            lastChange = currentTime;
            return duration;
        }

        function availWidthHeight() {
            var avail = {};
            avail.height = RS3.global.viewportDimensions().cHeight - 100;
            avail.width = RS3.global.viewportDimensions().cWidth - 100;
            return avail;
        }

        function makeImageGalleries() {
            $lightboxTrigger.each(function () {
                var $this = $(this), rel = $this.attr('rel'),
                    imgSource = $this.attr('href');
                if (galleries[rel]) {
                    galleries[rel].img.push(imgSource);
                }
                else {
                    galleries[rel] = [];
                    galleries[rel].img = [];
                    galleries[rel].width = 0;
                    galleries[rel].height = 0;
                    galleries[rel].img.push(imgSource);
                }
            });
        }

        function measureImageSize() {
            for (gallery in galleries) {
                var imgArr = galleries[gallery].img,
                    img = new Image();
                img.src = imgArr[0];
                img.onload = function () {
                    if (galleries[gallery].width < img.width) {
                        galleries[gallery].width = img.width;
                    }
                    if (galleries[gallery].height < img.height) {
                        galleries[gallery].height = img.height;
                    }
                }
            }
        }

        function htmlString(arr, rel) {
            var html, elWidth = arr.width, elHeight = arr.height, ratio = elWidth / elHeight, images = arr.img,
                availHeight = availWidthHeight().height,
                availWidth = availWidthHeight().width;
            if (availWidth < elWidth) {
                elWidth = availWidth;
                elHeight = elWidth / ratio;
            }
            if (availHeight < elHeight) {
                elHeight = availHeight;
                elWidth = ratio * elHeight;
            }

            if ($('html').hasClass('no-borderimage')) {
                elHeight += 10;
            }
            html = '<div id="' + rel + '" class="flexsliderLightbox" style="overflow:hidden;width: ' + elWidth + 'px; height: ' + elHeight + 'px;">'
                + '<div class="tl corner"></div>'
                + '<div class="tr corner"></div>'
                + '<div class="bl corner"></div>'
                + '<div class="br corner"></div><ul class="slides">';
            for (var i = 0; i < images.length; i++) {
                html += '<li><img src="' + images[i] + '"></li>';
            }
            html += '</ul></div>';
            return html;
        }

        function getIndex(imgSource, arr) {
            var i;
            for (i = 0; i < arr.length; i++) {
                if (arr[i] === imgSource) {
                    break;
                }
            }
            return i;
        }

        makeImageGalleries();
        measureImageSize();
        $carousel.flexslider({
            animation: 'fade',
            slideshow: false,
            slideshowSpeed: 7000,
            controlNav: false,
            before: function (obj) {
                var target = obj.animatingTo, targetImg = obj.slides.eq(target).find('img'), $li = $('.flex-active-slide'), $li_a = $li.find('a'), bannerSrc = $li_a.find('img').attr("src");
                if (targetImg.attr('src') != targetImg.data('src')) {
                    targetImg.attr('src', targetImg.data('src'));
                }
                RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'unload', bannerSrc, getDuration());
            },
            after: function (obj) {
                var $li = $('.flex-active-slide'),
                    $li_a = $li.find('a'),
                    bannerSrc = $li_a.find('img').attr("src"),
                    nthSlide = $li.index('ul.slides>li');

                RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'load', bannerSrc, nthSlide);

            },
            start: function (obj) {
                $('.flex-viewport .slides a').click(function () {
                    var $li = $('.flex-active-slide'),
                        $li_a = $li.find('a'),
                        bannerSrc = $li_a.find('img').attr("src"),
                        nthSlide = $li.index('ul.slides>li');

                    RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'click', bannerSrc, nthSlide);

                });
            }
        });
        $carousel.on('click', '.galleryLightBox', function (ev) {
            ev.preventDefault();
            var $this = $(this), id = $this.attr('rel'),
                imgSource = $this.attr('href'),
                index = getIndex(imgSource, galleries[id].img), $slider;

            if ($this.hasClass('inactive')) return;

            $body.append(htmlString(galleries[id], id));

            $('#' + id).flexslider({
                animation: 'fade',
                slideshow: false,
                controlNav: false,
                slideshowSpeed: 7000,
                startAt: index,
                before: function (slider) {
                    var $li = $('.flex-active-slide'),
                        $li_a = $li.find('a'),
                        bannerSrc = $li_a.find('img').attr("src");

                    RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'unload', bannerSrc, getDuration());

                },
                after: function (slider) {
                    var $li = $('.flex-active-slide'),
                        $li_a = $li.find('a'),
                        bannerSrc = $li_a.find('img').attr("src"),
                        nthSlide = $li.index('ul.slides>li');

                    RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'load', bannerSrc, nthSlide);

                },
                start: function (slider) {
                    $('.flex-viewport .slides a').click(function () {
                        var $li = $('.flex-active-slide'),
                            $li_a = $li.find('a'),
                            bannerSrc = $li_a.find('img').attr("src"),
                            nthSlide = $li.index('ul.slides>li');

                        RS3.tracking.pushTrackEvent('media_slider_' + $('body').attr('id'), 'click', bannerSrc, nthSlide);
                    });
                }
            });

            $.fancybox.open($('#' + id), {
                openEffect: 'none',
                closeEffect: 'none',
                wrapCSS: 'galleryWrap',
                afterClose: function () {
                    $('#' + id).remove();
                },
                tpl: {
                    wrap: RS3.global.lightbox.wrap
                },
                scrolling: false,
                fitToView: false,
                onUpdate: RS3.global.lightbox.onUpdate
            });
        });
    },
    gfLightbox: function () {
        var $lightbox = $('.greyFrameLightbox');
        $lightbox.fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1">'
                + '<div class="fancybox-skin"><div class="fancybox-outer">'
                + '<div class="fancybox-inner greyFrame"><div class="tl corner">'
                + '</div><div class="tr corner"></div><div class="bl corner"></div><div class="br corner">'
                + '</div></div></div></div></div>'
            },
            scrolling: false,
            helpers: {
                media: {}
            },
            onUpdate: function () {

                var $greyFrame = $('.fancybox-inner.greyFrame');
                if ($('html').hasClass('no-borderimage')) {
                    $greyFrame.find('iframe').width($greyFrame.width() - 10).height($greyFrame.height() - 10);
                }
            }
        });
    },

    countdownTimer: function (countdownWrapper, animationRequired) {
        'use strict';
        var secsMilli, minsMilli, hoursMilli, daysMilli,
            diff, timeUnits, timeUnitsOldDom, timeUnitsNewDom,
            interval, currentMillis, endMillis;
        currentMillis = new Date().getTime();
        secsMilli = 1000;
        minsMilli = 60 * secsMilli;
        hoursMilli = 60 * minsMilli;
        daysMilli = 24 * hoursMilli;
        endMillis = countdownWrapper.data('endmillis');
        timeUnits = {
            days100: {
                oldNumber: 0,
                newNumber: 0
            },
            days10: {
                oldNumber: 0,
                newNumber: 0
            },
            days0: {
                oldNumber: 0,
                newNumber: 0
            },
            hours10: {
                oldNumber: 0,
                newNumber: 0
            },
            hours0: {
                oldNumber: 0,
                newNumber: 0
            },
            minutes10: {
                oldNumber: 0,
                newNumber: 0
            },
            minutes0: {
                oldNumber: 0,
                newNumber: 0
            },
            seconds10: {
                oldNumber: 0,
                newNumber: 0
            },
            seconds0: {
                oldNumber: 0,
                newNumber: 0
            }
        };
        timeUnitsNewDom = {
            days100: countdownWrapper.find(".day100 span.newNumber"),
            days10: countdownWrapper.find(".day10 span.newNumber"),
            days0: countdownWrapper.find(".day0 span.newNumber"),
            hours10: countdownWrapper.find(".hour10 span.newNumber"),
            hours0: countdownWrapper.find(".hour0 span.newNumber"),
            minutes10: countdownWrapper.find(".minutes10 span.newNumber"),
            minutes0: countdownWrapper.find(".minutes0 span.newNumber"),
            seconds10: countdownWrapper.find(".seconds10 span.newNumber"),
            seconds0: countdownWrapper.find(".seconds0 span.newNumber")
        };
        timeUnitsOldDom = {
            days100: countdownWrapper.find(".day100 span.oldNumber"),
            days10: countdownWrapper.find(".day10 span.oldNumber"),
            days0: countdownWrapper.find(".day0 span.oldNumber"),
            hours10: countdownWrapper.find(".hour10 span.oldNumber"),
            hours0: countdownWrapper.find(".hour0 span.oldNumber"),
            minutes10: countdownWrapper.find(".minutes10 span.oldNumber"),
            minutes0: countdownWrapper.find(".minutes0 span.oldNumber"),
            seconds10: countdownWrapper.find(".seconds10 span.oldNumber"),
            seconds0: countdownWrapper.find(".seconds0 span.oldNumber")
        };
        var calculateTimeRemaining = function () {
            var days, hours, minutes, seconds, rem;
            currentMillis = new Date().getTime();
            diff = parseInt(endMillis) - currentMillis;
            days = Math.floor(diff / daysMilli);
            rem = diff - (days * daysMilli);
            hours = Math.floor(rem / hoursMilli);
            rem = rem - (hours * hoursMilli);
            minutes = Math.floor(rem / minsMilli);
            rem = rem - (minutes * minsMilli);
            seconds = Math.floor(rem / secsMilli);
            $.each(timeUnits, function (key) {
                this.oldNumber = this.newNumber;
            });
            timeUnits.days100.newNumber = Math.floor(days / 100);
            timeUnits.days10.newNumber = Math.floor((days % 100) / 10);
            timeUnits.days0.newNumber = days % 10;
            timeUnits.hours10.newNumber = Math.floor(hours / 10);
            timeUnits.hours0.newNumber = hours % 10;
            timeUnits.minutes10.newNumber = Math.floor(minutes / 10);
            timeUnits.minutes0.newNumber = minutes % 10;
            timeUnits.seconds10.newNumber = Math.floor(seconds / 10);
            timeUnits.seconds0.newNumber = seconds % 10;
        };
        var updateTimeUnit = function (elem, number, anim) {
            var reset;
            if (number.oldNumber !== number.newNumber) {
                changeImgSrc(timeUnitsOldDom[elem], number.oldNumber, "oldNumber");
                timeUnitsOldDom[elem].show();
                changeImgSrc(timeUnitsNewDom[elem], number.newNumber, "newNumber");

                if (anim) {
                    reset = function () {
                        timeUnitsOldDom[elem].hide().css({
                            opacity: 1
                        }).removeClass("animate");
                    };
                    timeUnitsOldDom[elem].animate({
                        opacity: 0
                    }, 800, reset).addClass("animate");
                    ;
                }

                else {
                    timeUnitsOldDom[elem].hide();
                }
                timeUnitsNewDom[elem].show();
            }
        };
        var changeImgSrc = function (elem, number, extraClass) {
            elem.removeClass().addClass(extraClass + " num" + number);
        };
        var modifyDom = function (anim) {
            if (diff < 0) {
                clearInterval(interval);
                return;
            }
            $.each(timeUnits, function (key) {
                updateTimeUnit(key, timeUnits[key], anim);
            });
        };
        var run = function (anim) {
            calculateTimeRemaining();
            modifyDom(anim);
        };
        run(false);
        if (diff > 0) {
            countdownWrapper.show().animate({'opacity': '1'});
            interval = setInterval(function () {
                run(animationRequired)
            }, 1000);
        }
    },
    tooltip: function () {
        $('.tooltipLink').on('mouseenter', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover);
            $tip.show();
        }).on('mouseleave', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover);
            $tip.hide();
        }).on('click', function (e) {
            e.preventDefault();
        }).on('mousemove', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover), offset = $ele.parents('.tooltipOuter').offset();
            $tip.css({'top': (e.pageY - offset.top) + 10, 'left': (e.pageX - offset.left) + 20});
        })
    },
    detailsElement: function () {
        var summaryElements = $('summary');
        summaryElements.children().on('click', function (ev) {
            ev.stopPropagation();
        });
        summaryElements.on('click', function () {
            $(this).parent().toggleClass('details--show');
        });
    },
    init: function () {
        'use strict';
        RS3.global.facebook.loadFacebookAPI(RS3.global.facebook.setUpEvents);
        RS3.global.external();
        RS3.global.internal();
        RS3.global.playerCount();
        RS3.global.logIn();
        RS3.global.lightbox.init();
        RS3.global.tabbedElement();
        RS3.global.responsiveMenu();
        RS3.global.headerDescriptionToggle();
        RS3.global.categoryDropDown.getNewsCats();
        RS3.global.galleryLightBox();
        RS3.global.gfLightbox();
        RS3.global.tooltip();
    }
};
RS3.splashpage = {
    tracking: function () {
        'use strict';
        $('#continue').on('click', function (ev) {
            RS3.tracking.pushTrackEvent('mainsite_continue_splash', 'click', pageLocation);
        });
        $('#trailerWatch').on('click', function () {
            RS3.tracking.pushTrackEvent('video_splash', 'click', pageLocation);
        });
    },
    init: function () {
        'use strict';
        RS3.splashpage.tracking();
    }
};
RS3.tracking = {
    trackingPixel: function (ev, strTPTag, newLocation, newStyle) {
        'use strict';
        var urlPrefix = baseURL + '/img/track/2013_';
        if (newStyle) {
            urlPrefix = baseURL + '/img/track/track.gif?jptg=rswf2&jptv=0-';
        }
        if (ev != null) {
            ev.preventDefault();
        }
        //Create pixel at defined src
        var track = new Image();
        track.src = urlPrefix + strTPTag;
        if (newLocation) {
            window.location = newLocation;
        }
        ;
        return false;
    },
    pushTrackEvent: function (category, action, optLabel, optValue) {
        'use strict';
        if (category && action) {
            _gaq.push(['_trackEvent', category, action, optLabel, optValue, true]);

        }
    },
    init: function () {
        $('.userBarContent .play').on('click', function () {
            var isLoggedIn = 0;
            if (loggedIn !== -1) {
                isLoggedIn = 1;
            }
            RS3.tracking.pushTrackEvent('play_button_topnav', 'click', pageLocation, isLoggedIn);
        });
        $('#newAccount').on('click', function () {
            RS3.tracking.pushTrackEvent('account_create', 'click', pageLocation);
        });
        $('.language div ul li').on('click', function () {
            var langClass, langNum;
            langClass = $(this).attr('class');
            if (langClass === 'en') {
                langNum = 0;
            } else if (langClass === 'de') {
                langNum = 1;
            } else if (langClass === 'fr') {
                langNum = 2;
            } else if (langClass === 'pt') {
                langNum = 3;
            } else if (langClass === 'es') {
                langNum = 6;
            }
            RS3.tracking.pushTrackEvent('language_selector', 'click', langClass, langNum);
        });
    }
}
RS3.homepage = {
    carousel: function () {
        'use strict';
        var $carousel = $('#carousel'), lastChange = new Date();

        function getDuration() {
            var lastMillis, currentTime, currentMillis, duration, bannerSrc, bannerHref;
            lastMillis = lastChange.getTime();
            currentTime = new Date();
            currentMillis = currentTime.getTime();
            duration = currentMillis - lastMillis;
            lastChange = currentTime;
            return duration;
        }

        if ($carousel.find('li').length > 1) {
            $carousel.flexslider({
                animation: 'fade',
                slideshow: true,
                slideshowSpeed: 7000,
                before: function (obj) {
                    var target = obj.animatingTo, targetImg = obj.slides.eq(target).find('img'), $li = $('.flex-active-slide'), $li_a = $li.find('a'), bannerSrc = $li_a.find('img').attr("src");
                    if (targetImg.attr('src') != targetImg.data('src')) {
                        targetImg.attr('src', targetImg.data('src'));
                    }
                },
                after: function (slider) {
                    var $li = $('.flex-active-slide'),
                        $li_a = $li.find('a'),
                        bannerSrc = $li_a.find('img').attr("src"),
                        nthSlide = $li.index('#carousel ul.slides>li');
                },
                start: function (slider) {
                    $('.flex-viewport .slides a').click(function () {
                        var $li = $('.flex-active-slide'),
                            $li_a = $li.find('a'),
                            bannerSrc = $li_a.find('img').attr("src"),
                            nthSlide = $li.index('#carousel ul.slides>li');
                        RS3.tracking.pushTrackEvent('community_banner', 'click', bannerSrc, nthSlide);
                    });
                }
            });
        }
    },
    promos: {
        savePromos: function (settings) {
            var data = 'promoBoxes=' + encodeURIComponent(settings);
            $.ajax({
                type: 'POST',
                url: savePromoBoxes,
                data: data,
                dataType: 'jsonp',
                cache: false,
                success: function (data) {

                },
                error: function (request, status, error) {
                    alert('There has been an error saving your choice. Please try again. [' + error + ']');
                }
            });
        },
        tracking: function ($promos) {
            $promos.on('click', '.selection li h3 a', function (ev) {
                var $this = $(this), text = $this.text(), index = $this.index('.selection li h3 a') + 1;
                RS3.tracking.pushTrackEvent('community_promo', 'click', text, index);
            });
        },
        init: function () {
            var $promos = $('#promos');
            RS3.homepage.promos.tracking($promos);
            $promos.on('click', '.popup a', function (e) {
                e.preventDefault();

                var $self = $(this), contentType = $self.data('content'), text = $self.text(), parent = $self.parents('.custom'), href = this.href;
                parent.find('.active').removeClass('active');
                $self.addClass('active');
                parent.removeClass().addClass(contentType + ' custom');
                if (contentType === 'youtube') {
                    parent.find('h3').html('<a target="_blank" href="' + href + '">' + text + '</a>');
                } else {
                    parent.find('h3').html('<a href="' + href + '">' + text + '</a>');
                }

                if (loggedIn != -1) {

                    var box0 = parseInt($promos.find('.custom:eq(0) .active').data('id')),
                        box1 = parseInt($promos.find('.custom:eq(1) .active').data('id')),
                        box2 = parseInt($promos.find('.custom:eq(2) .active').data('id'));
                    settings = {box0: box0, box1: box1, box2: box2};
                    RS3.homepage.promos.savePromos(JSON.stringify(settings));
                }
            });
        }
    },
    poll: function () {
        'use strict';
        var widths = [];
        var $pollResults = $("ul#pollResults");
        var $arrpollResult = $("ul#pollResults").find('li.pollResult');


        $arrpollResult.removeClass('real');


        $arrpollResult.find('div').hide();


        $arrpollResult.each(function (index, ele) {
            var $el = $(ele);
            var $voteBar = $el.find("div.voteBar");
            var voteBarWidth = $voteBar.width();
            widths.push([index, voteBarWidth]);
        });


        widths.sort(function (a, b) {
            return b[1] - a[1]
        });


        $.each(widths, function (index, arr) {
            var $old = $pollResults.find('li.pollResult:eq(' + arr[0] + ')');
            var $clone = $old.clone();
            $clone.addClass('real').appendTo('ul#pollResults');

        });


        $arrpollResult.not('.real').remove();


        var $arrpollResult = $("ul#pollResults").find('li.pollResult');


        $arrpollResult.each(function (index, ele) {
            var $el = $(ele);
            var $voteText = $el.find("div.voteText");
            var $voteOuter = $el.find("div.voteBarOuter");
            var $voteBar = $voteOuter.find('div.voteBar');

            var voteBarWidth = $voteBar.width(), maxAnimateSpeed = 1000;
            $voteBar.width(0);


            $voteText.fadeIn(1000, function () {
                $voteOuter.show();
                $voteBar.show().animate({'width': voteBarWidth + 'px'}, maxAnimateSpeed);
            });
        });
    },
    playNow: function () {

        $('#play').on('click', function (ev) {
            RS3.tracking.pushTrackEvent('home_page_header_play_button_clicked', 'click', pageLocation);
            RS3.tracking.trackingPixel(ev, '9200', this.href, true);
        });
    },
    accountsCreated: function () {
        RS3.homepage.accountsCreatedAjax();
        setInterval(function () {
            RS3.homepage.accountsCreatedAjax();
        }, 30000);
    },
    accountsCreatedAjax: function () {
        $.ajax({
            url: "http://services.runescape.com/m=account-creation-reports/rsusertotal.ws",
            dataType: 'jsonp',
            timeout: 3000
        }).done(function (data) {
            if (typeof data.accountsformatted !== 'undefined') {
                $('#numberAccounts').html(data.accountsformatted);
            }
        });
    },
    lazyLoadVideos: function (ev) {
        $('#videoLink').on('click', function (ev) {
            $('#videos figure img').each(function () {
                $(this).attr('src', $(this).data('original'));
            });
            $(this).unbind('click');
        });
    },
    init: function () {
        'use strict';
        RS3.tracking.trackingPixel(null, 9000, null, true);
        RS3.homepage.carousel();
        RS3.homepage.promos.init();
        RS3.homepage.playNow();
        RS3.homepage.accountsCreated();
        if ($('#pollResults').length) {
            RS3.homepage.poll();
        }
        RS3.homepage.lazyLoadVideos();
    }
};
RS3.news = {
    youtube: function () {

        'use strict';
        var $youtube = $('#youtube'), video = [];
        $youtube.hide();
        $.ajax({
            type: "GET",
            url: "http://gdata.youtube.com/feeds/users/runescape/uploads?alt=json-in-script&format=5&max-results=1",
            cache: false,
            dataType: 'jsonp',
            success: function (data) {
                video.push('<div class="index">');
                $(data.feed.entry).each(function (entry) {
                    var url = this.link[0].href, thumbnail = this.media$group.media$thumbnail[0].url, title = this.media$group.media$title.$t, description = this.media$group.media$description.$t, category = this.media$group.media$category[0].label, published = this.published.$t;
                    var date = Date.fromString(published);
                    var dayDate = date.getDate(), newDate = dayDate + (dayDate > 10 && dayDate < 20 ? 'th' : {
                            1: 'st',
                            2: 'nd',
                            3: 'rd'
                        }[dayDate % 10] || 'th');
                    var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][date.getMonth()];
                    var year = date.getFullYear();
                    video.push('<article class="video">');
                    video.push('<figure class="greyFrame youtube">');
                    video.push('<div class="tl corner"></div>');
                    video.push('<div class="tr corner"></div>');
                    video.push('<div class="bl corner"></div>');
                    video.push('<div class="br corner"></div>');
                    video.push('<a class="lightbox" href="' + url + '"><img src="' + thumbnail + '" alt="' + title + '" title="' + description + '" /></a></figure>');
                    video.push('<h4><a href="' + url + '" class="lightbox">' + title + '</a></h4><time>Published on ' + newDate + ' ' + month + ' ' + year + '</time>');
                    video.push('<p>' + description + '</p>');
                    video.push('</article>');
                });
                video.push('</div>');
                video.push("<div class=\"btnWrap more\"><div class=\"btn\"><a href=\"http://www.youtube.com/RuneScape\" data-title=\"Visit Channel\" class=\"ext\"><span>Visit Channel</span></a></div></div>");
                $youtube.html(video.join(''));
                $youtube.fadeIn();
            }
        });
    },
    moreNews: function () {
        var $moreNews = $('#moreNews,#moreVideos');
        var ajaxResponse = '', isArchive, currentTotal;

        $moreNews.on('click', 'a', function (e) {
            e.preventDefault();
            //determines if you're exploring the archive'
            if ($(this).parents('#moreNews').hasClass('moreArchive')) {
                isArchive = true;
            }
            ;
            var $self = $(this), id = $self.parents('.btnWrap').attr('id'), href = this.href,
                currentPage = href.substr(href.indexOf('&page=') + 6), nextPage = parseInt(currentPage) + 1,
                newHref = href.substr(0, href.length - currentPage.length) + nextPage,
                parent = id === 'moreNews' ? '#newsSection' : '#videoSection';
            $.ajax({
                url: href,
                success: function (dataIn) {
                    var data = $.parseHTML(dataIn);

                    var response = $(data).find(parent + ' article');
                    var responseHTML = $(data).find(parent).html();
                    if (responseHTML == ajaxResponse) {
                        $self.parents('.btnWrap').hide();
                    }
                    else {
                        $(parent).append(response);
                        ajaxResponse = data;
                        if (!isArchive && nextPage === 4) {
                            $self.attr('href', 'http://services.runescape.com/m=news/archive');
                            $self.find('span').text('Visit Archive');
                            $self.parents('.btnWrap').removeClass('showMore').unbind('click');
                        } else {
                            $self.attr('href', newHref);
                        }
                        ajaxResponse = responseHTML;
                        //needs to trigger post update to count new items
                        if (fullTotal) {
                            currentTotal = $('#newsSection').children("article").length;
                            if (currentTotal >= fullTotal) {
                                $self.parents('.btnWrap').hide();
                            }
                            ;
                        }
                        ;
                    }
                },
                error: function () {
                    alert('Sorry - there has been an error processing this request. Please try again.');
                }
            });
        });
    },
    archiveButton: function () {
        var $button = $('#archiveButton'), $archive = $('#archiveSelect');
        $button.on('click', function (e) {
            e.preventDefault();
            $archive.stop().slideToggle(250, function () {
                var $self = $(this);
                $button.toggleClass('expand', $self.is(':visible'));
            });
        });
    },
    archiveFilter: function () {
        var $button = $('.archiveSelectSubmit').slideDown();
        $('.months a').on('click', function (ev) {
            ev.preventDefault();
            var $this = $(this);
            $('.months li').removeClass('active');
            $this.parent().addClass('active');
            $('#month').val($this.attr('data-month'));
        });
        $('.years a').on('click', function (ev) {
            ev.preventDefault();
            var $this = $(this);
            $('.years li').removeClass('active');
            $this.parent().addClass('active');
            $('#year').val($this.attr('data-year'));
        });
    },
    init: function () {
        RS3.news.youtube();
        RS3.news.moreNews();
        RS3.news.archiveButton();
        RS3.news.archiveFilter();
    }
};
RS3.mediavideos = {
    youtube: function () {
        if ($('#videoPanelArea').length) {
            var panelArea = $('#videoPanelArea');
            var playlistID = 'PLMjuVhi1Lg6f-Zyo6ZZPuU0deaAGCy_G2';


            panelArea.html('Loading Videos...');


            var numberVideos = panelArea.data('value');
            var videoURL = 'http://www.youtube.com/embed/';
            var videoBoxStart = "<div class='mediaEntity video'>";
            var videoHoverButtons = "<span class='HoverImgJs'><img src='http://www.runescape.com/img/global/buttons/navigationNext.png' alt='Video' title='Video'><img src='http://www.runescape.com/img/global/buttons/navigationNext.png' class='HoverImgJsFg'></span>";
            $.ajax({
                type: "GET",
                url: "http://gdata.youtube.com/feeds/api/playlists/" + playlistID + "?v=2&alt=json&orderby=published&max-results=" + numberVideos,
                cache: false,
                dataType: 'jsonp',
                success: function (data) {
                    var list_data = "";
                    $.each(data.feed.entry, function (i, item) {
                        var feedTitle = item.title.$t;
                        var feedURL = item.link[1].href;
                        var fragments = feedURL.split("/");
                        var videoID = fragments[fragments.length - 2];
                        var url = videoURL + videoID + '?autoplay=1';
                        var thumb = "http://img.youtube.com/vi/" + videoID + "/hqdefault.jpg";
                        list_data += videoBoxStart + "<a href='" + url + "' rel='shadowbox[videos];height=315;width=560;player=iframe'>" + videoHoverButtons + "<img src='" + thumb + "' alt='Video' title='Video' width='218' height='164' class='thumb'></a></div>";
                    });
                    panelArea.html(list_data);
                    Shadowbox.init({
                        continuous: true,
                        counterType: "skip",
                        overlayColor: "#000000",
                        overlayOpacity: 0.8,
                        players: ['img', 'iframe']
                    });
                }
            });
        }
    }
};
RS3.infographic = {
    createDownLoadLink: function () {
        'use strict';


        var settings = ["Source: RuneScape 3", "RuneScape 3 - Free-to-play MMORPG", "RuneScape 3 - The Online Medieval Fantasy Adventure", "RuneScape Infographic: 1 million years of game play", "Free MMORPG", "RuneScape 3: Your Greatest Adventure"];

        var num = 6;
        var rand = Math.floor(Math.random() * num);
        var richtext = settings[rand];

        var $textarea = $('#infographicCode');
        var environment = "http://www.runescape.com";
        var page = 'media/infographics/rs3_mmorpg_1_million_years_in_making';
        var image = '/img/3rdparty/RuneScape-Infographic-1million-Years.jpg';
        var lang = '/';

        var textareaVal = "<a href=\"http://www.runescape.com" + lang + page + "\"><img src=\"" + environment + image + "\" alt=\"RuneScape Infographic: 1 million years of game play\" title=\"RuneScape Infographic: 1 million years of game play\" /></a><br /><a href=\"http://www.runescape.com" + lang + "\">" + richtext + "</a>";

        $textarea.val(textareaVal);
    },
    infographicEvents: function () {
        $('#infographicCode').on('click', function () {
            $(this).select();
        });

        $('#infoEmbedLink').on('click', 'a', function (e) {
            e.preventDefault();
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 500);
        });

        $('#infoPlayButton').on('click', function () {
            RS3.tracking.pushTrackEvent('play_button_bottom', 'click', document.location.href);
        });


        $twitterAnchor = $('ul.infoFollow').find('li.twitter a');
        $twitterAnchor.each(function () {
            var $ele = $(this), href = $ele.attr('href');
            href = href + 'text=' + encodeURIComponent("RuneScape 3 - 1 million years in the making #mmorpg") + '&amp;http://www.runescape.com/media/infographics/eoc-mmorpg-1-million-years-in-making';
            $ele.attr('href', href);
        });
    },
    init: function () {
        RS3.infographic.createDownLoadLink();
        RS3.infographic.infographicEvents();
    }
};
RS3.membersBenefits = {
    scrollTo: function () {
        'use strict';

        $('#allBenefits').on('click', 'a', function (ev) {
            ev.preventDefault();
            var $anchor = $(this);

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 500);
        });
    },
    hexPanel: function () {

        $('.hexPanel').on('click', function (ev) {
            var $this = $(this), $parent = $this.parent();
            $parent.find('.hexContent').fadeIn(200);
            $this.parents('.hexBoxes').find('.deco').fadeOut(180);
            RS3.tracking.trackingPixel(ev, 7000 + '/' + $parent.attr('class') + '/' + 1);
            RS3.tracking.pushTrackEvent('members_benefits', 'hexpanel_click', $this.find('p').text(), $this.index('.hexPanel') + 1);
        });
        $('.hexContent .close').on('click', function () {
            var $this = $(this);
            var $parent = $this.parent();
            $parent.fadeOut(200);
            $this.parents('.hexBoxes').find('.deco').fadeIn(220);
            RS3.tracking.pushTrackEvent('members_benefits', 'hexcontent_close', $parent.find('h3').text(), $parent.index('.hexContent') + 1);
        });
    },
    buyNowButtons: function () {

        if (!visitorReady) {
            var $buttons = $("a.openLoginPanelMB,.btnWrap:not(.noLogin) a");
            $buttons.each(function (i) {
                var $this = $(this),
                    oldHref = $this.attr("href");
                $this.attr("data-href", oldHref);
                $this.attr("href", '#loginPanel').fancybox({'wrapCSS': 'loginFancyBox'});
            });
        }


        $("a.openLoginPanelMB,.btnWrap a").on('click', function (ev) {
            var $anchor = $(this),
                dest = 'packagegroupredirect.ws',
                href = $anchor.attr('data-href'),
                package = 0,
                strPackage = 'bottom',
                strParam = 'rs1m';
            if (href.indexOf('rs1w') > 0) {
                package = 1;
                strPackage = '1 Week';
                strParam = 'rs1w';
            }
            if (href.indexOf('rs1m') > 0) {
                package = 1;
                strPackage = '1 Month';
            }
            if (href.indexOf('rs3m') > 0) {
                package = 3;
                strPackage = '3 Months';
                strParam = 'rs3m';
            }
            if (href.indexOf('rs4m') > 0) {
                package = 4;
                strPackage = '4 Months';
                strParam = 'rs4m';
            }
            if (href.indexOf('rs4m') > 0) {
                package = 4;
                strPackage = '4 Month';
                strParam = 'rs4m';
            }
            if (href.indexOf('rs6m') > 0) {
                package = 6;
                strPackage = '6 Months';
                strParam = 'rs6m';
            }
            if (href.indexOf('rs12m') > 0) {
                package = 12;
                strPackage = '12 Months';
                strParam = 'rs12m';
            }
            if (href.indexOf('purchase.ws') > 0) {
                dest = 'purchase.ws'
            }
            else {
                if (href.indexOf('packagegroupredirect.ws') > 0) {
                    dest = dest + '?value=' + strParam;
                    RS3.membersBenefits.trackingPixel(null, null, 1, strParam);
                }
            }
            $('#loginPanel input[name=mod]').attr('value', 'billing_core');
            $('#loginPanel input[name=ssl]').attr('value', 1);
            $('#loginPanel input[name=dest]').attr('value', dest);
            $('.facebookLogin').data('redirecturl', "mod=billing_core&ssl=1&dest=" + dest);


            RS3.membersBenefits.updateAffiliateLinks();

            RS3.tracking.pushTrackEvent('members_benefits', 'buy_now', strPackage, package);
        });


        $('#membersBenefits #inline').on('click', function () {
            var destVal = $('#loginPanel input[name=dest]').val();
            if (destVal.indexOf('members_benefits') == -1) {
                $('#loginPanel input[name=mod]').attr('value', 'www');
                $('#loginPanel input[name=ssl]').attr('value', 0);
                $('#loginPanel input[name=dest]').attr('value', 'members_benefits');
            }
        });


        $('#playVideoLink').on('click', function () {
            RS3.tracking.pushTrackEvent('video_members_benefits', 'click', pageLocation);
        });


    },
    updateAffiliateLinks: function () {
        if ($('.alternativeLogin').length) {
            var $links = $('.alternativeLogin li').find('a:not(.facebookLogin)');
            $links.each(function (e) {
                var $ele = $(this), href = $ele.attr('href');
                var _parts = href.split('?');
                var link = _parts[0];
                var query = '?mod=' + $('#loginPanel input[name=mod]').val() + '&ssl=' + $('#loginPanel input[name=ssl]').val() + '&dest=' + encodeURIComponent($('#loginPanel input[name=dest]').val());
                var fullLink = link + query;
                $ele.attr('href', fullLink);
            })
        }
    },

    tracking: function () {
        'use strict';

        RS3.membersBenefits.trackingPixel(null, null, 0, '');

        $('ul#membersBenefitsHex').on('click', '.hexPanel', function () {
            var tracking = $(this).data('tracking');
            RS3.membersBenefits.trackingPixel(null, null, tracking, '1:expand');
        }),
            $('ul#membersBenefitsHex').on('click', '.close', function () {
                var tracking = $(this).parent().siblings('.hexPanel').data('tracking');
                RS3.membersBenefits.trackingPixel(null, null, tracking, '2:close');
            }),
            $('ul#membersBenefitsHex').on('click', '.buyNow', function () {
                var tracking = $(this).parent().siblings('.hexPanel').data('tracking');
                RS3.membersBenefits.trackingPixel(null, null, tracking, '3:click buy');
            }),
            $('#seeAllBenefits').on('click', function () {
                RS3.membersBenefits.trackingPixel(null, null, 2, '');
            }),
            $('#buyNowBottom').on('click', function () {
                RS3.membersBenefits.trackingPixel(null, null, 20, '');
            }),
            $('#facebooklogin').on('click', function () {
                RS3.membersBenefits.trackingPixel(null, null, 27, '');
            }),
            $('ul#footerSocialButtons li a').on('click', function () {
                var tracking = null;
                switch ($(this).parent().attr('class')) {
                    case"rss":
                        tracking = 30;
                        break;
                    case"facebook":
                        tracking = 31;
                        break;
                    case"twitter":
                        tracking = 32;
                        break;
                    case"youtube":
                        tracking = 33;
                        break;
                    case"google":
                        tracking = 34;
                        break;
                    case"reddit":
                        tracking = 35;
                        break;
                }
                RS3.membersBenefits.trackingPixel(null, null, tracking, '');
            }),
            $('#footerLikeButton').on('click', function (e) {
                e.stopPropagation();
                RS3.membersBenefits.trackingPixel(null, null, 36, '');
            }),
            $('#footerTermsLink').on('click', function (ev) {
                RS3.membersBenefits.trackingPixel(null, null, 37, '');
            }),
            $('#footerPrivacyLink').on('click', function (ev) {
                RS3.membersBenefits.trackingPixel(null, null, 38, '');
            }),
            $('#footerCookiesLink').on('click', function (ev) {
                RS3.membersBenefits.trackingPixel(null, null, 39, '');
            }),
            $('#footerSelectLanguage').on('click', 'a', function (ev) {
                RS3.membersBenefits.trackingPixel(ev, $(this).attr('href'), 40, '');
            });

    },

    trackingPixel: function (ev, newLocation, action, param) {
        'use strict';
        var url = 'http://www.runescape.com/members_benefits?jptg=mbt&jptv=' + action + '|' + param;
        if (ev != null) {
            ev.preventDefault();
        }
        $.ajax({
            url: url,
            dataType: 'jsonp',
            timeout: 3000,
            complete: function () {
                if (newLocation) {
                    window.location = newLocation;
                }
            }
        });
        return false;
    },
    init: function () {
        'use strict';
        RS3.membersBenefits.hexPanel();
        RS3.membersBenefits.scrollTo();
        RS3.membersBenefits.buyNowButtons();
        RS3.membersBenefits.tracking();
    }
};
RS3.hiscores = {
    init: function () {
        'use strict';
        RS3.hiscores.countdownTimer();
        RS3.hiscores.compareFormToggle();
        RS3.hiscores.findMeToggle();
        RS3.hiscores.factionWidthBar();
        RS3.hiscores.tempShowMoreIcon();
        RS3.hiscores.tempFilterShowMoreIcon();
        RS3.hiscores.tempDescriptionHover();
        RS3.hiscores.tempRankingSidebar();
        RS3.hiscores.overviewPage.init();
    },
    countdownTimer: function () {
        var myTimers = [];
        $('.countdownTimer').each(function () {
            var that = $(this);
            myTimers.push(RS3.global.countdownTimer(that));
        });
    },
    compareFormToggle: function () {
        if ($('.changeCharacterLink').length && $('.changeCharacterForm').length) {
            $('a.changeCharacterLink').on('click', function (e) {
                e.preventDefault();
                $('a.changeCharacterLink').toggleClass('changeCharacterOpen');
                $('.changeCharacterForm').slideToggle();
            });
        }
    },
    findMeToggle: function () {
        if ($('.findMeContent').length && $('.findMeLink').length) {
            $('a.findMeLink').on('click', function (e) {
                e.preventDefault();
                $(this).toggleClass('findMeLinkOpen');
                $('.findMeContent').slideToggle();
            });

        }
    },
    factionWidthBar: function () {
        if ($('#factionWidth').length) {
            var $ele = $('#factionWidth');
            var moveToWidth = $ele.data('width');
            setTimeout(function () {
                $ele.animate({'width': moveToWidth + 'px'});
            }, 1000);

        }
    },
    tempDescriptionHover: function () {

        $('#futureHiScoreSegment').on('mouseenter', 'a.descLinkStyle', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover);
            $tip.show();
        }).on('mousemove', 'a.descLinkStyle', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover), offset = $ele.parents('.paraDesc').offset();
            $tip.css({'top': (e.pageY - offset.top) + 10, 'left': (e.pageX - offset.left) + 20});
        }).on('mouseleave', 'a.descLinkStyle', function (e) {
            var $ele = $(this), dataHover = $ele.data('target'), $tip = $('#' + dataHover);
            $tip.hide();
        }).on('click', 'a.descLinkStyle', function (e) {
            e.preventDefault();
        });
    },
    tempFilterShowMoreIcon: function () {
        if ($('.tempHSFilterShowMoreButton').length) {
            var $ajaxLoadDiv = $('#ajaxLoadOuter');

            $('#futureHiScoreSegment,#pastHiScoreSegment').on('click', '.tempHSFilterShowMoreButton a', function (e) {
                e.preventDefault();
                var $ele = $(this), href = $ele.attr('href') + ' #ajaxLoadInner', $parent = $ajaxLoadDiv.parent('.tempHSOverviewSegment');
                $parent.css('min-height', $parent.outerHeight());
                $ajaxLoadDiv.fadeOut(250, function () {
                    $parent.addClass('tempLoading');
                    $ajaxLoadDiv.load(href, function () {
                        $parent.removeClass('tempLoading');
                        $ajaxLoadDiv.fadeIn(250);
                        RS3.hiscores.tempDescriptionHover();
                    })
                });
            });

        }
    },
    tempShowMoreIcon: function () {
        var i = 1;
        if ($('.tempHSExtraBoxContainer').length > 0) {
            $('.tempHSShowMoreButton a span').html('Show More');
            $('.tempHSOverDividerActive').addClass('tempHSOverDividerShort');
            $('.tempHSShowMoreContainerActive').show();
            $('.tempHSShowMoreButton a').on('click', function (e) {
                var $ele = $(this), elIndex = $('.tempHSShowMoreButton a').index(this),
                    box = $('.tempHSExtraBoxContainer').get(elIndex);

                if ($ele.parents('.activeShowMoreButton').length) {
                    if ($(box).data('shown') !== true) {
                        e.preventDefault();
                        $(box).data('shown', true);
                        $ele.parents('.btnWrap').hide();
                        $('.tempHSOverDividerActive').removeClass('tempHSOverDividerShort');
                        $(box).slideDown();
                    }
                }
                else {
                    if ($(box).data('shown') !== true) {
                        e.preventDefault();
                        $(box).data('shown', true);
                        $(box).slideDown();
                        $ele.find('span').html('View All');
                    }
                }
            });
        }
    },
    tempRankingSidebar: function () {
        $('a.seasonalEventLink').on('click', function (e) {
            e.preventDefault();
            var $ele = $(this), loadDiv = $ele.data('load'), $loadDiv = $('#' + loadDiv), $activeDiv = $('.seasonalEventShell:visible');
            $activeDiv.fadeOut(250, function () {
                $loadDiv.fadeIn(250);
            })
        });
    },
    overviewPage: {
        init: function () {
            RS3.hiscores.overviewPage.populateCarousel();
        },
        activateCarousel: function () {
            if ($('#hsCarousel').length) {
                var $carousel = $('#hsCarousel');
                $carousel.flexslider({
                    animation: 'fade',
                    slideshow: true,
                    slideshowSpeed: 7000,
                    controlNav: false,
                    start: function (obj) {
                        var target = obj.find('.slides .overviewSliderItem img');
                        if (target.data('src')) {
                            if (target.attr('src') != target.data('src')) {
                                target.attr('src', target.data('src'));
                            }
                        }
                    }
                });
            }
        },
        populateCarousel: function () {
            if ($('#hsCarousel').length) {


                var listItem1 = [];
                var hsAjax = $.getJSON('http://services.runescape.com/m=hiscore/ranking.json?table=0&category=0&size=3', function (data) {
                    listItem1.push('<thead><tr><th class="ov1">Rank</th><th class="ov2">Character Name</th></tr></thead><tbody>')
                    $.each(data, function (index, item) {
                        var encodedName = escape($.trim(item.name));
                        if (loggedIn != -1) {
                            var url = RS3.global.addCToLink('http://services.runescape.com/m=hiscore/compare?user1=' + encodedName);
                        }
                        else {
                            var url = 'http://services.runescape.com/m=hiscore/compare?user1=' + encodedName;
                        }

                        listItem1.push("<tr><td class=\"ov1\"><a href=\"" + url + "\">" + item.rank + "</a></td>");
                        listItem1.push("<td class=\"ov2\"><a href=\"" + url + "\">" + item.name + "</a></td></tr>");
                    });
                    listItem1.push("<tr class=\"hover loginRow\" id=\"hsRank\"><td class=\"ov1\">Your Rank</td><td class=\"ov2\"><a href=\"http://services.runescape.com/m=hiscore/overview?findme=1\" class=\"specialLink\">Log in</a> to find out</td></tr>");
                    listItem1.push('</tbody>');
                    $('#topHiscoreAjaxResponse').append(listItem1.join(''));
                    $('#topHiscoreOuter').show();

                }).fail(function () {
                    $('#topHiscoreOuter').hide();
                });


                var listItem2 = [];
                var clanAjax = $.getJSON('http://services.runescape.com/m=clan-hiscores/clanRanking.json', function (data) {
                    listItem2.push("<thead><tr><th class=\"ov1 textCenter\">Rank</th><th class=\"ov2\">Clan Name</th></tr></thead><tbody>")
                    $.each(data, function (index, item) {
                        var encodedName = escape($.trim(item.clan_name));
                        if (loggedIn != -1) {
                            var url = RS3.global.addCToLink('http://services.runescape.com/m=clan-hiscores/compare.ws?clanName=' + encodedName);
                        }
                        else {
                            var url = 'http://services.runescape.com/m=clan-hiscores/compare.ws?clanName=' + encodedName;
                        }

                        listItem2.push("<tr><td class=\"ov1\"><a href=\"" + url + "\">" + item.rank + "</a></td>");
                        listItem2.push("<td class=\"ov2\"><a href=\"" + url + "\">" + item.clan_name + "</a></td></tr>");
                    });
                    listItem2.push("<tr class=\"hover loginRow\" id=\"clanRank\"><td class=\"ov1\">Your Rank</td><td class=\"ov2\"><a href=\"http://services.runescape.com/m=hiscore/overview?findme=1\" class=\"specialLink\">Log in</a> to find out</td></tr>");
                    listItem2.push('</tbody>');
                    $('#clanHiscoreAjaxResponse').append(listItem2.join(''));
                    $('#clanHiscoreOuter').show();

                }).fail(function () {
                    $('#clanHiscoreOuter').hide();
                });


                $.when(hsAjax, clanAjax).then(function () {
                    if (loggedIn != -1) {
                        RS3.hiscores.overviewPage.getUserRank();
                    }
                    else {
                        RS3.hiscores.overviewPage.activateCarousel();
                    }
                }).fail(function () {
                    RS3.hiscores.overviewPage.activateCarousel();
                });
            }
        },
        getUserRank: function () {
            var ajaxURL = RS3.global.addCToLink('http://services.runescape.com/m=hiscore/userRanking.json');
            $('#hsRank').hide();
            var hsRankAjax = $.getJSON(ajaxURL, function (data) {
                if (data.displayName != '' && parseInt(data.rank) != 0) {
                    if ($('#hsRank').length) {
                        var listItem3 = [];
                        var encodedName = escape($.trim(data.displayName));
                        if (loggedIn != -1) {
                            var url = RS3.global.addCToLink('http://services.runescape.com/m=hiscore/compare?user1=' + encodedName);
                        }
                        else {
                            var url = 'http://services.runescape.com/m=hiscore/compare?user1=' + encodedName;
                        }
                        listItem3.push("<td class=\"ov1\"><a href=\"" + url + "\">" + data.userRank + "</a></td>");
                        listItem3.push("<td class=\"ov2\"><a href=\"" + url + "\">" + data.displayName + "</a></td>");
                        $('#hsRank').html(listItem3.join('')).removeClass('loginRow');
                    }
                }
                else {
                    $('#hsRank').html('<td colspan="2">There is no rank to show</td>');
                }
            }).fail(function () {
                $('#hsRank').html('<td colspan="2">There is no rank to show</td>');
            }).always(function () {
                $('#hsRank').show();
            });

            ajaxURL = RS3.global.addCToLink('http://services.runescape.com/m=clan-hiscores/userClanRanking.json');
            $('#clanRank').hide();
            var clanRankAjax = $.getJSON(ajaxURL, function (data) {
                if (data.displayName != '' && data.clanName != '' && parseInt(data.clanRank) != 0) {
                    if ($('#clanRank').length) {
                        var listItem4 = [];
                        var encodedName = escape($.trim(data.clanName));
                        if (loggedIn != -1) {
                            var url = RS3.global.addCToLink('http://services.runescape.com/m=clan-hiscores/compare.ws?clanName=' + encodedName);
                        }
                        else {
                            var url = 'http://services.runescape.com/m=clan-hiscores/compare.ws?clanName=' + encodedName;
                        }
                        listItem4.push("<td class=\"ov1\"><a href=\"" + url + "\">" + data.clanRank + "</a></td>");
                        listItem4.push("<td class=\"ov2\"><a href=\"" + url + "\">" + data.clanName + "</a></td>");
                        $('#clanRank').html(listItem4.join('')).removeClass('loginRow');
                    }
                }
                else {
                    $('#clanRank').html('<td colspan="2">There is no rank to show</td>');
                }
            }).fail(function () {
                $('#clanRank').html('<td colspan="2">There is no rank to show</td>');
            }).always(function () {
                $('#clanRank').show();
            });


            $.when(hsRankAjax, clanRankAjax).then(function () {
                RS3.hiscores.overviewPage.activateCarousel();
            }).fail(function () {
                RS3.hiscores.overviewPage.activateCarousel();
            });

        }
    }
};
RS3.assoc = {
    dom: {
        optionWrapper: null,
        associationWrapper: null,
        showAssocTrigger: null,
        newMemberPrompt: null,
        errorMessage: null,
        associationForm: null,
        usernameField: null,
        passwordField: null,
        body: null
    },
    showAssoc: function () {
        RS3.assoc.dom.associationWrapper.show();
        RS3.assoc.dom.optionWrapper.hide();
        RS3.assoc.dom.usernameField.focus();
    },
    showOptions: function () {
        RS3.assoc.dom.associationWrapper.hide();
        RS3.assoc.dom.optionWrapper.show();
    },
    showErrors: function (message) {
        RS3.assoc.dom.errorMessage.text(message).show();
        RS3.assoc.dom.usernameField.focus();
    },
    hideErrors: function () {
        RS3.assoc.dom.errorMessage.hide();
        RS3.assoc.dom.newMemberPrompt.show();
    },
    checkForm: function () {
        if (RS3.assoc.dom.usernameField.val() == ""
            || RS3.assoc.dom.passwordField.val() == "") {
            RS3.assoc.showErrors(assocVar.errorMessages.alertUsername);
            return false;
        }
        else {
            RS3.assoc.dom.errorMessage.hide();
            RS3.assoc.dom.associationForm.submit();
        }
    },
    init: function () {
        var els = {
            body: $('body'),
            optionWrapper: $('#associationForm #associationOptions'),
            associationWrapper: $('#associationForm #associateAccount'),
            showAssocTrigger: $('#associationForm #syncUser .btn a'),
            newMemberPrompt: $('#associationForm #createNewAccount'),
            errorMessage: $('#associationForm #errorMessage'),
            associationForm: $('#associationForm'),
            promptMessage: $('#associationForm #associatePrompt'),
            usernameField: $('#associationForm #username'),
            passwordField: $('#associationForm #password'),
            assocSubmitTrigger: $('#associationForm #existingplayer')
        };
        RS3.assoc.dom = els;
        RS3.assoc.dom.showAssocTrigger.on('click', function (ev) {
            ev.preventDefault();
            RS3.assoc.showAssoc();
        });
        RS3.assoc.dom.assocSubmitTrigger.on('click', function (ev) {
            ev.preventDefault();
            RS3.assoc.checkForm();
        });
        if (assocVar.errorCode > 0) {
            RS3.assoc.showAssoc();
            RS3.assoc.showErrors();
        }
    }
};
RS3.betaTest = {
    hasWebGL: false,
    isTestComplete: false,
    worldURL: null,
    featureDetectionCallback: function () {
        $('.loadingAnim').hide();
        if (this.hasWebGL) {
            $('.webGL').show();
            $('.nonWebGL').hide();
            RS3.tracking.trackingPixel(null, 6100, null, true);
        }
        else {
            $('.webGL').hide();
            $('.nonWebGL').show();
            RS3.tracking.trackingPixel(null, 6200, null, true);
        }
    },

    runFeatureDetection: function () {

        var dummyFunction = function () {
        };
        if (!window.ArrayBuffer) {
            window.ArrayBuffer
                = window.ArrayBufferView
                = window.Int8Array
                = window.Uint8Array
                = window.Uint8ClampedArray
                = window.Int16Array
                = window.Uint16Array
                = window.Int32Array
                = window.Uint32Array
                = window.Float32Array
                = window.Float64Array
                = window.DataView
                = dummyFunction;
        }

        if (window.runFeatureDetection !== undefined) {
            if (!RS3.betaTest.isTestComplete) {
                RS3.betaTest.isTestComplete = true;
                window.runFeatureDetection(RS3.betaTest.worldURL, RS3.betaTest.featureDetectionCallback);
            }
        }
    },
    initFeatureDetection: function () {

        var browser = JXGLOBAL.user.browser();
        if (browser.isChrome) {
            this.hasWebGL = true;
        }
        $.ajax({
            url: 'http://www.runescape.com/world_url',
            dataType: "jsonp",
            success: function (data) {
                RS3.betaTest.worldURL = data.world_url;
                $.ajax({
                    url: RS3.betaTest.worldURL,
                    dataType: 'script',
                    timeout: 2000,
                    complete: function (data) {
                        RS3.betaTest.runFeatureDetection();
                    }
                });
            }
        });
        setTimeout(function () {
            if (!this.isTestComplete) {
                RS3.betaTest.featureDetectionCallback();
            }
        }, 3000);
    },
    showView: function (view) {
        var speed = 400, domElement = $('.gameFlowOverlay'),
            loadCallback = function () {
                $('#backToClientChoice').click(function () {
                    $('html').removeClass('gameOptionLock');
                    $('.gameFlowOverlay').fadeOut(function () {
                        $(this).remove();
                    });
                });

                $('#chromeCampaignButton').click(function (ev) {
                    RS3.tracking.trackingPixel(null, 3510, null, true);
                    RS3.tracking.pushTrackEvent('download_chrome_button', 'click', document.location.href);
                });
                _gaq.push(['_trackPageview', '/funnel/chrome-dl-instructions']);
            };
        if (!domElement || domElement.length === 0) {
            $('body').append($('<div class="gameFlowOverlay"><div class="backdrop"></div></div>').css({'opacity': 0}).append($('#chromeDownloadTpl').html()));
            domElement = $('.gameFlowOverlay');
            domElement.find('.wrapper').show();
            domElement.stop(true, true).animate({'opacity': 1}, speed, function () {
                loadCallback();
                $('html').addClass('gameOptionLock');
            });
        }
    },
    init: function () {
        this.initFeatureDetection();

        $('.install').click(function (ev) {
            ev.preventDefault();
            var campaignScriptURL = $(this).data('adsafe');
            $.getScript(campaignScriptURL);
            RS3.tracking.trackingPixel(null, 3040, null, true);
            RS3.betaTest.showView();
        });

        $('#playHTML5Beta a').click(function (ev) {
            ev.preventDefault();
            RS3.tracking.pushTrackEvent('play_html5_button', 'click', document.location.href);
            RS3.tracking.trackingPixel(ev, 3610, this.href, true);
        });
    }
};
RS3.legacy = {
    init: function () {
        'use strict';
        RS3.global.external();
        RS3.global.playerCount();
        RS3.global.logIn();
        RS3.global.lightbox.init();
        RS3.global.facebook.loadFacebookAPI(RS3.global.facebook.setUpEvents);
        RS3.mediavideos.youtube();
        RS3.global.headerDescriptionToggle()
    }
};
RS3.weblogin = {
    init: function () {
        'use strict';

        if ($('.loginiframe').length) {
            window.parent.location.href = "https://secure.runescape.com/m=weblogin/loginform.ws?mod=www&ssl=1&dest=account_settings.ws";
        }

        $("#username").focus();
    }
};
RS3.affiliate = {
    init: function () {
        'use strict';
        RS3.affiliate.axeso5Toolbar();
    },
    axeso5Toolbar: function () {
        if ($('#dvAxeso5Toolbar').length && $.fn.axeso5Toolbar !== undefined) {
            $('#dvAxeso5Toolbar').axeso5Toolbar({
                'uFC': 'false',
                'uL': 'false',
                'uLC': 'false',
                'uMT': 'true'
            });
        }
    }
};
RS3.bonds = {
    carousel: function () {
        var $carousel = $('#carousel');
        $carousel.flexslider({
            animation: 'fade',
            slideshow: true,
            slideshowSpeed: 20000,
            controlNav: false,
            before: function (obj) {
                var target = obj.animatingTo, targetImg = obj.slides.eq(target).find('img'), $li = $('.flex-active-slide'), $li_a = $li.find('a'), bannerSrc = $li_a.find('img').attr("src");
                if (targetImg.attr('src') != targetImg.data('src')) {
                    targetImg.attr('src', targetImg.data('src'));
                }
            }
        });
    },
    init: function () {
        RS3.bonds.carousel();
    }
};
RS3.combat = {
    carousel: function () {
        var $carousel = $('#carousel');
        $carousel.flexslider({
            animation: 'fade',
            slideshow: true,
            slideshowSpeed: 5000,
            controlNav: false
        });
    },
    init: function () {
        this.carousel();
    }
}
RS3.storeloc = {
    init: function () {
        'use strict';
        RS3.storeloc.events();
    },
    events: function () {
        var $parent = $('#changeFlag');
        $parent.on('click', 'li', function () {
            var $ele = $(this);
            var textInner = $ele.text();
            var countryRef = $ele.data('flag');
            $parent.find('li').removeClass('selected');
            $ele.addClass('selected');
            RS3.storeloc.changeView(countryRef, textInner);
        });

        if ($('#currentCountry').length) {
            var $ele = $('#currentCountry');
            $ele.addClass('selected');
            var textInner = $ele.text();
            var countryRef = $ele.data('flag');
            RS3.storeloc.changeView(countryRef, textInner);
        }
        else {
            var $ele = $('.defaultCountry');
            $ele.addClass('selected');
            var textInner = $ele.text();
            var countryRef = $ele.data('flag');
            RS3.storeloc.changeView(countryRef, textInner);
        }
    },
    changeView: function (countryRef, textInner) {
        var $parent = $('#changeFlag');

        $parent.find('#countryName').text(textInner);

        $parent.removeClass().addClass('countryFlag ' + countryRef);

        $('.shopList').hide();
        $('#' + countryRef).fadeIn();
    }
}
RS3.friends = {
    init: function () {
        'use strict';
        RS3.friends.loadContent(1);
        RS3.friends.events();
    },
    results: {},
    type: 'all',
    events: function () {

        $('#friendsTable').on('click', 'a.ajaxClick', function () {
            var $ele = $(this);
            var page = parseInt($ele.data('page'));
            var currentPage = $('#pageNumberFinder').data('pagenumber');
            if (page != currentPage) {
                RS3.friends.loadContent(page);
            }
            return false;
        });

        $('#resultPerPage').on('change', function () {
            RS3.friends.loadContent(1);
        });

        $('#onlineFilterCont').on('click', '.onlineFilter', function () {
            var $ele = $(this);
            var type = $ele.data('type');
            RS3.friends.type = type;
            $('#onlineFilterCont').find('li').removeClass('active');
            $ele.parent().addClass('active');
            RS3.friends.loadContent(1);
            return false;
        });
    },
    loadContent: function (pageNumber) {
        if ($('#friendsTable').length) {
            if (pageNumber == 'undefined') {
                var pageNumber = 1;
            }
            else {
                var pageNumber = parseInt(pageNumber);
            }
            var $playerInfo = $(".PlayerInfoTitle");
            if ($playerInfo.length > 0) {

                var resultsPage = 25;


                var cacheName = 'page_' + pageNumber + '_size_' + resultsPage + '_display_' + RS3.friends.type;


                if (RS3.friends.results[cacheName]) {
                    $('#friendsTableContent').hide();
                    RS3.friends.parseContent(RS3.friends.results[cacheName], pageNumber);
                }
                else {
                    $('#friendsTableContent').hide();
                    $('#loadingAnim').show();


                    var buildURL = "https://secure.runescape.com/m=website-data/playerFriendsDetails.json?resultsPerPage=" + resultsPage + "&currentPage=" + pageNumber;
                    if (RS3.friends.type == 'online') {
                        buildURL += '&display=online';
                    }
                    else if (RS3.friends.type == 'offline') {
                        buildURL += '&display=offline';
                    }
                    var ajaxURL = RS3.global.addCToLink(buildURL + '&callback=?');
                    var friendList = $.getJSON(ajaxURL, function (data) {
                        if (data.friends && data.friends.length > 0) {
                            RS3.friends.results[cacheName] = data;
                            RS3.friends.parseContent(data, pageNumber);
                        }
                        else {

                            $('#friendsTableContent').html("<p class='error'>There are no friends to show</p>").show();
                            $('#loadingAnim').hide();
                        }
                    }).fail(function () {
                        $('#friendsTableContent').show();
                        $('#loadingAnim,.ajaxFriendInfo').hide();
                    }).always(function () {
                    });
                }
            }
        }
        else {
            $('.ajaxFriendInfo').hide();
        }
    },
    parseContent: function (data, pageNumber) {
        var tableContent = [], nextPage = (pageNumber + 1), prevPage = (pageNumber - 1);
        if (data.pageFriends > 0) {
            //populate side friend info
            $('#totalFriends').html(parseInt(data.allFriends));
            $('#onlineFriends').html(parseInt(data.online));
            $('#offlineFriends').html(parseInt(data.offline));

            //upper pagination
            if (parseInt(data.totalPages) > 1) {
                tableContent.push('<div id="pageNumberFinder" class="paginationOuter" data-pagenumber="' + pageNumber + '">');
                tableContent.push('<div class="pagination simple');
                if (pageNumber == 1) {
                    tableContent.push(' noFirst">')
                }
                else if (pageNumber == parseInt(data.totalPages)) {
                    tableContent.push(' noLast">')
                }
                else {
                    tableContent.push('">')
                }
                if (pageNumber != 1) {

                    tableContent.push('<a id="topPaginationPrev" data-page="' + prevPage + '" class="previous ajaxClick" href="?page=' + prevPage + "\">Previous</a>");
                }

                tableContent.push("<span>Page " + pageNumber + '/' + data.totalPages + '</span>');
                if (pageNumber != parseInt(data.totalPages)) {

                    tableContent.push('<a id="topPaginationNext" data-page="' + nextPage + '" class="next ajaxClick" href="?page=' + nextPage + "\">Next</a>");
                }
                tableContent.push('</div></div>');
            }


            tableContent.push('<table><thead><tr><th class="col1">Name</th><th class="align">World</th><th class="align">Status</th></tr></thead><tbody>');
            $.each(data.friends, function (index, item) {

                if (index % 2 == 0) {
                    var rowClass = "";
                }
                else {
                    var rowClass = ' class="oddRow"';
                }

                if (item.status == 'offline') {
                    var img = '<img alt="Not Online" title="Not Online" src="https://www.runescape.com/img/eoc/global/icon-status-red.png">';
                    var worldName = '';
                }
                else {
                    var img = '<img alt="Online" title="Online" src="https://www.runescape.com/img/eoc/global/icon-status-green.png">';
                    var worldName = item.world;
                }
                var playerLink = RS3.global.addCToLink("http://services.runescape.com/m=adventurers-log/display_player_profile.ws?searchName=" + escape($.trim(item.name)));
                tableContent.push('<tr' + rowClass + '><td><a class="icon" href="' + playerLink + '"><img class="avatar" alt="' + item.name + '" src="http://services.runescape.com/m=avatar-rs/' + escape($.trim(item.name)) + '/chat.png?w=27&h=27"> ' + item.name + '</a></td>');
                tableContent.push('<td class="align"><a class="icon" href="' + playerLink + '">' + worldName + '</a></td>');
                tableContent.push('<td class="align"><a class="icon" href="' + playerLink + '">' + img + '</a></td></tr>');
            });
            tableContent.push('</table>');
            //lower pagination
            if (parseInt(data.totalPages) > 1) {
                tableContent.push('<div class="paginationWrap clear">');
                tableContent.push('<div class="pagination">');
                tableContent.push('<div class="paging">');
                tableContent.push('<ul id="ajaxPaginationBot" class="pageNumbers ' + pageNumber + '">');
                for (var i = 1; i <= data.totalPages; i++) {
                    var className = '';
                    if (i == pageNumber) {
                        var className = ' class="current"';
                    }
                    tableContent.push('<li' + className + '><a class="ajaxClick" data-page="' + i + '" id="paginationListItem' + i + '" href="?page=' + i + '">' + i + '</a></li>');
                }
                tableContent.push('</ul></div></div></div>');
            }
        }
        $('#friendsTableContent').html(tableContent.join(''))
        $('#friendsTableContent').fadeIn(500);
        $('#loadingAnim').hide();
    }
};
RS3.goodwill = {
    init: function () {
        RS3.goodwill.getHiScores();
        $('.goodwill-tab').on('click', function (ev) {
            ev.preventDefault();
            var navOption = $(this), sectionId = navOption.attr('href');
            $('.tabbednav__tab--active').removeClass('tabbednav__tab--active');
            $('.method--show').removeClass('method--show');
            navOption.parent().addClass('tabbednav__tab--active');
            $(sectionId).addClass('method--show');
        });
        $('#coins_thermometer').css('height', thermometerHeight);
    },
    getHiScores: function () {
        //Get coin totals
        $.ajax({
            url: ajaxCoinURL,
            dataType: 'jsonp',
            timeout: 3000,
            success: function (data) {
                RS3.goodwill.enterPlayers(data, 'coins');
            }
        });
        //Get bond totals
        $.ajax({
            url: ajaxBondURL,
            dataType: 'jsonp',
            timeout: 3000,
            success: function (data) {
                RS3.goodwill.enterPlayers(data, 'bonds');
            }
        });
    },
    enterPlayers: function (data, type) {
        var rankLength = data.length;
        if (rankLength >= 3) {
            rankLength = 2;
        }
        var domId, photoDom;
        for (i = 0; i <= rankLength; i++) {
            domId = '#' + type + '_donator_' + i;
            photoDom = $(domId + ' .top-donators__avatar');
            photoDom.attr("src", avatarBase + data[i].name + "/chat.png?h=75&w=75");
            photoDom.attr("title", data[i].name);
            photoDom.attr("alt", data[i].name);
            $(domId + ' .top-donators__name').html(data[i].name);
            if (type == 'coins') {
                $(domId + ' .top-donators__total').html(data[i].score_formatted + millionChar);
            }
            else {
                $(domId + ' .top-donators__total').html(data[i].score_formatted);
            }
        }
    },
};
RS3.premierClub = {
    init: function () {
        RS3.premierClub.events();
        RS3.premierClub.buyNowButtons();
        RS3.premierClub.trackingPixel();
    },
    events: function () {
        $('.segContentOuter').on('mouseenter', function () {
            var $ele = $(this);
            var $target = $ele.find('.segContent');
            $target.stop().fadeIn(250, function () {
                var $textChange = $ele.next('.segPrice').addClass('segPriceHover');
            });
        }).on('mouseleave', function () {
            var $ele = $(this);
            var $target = $ele.find('.segContent');
            $target.stop().fadeOut(250, function () {
                var $textChange = $ele.next('.segPrice').removeClass('segPriceHover');
            });
        });
    },
    buyNowButtons: function () {

        var $buttons = $("a.openLoginPanelPC,.btnWrap a");
        $buttons.each(function (i) {

            var $this = $(this),
                oldHref = $this.attr("href");
            $this.attr("data-href", oldHref);
            $this.attr("href", '#loginPanel').fancybox({'wrapCSS': 'loginFancyBox'});

        });


        $("a.openLoginPanelPC,.btnWrap a").on('click', function (ev) {
            var $anchor = $(this),
                dest = 'packagegroupredirect.ws',
                href = $anchor.attr('data-href'),
                package = '12',
                strPackage = 'Default Gold',
                strParam = 'rsprem13ga';
            if (href.indexOf('rsprem13ba') > 0) {
                package = 3;
                strPackage = 'Bronze';
                strParam = 'rsprem13ba ';
            }
            if (href.indexOf('rsprem13sa') > 0) {
                package = 6;
                strPackage = 'Silver';
                strParam = 'rsprem13sa';
            }
            if (href.indexOf('rsprem13gb') > 0) {
                package = 12;
                strPackage = 'Gold';
                strParam = 'rsprem13gb';
            }
            if (href.indexOf('purchase.ws') > 0) {
                dest = 'purchase.ws'
            }
            else {
                if (href.indexOf('packagegroupredirect.ws') > 0) {
                    dest = dest + '?value=' + strParam;
                }
            }
            $('#loginPanel input[name=mod]').attr('value', 'billing_core');
            $('#loginPanel input[name=ssl]').attr('value', 1);
            $('#loginPanel input[name=dest]').attr('value', dest);
            $('.facebookLogin').data('redirecturl', "mod=billing_core&ssl=1&dest=" + dest);


            RS3.membersBenefits.updateAffiliateLinks();

            RS3.tracking.pushTrackEvent('premier_club', 'buy_now', strPackage, package);
        });


        $('#premierClub #inline').on('click', function () {
            var destVal = $('#loginPanel input[name=dest]').val();
            if (destVal.indexOf('premier_club') == -1) {
                $('#loginPanel input[name=mod]').attr('value', 'www');
                $('#loginPanel input[name=ssl]').attr('value', 0);
                $('#loginPanel input[name=dest]').attr('value', 'premier_club');
            }
        });
    },

    trackingPixel: function () {
        'use strict';
        var url = 'http://www.runescape.com/track/?jptg=pc&jptv=0';
        $.ajax({
            url: url,
            dataType: 'jsonp',
            timeout: 3000,
            complete: function () {
            }
        });
    }
};
RS3.companion = {
    carousel: function () {
        var $carousel = $('#carousel');
        $carousel.flexslider({
            animation: 'fade',
            slideshow: false,
            slideshowSpeed: 20000,
            controlNav: true,
            before: function (obj) {
                var target = obj.animatingTo, targetImg = obj.slides.eq(target).find('img'), $li = $('.flex-active-slide'), $li_a = $li.find('a'), bannerSrc = $li_a.find('img').attr("src");
                if (targetImg.attr('src') != targetImg.data('src')) {
                    targetImg.attr('src', targetImg.data('src'));
                }
            }
        });
    },
    preloadImages: function () {
        var img1 = new Image();
        img1.src = "http://www.runescape.com/img/eoc/companion_app/slide1.jpg";
        var img2 = new Image();
        img2.src = "http://www.runescape.com/img/eoc/companion_app/slide2.jpg";
        var img3 = new Image();
        img3.src = "http://www.runescape.com/img/eoc/companion_app/slide3.jpg";
        var img4 = new Image();
        img4.src = "http://www.runescape.com/img/eoc/companion_app/slide4.jpg";
    },
    events: function () {
        $('#appCode').on('click', function () {
            $(this).select();
        });
    },
    init: function () {
        RS3.companion.carousel();
        RS3.companion.preloadImages();
        RS3.companion.events();
    }
};
RS3.topTrumps = {
    init: function () {
        'use strict';
        var imagePreloadArray = [];
        RS3.topTrumps.pageLoadTrack();
        $('.trumpJSInfoSection').hide();
        $('.trumpSelectionSection').show();
        imagePreloadArray[0] = new Image();
        imagePreloadArray[1] = new Image();
        imagePreloadArray[2] = new Image();
        imagePreloadArray[3] = new Image();
        imagePreloadArray[4] = new Image();
        imagePreloadArray[5] = new Image();
        imagePreloadArray[0].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_hero_kre.jpg';
        imagePreloadArray[1].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_hero_zil.jpg';
        imagePreloadArray[2].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_hero_kin.jpg';
        imagePreloadArray[3].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_hero_sar.jpg';
        imagePreloadArray[4].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_hero_evi.jpg';
        imagePreloadArray[5].src = 'http://www.runescape.com/img/eoc/top_trumps/trump_all_cards.jpg';
        if (window.location.hash.length && (window.location.hash === '#kreearra' || window.location.hash === '#commander_zilyana' || window.location.hash === '#king_black_dragon' || window.location.hash === '#saradomin_colossus' || window.location.hash === '#evil_chicken' || window.location.hash === '#runescape_top_trumps')) {
            RS3.topTrumps.cardSelect($(window.location.hash + '_select'));
        }
        $('.trumpBuyNow').on('click', 'a', function (e) {
            var track = new Image();
            track.src = '/track/?jptg=tt&jptv=1';
        });
        $('.trumpSelectionCard')
            .on('mouseenter', function () {
                if (!$(this).hasClass('trumpSelectionCardCurrent')) {
                    $(this).stop().animate({marginTop: '40px'}, 'fast');
                }
            })
            .on('mouseleave', function () {
                if (!$(this).hasClass('trumpSelectionCardCurrent')) {
                    $(this).stop().animate({marginTop: '60px'}, 'fast');
                }
            })
            .on('click', function (event) {
                if (!$(this).hasClass('trumpSelectionCardCurrent')) {
                    RS3.topTrumps.cardSelect($(this));
                }
            });
        RS3.topTrumps.showBonds();
    },
    cardSelect: function ($card) {
        'use strict';
        $('.trumpSelectionCardCurrent').stop().animate({marginTop: '60px'}, 'fast');
        $('.trumpSelectionCardCurrent').removeClass('trumpSelectionCardCurrent');
        $('.trumpCardDetailsCurrent').fadeOut('fast').removeClass('trumpCardDetailsCurrent');
        $card.stop().animate({marginTop: '0px'}, 'fast');
        $card.addClass('trumpSelectionCardCurrent');
        $('#' + $card.data('assoc')).fadeIn('fast').addClass('trumpCardDetailsCurrent');
    },
    pageLoadTrack: function () {
        'use strict';
        var url = 'http://www.runescape.com/track/?jptg=tt&jptv=0';
        $.ajax({
            url: url,
            dataType: 'jsonp',
            timeout: 3000,
            complete: function () {
            }
        });
    },
    showBonds: function () {
        var $link = $('#showBonds'), $div = $('#collapse');
        $link.click(function (e) {
            e.preventDefault();
            $div.stop().slideToggle();
        });
    }
};
RS3.gamePage = {

    overlayTracking: function () {
        if (window.addEventListener) {
            window.addEventListener('message', trackOverlay);
        }
        else {
            if (window.attachEvent) {
                window.attachEvent("onmessage", trackOverlay)
            }
        }
        function trackOverlay(event) {
            var response,
                responseObj = JSON.parse ? JSON.parse(JSON.stringify(event.data)) : {};
            if (responseObj.type !== 'TRACKING') {
                return false;
            }
            if (responseObj.text === 'opened') {
                RS3.tracking.pushTrackEvent('game_html5_overlay_loaded', 'load', pageLocation);
                RS3.tracking.trackingPixel(null, 5100, null, true);
            }
            else {
                response = parseInt(responseObj.text.split('reply-')[1]);
                if (response === 0) {
                    RS3.tracking.pushTrackEvent('game_html5_overlay_accepted', 'accepted', pageLocation);
                    RS3.tracking.trackingPixel(null, 5110, null, true);
                }
                else if (response === 1 || response === 3) {
                    RS3.tracking.pushTrackEvent('game_html5_overlay_declined', 'declined', pageLocation);
                    RS3.tracking.trackingPixel(null, 5120, null, true);
                }
                else if (response === 2) {
                    RS3.tracking.pushTrackEvent('game_html5_overlay_error', 'error', pageLocation);
                    RS3.tracking.trackingPixel(null, 5130, null, true);
                }
            }
        }
    },
    checkJs: function () {
        var ifr = document.getElementById('game'), jssrc;
        if (ifr) {
            jssrc = ifr.src.split(',j0,').join(',j1,');
            ifr.src = jssrc;
        }
    },
    checkJava: function () {
        RS3.gamePageConstants.hasJava = deployJava.getJREs().length >= 1 ? true : false;
        RS3.gamePageConstants.hasJavaVersion = deployJava.versionCheck(RS3.gamePageConstants.javaVersion);
    },
    showInstallScreen: function () {
        var os = JXGLOBAL.user.os(), browser = JXGLOBAL.user.browser(), location = baseURL + '/downloads/';
        $.removeCookie('JXRSLAUNCHING');
        $('#gameInstallPage').addClass('game-install-page--show');
        $('#gameLaunchingPage,#game,#gameUnsupportedPage').remove();
        if (browser.isChrome) {
            $('#gameInstallChromeError').addClass('game-install-page__chrome-error--show');
        }
        if (os.isMac) {
            var macInstall = _.filter(JXGLOBAL.client, function (installer) {
                return installer.platform.indexOf('OSX') > -1;
            });
            $('#clientLocation').text(clientMacLocation);
            $('#downloadClientBtn a,#downloadLatestClient').attr('href', location + macInstall[0].filename + '?' + macInstall[0].version);
            $('#downloadSize').text(macInstall[0].size + 'MB');
        }
        else {
            var clientInstall = _.filter(JXGLOBAL.client, function (installer) {
                return installer.platform.indexOf('Windows') > -1;
            });
            $('#clientLocation').text(clientWindowsLocation);
            $('#downloadClientBtn a,#downloadLatestClient').attr('href', location + clientInstall[0].filename + '?' + clientInstall[0].version);
            $('#downloadSize').text(clientInstall[0].size + 'MB');
        }
        $('#gameInstallPage').addClass('game-install-page--show');
        $('#playClientBtn').on('click', function () {
            $('#gameInstallNoRun').addClass('game-install-page__no-run--show');
        });
    },
    showUnsupportedScreen: function () {
        $.removeCookie('JXRSLAUNCHING');
        $('#gameInstallPage,#game,#gameLaunchingPage').remove();
        $('#gameUnsupportedPage').addClass('game-unsupported-page--show');
    },
    showThanksScreen: function () {
        $.removeCookie('JXRSLAUNCHING');
        $('#launchingTitle,#gameUnsupportedPage').remove();
        $('#thanksTitle').addClass('game-launch-page__title--show');
        $('#gameLaunchingContent').addClass('game-launch-page__content--nospin');
    },
    showGameScreen: function () {
        var gameFrame = $('#game');
        $.removeCookie('JXRSLAUNCHING');
        $('#gameStateScreens,footer').remove();
        gameFrame.addClass('java-frame--show');
        gameFrame.attr('src', worldLink);
        //Check for js and append to the iframe
        RS3.gamePage.checkJs();
    },
    init: function () {
        //If we are running the java client perform the relevant checks
        if ($('.java-frame').length) {
            //Listen for the not loading link
            $('#gameLaunchNotLoad').on('click', function () {
                RS3.gamePage.showInstallScreen();
            });
            //We need to set a cookie so the script is not re-ran when we fire the protocol link
            if (!$.cookie('JXRSLAUNCHING')) {
                //Run the java check
                RS3.gamePage.checkJava();
                //Deploy global checks of browser and os
                var browser = JXGLOBAL.user.browser(),
                    os = JXGLOBAL.user.os(),
                    installerMatch = os.installerMatch(),
                    currentBrowser = null;
                //Set the current browser string used for protocol checker
                if (browser.isChrome) {
                    currentBrowser = 'chrome';
                }
                else if (browser.isFirefox) {
                    currentBrowser = 'mozilla';
                }
                else if (browser.isIE) {
                    currentBrowser = 'msie';
                }
                //We now need to wait to ensure all our vars have been retreived
                var varTimeout = setTimeout(function () {
                    //Get variables
                    var hasJava = RS3.gamePageConstants.hasJava,
                        hasJavaVersion = RS3.gamePageConstants.hasJavaVersion;
                    //If Java is supported load the client as normal and remove everything else
                    if (hasJava && hasJavaVersion) {
                        RS3.gamePage.showGameScreen();
                    }
                    else {
                        //Set the launcing cookie to avoid duplicate runs
                        $.cookie('JXRSLAUNCHING', true);
                        $('#gameShouldLaunch').addClass('game-launch-page__should--show');
                        //Check if the protocol works
                        var gameProtocol = 'jagex-jav://www.runescape.com/l=' + currentLangId + '/jav_config.ws';
                        checkProtocol(gameProtocol, currentBrowser);
                        //Wait for our check protocol to return
                        var protocolTimeout = setTimeout(function () {
                            //If the protocol is not supported removing the launching cookie and show the install screen unless the client cookie is set
                            if (!protocolSupport[gameProtocol]) {
                                $.removeCookie('JXRSLAUNCHING');
                                //Any value for this cookie will indicate that the client will have been installed with the protocol
                                if ($.cookie('JXRSCLIENTVERSION')) {
                                    RS3.gamePage.showThanksScreen();
                                }
                                else {
                                    if (installerMatch !== null) {
                                        RS3.gamePage.showInstallScreen();
                                    }
                                    else {
                                        RS3.gamePage.showUnsupportedScreen();
                                    }
                                }
                            }
                            //If the protocol is supported show the launching screen for 20 more seconds before showing the thanks screen
                            else {
                                var launchTimeOut = setTimeout(function () {
                                    RS3.gamePage.showThanksScreen();
                                    clearTimeout(launchTimeOut);
                                }, 10000);
                            }
                            clearTimeout(protocolTimeout);
                        }, 2000);

                    }
                    clearTimeout(varTimeout);
                }, 1000);
            }
            //If the cookie is set we need show the launch text and then remove the cookie and finally fallback to the install screen
            else {
                var launchTextTimeout = setTimeout(function () {
                    $('#gameShouldLaunch').addClass('game-launch-page__should--show');
                    clearTimeout(launchTextTimeout);
                }, 1000);
                $.removeCookie('JXRSLAUNCHING');
                var launchTimeOut = setTimeout(function () {
                    RS3.gamePage.showThanksScreen();
                    clearTimeout(launchTimeOut);
                }, 20000);
            }
        }//if($('.java-frame').length)
        //Perform tracking logic
        if ($('body').hasClass('html5Player')) {
            RS3.tracking.pushTrackEvent('game_html5_client_loaded', 'load', pageLocation);
            RS3.tracking.trackingPixel(null, 5000, null, true);
        }
        else {
            RS3.tracking.pushTrackEvent('game_java_client_loaded', 'load', pageLocation);
            RS3.tracking.trackingPixel(null, 4000, null, true);
        }
        RS3.gamePage.overlayTracking();
    }
};
RS3.gamePageConstants = {
    javaVersion: '1.7.0_25+',
    hasJava: false,
    hasJavaVersion: false,
};
RS3.grandExchange = {
    ticker: function () {
        var $ticker = $('#ticker'), $tickerWrap = $ticker.find('.ticker-wrap');
        $tickerWrap.webTicker({
            speed: 50,
            direction: "left",
            moving: true,
            startEmpty: false,
            duplicate: false,
            rssurl: false,
            rssfrequency: 0,
            updatetype: "reset"
        });
    },
    chart: function () {

        // Setup initial 1 Month chart for ChartJS
        var averageChartData = {
            labels: average30.labels,
            datasets: [
                {
                    strokeColor: "#b2dbee",
                    pointColor: "#b2dbee",
                    pointStrokeColor: "#b2dbee",
                    data: average30.monthly
                },
                {
                    strokeColor: "#e1bb34",
                    pointColor: "#e1bb34",
                    pointStrokeColor: "#e1bb34",
                    data: average30.daily
                }
            ]

        };

        // Global chart options for style and layout
        var steps = 5, averageMax = Math.max.apply(Math, average30.daily), averageMin = Math.min.apply(Math, average30.daily);
        var chartOptions = {
            scaleLineColor: "#2c3d49",
            scaleLineWidth: 1,
            scaleGridLineColor: "#2c3d49",
            scaleGridLineWidth: 1,
            scaleShowDecimals: true,
            scaleOverride: true,
            scaleSteps: steps,
            scaleStepWidth: Math.ceil((averageMax - averageMin) / steps),
            scaleStartValue: Math.floor(averageMin),
            bezierCurve: false,
            pointDot: true,
            scaleFontFamily: "'MuseoSans500'",
            scaleLabel: "<%=value%>",
            scaleLabelFilter: scaleLabelFilter,
            scaleFontColor: "#d7dbe1",
            pointDotRadius: 2,
            datasetFill: false
        };

        // Fix if Data point value doesn't change
        if (averageMax === averageMin) {
            chartOptions.scaleSteps = 3;
            chartOptions.scaleStepWidth = 1;
            chartOptions.scaleStartValue = averageMin - 2;
        }

        // Draw chart with chart options and data
        var averageChart = new Chart(document.getElementById("itemaverage").getContext("2d")).Line(averageChartData, chartOptions);

        // Capture filters for displaying different data
        var $filter = $('#filter');
        $filter.on('click', 'a', function (e) {
            e.preventDefault();
            var averageTitle, $self = $(this), $current = $self.parent(), $parent = $self.parents('ul');


            if (!$current.hasClass('active')) {
                // Move LI to top of list
                $parent.find('.active').removeClass('active');
                $current.addClass('active');
                $parent.prepend($current);
                // Capture new array
                var dataSet = window[this.hash.substr(1)];
                // Update data values for new chart
                averageChartData.labels = dataSet.labels;
                averageChartData.datasets[0].data = dataSet.monthly;
                averageChartData.datasets[1].data = dataSet.daily;
                // Update Scale options
                var averageMax = Math.max.apply(Math, dataSet.daily), averageMin = Math.min.apply(Math, dataSet.daily);
                chartOptions.scaleStepWidth = Math.ceil((averageMax - averageMin) / steps);
                chartOptions.scaleStartValue = Math.floor(averageMin);
                // Hide points if 3 or 6 months
                dataSet === average30 ? chartOptions.pointDot = true : chartOptions.pointDot = false;
                // Fix if Data point value doesn't change
                if (averageMax === averageMin) {
                    chartOptions.scaleSteps = 3;
                    chartOptions.scaleStepWidth = 1;
                    chartOptions.scaleStartValue = averageMin - 2;
                }
                // Replace existing and draw new chart
                $('#itemaverage').replaceWith('<canvas id="itemaverage" height="350" width="645"></canvas>');
                var averageChart = new Chart(document.getElementById("itemaverage").getContext("2d")).Line(averageChartData, chartOptions);
                // Update Key
                switch (dataSet) {
                    case average30:
                        averageTitle = '1 Month Average';
                        break;
                    case average90:
                        averageTitle = '3 Month Average';
                        break;
                    case average180:
                        averageTitle = '6 Month Average';
                        break;
                }
                $('#monthAverage').text(averageTitle);
            }
        });

    },
    searchFilter: function () {
        $('.filter-link').on('click', function (ev) {
            ev.preventDefault();
            var filterBox = $('#filter');
            if (filterBox.hasClass('showElement')) {
                filterBox.removeClass('showElement');
                $('.filter-link').removeClass('expand');
            }
            else {
                filterBox.addClass('showElement');
                $('.filter-link').addClass('expand');
            }
        });
        this.updateMinMax();
    },
    updateMinMax: function () {
        if ($('#priceSlider').length) {
            var sliderVals = $('#priceSlider').val().split(';');
            if (sliderVals[0] > 495500) {
                $('#minPrice').val('495500');
            } else {
                $('#minPrice').val(sliderVals[0]);
            }
            ;
            if (sliderVals[1] == maxScale) {
                sliderVals[1] = '+';
                $('.jslider-value-to span').html('500,000+');
            }
            $('#maxPrice').val(sliderVals[1]);
        }
    },
    init: function () {
        this.ticker();
        this.searchFilter();
        if (typeof average30 != 'undefined') this.chart();
    }
};
RS3.polls = {

    drawPollResults: function (pollId) {
        var previousPoll = $('.showElement'), previousPollId = previousPoll.parent().attr('id');
        $('.pollExpand.expanded').removeClass("expanded");
        $('.polls').find('.showContent').removeClass('showContent');

        if (previousPollId != "pollTab" + pollId) {
            $('#pollTab' + pollId + ' .pollExpand').addClass("expanded");
            $('.expanded').parent().parent().addClass('showContent');
        }

        var currentPoll = $('#pollDetails' + pollId);
        if (currentPoll.length > 0) {
            currentPoll.addClass("showElement");
        }

        else {
            RS3.polls.requestPoll(pollId);
        }
        previousPoll.removeClass("showElement");
    },

    calculateReturnString: function (seconds) {
        var remainingString = "", timeRemaining = 0;
        if (seconds >= 216000) {

            timeRemaining = seconds / (60 * 60 * 24);
            remainingString = timeRemaining.toFixed(1) + " days";
        }
        else if (seconds >= 3600) {

            timeRemaining = seconds / (60 * 60);
            remainingString = timeRemaining.toFixed(1) + " hours";
        }
        else if (seconds >= 60) {

            timeRemaining = seconds / 60;
            remainingString = timeRemaining.toFixed(1) + " minutes";
        }
        else {

            remainingString = seconds + " seconds";
        }
        return remainingString;
    },

    requestPoll: function (pollId) {
        $.ajax({
            url: pollUrl + '/pollResults.json?id=' + pollId,
            dataType: 'jsonp',
            timeout: 3000,
            success: function (jsonPoll) {
                if (jsonPoll.errorCode > 0) {
                    RS3.polls.drawErrorPane(pollId, jsonPoll.errorCode);
                }
                //If the player is banned and poll is active
                else if (jsonPoll.isBanned && jsonPoll.status == 1) {
                    RS3.polls.drawErrorPane(pollId, 9);
                }
                //If the player has voted and the answers are to be hidden
                else if (jsonPoll.pollDetails[0].options[0].hidden && jsonPoll.playerAlreadyVoted) {
                    RS3.polls.drawHiddenMessagePane(pollId);
                }
                //If the polls are too big send them to the poll page
                else if (jsonPoll.pollDetails[0].options.length > 9 || jsonPoll.pollDetails.length > 1) {
                    if (!jsonPoll.running || jsonPoll.playerAlreadyVoted || !jsonPoll.canPlayerVote) {
                        window.location = pollUrl + "/results.ws?id=" + pollId;
                    }
                    else {
                        window.location = pollUrl + "/vote.ws?id=" + pollId;
                    }
                }
                else {
                    var pollAnswers = RS3.polls.generateResultsPane(jsonPoll, pollId);
                    var pollDesc = RS3.polls.generateDetailsPane(jsonPoll, pollId);
                    var pollDetails = "<article class='pollDetails clear' id='pollDetails" + pollId + "'><div class='pollDetailsTable'><div class='pollDetailsLeft'>" + pollDesc + "</div>";
                    pollDetails += "<div class='pollDetailsRight'>" + pollAnswers + "</div></div></article>";

                    $('#pollTab' + pollId).append(pollDetails);

                    setTimeout(function () {
                        $('#pollDetails' + pollId).addClass("showElement")
                    }, 1);

                    RS3.polls.onPageSubmit();
                }
            },
            failure: function () {
                RS3.polls.drawErrorPane(pollId, -1);
            },
            error: function () {
                RS3.polls.drawErrorPane(pollId, -1);
            }
        });
    },

    generateDetailsPane: function (jsonPoll, pollId) {
        var pollDesc = "<h5>Description</h5><p class='pollDesc'>" + jsonPoll.pollDetails[0].questionText + "</p>";

        if (jsonPoll.playerAlreadyVoted > 0) {
            pollDesc += "<p class='totalVotes'><strong>Total Votes: </strong>" + jsonPoll.totalNumberofVotes + "</p><p class='youHave'>You have voted on this Poll</p>"
        }
        if (jsonPoll.secondsRemaining > 0) {
            pollDesc += "<h6 class='time'>" + RS3.polls.calculateReturnString(jsonPoll.secondsRemaining) + " Remaining</h6>";
        }
        if (jsonPoll.playerAlreadyVoted) {
            pollDesc += "<p class='yourVote'>= Your Vote";
            if (jsonPoll.canPlayerVote) {
                pollDesc += " (<a class='changeVote' href='vote.ws?id=" + pollId + "'>Change</a>)";
            }
            pollDesc += "</p>";
        }
        return pollDesc;
    },

    generateResultsPane: function (jsonPoll, pollId) {
        var pollNumOptions = jsonPoll.pollDetails[0].options.length;
        var pollAnswers = "";
        var answerPercentage = 0;

        if (jsonPoll.playerAlreadyVoted || jsonPoll.status == 0 || !jsonPoll.canPlayerVote) {
            pollAnswers += "<h5>Results</h5><ul class='poll'>";
            for (var i = 0; i < pollNumOptions; i++) {
                answerPercentage = jsonPoll.pollDetails[0].options[i].optionPercentage;
                pollAnswers += "<li class='pollsVoteTitle'>" + jsonPoll.pollDetails[0].options[i].optionName + "</li>";
                pollAnswers += "<li class='pollsVoteBar clear ";
                if (jsonPoll.pollDetails[0].options[i].voted) {
                    pollAnswers += "votedAnswer";
                }
                pollAnswers += "'><div class='voteBarFill' style='width: " + answerPercentage + "%'></div><p class='voteStats'";

                if (answerPercentage > 80) {
                    pollAnswers += "style='right: " + (100 - answerPercentage) + "%' ";
                }
                pollAnswers += ">(" + jsonPoll.pollDetails[0].options[i].optionTotal + " votes) " + jsonPoll.pollDetails[0].options[i].optionPercentage + "%</p></li>"
            }
            pollAnswers += "</ul>";
        }
        else {
            pollAnswers += "<h5>Options</h5><form action='confirmation.ws' method='post' class='voteForm'><input type='hidden' name='id' value='" + pollId + "'><ul class='poll'>"
            for (var i = 0; i < pollNumOptions; i++) {
                pollAnswers += "<li><input class='htmlRadio' type='radio' name='question0' id='answer" + pollId + "-0-" + i + "' value='" + i + "' required /><label class='answerLabel' for='answer" + pollId + "-0-" + i + "'><span class='radio'></span><span class='text'>" + jsonPoll.pollDetails[0].options[i].optionName + "</span><span class='hint'><div class='pollToolTip'>" + jsonPoll.description[i] + "</div></span></label></li>";
            }
            pollAnswers += '</ul><div class="btnWrap"><div class="btn"><div class="btnRight"><input type="submit" value="Vote Now" name="Vote Now" class="voteButton" /></div></div></div></form>';
        }
        return pollAnswers;
    },
    drawErrorPane: function (pollId, errorCode) {
        $('.pollError').removeClass('showElement');
        if ($('#pollDetails' + pollId).length > 0) {
            $('#pollDetails' + pollId).addClass('showElement');
        }
        else {
            var errorMessage = "<article class='pollMessage pollError' id='pollDetails" + pollId + "'><p class='messageText'>";
            switch (errorCode) {
                case 0:


                    errorMessage += "You have already voted on this poll <a href='results.ws?id=" + pollId + "'>View the current results</a>";
                    break;
                case 1:


                    errorMessage += "Sorry, you may only vote on this poll if you are a RuneScape Member. <a href='http://www.runescape.com/members_benefits'>Become a Member</a> to cast your vote";
                    break;
                case 2:
                case 9:
                case 10:
                case 11:

                    errorMessage += "Sorry, you are not eligible to vote on this poll.";
                    break;
                case 3:

                    errorMessage += "Sorry, this poll is only open to more experienced RuneScape players. Continue playing to gain the opportunity to vote on polls for more experienced players.";
                    break;
                case 5:


                    errorMessage += "Sorry, this poll is now closed. <a href='results.ws?id=" + pollId + "'>View the results</a>.";
                    break;
                case 12:

                    errorMessage += "Sorry, this poll is only open to more experienced RuneScape players. Continue playing to gain the opportunity to vote on polls for higher level players.";
                    break;
                case 20:

                    errorMessage += "Sorry, you have already voted on this poll in another language.";
                    break;
                default:

                    errorMessage += "Sorry, an error occurred while processing this poll. Please try again later.";
                    break;
            }
            errorMessage += "</p></article>";
            $('#pollTab' + pollId).append(errorMessage);
            setTimeout(function () {
                $('#pollDetails' + pollId).addClass("showElement")
            }, 1);
        }
    },
    drawHiddenMessagePane: function (pollId) {

        var hiddenMessage = "<article class='pollMessage pollHidden' id='pollDetails" + pollId + "'><p class='messageText'>The results of this poll are hidden</p></article>";
        $('#pollTab' + pollId).append(hiddenMessage);
        setTimeout(function () {
            $('#pollDetails' + pollId).addClass("showElement")
        }, 1);
    },

    onPageSubmit: function () {

        $('.voteForm').submit(function (ev) {
            ev.preventDefault();
            var thisForm = $(this);
            var pollId = thisForm.find("input[name='id']").val();
            var formData = thisForm.serialize();
            if ($("input[name='question0']:checked").length > 0) {
                $.ajax({
                    dataType: 'jsonp',
                    url: pollUrl + '/postPollVote.json?' + formData,
                    timeout: 3000,
                    success: function (voteResponse) {
                        $('#pollTab' + voteResponse.pollID).addClass("pollTab--voted");
                        var rightPane = $('#pollDetails' + voteResponse.pollID + ' .pollDetailsRight'), leftPane = $('#pollDetails' + voteResponse.pollID + ' .pollDetailsLeft');
                        rightPane.html("<p>loading...</p>");
                        $.ajax({
                            dataType: 'jsonp',
                            url: pollUrl + '/pollResults.json?id=' + voteResponse.pollID,
                            timeout: 3000,
                            success: function (resultResponse) {
                                //If results are hidden draw the hidden pane
                                if (resultResponse.pollDetails[0].options[0].hidden && resultResponse.playerAlreadyVoted) {
                                    $('#pollDetails' + voteResponse.pollID).remove();
                                    RS3.polls.drawHiddenMessagePane(pollId);
                                }
                                else {
                                    var rightPaneContents = RS3.polls.generateResultsPane(resultResponse, voteResponse.pollID), leftPaneContents = RS3.polls.generateDetailsPane(resultResponse, voteResponse.pollID);
                                    rightPane.html(rightPaneContents);
                                    leftPane.html(leftPaneContents);
                                }
                            },
                            fail: function () {
                                RS3.polls.drawErrorPane(pollId, -1);
                            },
                            error: function () {
                                RS3.polls.drawErrorPane(pollId, -1);
                            }
                        })//ajax2
                    },
                    fail: function () {
                        RS3.polls.drawErrorPane(pollId, -1);
                    },
                    error: function () {
                        RS3.polls.drawErrorPane(pollId, -1);
                    }
                });//ajax1
            }//if checked
        });//Click event
    },
    init: function () {

        $('.pollExpand').on('click', function (ev) {
            if (loggedIn > 0) {
                ev.preventDefault();
                var pollId = this.getAttribute('data-pollid');
                RS3.polls.drawPollResults(pollId);
            }
        });
    }
};
RS3.offerwall = {
    callSonic: function () {
        $('#offerwallBox').append('<iframe id="offerWallFrame" frameborder="0" ' +
            'src="https://www.supersonicads.com/delivery/panel.php?noDirectPayments=1&' +
            coreWallParams + '&custom_button=' + supersonicButton + '"></iframe>');
    },
    callPeanut: function () {
        $('#offerwallBox').append('<iframe src="https://www.peanutlabs.com/userGreeting.php?' +
            coreWallParams +
            '" width="950px" height="533px" id="offerWallFrame" frameborder="0" scrolling="yes"></iframe>');
    },
    callSuperRewards: function () {
        $('#offerwallBox').append('<iframe src="https://wall.superrewards.com/super/offers?' +
            coreWallParams +
            '" width="950px" id="offerWallFrame" frameborder="0" scrolling="no"></iframe>');
    },
    drawError: function () {
        $('#offerwallBox').append("<p>There was an error loading the wall - please try again later</p>");
    },
    init: function () {
        if (wallOrigin == 'game') {
            $('#closeWall').on('click', function (ev) {
                ev.preventDefault();
                document.location = unshim + '?closeBilling=1';
            });
        } else if (wallOrigin == 'store') {
            $('#closeWall').on('click', function (ev) {
                ev.preventDefault();
                document.domain = '';
                window.parent.$('#closeBillingWindow').click();
            });
        }
        switch (wallProvider) {
            case 0:
                if (supersonicButton == '') {
                    this.drawError();
                } else {
                    this.callSonic();
                }
                break;
            case 1:
                this.callPeanut();
                break;
            case 2:
                this.callSuperRewards();
                break;
            default:
                //unknown wall type
                this.drawError();
                break;
        }
    }
};
RS3.support = {
    init: function () {
        $('#internet_type').change(function () {
            var $this = $(this);
            var $other = $this.parents('.formPartPair').find('.formAdditional');
            if ($this.val() === '0') {
                $other.show();
                $('#internet_type_other').attr('required', 'required');
            } else {
                $other.val('').hide();
                $('#internet_type_other').removeAttr('required');
            }
        });
        var BrowserDetect = {
            init: function () {
                this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
                this.version = this.searchVersion(navigator.userAgent)
                    || this.searchVersion(navigator.appVersion)
                    || "an unknown version";
                this.OS = this.searchString(this.dataOS) || "an unknown OS";
            },
            searchString: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    var dataProp = data[i].prop;
                    this.versionSearchString = data[i].versionSearch || data[i].identity;
                    if (dataString) {
                        if (dataString.indexOf(data[i].subString) != -1)
                            return data[i].identity;
                    }
                    else if (dataProp)
                        return data[i].identity;
                }
            },
            searchVersion: function (dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index == -1) return;
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            },
            dataBrowser: [
                {
                    string: navigator.userAgent,
                    subString: "Chrome",
                    identity: "Chrome"
                },
                {
                    string: navigator.userAgent,
                    subString: "OmniWeb",
                    versionSearch: "OmniWeb/",
                    identity: "OmniWeb"
                },
                {
                    string: navigator.vendor,
                    subString: "Apple",
                    identity: "Safari",
                    versionSearch: "Version"
                },
                {
                    prop: window.opera,
                    identity: "Opera",
                    versionSearch: "Version"
                },
                {
                    string: navigator.vendor,
                    subString: "iCab",
                    identity: "iCab"
                },
                {
                    string: navigator.vendor,
                    subString: "KDE",
                    identity: "Konqueror"
                },
                {
                    string: navigator.userAgent,
                    subString: "Firefox",
                    identity: "Firefox"
                },
                {
                    string: navigator.vendor,
                    subString: "Camino",
                    identity: "Camino"
                },
                {  // for newer Netscapes (6+)
                    string: navigator.userAgent,
                    subString: "Netscape",
                    identity: "Netscape"
                },
                {
                    string: navigator.userAgent,
                    subString: "MSIE",
                    identity: "Explorer",
                    versionSearch: "MSIE"
                },
                {
                    string: navigator.userAgent,
                    subString: "Gecko",
                    identity: "Mozilla",
                    versionSearch: "rv"
                },
                {   // for older Netscapes (4-)
                    string: navigator.userAgent,
                    subString: "Mozilla",
                    identity: "Netscape",
                    versionSearch: "Mozilla"
                }
            ],
            dataOS: [
                {
                    string: navigator.platform,
                    subString: "Win",
                    identity: "Windows"
                },
                {
                    string: navigator.platform,
                    subString: "Mac",
                    identity: "Mac"
                },
                {
                    string: navigator.userAgent,
                    subString: "iPhone",
                    identity: "iPhone/iPod"
                },
                {
                    string: navigator.platform,
                    subString: "Linux",
                    identity: "Linux"
                }
            ]
        };
        BrowserDetect.init();

        function cookies_enabled() {
            var cookieEnabled = (navigator.cookieEnabled) ? true : false;
            if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
                document.cookie = "testcookie";
                cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
            }
            return (cookieEnabled);
        }

        $("#cookies_status").val("FALSE");
        if (cookies_enabled())$("#cookies_status").val("TRUE");
        $("#browser_name").val(BrowserDetect.browser);
        $("#browser_version").val(BrowserDetect.version);

        $("#charlimit_text_msg").on('focus', function () {
            $('#charRemText').stop().fadeIn('slow');
        }).on('blur', function () {
            $('#charRemText').stop().fadeOut('slow');
        });

        $("#emailAddress").on('focus', function () {
            $('#inputNotes').stop().fadeIn('slow');
        }).on('blur', function () {
            $('#inputNotes').stop().fadeOut('slow');
        });

        var alerted = false;

        function do_watch(msg, element, count, max, submit) {
            try {
                var stri = element.value.replace(/\r/g, "");
                if (submit) if (stri.length > max) submit.disabled = true;
                if (stri.length > max) {
                    if (msg == true && alerted == false) {
                        alert('You have gone over your character limit for this message');
                        alerted = true;
                    }
                    element.value = stri = stri.substring(0, max);
                }
                count.childNodes[0].nodeValue = max - stri.length;
            }
            catch (e) {
            }
        }

        function install_watch(msg, element, count, max, form, submit, reset) {
            try {
                element.onkeyup = function () {
                    do_watch(msg, element, count, max, submit);
                };
                element.onkeydown = function () {
                    do_watch(msg, element, count, max, submit);
                };
                element.onkeypress = function () {
                    do_watch(msg, element, count, max, submit);
                };
                element.onmousemove = function () {
                    do_watch(msg, element, count, max, submit);
                };
                element.onchange = function () {
                    do_watch(false, element, count, max, submit);
                };
                if (form) {
                    form.onsubmit = function () {
                        do_watch(msg, element, count, max, submit);
                    };
                }
                if (reset && form) {
                    reset.onclick = function () {
                        form.reset();
                        do_watch(msg, element, count, max, submit);
                    }
                }
                do_watch(false, element, count, max, submit);
            }
            catch (e) {
            }
        }

        var charlimiter_run = false;

        function install_charlimiters() {
            if (charlimiter_run) return;
            charlimiter_run = true;
            try {
                var textboxes = document.getElementsByTagName("textarea");
                for (var i = 0; i < textboxes.length; i++) install(textboxes[i]);
                var inputs = document.getElementsByTagName("input");
                for (var i = 0; i < inputs.length; i++) install(inputs[i]);
            }
            catch (e) {
            }
        }

        function install(element) {
            var textbox_id_len = new String("charlimit_text").length;
            var text_id = element.id.toString();
            if (text_id.match(/^charlimit_text/i) && text_id.length >= textbox_id_len) {
                var identifier = text_id.substr(textbox_id_len);
                var info = document.getElementById("charlimit_info" + identifier);
                var count = document.getElementById("charlimit_count" + identifier);
                var form = document.getElementById("charlimit_form" + identifier);
                var submit = document.getElementById("charlimit_submit" + identifier);
                var reset = document.getElementById("charlimit_reset" + identifier);
                if (info && count) {
                    var msg = false;
                    if (identifier.match(/^_msg/i)) msg = true;
                    var max_val = parseInt(count.childNodes[0].nodeValue);
                    install_watch(msg, element, count, max_val, form, submit, reset);
                    info.style.display = 'inline';
                }
            }
        }

        if (window.addEventListener) window.addEventListener('load', install_charlimiters, true);
        else if (window.attachEvent) window.attachEvent('onload', install_charlimiters);
        else window.onload = install_charlimiters;

    }
};
RS3.authenticator = {
    init: function () {
        $('#authenticator').parent().css('height', 'auto');
        $('.dropTitle').click(function () {
            var $self = $(this);
            $self.toggleClass('active');
        });
    }
};
RS3.runelabs = {
    updateLastSearch: function () {
        var searchField = $('#search-ideas'), sortField = $('#sortSelect');
        sortField.on('change', function () {
            $('#previousQuery').val(searchField.val());
        });
        searchField.on('change', function () {
            if (!searchField.val() || searchField.val() === '') {
                if (sortField.val() == 4) {
                    sortField.val(1);
                }
                $('#relevanceOption').prop("disabled", true);
            }
            else {
                $('#relevanceOption').prop("disabled", false);
            }
            if (!($('.ideaFilter__details').prop('open') || $('.ideaFilter__details').hasClass('details--show'))) {
                sortField.val(4);
            }
        });
        searchField.on('keyup', function (ev) {
            ev.preventDefault();
        });
    }
};
RS3.seasonpromo = {
    getPromoPrices: function (storeId, elementId, bonds) {
        $.ajax({
            url: baseURL + '/billing_store.ajax?storeId=' + storeId,
            dataType: 'json',
            timeout: 3000,
            success: function (data) {
                var priceString = data.results[0].localisedPrice;
                if (bonds) {
                    priceString = data.results[0].localisedName;
                }
                $('#' + elementId).html(priceString);
            }
        });
    },
    init: function () {
        this.getPromoPrices(promoStores[0], 'title-price', false);
        this.getPromoPrices(promoStores[1], 'usually-price', false);
    }
};
RS3.onLoad = {
    init: function () {
        alert("Hi");
        'use strict';
        RS3.tracking.init();
        if (typeof Modernizr != 'undefined') {
            if (!Modernizr.input.placeholder) {
                $('.placeholder').placeholder();
            }
        }

        var id = document.getElementsByTagName('body')[0].id;
        if (id != 'legacy') {
            RS3.global.init();
        }
        switch (id) {
            case 'home':
                RS3.homepage.init();
                break;
            case 'splash':
                RS3.splashpage.init();
                break;
            case 'news':
                RS3.news.init();
                break;
            case 'membersBenefits':
                RS3.membersBenefits.init();
                break;
            case 'hiscore':
                RS3.hiscores.init();
                break;
            case 'accountAssoc':
                RS3.assoc.init();
                break;
            case 'legacy':
                RS3.legacy.init();
                break;
            case 'infographic':
                RS3.infographic.init();
            case 'weblogin':
            case 'weblogin loginiframe':
                RS3.weblogin.init();
                break;
            case 'beta':
                RS3.betaTest.init();
                break;
            case 'storeLoc':
                RS3.storeloc.init();
            case 'friends':
                RS3.friends.init();
                break;
            case 'bonds':
                RS3.bonds.init();
                break;
            case 'combat':
                RS3.combat.init();
                break;
            case 'goodwill':
                RS3.goodwill.init();
                break;
            case 'premierClub':
                RS3.premierClub.init();
                break;
            case 'companion':
                RS3.companion.init();
                break;
            case 'topTrumps':
                RS3.topTrumps.init();
                break;
            case 'gamePage':
                RS3.gamePage.init();
                break;
            case 'grandexchange':
                RS3.grandExchange.init();
            case 'polls':
                RS3.polls.init();
                break;
            case 'offerwall':
                RS3.offerwall.init();
                break;
            case 'support':
                RS3.support.init();
                break
            case 'authenticator':
                RS3.authenticator.init();
                break;
            case 'player-proposal--ideas':
                RS3.global.detailsElement();
                RS3.runelabs.updateLastSearch();
                break;
            case 'season-promo':
                RS3.seasonpromo.init();
                break;
        }
    }
};
$(window).ready(
    //alert("Hi");
    RS3.onLoad.init()
);