/*!
 * Copyright Jagex Games Ltd 2015
 * Some code used from http://www.rajeshsegu.com
 * Used to test whether a given protocol is supported
 * Please allow ~1000ms for the result as some systems take longer than others
 * protocolStr represents the full url we wish to test
 */
var protocolSupport = [],
    checkProtocol = function (protocolStr, browser) {
        //Updates the array with the result for the given protcol
        function updateResult(result) {
            protocolSupport[protocolStr] = result;

        }

        //Define result as null
        updateResult(null);
        bodyElement = $('body');
        //Different browsers have different detection methods so run the relevant one
        //Firefox
        function checkMozilla() {
            bodyElement.append("<iframe src='#' id='hiddenIframe' style='display: none;'></iframe>");
            var iFrame = $('#hiddenIframe');
            //Set iframe.src and handle exception
            try {
                iFrame[0].contentWindow.location.href = protocolStr;
                if (browser !== 'msie') {
                    iFrame.remove()
                }
                ;
                updateResult(true);
            }
            catch (e) {
                if (browser !== 'msie') {
                    iFrame.remove()
                }
                ;
                updateResult(false);
            }
        }

        //Chrome
        function checkChrome() {
            bodyElement.append("<input type='text' id='focusInput' style='background: transparent;border: none;height: 0px;width: 0px;' />");
            var focusBodyElement = $('#focusInput')[0], temporaryResult = false;
            focusBodyElement.focus();
            focusBodyElement.onblur = function () {
                updateResult(true);
                return;
            };
            //will trigger onblur
            location.href = protocolStr;

            //Note: timeout could vary as per the browser version, have a higher value
            setTimeout(function () {
                    focusBodyElement.onblur = null;
                    if (protocolSupport[protocolStr] === null) {
                        updateResult(false);
                    }
                },
                1000
            );

        }

        //Detect which tests to run - if we dont know the browser assume chrome
        switch (browser) {
            case 'mozilla':
                checkMozilla();
                break;
            case 'chrome':
                checkChrome();
                break;
            case 'msie':
                checkMozilla();//Temporary fix to use FF method. We need to address this later
                break;
            default:
                checkChrome();
                break;
        }
    };