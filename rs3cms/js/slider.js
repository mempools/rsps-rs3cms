var community = {
    memory: {
        flash: '8.0.0'
    },
    initialise: function () {
        community.slider();
    },
    // SLIDER
    slider: function () {
        if (swfobject.hasFlashPlayerVersion(community.memory.flash)) {
            $("#bannerReel a").each(function (i) {
                var $this = $(this);
                var src = $this.find('img').attr("src");
                var href = $this.attr("href");
                if (src.match("/swf/")) {
                    var newID = "slideSWF" + i;
                    $this.replaceWith("<div><div id='" + newID + "'></div></div>").attr({id: newID});

                    swfobject.embedSWF(
                        src.replace(/\.[^\.]{3,4}$/, ".swf"),
                        newID,
                        "1000",
                        "384",
                        community.memory.flash,
                        "",
                        {
                            click_url: href
                        },
                        {
                            wmode: "opaque"
                        }
                    );
                }
            });
        }

        $("#bannerReel").arturoSlider({
            nextID: "bannerNext",
            prevID: "bannerPrev",
            speed: 800,
            period: 10000
        });
    }
};

$(function () {
    community.initialise()
});