<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define("PAGE_TITLE", "Home");

require_once("global.php");
require_once("header.php");
?>

<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/news-50.css"/>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/beginnersguide-50.css"/>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/home.css"/>
<script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/shadowbox/shadowbox-0.js"></script>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/shadowbox/shadowbox-0.css"/>

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">
                                <h3 class="Gradient DoubleFlourish">
                                    <span class="spacing" aria-hidden="true">Home</span>
                                    <span class="G0">Home</span>
                                    <span class="G1" aria-hidden="true">Home</span>
                                    <span class="G2" aria-hidden="true">Home</span>
                                    <span class="G3" aria-hidden="true">Home</span>
                                    <span class="G4" aria-hidden="true">Home</span>
                                    <span class="G5" aria-hidden="true">Home</span>
                                    <span class="G6" aria-hidden="true">Home</span>
                                    <span class="mask">
                                        <span class="spacing" aria-hidden="true">Home</span>
                                        <span class="leftInnerFlourish"></span>
                                        <span class="centreFlourish"></span>
                                        <span class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/>
                                        <span class="spacing" aria-hidden="true">Home</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div id="maintop">
                        <div class="topLeftBar">
                            <img src="<?php printf(SITE_ADDRESS); ?>img/main/beginnersguide/topLeftBar.png" alt="line"/>
                        </div>
                        <div class="whatisrs">
                            <h2 class="Gradient NoFlourish">
                                <span class="G0"><?php printf(SITE_NAME); ?></span>
                                <span class="G1" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                                <span class="G2" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                                <span class="G3" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                                <span class="G4" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                                <span class="G5" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                                <span class="G6" aria-hidden="true"><?php printf(SITE_NAME); ?></span>
                            </h2>

                            <p><span class="FirstLetter">W</span>elcome to <?php printf(SITE_NAME); ?>. A brand new
                                RuneScape private server that gives you access to all member features, without paying
                                for them! </p>
                            <br/>

                            <p>Powered by <a href="http://joltdev.com/">Jolt
                                    Environment</a>, <?php printf(SITE_NAME); ?> offers new robust features and
                                stability that will never be seen anywhere else! Full integration between the website
                                and server makes everything such as account manage even easier than ever before.</p>

                            <br/>
                            <br/>

                            <div class="playnow">
                                <a class="HoverImg" href="play" id="PlayNowWrapperPlayFree">
                                    <img src="<?php printf(SITE_ADDRESS); ?>img/main/beginnersguide/playnow.png"
                                         alt="Play Now">
                                </a>
                            </div>
                        </div>
                        <div class="relatedtopics">
                        </div>
                    </div>

                    <div id="rightColumn">
                        <br/>
                        <br/>

                        <div id="advert">
                            <iframe src="http://www.runescape.com/g=runescape/advert/advert.ws?placement=204"
                                    allowtransparency="true" width="300" height="269" scrolling="no"
                                    frameborder="0"></iframe>
                            <span>Advert</span>
                        </div>

                        <div id="SNcontainer">
                            <div id="events">
                                <div id="eventsInner">

                                    <h3 class="Gradient">
                                        <span class="G0">Events</span>
                                        <span class="G1" aria-hidden="true">Events</span>
                                        <span class="G2" aria-hidden="true">Events</span>
                                        <span class="G3" aria-hidden="true">Events</span>
                                        <span class="G4" aria-hidden="true">Events</span>
                                        <span class="G5" aria-hidden="true">Events</span>
                                        <span class="G6" aria-hidden="true">Events</span>
                                        <span class="mask"><span class="spacing">Events</span>
                                            <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Events</span></span>
                                        </span>
                                        <span class="rightUnderscore">
                                            <img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                                class="right" alt=""/><span class="spacing"
                                                                            aria-hidden="true">Events</span>
                                        </span>
                                        <span class="leftUnderscore"><img
                                                src="img/global/gradient_header/underscore_flourish_left.png"
                                                class="left" alt=""/></span>
                                    </h3>

                                    <div id="eventsContent">
                                        <a href="http://www.runescape.com/g=runescape/events.ws"
                                           class="Button Button29"><span><span><span class=""><b>View All
                                                            Events</b></span></span></span></a>

                                        <div id="eventImage">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/event.jpg" alt=""
                                                 title=""/>
                                        </div>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">The RuneZone New Years Party</h4>
                                            Location: <span>Varrock</span><br/>
                                            When: <span>06/01/2012 - 9pm</span>
                                        </a>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">Wilderness Battle (F2P)</h4>
                                            Location: <span>North Varrock</span><br/>
                                            When: <span>21-01-2012 - 9pm</span>
                                        </a>
                                        <a href="http://www.runescape.com/g=runescape/events.ws" class="event">
                                            <div class="img">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/events/flag-user.png"
                                                    alt="" title=""/>
                                            </div>
                                            <h4 class="FlatHeader">RuneScape Rugby Premiership</h4>
                                            Location: <span>Clan Citadel</span><br/>
                                            When: <span>26-01-2012 to 11-03-2012</span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="NewsArticles">

                        <?php
                        $news_qry = dbquery("SELECT * FROM web_news ORDER BY id DESC LIMIT 3;");

                        if (mysql_num_rows($news_qry) > 0) {
                            while ($news = mysql_fetch_assoc($news_qry)) {
                                $news_id = $news['id'];
                                $news_date = $news['date'];
                                $news_author = agent::username_from_id($news['author_id']);
                                $news_title = $news['title'];
                                $news_desc = $news['desc'];
                                $category_id = $news['category_id'];
                                $seo_url = $news['seo_url'];
                                ?>
                                <div class="Article">
                                    <div>
                                        <div class="NewsHeaderImage">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/global/news/NoImage.jpg" alt=""
                                                 title=""/>

                                            <div></div>
                                        </div>
                                        <?php
                                        $category_name = news_category($category_id);
                                        printf("<img src=\"" . SITE_ADDRESS . "img/global/news/categories/Cat_" . $category_id . ".png\" alt=\"" . $category_name . "\" title=\"" . $category_name . "\" class=\"Category\"/>");
                                        ?>
                                        <div class="Title">
                                            <a href="<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>">
                                                <h4 class="FlatHeader"><?php printf($news_title); ?></h4></a>
                                            <br style="clear:both"/>
                                            <span><?php printf(compare_dates($news_date, time())); ?></span>
                                        </div>
                                        <div class="Content">
                                            <?php printf($news_desc); ?>
                                        </div>
                                        <span class="SNbutton">
                                            <span class="left"></span>
                                            <span class="middle">
                                                <span class="contentHolder">
                                                    <a class="facebook SB_link"
                                                       href="http://facebook.com/sharer.php?u=<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"
                                                       target="_blank" title="Add this page to Facebook">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNfacebook.png"
                                                            alt="Facebook" title="Facebook"/>
                                                        <span class="SB_label">Facebook</span>
                                                    </a>
                                                    <a href="http://twitter.com/share?text=<?php printf($news_title); ?>&url=<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"
                                                       target="_blank" class="twitter SB_link">
                                                        <img
                                                            src="<?php printf(SITE_ADDRESS); ?>img/global/social/SNtwitter.png"
                                                            alt="Twitter" title="Twitter"/>
                                                        <span class="SB_label">Twitter</span>
                                                    </a>
                                                </span>
                                                <span class="right">Share</span>
                                            </span>
                                        </span>
                                        <a class="HoverImg More"
                                           href="<?php printf(SITE_ADDRESS . "news/article/" . $news_id . "-" . $seo_url); ?>"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/news/ReadMore.png" alt=""
                                                title=""/></a>
                                    </div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                    </div>
                    <form method="post" style="display: inline;" action="list.ws&amp;cat=0">
                        <div class="PageControl">
                            <a href="<?php printf(SITE_ADDRESS); ?>news" class="Button Button29"><span><span><span
                                            class=""><b>View all</b></span></span></span></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <?php require_once("footer.php"); ?>
