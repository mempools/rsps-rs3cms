<?php
define("PAGE_TITLE", "Hiscores");

require_once("global.php");
require_once("header.php");
require_once("includes/class_character_statistics.php");

//$qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank, total_level, total_exp, master_id FROM characters_statistics ORDER BY total_level DESC, total_exp DESC LIMIT $start_from,$show ) AS cstats INNER JOIN characters ON characters.id = cstats.master_id");
function getRank($id, $order)
{
    $count = 1;
    $res = dbquery("SELECT total_level, master_id FROM characters_statistics ORDER BY $order");
    if (mysql_num_rows($res) > 0) {
        while ($row = mysql_fetch_array($res)) {
            $userid = $row['master_id'];
            //  echo $count;
            if ($userid == $id) {
                return $count;
            }
            $count++;
        }
    }
    return -1;
}

if (isset($_GET['skill'])) {
    $skill_id = $_GET['skill'];
    switch ($skill_id) {
        case 0:
            $skill = "total";
            break;
        case 1:
            $skill = "attack";
            break;
        case 2:
            $skill = "defence";
            break;
        case 3:
            $skill = "strength";
            break;
        case 4:
            $skill = "constitution";
            break;
        case 5:
            $skill = "range";
            break;
        case 6:
            $skill = "prayer";
            break;
        case 7:
            $skill = "magic";
            break;
        case 8:
            $skill = "cooking";
            break;
        case 9:
            $skill = "woodcutting";
            break;
        case 10:
            $skill = "fletching";
            break;
        case 11:
            $skill = "fishing";
            break;
        case 12:
            $skill = "firemaking";
            break;
        case 13:
            $skill = "crafting";
            break;
        case 14:
            $skill = "smithing";
            break;
        case 15:
            $skill = "mining";
            break;
        case 16:
            $skill = "herblore";
            break;
        case 17:
            $skill = "agility";
            break;
        case 18:
            $skill = "thieving";
            break;
        case 19:
            $skill = "slayer";
            break;
        case 20:
            $skill = "farming";
            break;
        case 21:
            $skill = "runecrafting";
            break;
        case 22:
            $skill = "construction";
            break;
        case 23:
            $skill = "hunter";
            break;
        case 24:
            $skill = "summoning";
            break;
        case 25:
            $skill = "dungeoneering";
            break;
        default:
            $skill = "total";
            break;
    }
} else {
    $skill = "total";
}

/* check if friends only ranking */
if (isset($_GET['friends'])) {
    $friends = true;
}

?>

    <link href="<?php printf(SITE_ADDRESS); ?>css/hiscores_global-52.css" rel="stylesheet"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/hiscores-52.css" rel="stylesheet"/>

    <div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">

                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">
                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Hiscores</span>
                                    <span class="G0">Hiscores</span>
                                    <span class="G1" aria-hidden="true">Hiscores</span>
                                    <span class="G2" aria-hidden="true">Hiscores</span>
                                    <span class="G3" aria-hidden="true">Hiscores</span>
                                    <span class="G4" aria-hidden="true">Hiscores</span>
                                    <span class="G5" aria-hidden="true">Hiscores</span>
                                    <span class="G6" aria-hidden="true">Hiscores</span>
                                                        <span class="mask"><span class="spacing" aria-hidden="true">Hiscores</span>
                                                            <span class="leftInnerFlourish"></span><span
                                                                class="centreFlourish"></span><span
                                                                class="rightInnerFlourish"></span>
                                                        </span>
                                                        <span class="rightUnderscore">
                                                            <img
                                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                                                class="right" alt=""/><span class="spacing"
                                                                                            aria-hidden="true">Hiscores</span>
                                                        </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <nav>
                        <div class="SubNavigation">
                            <a href="#" class="HoverGradient " id="">
                                <span class="Gradient_link4"><span style="position:relative">Hall of Heroes</span><span
                                        class="g1" aria-hidden="true">Hall of Heroes</span><span class="g2"
                                                                                                 aria-hidden="true">Hall of Heroes</span><span
                                        class="g3" aria-hidden="true">Hall of Heroes</span><span class="g4"
                                                                                                 aria-hidden="true">Hall of Heroes</span><span
                                        class="g5" aria-hidden="true">Hall of Heroes</span></span></a>

                            <div id="" class="MenuItemTransparentBg"><span class="TransparentBg"><h5
                                        class="TransparentText"><span class="Gradient_link4"><span
                                                style="position:relative">Skills</span><span aria-hidden="true"
                                                                                             class="g1">Skills</span><span
                                                aria-hidden="true" class="g2">Skills</span><span aria-hidden="true"
                                                                                                 class="g3">Skills</span><span
                                                aria-hidden="true" class="g4">Skills</span><span aria-hidden="true"
                                                                                                 class="g5">Skills</span></span>
                                    </h5></span><span class="TransparentBgLeft"></span></div>

                        </div>
                    </nav>
                    <div id="hiscoresSearch">
                        <form action="<?php printf(SITE_ADDRESS); ?>hiscores_compare" method="get">

                            <div class="EncrustedInputBoxWrapper">
                                <div class="InputBoxLeft"><input type='text' title="Player" name='user1'
                                                                 maxlength='20' size='15' value="" id='compare'
                                                                 autocomplete='off'></div>
                                <div class="InputBoxRight"></div>
                                <div class="InputBoxLeft"><input type='text' title="Other player" name='user2'
                                                                 maxlength='20' size='15' value="" id='compare'
                                                                 autocomplete='off'></div>
                                <div class="InputBoxRight"></div>
                            </div>
                            <span class="Button Button29"><span><span><span class=""><b>Compare</b></span><input
                                            type="submit" title="Find"/></span></span></span>
                        </form>
                    </div>

                    <!--                    <div id="Advert">-->
                    <!--                        <iframe src="<?php printf(SITE_ADDRESS); ?>webclient" allowtransparency="true" width="730" height="100"-->
                    <!--                                scrolling="no" frameborder="0"></iframe>-->
                    <!--                    </div>-->

                    <div id="rankingWrapper">

                        <style>
                            #skillsDropDown.InPageDropDownNew.NoJS:hover .DropListWrapper,
                            #skillsDropDown.InPageDropDownNew.CSS3:hover .DropListWrapper {
                                top: -8px;
                            }

                            #skillsDropDown.InPageDropDownNew.NoJS .DropListWrapper,
                            #skillsDropDown.InPageDropDownNew.CSS3 .DropListWrapper {
                                top: -460px;
                            }

                            #skillsDropDown:hover {
                                height: 488px;
                            }
                        </style>
                        <div class="InPageDropDownNew NoJS CSS3 width150" id="skillsDropDown" data-expand="488px">
                            <div class="Mask"></div>
                            <div class="DropListWrapper" data-retract="-460px">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/DropDownFlourish.png"
                                     class="Flourish" alt="">
                                <span class="firstValue"
                                      style="text-transform: capitalize;"><?php printf($skill); ?></span>
                                <ul>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=0">Total</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=1">Attack</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=2">Defence</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=3">Strength</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=4">Constitution</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=5">Ranged</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=6">Prayer</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=7">Magic</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=8">Cooking</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=9">Woodcutting</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=10">Fletching</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=11">Fishing</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=12">Firemaking</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=13">Crafting</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=14">Smithing</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=15">Mining</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=16">Herblore</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=17">Agility</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=18">Thieving</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=19">Slayer</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=20">Farming</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=21">Runecrafting</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=22">Hunter</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=23">Construction</a>
                                    </li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=24">Summoning</a></li>
                                    <li><a href="<?php printf(SITE_ADDRESS); ?>hiscores.php?skill=25">Dungeoneering</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="OrnamentalBox Width863 SegmentOne"
                             id="<?php printf(isset($friends) ? "friends" : "player"); ?>Hiscores">
                            <div class="OrnamentalBoxTop"></div>
                            <div class="OrnamentalBoxTitle">
                                <div class="Centre1">
                                    <div class="Centre2"><h3 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                                                      aria-hidden="true">Member Rankings</span>
                                            <span class="G0">Member Rankings</span>
                                            <span class="G1" aria-hidden="true">Member Rankings</span>
                                            <span class="G2" aria-hidden="true">Member Rankings</span>
                                            <span class="G3" aria-hidden="true">Member Rankings</span>
                                            <span class="G4" aria-hidden="true">Member Rankings</span>
                                            <span class="G5" aria-hidden="true">Member Rankings</span>
                                            <span class="G6" aria-hidden="true">Member Rankings</span>
                                        </h3></div>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBg">
                                <div class="OrnamentalBoxContent">
                                    <div id="hiscoresControls">
                                        <a id="all" href="<?php printf(SITE_ADDRESS); ?>hiscores">
                                            <div class="hiscoresBoxTopLeft"></div>
                                            <div class="hiscoresBoxTopRight"></div>
                                            <h4>All Players</h4>
                                            <span class="subHeading">A ranking of all players</span>

                                            <div class="hiscoresBoxBottomLeft"></div>
                                            <div class="hiscoresBoxBottomRight"></div>
                                        </a>
                                        <a id="friends"
                                           <?php if (!$agent->logged_in) { ?>class="OpenShadowbox"<?php } ?>
                                           href="<?php printf(SITE_ADDRESS); ?>hiscores/friends">
                                            <div class="hiscoresBoxTopLeft"></div>
                                            <div class="hiscoresBoxTopRight"></div>
                                            <h4>Friends</h4>
                                            <span class="subHeading">A ranking of players on your Friends List</span>

                                            <div class="hiscoresBoxBottomLeft"></div>
                                            <div class="hiscoresBoxBottomRight"></div>
                                        </a>
                                    </div>
                                    <div class="tableContainer cssTableSmall">
                                        <div class="hiscoresBoxTopLeft"></div>
                                        <div class="hiscoresBoxTopRight"></div>
                                        <div class="tableInnerContainer">
                                            <div class="header">
                                                <span class="columnRank">Rank</span>
        <span class="columnName">
            <span>Name</span>
        </span>
        <span class="columnLevel">
            <span>Level</span>
        </span>
        <span class="columnXp">
            <span>XP</span>
        </span>
                                            </div>

                                            <?php
                                            $show = 25;
                                            $page = 1;

                                            if (isset($_GET['page']) && is_numeric($_GET['page'])) {
                                                $page = $_GET['page'];
                                            }

                                            if (isset($_GET['rank']) && is_numeric($_GET['rank'])) {
                                                $rank_loc = $_GET['rank'];
                                                $page = ceil(($rank_loc / $show));
                                            }

                                            $start_from = ($page - 1) * $show;

                                            $total_results = dbevaluate("SELECT COUNT(*) FROM characters_statistics;");
                                            $total_pages = ceil($total_results / $show);

                                            dbquery("SET @rankcount = $start_from;");
                                            $clevel = $skill . "_level";
                                            $cexp = $skill . "_exp";

                                            if ($skill == "total") {
                                                $order = "total_level DESC, total_exp DESC";
                                                $columns = "total_level,total_exp";
                                            } else {
                                                $order = "$cexp DESC";
                                                $columns = "$cexp";
                                            }

                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank, $columns, master_id FROM characters_statistics ORDER BY $order LIMIT $start_from,$show ) AS cstats INNER JOIN characters ON characters.id = cstats.master_id");

                                            // Check if there's
                                            if (isset($friends)) {
                                                $uid = $agent->master_id;
                                                $friends_qry = dbquery("SELECT characters.id FROM characters_contacts LEFT JOIN characters ON characters_contacts.contact_id = characters.id WHERE characters_contacts.master_id = '$uid';");

                                                if (mysql_num_rows($friends_qry) > 0) {
                                                    $in = "";
                                                    while ($friend = mysql_fetch_array($friends_qry)) {
                                                        $fid = $friend['id'];

                                                        if (strlen($in) > 0) {
                                                            $in .= ",";
                                                        }
                                                        $in .= "'$fid'";
                                                    }

                                                    $rqry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT total_level, total_exp, master_id FROM characters_statistics ORDER BY total_level DESC, total_exp DESC ) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id IN($in);");
                                                    if (mysql_num_rows($rqry)) {
                                                        $total_results = mysql_num_rows($rqry);
                                                        $total_pages = ceil($total_results / $show);
                                                        $qry = $rqry;
                                                    }
                                                }
                                            }

                                            //                                            while ($user = mysql_fetch_assoc($qry)) {
                                            $qry = dbquery("SELECT * FROM characters_statistics ORDER BY $order");
                                            while ($user = mysql_fetch_assoc($qry)) {
                                                ?>
                                                <a name="<?php echo $user['rank'] ?>"></a>
                                                <a href="<?php printf(SITE_ADDRESS); ?>profile/<?php echo $agent->username_from_id($user['master_id']); ?>"
                                                   class="row  ">
            <span class="columnRank">
<!--                <span>--><?php //printf(number_format($user['rank']));
                ?><!--</span>-->
                <span><?php printf(number_format(getRank($user['master_id'], $order))); ?></span>
            </span>
            <span class="columnName">
                <span><?php printf($agent->username_from_id($user['master_id'])); ?></span>
            </span>
            <span class="columnLevel">
                <span>
                <?php
                if ($skill == "total") {
                    printf(number_format($user[$clevel]));
                } else {
                    printf(character_statistics::level_for_experience(($skill_id - 1), $user[$cexp]));
                }
                ?>
                </span>
            </span>
            <span class="columnXp">
                <span><?php printf(number_format($user[$cexp])); ?></span>
            </span>
                                                </a>
                                                <?php
                                            }

                                            if (mysql_num_rows($qry) == 0) {
                                                printf("<center>No results.</center>");
                                            } else {
                                            ?>

                                            <div class="PageControl">
                                                <?php
                                                $base_url = SITE_ADDRESS . "hiscores.php?";
                                                if (isset($skill_id)) {
                                                    $base_url .= "skill=" . $skill_id . "/";
                                                }

                                                if ($page > 1) {
                                                    printf("<a href=\"" . $base_url . "p" . ($page - 1) . "\" class=\"Button Button29 Prev\"><span><span><span class=\"\"><b>Prev</b></span></span></span></a>");
                                                } else {
                                                    printf("<span class=\"Button Button29disabled Prev\"><span><span><span class=\"\"><b>Prev</b></span></span></span></span>");
                                                }

                                                if ($page < $total_pages) {
                                                    printf("<a href=\"" . $base_url . "p" . ($page + 1) . "\" class=\"Button Button29 Next\"><span><span><span class=\"\"><b>Next</b></span></span></span></a>");
                                                } else {
                                                    printf("<span class=\"Button Button29disabled Next\"><span><span><span class=\"\"><b>Next</b></span></span></span></span>");
                                                }

                                                $adjacents = 1;

                                                // start on pagignition
                                                printf("<ul>");
                                                if ($total_pages < (7 + ($adjacents * 2))) {
                                                    for ($i = 1; $i <= $total_pages; $i++) {
                                                        if ($i == $page) {
                                                            printf("<li class=\"current\">" . $page . "</li>");
                                                        } else {
                                                            printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                        }
                                                    }
                                                } elseif ($total_pages > 5 + ($adjacents * 2)) {
                                                    if ($page < 1 + ($adjacents * 2)) {
                                                        for ($i = 1; $i < 4 + ($adjacents * 2); $i++) {
                                                            if ($i == $page) {
                                                                printf("<li class=\"current\">" . $page . "</li>");
                                                            } else {
                                                                printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                            }
                                                        }
                                                        printf("<li class=\"ellipses\">...</li>");
                                                        printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                                                    } elseif ($total_pages - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                                                        printf("<li><a href=\"" . $base_url . "p1\">1</a></li>");
                                                        printf("<li class=\"ellipses\">...</li>");
                                                        for ($i = $page - $adjacents; $i <= $page + $adjacents; $i++) {
                                                            if ($i == $page) {
                                                                printf("<li class=\"current\">" . $page . "</li>");
                                                            } else {
                                                                printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                            }
                                                        }
                                                        printf("<li class=\"ellipses\">...</li>");
                                                        printf("<li><a href=\"" . $base_url . "p" . $total_pages . "\">" . $total_pages . "</a></li>");
                                                    } else {
                                                        printf("<li><a href=\"" . $base_url . "p1\">1</a></li>");
                                                        printf("<li class=\"ellipses\">...</li>");
                                                        for ($i = $total_pages - (2 + ($adjacents * 2)); $i <= $total_pages; $i++) {
                                                            if ($i == $page) {
                                                                printf("<li class=\"current\">" . $page . "</li>");
                                                            } else {
                                                                printf("<li><a href=\"" . $base_url . "p" . $i . "\">" . $i . "</a></li>");
                                                            }
                                                        }
                                                    }
                                                }
                                                printf("</ul>");
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="hiscoresBoxBottomLeft"></div>
                                        <div class="hiscoresBoxBottomRight"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="OrnamentalBoxBottom"></div>
                        </div>
                    </div>

                    <div class="ModalLogin">
                        <a id="shadowBoxCloseButton" href="#" onclick="Shadowbox.close();return false;"
                           class="HoverImg"><img alt="Close"
                                                 src="<?php printf(SITE_ADDRESS); ?>img/global/login/modalCloseButton.png"/></a>

                        <div class="ModalTitle">
                            <div class="Centre1">
                                <div class="Centre2"><h4 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                                                  aria-hidden="true">RuneScape Login</span>
                                        <span class="G0"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G1" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G2" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G3" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G4" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G5" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                        <span class="G6" aria-hidden="true"><?php printf(SITE_NAME); ?> Login</span>
                                    </h4></div>
                            </div>
                        </div>
                        <div class="ModalLeft">
                            <h3 class="FlatHeader">First time playing <?php printf(SITE_NAME); ?>?</h3>
                            <a href="<?php printf(SITE_ADDRESS); ?>play" class="Button Button29"
                               id="register"><span><span><span class=""><b>Register</b></span></span></span></a>
                        </div>
                        <div class="ModalRight">
                            <form class="LoginForm" action="<?php printf(SITE_ADDRESS); ?>login" method="post"
                                  autocomplete="off">
                                <span class="inputLabel FlatHeader">Login:</span>

                                <div class="DarkInputBoxWrapper">
                                    <div class="InputBoxLeft"><input type="text" title="Login" name="email"
                                                                     maxlength="200" size="26" value="" id="email"
                                                                     class="NoPlaceholder"></div>
                                    <div class="InputBoxRight"></div>
                                </div>
                                <span class="inputLabel FlatHeader">Password:</span>

                                <div class="DarkInputBoxWrapper">
                                    <div class="InputBoxLeft"><input type="password" title="Password" name="password"
                                                                     maxlength="200" size="26" value="" id="password"
                                                                     class="NoPlaceholder"></div>
                                    <div class="InputBoxRight"></div>
                                </div>
                                <span class="Button Button29" id="modalLoginButton"><span><span><span
                                                class=""><b>Login</b></span><input type="submit" name="submit"
                                                                                   title="Login"/></span></span></span>
                                <input type="hidden" value="hiscore" name="mod">
                                <input type="hidden" value="0" name="ssl">
                                <input type="hidden" value="<?php printf(SITE_ADDRESS); ?>hiscores/friends/"
                                       name="dest">
                                <a class="PasswordLink" href="/forgot_password" target="_parent">Forgot Password?</a>
                            </form>
                        </div>
                        <script type="text/javascript">
                            Shadowbox.init();
                            $(function () {
                                // brief form check before sending data
                                $("#modalLoginButton input").live('click', function () {
                                    return SetFocus();
                                });
                                $("#ModalFacebook").live('click', function () {
                                    Shadowbox.close();
                                });
                                // Shadowbox code for loginform
                                var myLoginBox = "<div class='ModalLogin'>" + $(".ModalLogin")
                                        .detach()
                                        .html() + "</div>";
                                $(".OpenShadowbox").click(function (ev) {
                                    ev.preventDefault();
                                    if (typeof Shadowbox === "object") {
                                        Shadowbox.open({
                                            content: myLoginBox,
                                            player: "html",
                                            width: 429,
                                            height: 235,
                                            options: {
                                                onFinish: function () {
                                                    $(".ModalLogin #username").focus();
                                                },
                                                enableKeys: false
                                            }
                                        });
                                        $("#sb-container").addClass("ModalLoginShadowbox");
                                    }
                                }).css("display", "block");
                            });
                            function SetFocus() {
                                /* If the username field is empty, it gets focus. If the password field is empty but username is filled,
                                 it gets focus. Returns false to prevent submit if a field is empty. */
                                var Username = $(".ModalLogin #username");
                                var Password = $(".ModalLogin #password");
                                if (Username.value == '' || $(Username).hasClass("placeholder")) {
                                    Username.focus();
                                    return false;
                                }
                                if (Password.value == '' || $(Password).hasClass("placeholder")) {
                                    Password.focus();
                                    return false;
                                }
                                return true;
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once("footer.php"); ?>