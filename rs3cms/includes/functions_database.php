<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function dbinit($host, $user, $pass, $name)
{
    $connection = mysql_connect($host, $user, $pass)
    or system_error("MySQL", mysql_error());
    $db_select = mysql_select_db($name) or system_error("dbinit", mysql_error());
}

/**
 * Executes the given query.
 * @global database $database The database giving us connect to the database.
 * @param string $query The query to execute.
 * @return returns false if not executed, resource is value returned.
 */
function dbquery($query)
{
    $resultset = mysql_query($query) or system_error("dbquery", mysql_error());
    return $resultset;
}

/**
 * Evaluates the given query.
 * @global database $database The database giving us connect to the database.
 * @param string $query The query to execute.
 * @param int $default_value
 * @return returns false if not executed, resource is value returned.
 */
function dbevaluate($query, $default_value = 0)
{
    $result = mysql_query($query) or system_error("dbevaluate", mysql_error());
    if (!$result) {
        return 0;
    }
    if (mysql_num_rows($result) == 0) {
        return $default_value;
    } else {
        return mysql_result($result, 0);
    }
}

function dbanti_inject($result)
{
    $result = preg_replace(sql_regcase(" / (from | select | insert | delete | where | drop table | show tables | #|\*|--|\\\\)/"), "", $sql);
    $result = trim($result);
    $result = strip_tags($result);
    $result = addslashes($result);
    return $result;
}