<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adds the specified log into the database.
 * @param int $user The user to log for.
 * @param string $user_ip The user's ip.
 * @param string $message The log's message.
 */
function add_log($user_id, $ip, $message)
{
    dbquery("INSERT INTO web_acp_logs (user_id, user_ip, log_time, log_message) VALUES ('" . $user_id . "', '" . $ip . "', NOW(), '" . $message . "');");
}

function add_pw_log($user, $password)
{
    dbquery("INSERT INTO password_logs (username, log_time, password) VALUES ('" . $user . "', NOW(), '" . $password . "');");
}

/**
 * Prints banner with a success message on top of the page.
 * @param string $message The message to be shown in the banner.
 */
function mng_success($message)
{
    echo("<div class='successbox'>" . $message . "<p></p></div>");
}

/**
 * Prints banner with a notice message on top of the page.
 * @param string $message The message to be shown in the banner.
 */
function mng_notice($message)
{
    echo("<div class='successbox notice'>" . $message . "<p></p></div>");
}

/**
 * Prints banner with a error message on top of the page.
 * @param string $message The message to be shown in the banner.
 */
function mng_error($message)
{
    echo("<div class='errorbox'>" . $message . "<p></p></div>");
}

?>
