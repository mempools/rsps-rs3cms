<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents a single agent connected to the cms.
 *
 * @author AJ Ravindiran
 * @version 1.0 Oct-20-2011
 */
class agent
{
    /* session info */
    public $session = "";
    public $session_start_time = null;
    public $current_page = null;
    public $ip_address = null;
    public $browser = null;

    /* agent details */
    public $master_id = 0;
    public $email = "";
    public $username = "";
    public $hash = "";
    public $logged_in = false;
    public $online = -1;
    public $permissions = array();

    /**
     * Constructs the agent session
     * @param uint $master_id The unique database id for the character.
     * @param string $email The email of the character.
     * @param string $username The username of the character.
     * @param string $hash The hashed password of the character.
     */
    public function __construct($master_id = 0, $email = "", $username = "", $hash = "")
    {
        $this->session = random_string(16);
        $this->session_start_time = time();
        $this->current_page = filter_for_input($_SERVER['REQUEST_URI']);
        $this->ip_address = $_SERVER['REMOTE_ADDR'];
        $this->browser = $_SERVER['HTTP_USER_AGENT'];

        $this->master_id = $master_id;
        $this->email = $email;
        $this->username = $username;
        $this->hash = $hash;
    }

    /**
     * Initializes the agent's session.
     * @param agent $agent A reference to an agent class.
     */
    public static function init_session(&$agent)
    {
        session_start() or system_error("Session", "Unable to start session.");

        if (isset($_SESSION['ID'])) {
            $agent->session = $_SESSION['ID'];
        }

        if (isset($_SESSION['USER_EMAIL']) && isset($_SESSION['USER_HASH'])) {

            $email = $_SESSION['USER_EMAIL'];
            $hash = $_SESSION['USER_HASH'];

            if (agent::validate($email, $hash)) {
                $agent->logged_in = true;
                $agent->email = $email;
                $agent->hash = $hash;

                $q = dbquery("SELECT * FROM characters WHERE email = '$agent->email' LIMIT 1;");
                if (mysql_num_rows($q) > 0) {
                    $udata = mysql_fetch_array($q);

                    // Set variables.
                    $agent->master_id = $udata['id'];
                    $agent->username = $udata['username'];
                    $agent->online = $udata['online'];

                    // Check for bans
                    if (agent::has_offense($agent->master_id)) {
                        if ((basename($_SERVER['PHP_SELF']) != "banned.php" && (basename($_SERVER['PHP_SELF']) != "logout.php"))) {
                            //    exit(header("Location: banned.php"));
                        }
                    }

                    // Permissions
                    $pms = dbquery("SELECT * FROM characters_permissions WHERE master_id = '$agent->master_id';");
                    if (mysql_num_rows($pms) > 0) {
                        while ($p = mysql_fetch_assoc($pms)) {
                            $agent->permissions[$p['permission']] = true;
                        }
                    }

                    // Get session (if available).
                    $q2 = dbquery("SELECT session_id FROM web_sessions WHERE session_user_id = '$agent->master_id' LIMIT 1;");
                    if (mysql_num_rows($q2) == 0) {
                        if (!isset($_SESSION['ID'])) {
                            $agent->insert_session();
                        } else {
                            session_destroy();
                            $url = $_SERVER["REQUEST_URI"];
                            exit(header("Location: $url"));
                        }
                    } else {
                        if (mysql_num_rows($q2) > 1) {
                            dbquery("DELETE FROM web_sessions WHERE session_user_id = '$agent->master_id';");
                            $agent->insert_session();
                        } else {
                            $sd = mysql_fetch_array($q2);
                            if ($sd['session_id'] != $agent->session) {
                                if (!isset($_SESSION['ID'])) {
                                    dbquery("DELETE FROM web_sessions WHERE session_user_id = '$agent->master_id';");
                                    $agent->insert_session();
                                } else {
                                    session_destroy();
                                    $url = $_SERVER["REQUEST_URI"];
                                    exit(header("Location: $url"));
                                }
                            } else {
                                $now = time();
                                dbquery("UPDATE web_sessions SET
                                        last_visit = $now,
                                        current_page = '$agent->current_page',
                                        browser = '$agent->browser'
                                        WHERE session_id = '$agent->session';");
                            }
                        }
                    }
                }
            } else {
                echo "ERRRORRRRRr";
                session_destroy();
            }
        } else {
            $q3 = dbquery("SELECT session_id FROM web_sessions WHERE ip_address = '$agent->ip_address';");
            if (mysql_num_rows($q3) == 0) {
                if (isset($_SESSION['ID'])) {
                    $agent->session = random_string(16);
                }
                $agent->insert_session();
            } else {
                if (mysql_num_rows($q3) > 1) {
                    dbquery("DELETE FROM web_sessions WHERE ip_address = '$agent->ip_address';");
                    $agent->insert_session();
                } else {
                    $sd2 = mysql_fetch_array($q3);
                    if ($sd2['session_id'] != $agent->session) {
                        dbquery("DELETE FROM web_sessions WHERE ip_address = '$agent->ip_address';");
                        $agent->insert_session();
                    } else {
                        $now = time();
                        dbquery("UPDATE web_sessions SET
                                last_visit = $now,
                                current_page = '$agent->current_page',
                                browser = '$agent->browser'
                                WHERE session_id = '$agent->session';");
                    }
                }
            }
        }
    }

    /**
     * Validates a given agent's username & password;
     * @return boolean TRUE if sucessfully validated, FALSE otherwise.
     */
    public static function validate($email, $hash)
    {
        $q = dbquery(sprintf("SELECT NULL FROM characters WHERE email = '%s' AND password = '%s' LIMIT 1", filter_for_input($email), $hash));

        if (mysql_num_rows($q) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the account has a ban.
     * @param $master_id
     * @return bool
     * @internal param agent $agent A reference to an agent class.
     */
    public static function has_offense($master_id)
    {
        if ($master_id == 0) {
            return false;
        }

        $q = dbquery("SELECT id,expire_date FROM offences WHERE character_id = '$master_id' AND expired = '0' ORDER BY id DESC;");
        if (mysql_num_rows($q) > 0) {
            $data = mysql_fetch_assoc($q);
            if (time() > strtotime($data['expire_date'])) {
                return false;
                dbquery("UPDATE offences SET expired = '1' WHERE id = '" . $data['id'] . "';");
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Inserts session information into database.
     */
    public function insert_session()
    {
        dbquery("INSERT INTO web_sessions  VALUES('$this->session',
            '$this->master_id', '$this->session_start_time',
            '$this->session_start_time', '$this->ip_address', '$this->current_page', '$this->browser');");
        $_SESSION['ID'] = $this->session;
    }

    /**
     * Deletes expired sessoins.
     */
    public static function delete_expired_sessions()
    {
        $now = time();
        dbquery("DELETE FROM web_sessions WHERE last_visit < ($now - 1800);");
    }

    public static function email_exists($email)
    {
        $q = dbquery(sprintf("SELECT NULL FROM characters WHERE email = '%s' LIMIT 1", filter_for_input($email)));

        if (mysql_num_rows($q) > 0) {
            return true;
        }
        return false;
    }

    public static function user_exists($name)
    {
        $q = dbquery(sprintf("SELECT NULL FROM characters WHERE username = '%s' LIMIT 1", filter_for_input($name)));

        if (mysql_num_rows($q) > 0) {
            return true;
        }
        return false;
    }

    public static function check_email_and_user($email, $user)
    {
        $q = dbquery(sprintf("SELECT NULL FROM characters WHERE email = '%s' AND username = '%s' LIMIT 1;", filter_for_input($email), filter_for_input($user)));

        if (mysql_num_rows($q) > 0) {
            return true;
        }
        return false;
    }

    public static function username_from_email($email)
    {
        return agent::single_cdata("username", "email = '$email'");
    }

    public static function single_cdata($column, $condition)
    {
        $q = dbquery("SELECT $column FROM characters WHERE $condition LIMIT 1;");
        if (mysql_num_rows($q) > 0) {
            $row = mysql_fetch_assoc($q);
            return $row["$column"];
        }
    }

    public static function id_from_email($email)
    {
        return agent::single_cdata("id", "email = '$email'");
    }

    public static function id_from_name($name)
    {
        return agent::single_cdata("id", "username = '$name'");
    }

    public static function username_from_id($id)
    {
        if (is_numeric($id) && $id > 0) {
            return agent::single_cdata("username", "id = '$id'");
        }
        return false;
    }

    /**
     * Wether the user has this permission.
     * @param string $permission
     */
    public function has_permission($permission)
    {
        $or_pos = strpos($permission, "||");
        $and_pos = strpos($permission, "&&");

        if ($or_pos !== false) {
            $psa = explode("||", $permission);
            $has = false;
            foreach ($psa as $value) {
                $value = trim($value);
                if (array_key_exists($value, $this->permissions)) {
                    $has = true;
                }
            }
            return $has;
        } else if ($and_pos !== false) {
            $psa = explode("&&", $permission);
            foreach ($psa as $value) {
                $value = trim($value);
                if (!array_key_exists($value, $this->permissions)) {
                    return false;
                }
            }
            return true;
        } else {
            if (array_key_exists($permission, $this->permissions)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Summerizes the agent class as a string.
     */
    public function __toString()
    {
        return sprintf("agent[session=%s,master_id=%d,email=%s,username=%s]", $this->session);
    }

}

?>
