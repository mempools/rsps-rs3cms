<?php
date_default_timezone_set('America/Los_Angeles');
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Specify an character's rank.
 */
function get_rank($agent)
{
    if ($agent->has_permission("web_admin || client_admin")) {
        return "Administrator";
    } else if ($agent->has_permission("web_mod || client_mod")) {
        return "Moderator";
    } else {
        return "Player";
    }
}

/**
 * Generates a random string.
 * @param int $length number of characters.
 * @return string A string cotnaining random characters.
 */
function random_string($length)
{
    $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $string = "";
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $string;
}

/**
 * Filters the given input by stripping slashes, trimming
 * eccess bits, and adding mysql escapes, to the string, thus
 * now making it safe enough to input into a mysql database.
 * @param string $string The string to filter.
 * @return string The string suitable to be stored into a mysql database.
 */
function filter_for_input($string)
{
    return mysql_real_escape_string(stripslashes(trim($string)));
}

/**
 * Filters the given input by stripping slashs, trimming
 * eccess bits and setting specified conditions.
 * @param string $string The string to filer.
 * @param bool $ignore_html Whether to ignore html tagging.
 * @param bool $break_lines Whether to insert breaklines after every html newline.
 * @return string The string suiable to be displayed.
 */
function filter_for_outout($string, $ignore_html = false, $break_lines = false)
{
    $output = stripslashes(trim($string));

    if ($ignore_html) {
        $output = htmlentities($output);
    }

    if ($break_lines) {
        $output = nl2br($output);
    }

    return $output;
}

/**
 * Formats a given size.
 * @param int $data The data to format.
 * @return string The formatted size.
 */
function formatsize($data)
{
    return number_format(round(($data / 1024), 2)) . ' KiB';
}

/**
 * Generates a visually pretty date with a specified timestamp.
 * @param int $timestamp The timestamp.
 * @return string A pretty date.
 */
function prettydate($timestamp)
{
    return date("F jS, Y", $timestamp);
}

/**
 * Generates a simplized date with the specified timestamp.
 * @param int $timestamp The timestamp.
 * @return string A simple date.
 */
function simpledate($timestamp)
{
    return date('m-d-y', $timestamp);
}

/**
 * Formats a timestamp to MySQL datetime format.
 * @param int $timestamp The time to format.
 * @return string The formatted time.
 */
function dbdate($timestamp)
{
    return date("Y-m-d H:i:s", $timestamp);
}

/**
 * Formats a DateTime object to MySQL datetime format.
 * @param DateTime $date The time to format
 * @return string The formatted time.
 */
function dbdate_format($date)
{
    return date_format($date, "Y-m-d H:i:s");
}

/**
 * Check if the email is a valid entry.
 * @param string $email Email to check.
 * @return boolean Whether the entry is valid or not.
 */
function check_email($email)
{
    if ($email == "" && !preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email)) {
        return false;
    } else {
        return true;
    }
}

/**
 * Get the type of ban.
 * @param <type> $type Data to decide type for.
 * @return <type> Returns Mute, Ban, or N/A.
 */
function ban_type($type)
{
    if ($type == 1) {
        return "Mute";
    } else if ($type == 2) {
        return "Ban";
    } else {
        return "N/A";
    }
}

/**
 * Gets the status of a character.
 * @param int $status The status value.
 * @return string The status as a user-readable phrase.
 */
function online_status($status)
{
    if (is_numeric($status)) {
        if ($status == 0) {
            return "Offline";
        } else if ($status > 0) {
            return "World " . $status;
        } else if ($status < 0) {
            return "Website";
        }
    }
    return "Offline";
}

function login_type($type)
{
    if (is_numeric($type)) {
        if ($type == -1) {
            return "Website";
        } else if ($type == 0) {
            return "Lobby";
        } else if ($type > 0) {
            return "World " . $type;
        }
    }
    return "Unknown";
}

/**
 * Compares two dates and returns the difference between them.
 * @param int $data1 Unix timestamp.
 * @param int $date2 Unix timestamp.
 * @return string Comparison between the two dates.
 */
function compare_dates($date1, $date2)
{
    $blocks = array(
        array('name' => 'year', 'amount' => 60 * 60 * 24 * 365),
        array('name' => 'month', 'amount' => 60 * 60 * 24 * 31),
        array('name' => 'week', 'amount' => 60 * 60 * 24 * 7),
        array('name' => 'day', 'amount' => 60 * 60 * 24),
        array('name' => 'hour', 'amount' => 60 * 60),
        array('name' => 'minute', 'amount' => 60),
        array('name' => 'second', 'amount' => 1)
    );

    $diff = abs($date1 - $date2);

    $levels = 2;
    $current_level = 1;
    $result = array();
    foreach ($blocks as $block) {
        if ($current_level > $levels) {
            break;
        }
        if ($diff / $block['amount'] >= 1) {
            $amount = floor($diff / $block['amount']);
            if ($amount > 1) {
                $plural = 's';
            } else {
                $plural = '';
            }
            $result[] = $amount . ' ' . $block['name'] . $plural;
            $diff -= $amount * $block['amount'];
            $current_level++;
        }
    }
    return implode(' ', $result) . ' ago';
}

/**
 * Gets the category name for the given category id.
 * @param int $category_id The category ID.
 * @return string The category name.
 */
function news_category($category_id)
{
    switch ($category_id) {
        case 1:
            return "Game Updates";
            break;
        case 2:
            return "Website";
            break;
        case 3:
            return "Customer Support";
            break;
        case 4:
            return "Technical";
            break;
        default:
            return "Uncategorized";
            break;
    }
}

function seo_url($string)
{
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}

function startsWith($haystack, $needle)
{
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function endsWith($haystack, $needle)
{
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
}

?>
