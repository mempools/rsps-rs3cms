<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General License for more details.
 * 
 *     You should have received a copy of the GNU General License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("class_runescape_skill.php");

/**
 * Used for sorting skills information.
 *
 * @author AJ
 */
class
character_statistics
{
    /* skill ids */
    const attack = 0;
    const defence = 1;
    const strength = 2;
    const constitution = 3;
    const ranged = 4;
    const prayer = 5;
    const magic = 6;
    const cooking = 7;
    const woodcutting = 8;
    const fletching = 9;
    const fishing = 10;
    const firemaking = 11;
    const crafting = 12;
    const smithing = 13;
    const mining = 14;
    const herblore = 15;
    const agility = 16;
    const thieving = 17;
    const slayer = 18;
    const farming = 19;
    const runecrafting = 20;
    const construction = 21;
    const hunter = 22;
    const summoning = 23;
    const dungeoneering = 24;

    /* internal data */
    private $_skills;
    private $_total_levels;
    private $_total_exp;

    /* Constructs the character statistics class. */
    public function __construct($skills_data)
    {
        $sid = 0;
        $idx = 1;
        for ($i = 0; $i < 25; $i++) {
            $tmp_lvl = $skills_data[$idx++];
            $tmp_exp = $skills_data[$idx++];

            // get level for experience.
            $tmp_lvl = $this->level_for_experience($i, $tmp_exp);

            $this->_skills[$i] = new runescape_skill($i, $tmp_lvl, $tmp_exp);
            $this->_total_levels += $this->_skills[$i]->level;
            $this->_total_exp += $this->_skills[$i]->experience;
        }
    }

    /* property getters */

    public static function level_for_experience($skill_id, $exp)
    {
        $points = 0;
        $output = 0;
        $max = ($skill_id == character_statistics::dungeoneering ? 120 : 99);

        for ($lvl = 1; $lvl < ($max + 1); $lvl++) {
            $points += floor((double)$lvl + 300.0 * pow(2.0, (double)$lvl / 7.0));
            $output = (int)floor($points / 4);
            if (($output - 1) >= $exp) {
                return $lvl;
            }
        }
        return $max;
    }

    public static function get_skill_name($skill_id)
    {
        switch ($skill_id) {
            case character_statistics::attack:
                $skill_name = "Attack";
                break;
            case character_statistics::defence:
                $skill_name = "Defence";
                break;
            case character_statistics::strength:
                $skill_name = "Strength";
                break;
            case character_statistics::constitution:
                $skill_name = "Constitution";
                break;
            case character_statistics::ranged:
                $skill_name = "Ranged";
                break;
            case character_statistics::prayer:
                $skill_name = "Prayer";
                break;
            case character_statistics::magic:
                $skill_name = "Magic";
                break;
            case character_statistics::cooking:
                $skill_name = "Cooking";
                break;
            case character_statistics::woodcutting:
                $skill_name = "Woodcutting";
                break;
            case character_statistics::fletching:
                $skill_name = "Fletching";
                break;
            case character_statistics::fishing:
                $skill_name = "Fishing";
                break;
            case character_statistics::firemaking:
                $skill_name = "Firemaking";
                break;
            case character_statistics::crafting:
                $skill_name = "Crafting";
                break;
            case character_statistics::smithing:
                $skill_name = "Smithing";
                break;
            case character_statistics::mining:
                $skill_name = "Mining";
                break;
            case character_statistics::herblore:
                $skill_name = "Herblore";
                break;
            case character_statistics::agility:
                $skill_name = "Agility";
                break;
            case character_statistics::thieving:
                $skill_name = "Thieving";
                break;
            case character_statistics::slayer:
                $skill_name = "Slayer";
                break;
            case character_statistics::farming:
                $skill_name = "Farming";
                break;
            case character_statistics::runecrafting:
                $skill_name = "Runecrafting";
                break;
            case character_statistics::construction:
                $skill_name = "Construction";
                break;
            case character_statistics::hunter:
                $skill_name = "Hunter";
                break;
            case character_statistics::summoning:
                $skill_name = "Summoning";
                break;
            case character_statistics::dungeoneering:
                $skill_name = "Dungeoneering";
                break;
            default:
                $skill_name = "Total";
                break;
        }
        return $skill_name;
    }

    /* Arranges the skills from best to worst. */

    public function get_total_levels()
    {
        return $this->_total_levels;
    }

    /* Arranges the skills from best to worst. */

    public function get_total_exp()
    {
        return $this->_total_exp;
    }

    /* Gets gathering skills from best to worst */

    public function get_top_skills()
    {
        return $this->arrange($this->_skills);
    }

    /* Gets combat skills from best to worst */

    private function arrange($skills_data)
    {
        $tmp_sdata = $skills_data;
        $size = count($tmp_sdata);
        for ($i = 0; $i < $size; $i++) {
            for ($j = 0; $j < $size - 1 - $i; $j++) {
                if ($tmp_sdata[$j + 1]->level > $tmp_sdata[$j]->level) {
                    $tmp = $tmp_sdata[$j];
                    $tmp_sdata[$j] = $tmp_sdata[$j + 1];
                    $tmp_sdata[$j + 1] = $tmp;
                }
            }
        }
        return $tmp_sdata;
    }

    /* Gets support skills from best to worst */

    public function get_gathering_skills()
    {
        return $this->arrange(
            array(
                $this->_skills[character_statistics::farming],
                $this->_skills[character_statistics::fishing],
                $this->_skills[character_statistics::hunter],
                $this->_skills[character_statistics::mining],
                $this->_skills[character_statistics::woodcutting]
            )
        );
    }

    /* Gets artisan skills from best to worst */

    public function get_combat_skills()
    {
        return $this->arrange(
            array(
                $this->_skills[character_statistics::attack],
                $this->_skills[character_statistics::constitution],
                $this->_skills[character_statistics::defence],
                $this->_skills[character_statistics::magic],
                $this->_skills[character_statistics::prayer],
                $this->_skills[character_statistics::ranged],
                $this->_skills[character_statistics::strength],
                $this->_skills[character_statistics::summoning]
            )
        );
    }

    /* Gets member skills from best to worst */

    public function get_support_skills()
    {
        return $this->arrange(
            array(
                $this->_skills[character_statistics::agility],
                $this->_skills[character_statistics::dungeoneering],
                $this->_skills[character_statistics::slayer],
                $this->_skills[character_statistics::thieving]
            )
        );
    }

    /* Get level of specified skill */

    public function get_artisan_skills()
    {
        return $this->arrange(
            array(
                $this->_skills[character_statistics::construction],
                $this->_skills[character_statistics::cooking],
                $this->_skills[character_statistics::crafting],
                $this->_skills[character_statistics::firemaking],
                $this->_skills[character_statistics::fletching],
                $this->_skills[character_statistics::herblore],
                $this->_skills[character_statistics::runecrafting],
                $this->_skills[character_statistics::smithing]
            )
        );
    }

    /* Get experience of specified skill */

    public function get_member_skills()
    {
        return $this->arrange(
            array(
                $this->_skills[character_statistics::agility],
                $this->_skills[character_statistics::construction],
                $this->_skills[character_statistics::farming],
                $this->_skills[character_statistics::fletching],
                $this->_skills[character_statistics::herblore],
                $this->_skills[character_statistics::hunter],
                $this->_skills[character_statistics::slayer],
                $this->_skills[character_statistics::summoning],
                $this->_skills[character_statistics::thieving]
            )
        );
    }

    /* Get level for experience given. */

    public function get_skill_level($skill_id)
    {
        return $this->_skills[$skill_id]->level;
    }

    public function get_skill_exp($skill_id)
    {
        return $this->_skills[$skill_id]->experience;
    }

    /* Gets skill name of specified skill */

    function get_rank($user, $skill)
    {
        $res = dbquery("SELECT * FROM characters_statistics ORDER BY " . strtolower($skill) . "_xp DESC LIMIT 200");
        $count = 1;
        if ($res->num_rows == 0) {
            return -1;
        }
        while ($row = $res->fetch_assoc()) {
            if (strtolower($row['username']) == strtolower($user)) {
                return $count;
            }
            $count++;
        }
        return $count;
    }
}

function get_rank_name($playerRights)
{
    switch ($playerRights) {
        case 1:
            return "Mod";
        case 2:
            return "Admin";
        case 3:
            return "Donator";
        case 4:
            return "Super Donator";
    }
    return "Player";
}
