<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Gets a config.
 * @global array $config Array of configs and their values.
 * @param string $key The key of the config.
 * @return string The value of the config.
 */
function get_config($key)
{
    global $config;
    return $config[$key];
}

/**
 * Gets all the configs in the database, and returns an array.
 * @return array Array of configs and their values.
 */
function get_configs()
{
    $cfg_array = array();
    $configs = dbquery("SELECT * FROM configurations;");

    if (mysql_num_rows($configs) > 0) {
        while ($c = mysql_fetch_assoc($configs)) {
            $cfg_array[$c['name']] = $c['value'];
        }
    }
    return $cfg_array;
}

/**
 * Sets a specified config.
 * @param string $key The key of the config.
 * @param string $value The value of the config.
 */
function set_config($key, $value)
{
    if ($key == "" || $value == "") {
        echo $key . "<br>" . $value;
        system_error("Config", "Invalid key or value.");
    }

    $cfg = dbquery(sprintf("SELECT * FROM configurations WHERE name='%s';", $key));
    if (mysql_num_rows($cfg) > 0) {
        dbquery(sprintf("UPDATE configurations SET value = '%s' WHERE name='%s';", $value, $key));
    } else {
        dbquery(sprintf("INSERT INTO configurations VALUES('%s','%s');", $key, $value));
    }
}