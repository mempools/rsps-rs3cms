<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represents data for a runescape skill.
 *
 * @author AJ
 */
class runescape_skill
{
    /* skill data */
    public $id = 0;
    public $level = 0;
    public $experience = 0;

    /**
     * Constructs the skill data.
     * @param int $level The level of the skill.
     * @param double $experience The experience points of the skill.
     */
    public function __construct($id, $level, $experience)
    {
        $this->id = $id;
        $this->level = $level;
        $this->experience = $experience;
    }
}

?>
