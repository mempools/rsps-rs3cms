<?php

function get_recaptcha_response($post)
{
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $secret_key = RECAPTCHA_SECRET;
    $response = file_get_contents($url . "?secret=" . $secret_key . "&response=" . $post . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
    return json_decode($response);
}