<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Sends information to the given remote server.
 * @param string $data Contains command information.
 * @param string $ip The IP address connect to.
 * @param string $port The port to connect to.
 * @return bool|string
 */
function rmd_send($data, $ip, $port, $timeout = 1)
{
    $connection = @fsockopen($ip, $port, $fserrno, $fserrstr, $timeout);

    if ($connection) {
        fwrite($connection, $data);
        $rd = fread($connection, 256);
        fclose($connection);
        return $rd;
    } else {
        return false;
    }
}
