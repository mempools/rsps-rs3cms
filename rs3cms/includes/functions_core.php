<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Handles the core session of users.
 */
function handle_session()
{

}

/**
 * Alerts of a system error.
 * @param string $title The title of the error type.
 * @param string $data The error data.
 */
function system_error($title, $data)
{
    echo '<div style="width: 80%; padding: 15px 15px 15px 15px; margin: 50px auto; background-color: #F6CECE; font-family: arial; font-size: 12px; color: #000000; border: 1px solid #FF0000;">';
    echo '<b>' . $title . '</b><br />';
    echo $data;
    echo '<hr size="1" style="width: 100%; margin: 15px 0px 15px 0px;" />';
    echo 'Script execution was aborted. We apoligize for the possible inconvenience. If this problem is persistant, please contact an Administrator.';
    echo '</div><center style="font-family: arial; font-size: 10px;">Powered by <a href="http://ajravindiran.com/jolt/">Jolt Environment</a>.</center>';
    exit;
}
