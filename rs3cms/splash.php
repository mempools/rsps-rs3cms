<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <link href="css/fonts-50.css" rel="stylesheet"/>
    <link type="text/css" rel="stylesheet" href="css/jquery/jquery_jscrollpane_css_2_0_0b11-0.css"/>
    <link href="css/global-51.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="css/shadowbox/shadowbox-0.css"/>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script
        type="text/javascript">window.jQuery || document.write("<script src='js/jquery/jquery_1_7.js'>\x3C/script>")</script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
    <script>window.jQuery.ui || document.write("<script src='js/jquery/jquery_ui_1_8_6.js'>\x3C/script>")</script>
    <script type="text/javascript" src="js/modernizr_1_7_min.js"></script>
    <script type="text/javascript" src="js/jagex/jagex_cookie-0.js"></script>
    <script type="text/javascript" src="js/jquery/jquery_placeholder_1_2.js"></script>
    <script type="text/javascript" src="js/jquery/jquery_mousewheel_3_0_6.js"></script>
    <script>window.JSON || document.write("<script src='js/json2.js'>\x3C/script>")</script>
    <script type="text/javascript" src="js/jquery/jquery_jscrollpane_min_2_0_0b11-0.js"></script>
    <script type="text/javascript" src="js/shadowbox/shadowbox-0.js"></script>
    <script src="js/global-50.js"></script>
    <title>RuneScape - MMORPG - The No.1 Free Online Multiplayer Game</title>
    <meta name="keywords"
          content="MMORPG, free RPG games online, online multiplayer, role-playing, massively multiplayer games, mmo, RuneScape, Jagex, java"/>
    <meta name="description"
          content="Play RuneScape for free, and join a global community of millions as you complete quests and win enormous treasures in a 3D world full of magic and monsters."/>

    <link rel="icon" type="image/vnd.microsoft.icon" href="http://www.runescape.com/img/global/favicon.ico"/>
    <link rel="SHORTCUT ICON" href="http://www.runescape.com/img/global/favicon.ico"/>
    <link rel="apple-touch-icon" href="http://www.runescape.com/img/global/mobile.png">
    <meta property="fb:app_id" content="174823375888281"/>
    <meta property="og:image" content="http://www.runescape.com/img/global/facebook.png"/>

    <meta property="og:title" content="RuneScape - MMORPG - The No.1 Free Online Multiplayer Game"/>
    <meta property="og:type" content="game"/>
    <meta property="og:site_name" content="RuneScape"/>
    <meta property="og:description"
          content="Play RuneScape for free, and join a global community of millions as you complete quests and win enormous treasures in a 3D world full of magic and monsters."/>
    <link rel="stylesheet" type="text/css" href="http://www.runescape.com/g=runescape/css/splash-51.css">
    <link rel="stylesheet" type="text/css" href="http://www.jagex.com/css/shadowbox/shadowbox-0.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
    <script>window.swfobject || document.write("<script src='http://www.jagex.com/js/swfobject/swfobject_2_2.js'>\x3C/script>")</script>
    <script type="text/javascript" src="http://www.jagex.com/js/shadowbox/shadowbox-0.js"></script>
    <script src='http://www.runescape.com/js/splash-50.js'></script>
    <!--[if lte IE 6]>
    <meta http-equiv="refresh" content="0;url=http://www.runescape.com/g=runescape/unsupported_browser.ws"><![endif]-->
    <script type="text/javascript" src="http://www.jagex.com/js/jquery/jquery_thickbox_3.js"></script>
    <link type="text/css" rel="stylesheet" href="http://www.jagex.com/css/jquery/jquery_thickbox_3-0.css"/>
</head>
<body>
<script type="text/javascript">
    if ($("#fb-root").length == 0) {
        $("body").prepend('<div id="fb-root"></div>');
        window.fbAsyncInit = function () {
            FB.init({
                appId: '174823375888281',
                status: false,
                cookie: true,
                xfbml: true,
                oauth: true
            });
            if ($.browser.msie) {
                FB.UIServer.setLoadedNode = function (a, b) {
                    FB.UIServer._loadedNodes[a.id] = b;
                }
            }
        };
        (function () {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());
    }
</script>
<a style="display:none" id="SNbuttonLink" class="thickbox"></a>
<script type="text/javascript">
    function fbLoginRedirect(buttonName, loginUrl, destUrl, destStr) {
        $.getJSON('http://services.runescape.com/m=sn-integration/g=runescape/checkLogin.ws?json=?', {
            "tps": 0,
            "token": FB.getAuthResponse().accessToken,
            "expiry": FB.getAuthResponse().expiresIn,
            "signed": FB.getAuthResponse().signedRequest
        }, function (data) {
            if (data['sso'] < 0) {


                JAGEX.socialnetworks.SNNotification("There was a problem verifying your login. Please try again.");
            } else if (data['sso'].length > 1) {

                if (data['sna'] > -1) {


                    $("#SNbuttonLink").attr('href', loginUrl + 'key=' + data['sso'] + '&tps=0&' + destStr + '&KeepThis=true&TB_iframe=true&height=500&width=560');

                    $("#SNbuttonLink").click();
                    $("#TB_iframeContent").attr("allowTransparency", "true");

                    $("#TB_iframeContent").css("padding-left", "50px");

                } else {

                    window.location = destUrl + '?key=' + data['sso'] + '?' + destStr;
                }
            } else {


                $("#SNbuttonLink").attr('href', loginUrl + '?' + destStr + '&KeepThis=true&TB_iframe=true&height=400&width=442');

                $("#SNbuttonLink").click();
                $("#TB_iframeContent").attr("allowTransparency", "true");
            }
        });
        return false;
    }
</script>
<div id="topBarWrapper">
    <div id="topBar">
        <div id="registerLoginTab"></div>
        <div id="jagexLogo">
            <a href="http://www.jagex.com/g=runescape/" target="_blank"><img
                    src="http://www.runescape.com/img/main/splash/jagexLogo.png" alt="Jagex" title=""></a>
        </div>
        <ul id="topBarNav">
            <li><a href="http://services.runescape.com/m=rswiki/en/Customer_Support">Support</a></li>
            <li><a href="http://www.runescape.com/g=runescape/game.ws?autocreate=true" class="toggleMenu GameLink">Login
                    <span>or</span> Register</a></li>
        </ul>
        <div id="topBarRight">
            <div id="playerCount">
                <div><span>83,850</span> players online.</div>
                <a href="http://www.runescape.com/g=runescape/game.ws" class="HoverImg GameLink"><img
                        src="http://www.runescape.com/img/main/splash/topPlayNow.png" alt="Play Now" title=""></a>
            </div>

            <div class="LanguageButtonSurround">
                <div class="LanguageButton"><img src="http://www.runescape.com/img/global/language_selector/en.png"
                                                 title="en" alt="en" class="SelectedLang"><img
                        src="http://www.runescape.com/img/global/language_selector/LanguageButtonFg.png"
                        class="foreground"/>

                    <div class="LanguagePanel">
                        <div class="bottom">
                            <ul>
                                <li class="flag"><a href="http://www.runescape.com/g=runescape/splash.ws?set_lang=0"
                                                    title="English"><img
                                            src="http://www.runescape.com/img/global/language_selector/en.png"
                                            alt="English" class="flag"/><img
                                            src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                            class="LanguageArrow"></a></li>
                                <li class="flag"><a href="http://www.runescape.com/l=1/g=runescape/splash.ws?set_lang=1"
                                                    title="Deutsch"><img
                                            src="http://www.runescape.com/img/global/language_selector/de.png"
                                            alt="Deutsch" class="flag"/><img
                                            src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                            class="LanguageArrow"></a></li>
                                <li class="flag"><a href="http://www.runescape.com/l=2/g=runescape/splash.ws?set_lang=2"
                                                    title="Français"><img
                                            src="http://www.runescape.com/img/global/language_selector/fr.png"
                                            alt="Français" class="flag"/><img
                                            src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                            class="LanguageArrow"></a></li>
                                <li class="flag"><a href="http://www.runescape.com/l=3/g=runescape/splash.ws?set_lang=3"
                                                    title="Português (BR)"><img
                                            src="http://www.runescape.com/img/global/language_selector/pt.png"
                                            alt="Português (BR)" class="flag"/><img
                                            src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                            class="LanguageArrow"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="runescapeLogoTop"></div>
        <div id="registrationLoginWrapper">
            <form id="registrationForm" action="http://www.runescape.com/g=runescape/game.ws?1">
                <input type="hidden" name="autocreate" value="true">
<span class="inputWrapper" id="emailInputWrapper">
<input type="text" class="email" tabindex="1" value="" name="createEmail" id="email"
       placeholder="Enter your email address:">
<span class="Input_right"></span>
<label for="email" class="Kingthings">Register:</label>
</span>

                <div id="submitEmailWrapper" class="HoverImg">
                    <button tabindex="2" value="register" name="submit" type="submit" id="submitEmail">
                        <img alt="Register" src="http://www.runescape.com/img/main/splash/registerButton.jpg">
                    </button>
                </div>
            </form>
            <form id="loginForm" action="https://secure.runescape.com/m=weblogin/g=runescape/login.ws" method="post"
                  autocomplete="off">
                <div id="registerLoginTabBottom"></div>
<span class="inputWrapper" id="usernameWrapper">
<input type="text" class="username" tabindex="3" value="" name="username" id="username"
       placeholder="Username/Email Address">
<span class="Input_right"></span>
<label for="username" class="Kingthings">Login:</label>
</span>
<span class="inputWrapper" id="passwordWrapper">
<input type="password" class="password" maxlength="20" tabindex="4" value="" name="password" id="password"
       placeholder="Password">
<span class="Input_right"></span>
</span>

                <div id="loginWrapper" class="HoverImg">
                    <button tabindex="5" value="login" name="submit" type="submit" id="login">
                        <img alt="Login" src="http://www.runescape.com/img/main/splash/loginButton.jpg">
                    </button>
                </div>
                <a href="/forgot_password"
                   id="passwordInfo">Forgot Password?</a>
                <a href="https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?" id="fbLogin">Login
                    with <b>Facebook</b></a>
                <script type="text/javascript">
                    $("#fbLogin").live('click', function (ev) {
                        ev.preventDefault();
                        if (typeof(FB) == 'undefined' || typeof(FB.getSession) != 'function') {
                            /*

                             JAGEX.socialnetworks.SNNotification("Couldn't contact Facebook. Please try again later.");
                             */
                        }
                        else if (FB.getAuthResponse() == null) {


                            FB.login(function (response) {
                                    if (response.authResponse) {
                                        // Logged in
                                        fbLoginRedirect("fbLogin", "https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?", "http://www.runescape.com/g=runescape/game.ws", "");
                                    } else {
                                        // No session notification

                                        JAGEX.socialnetworks.SNNotification("Authentication with Facebook failed. Please try again.");
                                    }
                                }

                                , {scope: ''});

                        } else {
                            // Already has all settings
                            fbLoginRedirect("fbLogin", "https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?", "http://www.runescape.com/g=runescape/game.ws", "");
                        }
                    });
                </script>

                <input type="hidden" value="www" name="mod">
                <input type="hidden" value="0" name="ssl">
                <input type="hidden" value="game.ws?j=1" name="dest">
            </form>
        </div>
    </div>
</div>
<div id="headerWrapper">
    <div id="headerB">
        <a class="logoLink" href="http://www.runescape.com/g=runescape/title.ws">RuneScape</a>
        <nav>
            <ul class="MainMenu ">
                <li class="navGameInfo">
                    <a class="HoverImg" href="http://www.runescape.com/g=runescape/gameguide.ws">
                        <img src="http://www.runescape.com/img/main/splash/nav/GameInfo.jpg" alt="Game Info" title=""/>
                    </a>
                </li>
                <li class="navGameMedia CSS3">
                    <a class="HoverImg" href="http://www.runescape.com/g=runescape/media.ws">
                        <img src="http://www.runescape.com/img/main/splash/nav/GameMedia.jpg" alt="Game Media"
                             title=""/>
                    </a>

                    <div class="Mask"></div>
                    <div class="DropDown Kingthings">
                        <div>
                            <ul>
                                <li><a href="http://www.runescape.com/g=runescape/media_videos.ws">Videos</a></li>
                                <li><a href="http://www.runescape.com/g=runescape/media_screenshots.ws">Screenshots</a>
                                </li>
                                <li><a href="http://www.runescape.com/g=runescape/media_wallpaper.ws">Wallpaper</a></li>
                                <li><a href="http://www.runescape.com/g=runescape/media_conceptArt.ws">Concept Art</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="Community CSS3">
                    <a class="HoverImg" href="http://www.runescape.com/g=runescape/title.ws">
                        <img src="http://www.runescape.com/img/main/splash/nav/Community.jpg" alt="Community" title=""/>
                    </a>

                    <div class="Mask"></div>
                    <div class="DropDown Kingthings">
                        <div>
                            <ul>
                                <li><a href="http://services.runescape.com/m=forum/g=runescape/forums.ws">Forums</a>
                                </li>
                                <li><a href="http://services.runescape.com/m=clan-home/g=runescape/">Clans</a></li>
                                <li><a href="http://services.runescape.com/m=hiscore/g=runescape/heroes.ws">Hiscores</a>
                                </li>
                                <li><a href="http://services.runescape.com/m=itemdb_rs/g=runescape/frontpage.ws">Grand
                                        Exchange</a></li>
                                <li><a href="http://services.runescape.com/m=adventurers-log/g=runescape/">Adventurer's
                                        Log</a></li>
                                <li><a href="http://services.runescape.com/m=loyalty-points/g=runescape/">Loyalty
                                        Programme</a></li>
                                <li><a href="http://services.runescape.com/m=friend-referral/g=runescape/">Refer a
                                        Friend</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="navMembership">
                    <a class="HoverImg" href="http://www.runescape.com/g=runescape/members_benefits.ws">
                        <img src="http://www.runescape.com/img/main/splash/nav/Membership.jpg" alt="Membership"
                             title=""/>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<div id="PlayNowWrapper">
    <a id="PlayNowWrapperPlayFree" href="http://www.runescape.com/g=runescape/game.ws" class="HoverImg GameLink">
        <img src="http://www.runescape.com/img/global/header/banner/MainCTAPlayNow.png" title="" alt="Play Now Free">
    </a>

    <div id="SN_links" style="display: none">
        <a id="PlayNowWrapperPlayFB"
           href="https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?" class="HoverImg"><img
                src="http://www.runescape.com/img/global/header/banner/MainCTAPlayNowFB.png?0"
                alt="Play Now With Facebook"/></a>
        <script type="text/javascript">
            $("#PlayNowWrapperPlayFB").live('click', function (ev) {
                ev.preventDefault();
                if (typeof(FB) == 'undefined' || typeof(FB.getSession) != 'function') {
                    /*

                     JAGEX.socialnetworks.SNNotification("Couldn't contact Facebook. Please try again later.");
                     */
                }
                else if (FB.getAuthResponse() == null) {


                    FB.login(function (response) {
                            if (response.authResponse) {
                                // Logged in
                                fbLoginRedirect("PlayNowWrapperPlayFB", "https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?", "http://www.runescape.com/g=runescape/game.ws", "");
                            } else {
                                // No session notification

                                JAGEX.socialnetworks.SNNotification("Authentication with Facebook failed. Please try again.");
                            }
                        }

                        , {scope: ''});

                } else {
                    // Already has all settings
                    fbLoginRedirect("PlayNowWrapperPlayFB", "https://secure.runescape.com/m=sn-integration/g=runescape/facebook/gamelogin.ws?", "http://www.runescape.com/g=runescape/game.ws", "");
                }
            });
        </script>

    </div>
    <script type="text/javascript">
        $("#SN_links").css("display", "block");
    </script>
</div>
<div id="contentWrapper">
    <div id="mediaWrapper">
        <div id="trailerWrapper">
            <img id="trailerImage" src="http://www.runescape.com/img/main/splash/trailerStaticImage.jpg" title=""
                 alt="RuneScape Trailer">

            <div id="trailerOverlay"></div>
            <a id="trailerWatch" href="http://www.runescape.com/g=runescape/vidPlayer.ws?video=2" class="HoverImg"
               rel="shadowbox;height=414;width=649;player=iframe">
                <img src="http://www.runescape.com/img/main/splash/watchNow.png" alt="Watch the Trailer" title="">
            </a>
        </div>
        <a id="mediaWrapperPrevious" class="HoverImg">
            <img src="http://www.runescape.com/img/main/splash/prev-button.png" alt="" title="">
        </a>

        <div id="PromoPoints">
            <a class="HoverImg"><img src="http://www.runescape.com/img/global/buttons/PointYellowWhite.png" title=""
                                     alt=""></a>
            <a class="HoverImg"><img src="http://www.runescape.com/img/global/buttons/PointYellowWhite.png" title=""
                                     alt=""></a>
            <a class="HoverImg"><img src="http://www.runescape.com/img/global/buttons/PointYellowWhite.png" title=""
                                     alt=""></a>
            <a class="HoverImg"><img src="http://www.runescape.com/img/global/buttons/PointYellowWhite.png" title=""
                                     alt=""></a>
            <a class="HoverImg"><img src="http://www.runescape.com/img/global/buttons/PointYellowWhite.png" title=""
                                     alt=""></a>
        </div>
        <a id="mediaWrapperNext" class="HoverImg">
            <img src="http://www.runescape.com/img/main/splash/next-button.png" alt="" title="">
        </a>
    </div>
    <div id="middleWrapper">

        <h3 class="Gradient" id="screenshotsTitle">
            <span class="G0">Screenshots</span>
            <span class="G1" aria-hidden="true">Screenshots</span>
            <span class="G2" aria-hidden="true">Screenshots</span>
            <span class="G3" aria-hidden="true">Screenshots</span>
            <span class="G4" aria-hidden="true">Screenshots</span>
            <span class="G5" aria-hidden="true">Screenshots</span>
            <span class="G6" aria-hidden="true">Screenshots</span>
<span class="mask"><span class="spacing">Screenshots</span>
<span class="middleUnderscore"><span class="spacing" aria-hidden="true">Screenshots</span></span>
</span>
<span class="rightUnderscore">
<img src="http://www.runescape.com/img/global/gradient_header/underscore_right.png" class="right" alt=""/><span
        class="spacing" aria-hidden="true">Screenshots</span>
</span>
            <span class="leftUnderscore"><img
                    src="http://www.runescape.com/img/global/gradient_header/underscore_flourish_left.png" class="left"
                    alt=""/></span>
        </h3>
        <a href="http://www.runescape.com/g=runescape/media.ws" class="Button Button29"
           id="screenshotsAllMedia"><span><span><span class=""><b>View All Media</b></span></span></span></a>

        <div id="leftMiddleOverlap"></div>
        <div id="prevScreen"><a class="HoverImg"><img src="http://www.runescape.com/img/main/splash/prev-button.png"
                                                      alt="Previous" title=""></a></div>
        <div id="screenshotWrapper" class="nojs">
            <div>
                <a href="http://www.runescape.com/img/main/splash/screenshots/1.jpg" rel="shadowbox;gallery=screenshots"
                   target="_blank"><span></span><img src="http://www.runescape.com/img/main/splash/screenshots/1s.jpg"
                                                     alt="" title=""></a>
                <a href="http://www.runescape.com/img/main/splash/screenshots/2.jpg" rel="shadowbox;gallery=screenshots"
                   target="_blank"><span></span><img src="http://www.runescape.com/img/main/splash/screenshots/2s.jpg"
                                                     alt="" title=""></a>
                <a href="http://www.runescape.com/img/main/splash/screenshots/3.jpg" rel="shadowbox;gallery=screenshots"
                   target="_blank"><span></span><img src="http://www.runescape.com/img/main/splash/screenshots/3s.jpg"
                                                     alt="" title=""></a>
            </div>
        </div>
        <div id="nextScreen"><a class="HoverImg"><img src="http://www.runescape.com/img/main/splash/next-button.png"
                                                      alt="Next" title=""></a></div>
        <div id="rightMiddleOverlap"></div>
    </div>
    <div id="bottomWrapper">
        <div id="welcome">
            <div>
                <h4 class="FlatHeader">Start Playing Now!</h4>

                <p>
                    <span class="FirstLetter">L</span>
                    ooking to slay evils that have long lurked in underground dungeons, sealed away by the gods for
                    centuries?
                </p>

                <p>
                    We dare you to enter the world of RuneScape! Join today and explore the epic content it has to
                    offer. Hone your skills by slaughtering demons, wizards and vampyres that rampage through diverse
                    areas.
                    Adventure with clans to bring down the toughest bosses, and battle against violent creatures that
                    are looking to quench their bloodlust.
                </p>

                <p>
                    Pick up your sword and sign up to the world's greatest online adventure!
                </p>
                <a id="welcomePlay" href="http://www.runescape.com/g=runescape/game.ws" class="HoverImg GameLink">
                    <img src="http://www.runescape.com/img/main/splash/welcomeStartPlayingNow.png" title=""
                         alt="Start Playing Now">
                </a>

                <div class="clearB"></div>
            </div>
        </div>
        <div id="membersBenefits">
            <h4 class="FlatHeader">Become a Member and Get All These Benefits Today!</h4>

            <div class="membersBenefitsTrailer"><img
                    src="http://www.runescape.com/img/main/splash/membersBenefitsStatic.jpg" alt="" title=""></div>

            <a href="http://www.runescape.com/g=runescape/vidPlayer.ws?video=4"
               class="HoverImg HoverGradient WoodenPanel" id="membersBenefitsWatchNow"
               rel=shadowbox;height=264;width=390;player=iframe><span class="Gradient_link2"><span>Watch the Members' Benefits Trailer</span><span
                        class="g1" aria-hidden="true">Watch the Members' Benefits Trailer</span><span class="g2"
                                                                                                      aria-hidden="true">Watch the Members' Benefits Trailer</span><span
                        class="g3" aria-hidden="true">Watch the Members' Benefits Trailer</span><span class="g4"
                                                                                                      aria-hidden="true">Watch the Members' Benefits Trailer</span><span
                        class="g5" aria-hidden="true">Watch the Members' Benefits Trailer</span></span><span
                    class="ArrowWrapper"><img src="http://www.runescape.com/img/global/buttons/navigationNext.png"
                                              alt="" title=""/></span><span class="PanelRight"></span></a>

            <ul>
                <li>Over 150 incredible, immersive new quests</li>
                <li>20 fantastic minigames that are accessible to members only</li>
                <li>An expansive world three times larger than the free game</li>
                <li>Learn nine powerful new skills to become an all-conquering warrior</li>
            </ul>
            <a id="membersBenefitsUpgradeButton" href="http://www.runescape.com/g=runescape/members_benefits.ws"
               class="HoverImg">
                <img src="http://www.runescape.com/img/main/splash/membersBenefitsUpgradeMembership.png" title=""
                     alt="Upgrade your Membership">
            </a>
        </div>
        <div id="sideNavigation">
            <div id="benefitsTitle">
                <div class="Centre1">
                    <div class="Centre2">

                        <h3 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                     aria-hidden="true">Learn More</span>
                            <span class="G0">Learn More</span>
                            <span class="G1" aria-hidden="true">Learn More</span>
                            <span class="G2" aria-hidden="true">Learn More</span>
                            <span class="G3" aria-hidden="true">Learn More</span>
                            <span class="G4" aria-hidden="true">Learn More</span>
                            <span class="G5" aria-hidden="true">Learn More</span>
                            <span class="G6" aria-hidden="true">Learn More</span>
                        </h3>
                    </div>
                </div>
            </div>
            <nav>
                <div id="sideNavigationBeginnersGuide">

                    <h4 class="Gradient">
                        <span class="G0">Beginner's Guide</span>
                        <span class="G1" aria-hidden="true">Beginner's Guide</span>
                        <span class="G2" aria-hidden="true">Beginner's Guide</span>
                        <span class="G3" aria-hidden="true">Beginner's Guide</span>
                        <span class="G4" aria-hidden="true">Beginner's Guide</span>
                        <span class="G5" aria-hidden="true">Beginner's Guide</span>
                        <span class="G6" aria-hidden="true">Beginner's Guide</span>
<span class="mask"><span class="spacing">Beginner's Guide</span>
<span class="middleUnderscore"><span class="spacing" aria-hidden="true">Beginner's Guide</span></span>
</span>
<span class="rightUnderscore">
<img src="http://www.runescape.com/img/global/gradient_header/underscore_right.png" class="right" alt=""/><span
        class="spacing" aria-hidden="true">Beginner's Guide</span>
</span>
                        <span class="leftUnderscore"><img
                                src="http://www.runescape.com/img/global/gradient_header/underscore_flourish_left.png"
                                class="left" alt=""/></span>
                    </h4>

                    <p class="sideNaviDescription">An in-depth player guide that introduces beginners to the thrilling
                        world of RuneScape!</p>
                    <a href="http://www.runescape.com/g=runescape/beginnersguide.ws"
                       class="Button Button29"><span><span><span class=""><b>View Guide</b></span></span></span></a>
                </div>
                <div id="sideNavigationCommunitySite">

                    <h4 class="Gradient">
                        <span class="G0">Community Site</span>
                        <span class="G1" aria-hidden="true">Community Site</span>
                        <span class="G2" aria-hidden="true">Community Site</span>
                        <span class="G3" aria-hidden="true">Community Site</span>
                        <span class="G4" aria-hidden="true">Community Site</span>
                        <span class="G5" aria-hidden="true">Community Site</span>
                        <span class="G6" aria-hidden="true">Community Site</span>
<span class="mask"><span class="spacing">Community Site</span>
<span class="middleUnderscore"><span class="spacing" aria-hidden="true">Community Site</span></span>
</span>
<span class="rightUnderscore">
<img src="http://www.runescape.com/img/global/gradient_header/underscore_right.png" class="right" alt=""/><span
        class="spacing" aria-hidden="true">Community Site</span>
</span>
                        <span class="leftUnderscore"><img
                                src="http://www.runescape.com/img/global/gradient_header/underscore_flourish_left.png"
                                class="left" alt=""/></span>
                    </h4>

                    <p class="sideNaviDescription">Meet other players, make new friends and unearth RuneScape's secrets
                        in the ultimate MMO experience.</p>
                    <a href="http://www.runescape.com/g=runescape/title.ws" class="Button Button29"><span><span><span
                                    class=""><b>View Community Site</b></span></span></span></a>
                </div>
                <div id="sideNavigationMembershipCards">

                    <h4 class="Gradient">
                        <span class="G0">Membership Cards</span>
                        <span class="G1" aria-hidden="true">Membership Cards</span>
                        <span class="G2" aria-hidden="true">Membership Cards</span>
                        <span class="G3" aria-hidden="true">Membership Cards</span>
                        <span class="G4" aria-hidden="true">Membership Cards</span>
                        <span class="G5" aria-hidden="true">Membership Cards</span>
                        <span class="G6" aria-hidden="true">Membership Cards</span>
<span class="mask"><span class="spacing">Membership Cards</span>
<span class="middleUnderscore"><span class="spacing" aria-hidden="true">Membership Cards</span></span>
</span>
<span class="rightUnderscore">
<img src="http://www.runescape.com/img/global/gradient_header/underscore_right.png" class="right" alt=""/><span
        class="spacing" aria-hidden="true">Membership Cards</span>
</span>
                        <span class="leftUnderscore"><img
                                src="http://www.runescape.com/img/global/gradient_header/underscore_flourish_left.png"
                                class="left" alt=""/></span>
                    </h4>

                    <p class="sideNaviDescription">Membership cards offer up to 100 days of membership, with additional,
                        exclusive offers available to subscribers.</p>
                    <a href="http://www.runescape.com/g=runescape/store_locator.ws" class="Button Button29"><span><span><span
                                    class=""><b>More Information</b></span></span></span></a>
                </div>
            </nav>
        </div>
        <div id="Footer" class="Kingthings">
            <ul class="Main">
                <li>
                    <a href="https://www.runescape.com/g=runescape/account_settings.ws">Account</a>
                    <ul class="Sub">
                        <li><a href="https://www.runescape.com/g=runescape/account_settings.ws">Profile &amp;
                                Settings</a></li>
                        <li><a href="http://www.runescape.com/g=runescape/members_benefits.ws">Upgrade</a></li>
                        <li><a href="http://services.runescape.com/m=loyalty-points/g=runescape/">Loyalty Programme</a>
                        </li>
                        <li><a href="http://services.runescape.com/m=friend-referral/g=runescape/">Refer a Friend</a>
                        </li>
                        <li><a href="http://www.runescape.com/g=runescape/store_locator.ws">Membership Cards</a></li>
                    </ul>
                </li>
                <li>
                    <a href="http://services.runescape.com/m=rswiki/en/Customer_Support">Support</a>
                    <ul class="Sub">
                        <li><a href="http://services.runescape.com/m=rswiki/en/Player_Support">Player Support</a></li>
                        <li><a href="http://services.runescape.com/m=rswiki/en/Technical_Help">Tech Support</a></li>
                        <li><a href="http://services.runescape.com/m=rswiki/en/Billing_Advice">Billing Support</a></li>
                        <li><a href="http://services.runescape.com/m=rswiki/en/Safety_%26_Security">Safety &amp;
                                Security</a></li>
                        <li><a href="http://services.runescape.com/m=bugtracker_v4/g=runescape/">Submit a Bug Report</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="http://www.runescape.com/g=runescape/gameguide.ws">Game</a>
                    <ul class="Sub">
                        <li><a href="http://www.runescape.com/g=runescape/gameguide.ws">Game Guide</a></li>
                        <li><a href="http://www.runescape.com/g=runescape/beginnersguide.ws">Beginner's Guide</a></li>
                        <li><a href="http://www.runescape.com/g=runescape/world_locations.ws">Locations</a></li>
                        <li><a href="http://services.runescape.com/m=rswiki/en/">RuneScape Wiki</a></li>
                    </ul>
                </li>
                <li>
                    <a href="http://www.runescape.com/g=runescape/title.ws">Community</a>
                    <ul class="Sub">
                        <li><a href="http://services.runescape.com/m=forum/g=runescape/forums.ws">Forums</a></li>
                        <li><a href="http://services.runescape.com/m=clan-home/g=runescape/">Clans</a></li>
                        <li><a href="http://services.runescape.com/m=adventurers-log/g=runescape/">Adventurer's Log</a>
                        </li>
                        <li><a href="http://www.runescape.com/g=runescape/events.ws">Events</a></li>
                        <li><a href="http://services.runescape.com/m=hiscore/g=runescape/heroes.ws">Hiscores</a></li>
                        <li><a href="http://www.runescape.com/g=runescape/downloads.ws">Downloads</a></li>
                    </ul>
                </li>
            </ul>
            <h2 id="RuneScapeLogo"><a href="http://www.runescape.com/g=runescape/"><img
                        src="http://www.runescape.com/img/global/logos/runescape.png" alt="RuneScape" title=""/></a>
            </h2>

            <div class="FooterBottom">
                <a href="http://www.jagex.com/g=runescape/" id="JagexLogo"><img
                        src="http://www.runescape.com/img/global/logos/jagex.png" alt="Jagex" title=""/></a>

                <p>This website and its contents are copyright &copy; 1999 - 2011 Jagex Ltd<br/>Use of this website is
                    subject to our <a href="http://www.jagex.com/g=runescape/terms/terms.ws" target="_blank">Terms &amp;
                        Conditions</a> and <a href="http://www.jagex.com/g=runescape/privacy/privacy.ws"
                                              target="_blank">Privacy Policy</a>.</p>

                <div class="LanguageButtonSurround">
                    <div class="LanguageButton"><img src="http://www.runescape.com/img/global/language_selector/en.png"
                                                     title="en" alt="en" class="SelectedLang"><img
                            src="http://www.runescape.com/img/global/language_selector/LanguageButtonFg.png"
                            class="foreground"/>

                        <div class="LanguagePanel">
                            <div class="bottom">
                                <ul>
                                    <li class="flag"><a href="http://www.runescape.com/g=runescape/splash.ws?set_lang=0"
                                                        title="English"><img
                                                src="http://www.runescape.com/img/global/language_selector/en.png"
                                                alt="English" class="flag"/><img
                                                src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                                class="LanguageArrow"></a></li>
                                    <li class="flag"><a
                                            href="http://www.runescape.com/l=1/g=runescape/splash.ws?set_lang=1"
                                            title="Deutsch"><img
                                                src="http://www.runescape.com/img/global/language_selector/de.png"
                                                alt="Deutsch" class="flag"/><img
                                                src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                                class="LanguageArrow"></a></li>
                                    <li class="flag"><a
                                            href="http://www.runescape.com/l=2/g=runescape/splash.ws?set_lang=2"
                                            title="Français"><img
                                                src="http://www.runescape.com/img/global/language_selector/fr.png"
                                                alt="Français" class="flag"/><img
                                                src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                                class="LanguageArrow"></a></li>
                                    <li class="flag"><a
                                            href="http://www.runescape.com/l=3/g=runescape/splash.ws?set_lang=3"
                                            title="Português (BR)"><img
                                                src="http://www.runescape.com/img/global/language_selector/pt.png"
                                                alt="Português (BR)" class="flag"/><img
                                                src="http://www.runescape.com/img/global/language_selector/LanguageArrow.png"
                                                class="LanguageArrow"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="BottomNavigationFlag">

                    <div class="fb-like" data-href="http://www.facebook.com/RuneScape" data-send="false"
                         data-layout="button_count" data-width="45" data-show-faces="false"
                         data-colorscheme="dark"></div>
                </div>
                <div id="SnLinks">
                    <a href="http://services.runescape.com/m=news/g=runescape/latest_news.rss" title="RSS"
                       target="_blank" class="HoverImg NoFade"><img
                            src="http://www.runescape.com/img/global/logos/MediumRss.png" alt="RSS"/></a>

                    <a href="http://www.facebook.com/RuneScape" title="Follow us on Facebook" target="_blank"
                       class="HoverImg NoFade AddButtons"
                       onclick="try{pageTracker._trackPageview(&quot;/outgoing/bookmarking/facebook/http://www.runescape.com/g=runescape/&quot;)}catch(x){}; try{_pageTracker._trackPageview(&quot;/outgoing/bookmarking/facebook/http://www.runescape.com/g=runescape/&quot;)}catch(x){}"><img
                            src="http://www.runescape.com/img/global/social/MediumFacebook.png" alt="Facebook"/></a>

                    <a href="http://twitter.com/RuneScape" title="Follow us on Twitter" target="_blank"
                       class="HoverImg NoFade AddButtons"
                       onclick="try{pageTracker._trackPageview(&quot;/outgoing/bookmarking/twitter/http://www.runescape.com/g=runescape/&quot;)}catch(x){}; try{_pageTracker._trackPageview(&quot;/outgoing/bookmarking/twitter/http://www.runescape.com/g=runescape/&quot;)}catch(x){}"><img
                            src="http://www.runescape.com/img/global/social/MediumTwitter.png" alt="Twitter"/></a>

                    <a href="http://www.youtube.com/RuneScape" title="Follow us on YouTube" target="_blank"
                       class="HoverImg NoFade AddButtons"
                       onclick="try{pageTracker._trackPageview(&quot;/outgoing/bookmarking/youtube/http://www.runescape.com/g=runescape/&quot;)}catch(x){}; try{_pageTracker._trackPageview(&quot;/outgoing/bookmarking/youtube/http://www.runescape.com/g=runescape/&quot;)}catch(x){}"><img
                            src="http://www.runescape.com/img/global/social/MediumYoutube.png" alt="YouTube"/></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="promoPresenter">
    <div id="promo1" data-current="true" data-index="0"
         style="background-image:url(http://www.runescape.com/img/main/splash/promos/1.jpg)"></div>
    <div id="promo2"></div>
    <div id="flash"></div>
</div>

<script type="text/javascript">


    document.write(unescape("%3Cscript src='http://www.google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

</script>

<script type="text/javascript">
    try {
        var _pageTracker = _gat._getTracker("UA-2058817-15");
        _pageTracker._setDomainName(".runescape.com");


        _pageTracker._trackPageview();
    } catch (x) {
    }
</script>

<script type="text/javascript">
    try {
        var pageTracker = _gat._getTracker("UA-2058817-2");
        pageTracker._setDomainName(".runescape.com");


        pageTracker._trackPageview();
    } catch (x) {
    }
</script>


</body>
</html>
