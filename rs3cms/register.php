<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("global.php");
require_once("includes/recaptchalib.php");

$register_failed = false;
$error_msg = "";
/* If already logged in, redirect to homepage. */
if ($agent->logged_in) {
    die(header("Location: home"));
}

if (!DEMO_MODE && isset($_POST['submit'])) {
    $email_input = $_POST['email'];
    $username_input = $_POST['username'];
    $password_input = $_POST['password'];
    $password2_input = $_POST['password2'];


    $email = filter_for_input(strtolower($email_input));
    $username = filter_for_input(strtolower($username_input));
    $hash = filter_for_input(sha1($password_input));

    $recap = get_recaptcha_response($_POST['g-recaptcha-response']);
    if (isset($recap)) {
        if (!$recap->success) {
            die(header("Location: register?failed4"));
        }
    }

    if (empty($email_input) || empty($password_input)) {
        $url = SITE_ADDRESS . "register?failed1";
        $error_msg = "Please fill out all forms";
        $register_failed = true;
    } else if ($password_input != $password2_input) {
        $url = SITE_ADDRESS . "register?failed3";
        $error_msg = "Password doesn't match";
        $register_failed = true;
    }

    if ($register_failed) {
//        $url = SITE_ADDRESS . "register?failed";
        $_SESSION['error_msg'] = $error_msg;
        die(header("Location: $url"));
    }

    if (!agent::user_exists($username) && !agent::email_exists($email)) {
        dbquery("INSERT INTO characters (email, password, username, register_date, account_file) VALUES('$email', '$hash', '$username', CURRENT_TIMESTAMP, '$username');");
        $url = SITE_ADDRESS . "register?complete";
        die(header("Location: $url"));
    }

}

require_once("header.php");
?>
<link rel="stylesheet" type="text/css" href="<?php printf(SITE_ADDRESS); ?>css/error-50.css"/>

<div id="MainContentOuter">
    <div class="MainContentBg">
        <div class="MainContentTopBg">
            <div class="MainContentBottomBg">
                <div id="MainContent">
                    <div id="MainTitle">
                        <div class="Centre1">
                            <div class="Centre2">

                                <h3 class="Gradient DoubleFlourish"><span class="spacing"
                                                                          aria-hidden="true">Registration</span>
                                    <span class="G0">Registration</span>
                                    <span class="G1" aria-hidden="true">Registration</span>
                                    <span class="G2" aria-hidden="true">Registration</span>
                                    <span class="G3" aria-hidden="true">Registration</span>
                                    <span class="G4" aria-hidden="true">Registration</span>
                                    <span class="G5" aria-hidden="true">Registration</span>
                                    <span class="G6" aria-hidden="true">Registration</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">Registration</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Registration</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                            class="left" alt=""/></span>
                                </h3>
                            </div>
                        </div>
                    </div>

                    <?php if (isset($_GET['confirm'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">

                                <h2 class="Gradient">
                                    <span class="G0">Register an Account</span>
                                    <span class="G1" aria-hidden="true">Register an Account</span>
                                    <span class="G2" aria-hidden="true">Register an Account</span>
                                    <span class="G3" aria-hidden="true">Register an Account</span>
                                    <span class="G4" aria-hidden="true">Register an Account</span>
                                    <span class="G5" aria-hidden="true">Register an Account</span>
                                    <span class="G6" aria-hidden="true">Register an Account</span>
                                    <span class="mask"><span class="spacing">Register an Account</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Register an Account</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Register an Account</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">

                                    <form action="<?php printf(SITE_ADDRESS); ?>register" method="post"
                                          autocomplete="off">
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Email:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login" name='email'
                                                                                 maxlength='40' size='35' value=""
                                                                                 id='email' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="login">
                                            <span class="inputLabel FlatHeader">Username:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='text' title="Login"
                                                                                 name='username'
                                                                                 maxlength='40' size='35' value=""
                                                                                 id='username' class="NoPlaceholder">
                                                </div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="password">
                                            <span class="inputLabel FlatHeader">Password:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='password' title=""
                                                                                 name='password' maxlength='40'
                                                                                 size='35' value="" id='password'
                                                                                 class="NoPlaceholder"></div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>
                                        <div class="password">
                                            <span class="inputLabel FlatHeader">Repeat Pass:</span>

                                            <div class="EncrustedInputBoxWrapper">
                                                <div class="InputBoxLeft"><input type='password' title=""
                                                                                 name='password2' maxlength='40'
                                                                                 size='35' value="" id='password2'
                                                                                 class="NoPlaceholder"></div>
                                                <div class="InputBoxRight"></div>
                                            </div>
                                        </div>

                                        <a class="password" target="_parent"><?php
                                            if ($register_failed) {
                                                echo "Please make sure all forms are filled.";
                                                echo $error_msg;
                                            }
                                            ?></a> <br
                                            class="clear"/>

                                        <div class="section_form" id="recaptcha">
                                            <div id="recaptcha_box">
                                                <div id="recaptcha_image"></div>

                                                <div class="g-recaptcha"
                                                     data-sitekey=<?php printf(RECAPTCHA_SITEKEY); ?>></div>
                                            </div>

                                            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                            <div id="button">
                                                <button class="HoverImg" type="submit" name="submit">
                                                    <img
                                                        src="<?php printf(SITE_ADDRESS); ?>img/weblogin/recaptcha_login.png"
                                                        alt="Login" title=""/>
                                                </button>
                                            </div>
                                            <noscript>
                                                <div id="recaptcha_box_nojs">
                                                    <iframe id="recaptcha_nonjs"
                                                            src="https://www.google.com/recaptcha/api/noscript?k=<?php printf(RECAPTCHA_PUBLIC_KEY); ?>"></iframe>
                                                    <br/>

                                                    <div id="recaptcha_nojs_group">
                                                        <label for="recaptcha_challenge_field">
                                                            After completing the above challenge please enter your
                                                            confirmation code in the box below and click 'Login
                                                            Now!':</label><br class="clear"/>
                                                        <input id="recaptcha_challenge_field"
                                                               name="recaptcha_challenge_field">
                                                        <br class="clear"/>
                                                        <input type="hidden" name="recaptcha_response_field"
                                                               value="manual_challenge">
                                                    </div>
                                                    <br class="clear"/>
                                                </div>
                                            </noscript>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    <?php } else if (isset($_GET['complete'])) { ?>

                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Registration Complete</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Registration Complete</span>
                                    <span class="G1" aria-hidden="true">Registration Complete</span>
                                    <span class="G2" aria-hidden="true">Registration Complete</span>
                                    <span class="G3" aria-hidden="true">Registration Complete</span>
                                    <span class="G4" aria-hidden="true">Registration Complete</span>
                                    <span class="G5" aria-hidden="true">Registration Complete</span>
                                    <span class="G6" aria-hidden="true">Registration Complete</span>
                                    <span class="mask"><span class="spacing">Registration Complete</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Registration Complete</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Registration Complete</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>Your account has successfully been created. You may login now</p><br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>login?confirm"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b>Login</b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } else { ?>
                        <!-- Error registering -->
                        <div id="minHeightFix">
                            <div id="oneThirdRight">
                                <img src="<?php printf(SITE_ADDRESS); ?>img/global/error/errorPic.png"
                                     alt="Error Picture"/>
                            </div>
                            <div id="twoThirdsLeft">
                                <h3 class="orangeheader">Registration Failed</h3>

                                <h2 class="Gradient">
                                    <span class="G0">Invalid Email or Username</span>
                                    <span class="G1" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="G2" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="G3" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="G4" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="G5" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="G6" aria-hidden="true">Invalid Email or Username</span>
                                    <span class="mask"><span class="spacing">Invalid Email or Username</span>
                                        <span class="middleUnderscore"><span class="spacing" aria-hidden="true">Invalid Email or Username</span></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_right.png"
                                            class="right" alt=""/><span class="spacing" aria-hidden="true">Invalid Email or Username</span>
                                    </span>
                                    <span class="leftUnderscore"><img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/underscore_flourish_left.png"
                                            class="left" alt=""/></span>
                                </h2>

                                <div id="errorContent">
                                    <p>The email or password you entered was incorrect.<br/>

                                        The email or username either exists, please try again<br/>
                                        <?php $_SESSION['error_msg'] ?></p><br/>
                                    <a href="<?php printf(SITE_ADDRESS); ?>register?confirm"
                                       class="Button Button29 errorPageButton w160"><span><span><span
                                                    class=""><b><?php echo "Try Again" ?></b></span></span></span></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <?php require_once("footer.php"); ?>

