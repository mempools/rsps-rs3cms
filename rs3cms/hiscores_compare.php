<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define("PAGE_TITLE", "Hiscores");

require_once("global.php");
require_once("includes/class_character_statistics.php");

// determines if information for user1 & user2 was successfully retrieved.
$user1_good = false;
$user2_good = false;

/* data for user1 */
if (isset($_GET['user1'])) {
    $user1 = filter_for_input($_GET['user1']);

    $user1_qry = dbquery("SELECT * FROM characters WHERE characters.username = '$user1' LIMIT 1;");
    if (mysql_num_rows($user1_qry) > 0) {
        $user1_data = mysql_fetch_array($user1_qry);
        $user1_id = $user1_data['id'];

        $user1_qry2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.master_id = '$user1_id' LIMIT 1;");
        if (mysql_num_rows($user1_qry2) > 0) {
            $user1_sdata = mysql_fetch_array($user1_qry2);
            $user1_stats = new character_statistics($user1_sdata);
            $user1_good = true;
        }
    }
}

/* data for user2 */
if (isset($_GET['user2'])) {
    $user2 = filter_for_input($_GET['user2']);

    $user2_qry = dbquery("SELECT * FROM characters WHERE characters.username = '$user2' LIMIT 1;");
    if (mysql_num_rows($user2_qry) > 0) {
        $user2_data = mysql_fetch_array($user2_qry);
        $user2_id = $user2_data['id'];

        $user2_qry2 = dbquery("SELECT * FROM characters_statistics WHERE characters_statistics.master_id = '$user2_id' LIMIT 1;");
        if (mysql_num_rows($user2_qry2) > 0) {
            $user2_sdata = mysql_fetch_array($user2_qry2);
            $user2_stats = new character_statistics($user2_sdata);
            $user2_good = true;
        }
    }
}

/* comparison can only process if user1 is available */
if (!$user1_good) {
    exit(header("Location: " . SITE_ADDRESS . "hiscores"));
}

require_once("header.php");
?>
    <link href="<?php printf(SITE_ADDRESS); ?>css/hiscores_global-52.css" rel="stylesheet"/>
    <link href="<?php printf(SITE_ADDRESS); ?>css/hiscores_compare-53.css" rel="stylesheet">
    <script src="<?php printf(SITE_ADDRESS); ?>js/hiscores_global-50.js"></script>

    <div id="MainContentOuter">
        <div class="MainContentBg">
            <div class="MainContentTopBg">
                <div class="MainContentBottomBg">
                    <div id="MainContent">

                        <div id="MainTitle">
                            <div class="Centre1">
                                <div class="Centre2">

                                    <h3 class="Gradient DoubleFlourish"><span class="spacing" aria-hidden="true">Hiscores</span>
                                        <span class="G0">Hiscores</span>
                                        <span class="G1" aria-hidden="true">Hiscores</span>
                                        <span class="G2" aria-hidden="true">Hiscores</span>
                                        <span class="G3" aria-hidden="true">Hiscores</span>
                                        <span class="G4" aria-hidden="true">Hiscores</span>
                                        <span class="G5" aria-hidden="true">Hiscores</span>
                                        <span class="G6" aria-hidden="true">Hiscores</span>
                                    <span class="mask"><span class="spacing" aria-hidden="true">Hiscores</span>
                                        <span class="leftInnerFlourish"></span><span class="centreFlourish"></span><span
                                            class="rightInnerFlourish"></span>
                                    </span>
                                    <span class="rightUnderscore">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_right.png"
                                            class="right" alt=""/><span class="spacing"
                                                                        aria-hidden="true">Hiscores</span>
                                    </span>
                                        <span class="leftUnderscore"><img
                                                src="<?php printf(SITE_ADDRESS); ?>img/global/gradient_header/flourish_outer_left.png"
                                                class="left" alt=""/></span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <nav>
                            <div class="SubNavigation">
                                <a href="http://services.runescape.com/m=hiscore/heroes.ws" class="HoverGradient "
                                   id="">

                                    <span class="Gradient_link4"><span
                                            style="position:relative">Hall of Heroes</span><span class="g1"
                                                                                                 aria-hidden="true">Hall of Heroes</span><span
                                            class="g2" aria-hidden="true">Hall of Heroes</span><span class="g3"
                                                                                                     aria-hidden="true">Hall of Heroes</span><span
                                            class="g4" aria-hidden="true">Hall of Heroes</span><span class="g5"
                                                                                                     aria-hidden="true">Hall of Heroes</span></span></a>
                                <a href="http://services.runescape.com/m=hiscore/overall.ws?category_type=0&amp;table=0"
                                   class="HoverGradient " id="">

                                    <span class="Gradient_link4"><span style="position:relative">Skills</span><span
                                            class="g1" aria-hidden="true">Skills</span><span class="g2"
                                                                                             aria-hidden="true">Skills</span><span
                                            class="g3" aria-hidden="true">Skills</span><span class="g4"
                                                                                             aria-hidden="true">Skills</span><span
                                            class="g5" aria-hidden="true">Skills</span></span></a>

                            </div>
                        </nav>

                        <!--                        <div id="Advert">-->
                        <!--                            <iframe src="http://playcx.com/advert.html" allowtransparency="true" width="730"-->
                        <!--                                    height="100" scrolling="no" frameborder="0"></iframe>-->
                        <!--                        </div>-->

                        <div id="hiscoresStats" class="comparePlayers <?php if ($user2_good) {
                            printf("comparing");
                        } ?>">
                            <div id="hiscoresSearch">
                                <form action="<?php printf(SITE_ADDRESS); ?>hiscores_compare" method="get">

                                    <div class="EncrustedInputBoxWrapper">
                                        <div class="InputBoxLeft"><input type='text' title="Player" name='user1'
                                                                         maxlength='20' size='15' value="" id='compare'
                                                                         autocomplete='off'></div>
                                        <div class="InputBoxRight"></div>
                                        <div class="InputBoxLeft"><input type='text' title="Other player" name='user2'
                                                                         maxlength='20' size='15' value="" id='compare'
                                                                         autocomplete='off'></div>
                                        <div class="InputBoxRight"></div>
                                    </div>
                            <span class="Button Button29"><span><span><span class=""><b>Compare</b></span><input
                                            type="submit" title="Find"/></span></span></span>
                                </form>
                            </div>

                            <div class="OrnamentalBox Width923 SegmentTwo" id="skillStats">
                                <div class="OrnamentalBoxTop"></div>
                                <div class="OrnamentalBoxTitle">
                                    <div class="Centre1">
                                        <div class="Centre2">
                                            <h3 class="Gradient NoFlourish Centre"><span class="spacing"
                                                                                         aria-hidden="true">Skill Stats</span>
                                                <span class="G0">Skill Stats</span>
                                                <span class="G1" aria-hidden="true">Skill Stats</span>
                                                <span class="G2" aria-hidden="true">Skill Stats</span>
                                                <span class="G3" aria-hidden="true">Skill Stats</span>
                                                <span class="G4" aria-hidden="true">Skill Stats</span>
                                                <span class="G5" aria-hidden="true">Skill Stats</span>
                                                <span class="G6" aria-hidden="true">Skill Stats</span>
                                            </h3></div>
                                    </div>
                                </div>
                                <div class="FlatHeader playerNames">
                                    <div class="left"><?php printf($user1_data['username']); ?></div>
                                    <?php if ($user2_good) { ?>
                                        <div class="right"><?php printf($user2_data['username']); ?></div>
                                    <?php } ?>
                                </div>
                                <div class="playerAvatars">
                                    <div class="left">
                                        <img src="<?php printf(SITE_ADDRESS); ?>img/avatars/default.png" alt=""
                                             title=""/>
                                    </div>
                                    <?php if ($user2_good) { ?>
                                        <div class="right">
                                            <img src="<?php printf(SITE_ADDRESS); ?>img/avatars/default.png" alt=""
                                                 title=""/>
                                        </div>
                                    <?php } ?>
                                </div>

                                <style>
                                    #categorySelector.InPageDropDownNew.NoJS:hover .DropListWrapper,
                                    #categorySelector.InPageDropDownNew.CSS3:hover .DropListWrapper {
                                        top: -8px;
                                    }

                                    #categorySelector.InPageDropDownNew.NoJS .DropListWrapper,
                                    #categorySelector.InPageDropDownNew.CSS3 .DropListWrapper {
                                        top: -100px;
                                    }

                                    #categorySelector:hover {
                                        height: 128px;
                                    }
                                </style>
                                <div class="InPageDropDownNew NoJS CSS3 categorySelect" id="categorySelector"
                                     data-expand="128px">
                                    <div class="Mask"></div>
                                    <div class="DropListWrapper" data-retract="-100px">
                                        <img
                                            src="<?php printf(SITE_ADDRESS); ?>img/global/header/nav/DropDownFlourish.png"
                                            class="Flourish" alt="">
                                        <span class="firstValue">All</span>
                                        <ul>
                                            <li><a href="#gathering">Gathering</a></li>
                                            <li><a href="#combat">Combat</a></li>
                                            <li><a href="#support">Support</a></li>
                                            <li><a href="#artisan">Artisan</a></li>
                                            <li><a href="#members">Members Only</a></li>
                                            <li><a href="#all">All</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="OrnamentalBoxBg">
                                    <div class="OrnamentalBoxContent">
                                        <div class="header">
                                            <div class="left">
                                                <span class="columnLevel">Level</span>
                                                <span class="columnTotal">Total XP</span>
                                                <span class="columnRank">Rank</span>
                                            </div>
                                            <?php if ($user2_good) { ?>
                                                <div class="right">
                                                    <span class="columnRank">Rank</span>
                                                    <span class="columnTotal">Total XP</span>
                                                    <span class="columnLevel">Level</span>
                                                </div>
                                            <?php } ?>
                                        </div>

                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,total_level,total_exp FROM characters_statistics ORDER BY total_level DESC, total_exp DESC) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s0/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(number_format($qry_data['total_level'])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data['total_exp'])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/overall.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Overall</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,total_level,total_exp FROM characters_statistics ORDER BY total_level DESC, total_exp DESC) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s0/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data['total_exp'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(number_format($qry_data['total_level'])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "attack_exp";
                                            $order = "attack_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s1/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(0, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_attack_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Attack</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "attack_exp";
                                                $order = "attack_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s1/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(0, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "defence_exp";
                                            $order = "defence_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s2/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(1, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_defence_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Defence</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "defence_exp";
                                                $order = "defence_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s2/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(1, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "strength_exp";
                                            $order = "strength_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s3/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(2, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_strength_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Strength</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "strength_exp";
                                                $order = "strength_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s3/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(2, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "constitution_exp";
                                            $order = "constitution_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s4/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(3, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_constit_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Constitution</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "constitution_exp";
                                                $order = "constitution_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s4/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(3, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "range_exp";
                                            $order = "range_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s5/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(4, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_ranged_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Ranged</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "range_exp";
                                                $order = "range_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s5/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(4, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "prayer_exp";
                                            $order = "prayer_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s6/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(5, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_prayer_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Prayer</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "prayer_exp";
                                                $order = "prayer_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s6/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(5, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "magic_exp";
                                            $order = "magic_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s7/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(6, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_magic_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Magic</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "magic_exp";
                                                $order = "magic_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s7/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(6, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "cooking_exp";
                                            $order = "cooking_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s8/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(7, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_cooking_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Cooking</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "cooking_exp";
                                                $order = "cooking_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s8/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(7, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="gathering" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "woodcutting_exp";
                                            $order = "woodcutting_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s9/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(8, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_woodcutting_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Woodcutting</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "woodcutting_exp";
                                                $order = "woodcutting_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s9/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(8, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "fletching_exp";
                                            $order = "fletching_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s10/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(9, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_fletching_total.png?2"
                                                    alt="" title=""/>
                                                <span class="statName"><span><span><span>Fletching</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "fletching_exp";
                                                $order = "fletching_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s10/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(9, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="gathering" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "fishing_exp";
                                            $order = "fishing_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s11/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(10, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_fishing_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Fishing</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "fishing_exp";
                                                $order = "fishing_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s11/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(10, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "firemaking_exp";
                                            $order = "firemaking_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s12/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(11, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_firemaking_total.png?2"
                                                    alt="" title=""/>
                                                <span class="statName"><span><span><span>Firemaking</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "firemaking_exp";
                                                $order = "firemaking_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s12/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(11, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "crafting_exp";
                                            $order = "crafting_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s13/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(12, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_crafting_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Crafting</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "crafting_exp";
                                                $order = "crafting_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s13/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(12, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "smithing_exp";
                                            $order = "smithing_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s14/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(13, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_smithing_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Smithing</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "smithing_exp";
                                                $order = "smithing_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s14/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(13, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="gathering" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "mining_exp";
                                            $order = "mining_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s15/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(14, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_mining_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Mining</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "mining_exp";
                                                $order = "mining_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s15/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(14, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "herblore_exp";
                                            $order = "herblore_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s16/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(15, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_herblore_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Herblore</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "herblore_exp";
                                                $order = "herblore_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s16/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(15, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="support" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "agility_exp";
                                            $order = "agility_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s17/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(16, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_agility_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Agility</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "agility_exp";
                                                $order = "agility_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s17/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(16, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="support" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "thieving_exp";
                                            $order = "thieving_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s18/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(17, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_thieving_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Thieving</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "thieving_exp";
                                                $order = "thieving_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s18/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(17, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="support" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "slayer_exp";
                                            $order = "slayer_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s19/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(18, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_slayer_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Slayer</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "slayer_exp";
                                                $order = "slayer_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s19/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(18, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="gathering" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "farming_exp";
                                            $order = "farming_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s20/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(19, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_farming_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Farming</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "farming_exp";
                                                $order = "farming_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s20/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(19, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="artisan" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "runecrafting_exp";
                                            $order = "runecrafting_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s21/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(20, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_runecraft_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Runecrafting</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "runecrafting_exp";
                                                $order = "runecrafting_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s21/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(20, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="row" data-skill="gathering" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "hunter_exp";
                                            $order = "hunter_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s22/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(21, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_hunter_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Hunter</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "hunter_exp";
                                                $order = "hunter_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s22/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(21, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="row" data-skill="artisan" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "construction_exp";
                                            $order = "construction_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s23/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(22, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_construct_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Construction</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "construction_exp";
                                                $order = "construction_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s23/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(22, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row" data-skill="combat" data-member="true">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "summoning_exp";
                                            $order = "summoning_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s24/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(23, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_summoning_total.png?2"
                                                    alt="" title=""/>
                                                <span class="statName"><span><span><span>Summoning</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "summoning_exp";
                                                $order = "summoning_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s24/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(23, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>


                                        <div class="row last-row" data-skill="support" data-member="false">
                                            <?php
                                            $uid = $user1_id;
                                            $exp_column = "dungeoneering_exp";
                                            $order = "dungeoneering_exp DESC";
                                            dbquery("SET @rankcount = 0;");
                                            $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                            if (mysql_num_rows($qry) > 0) {
                                                $qry_data = mysql_fetch_assoc($qry);
                                                $rank_addr = SITE_ADDRESS . "hiscores/s25/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                ?>
                                                <div class="left">
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(24, $qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                    </a>
                                                    <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div class="middle">
                                                <img
                                                    src="<?php printf(SITE_ADDRESS); ?>img/hiscore/compare/skills/xp_dungeon_total.png?2"
                                                    alt="" title=""/>
                                                <span
                                                    class="statName"><span><span><span>Dungeoneering</span></span></span></span>
                                            </div>
                                            <?php
                                            if ($user2_good) {
                                                $uid = $user2_id;
                                                $exp_column = "dungeoneering_exp";
                                                $order = "dungeoneering_exp DESC";
                                                dbquery("SET @rankcount = 0;");
                                                $qry = dbquery("SELECT cstats.*, characters.username FROM ( SELECT @rankcount := @rankcount + 1 AS rank,master_id,$exp_column FROM characters_statistics ORDER BY $order) AS cstats INNER JOIN characters ON characters.id = cstats.master_id WHERE cstats.master_id = $uid;");
                                                if (mysql_num_rows($qry) > 0) {
                                                    $qry_data = mysql_fetch_assoc($qry);
                                                    $rank_addr = SITE_ADDRESS . "hiscores/s25/r" . $qry_data['rank'] . "#" . $qry_data['rank'];
                                                    ?>
                                                    <div class="right">
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnRank">
                                                        <?php printf(number_format($qry_data['rank'])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnTotal">
                                                        <?php printf(number_format($qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                        <a href="<?php printf($rank_addr); ?>">
                                                    <span class="columnLevel">
                                                        <?php printf(character_statistics::level_for_experience(24, $qry_data[$exp_column])); ?>
                                                    </span>
                                                        </a>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="OrnamentalBoxBottom"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php require_once("footer.php"); ?>