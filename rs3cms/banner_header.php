<?php


?>

<link href="<?php printf(SITE_ADDRESS); ?>css/banner-header.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script>window.swfobject || document.write("<script src='<?php printf(SITE_ADDRESS); ?>js/swfobject/swfobject_2_2.js'>\x3C/script>")</script>
<script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jquery/jquery_arturoSlider_0_9_6.js"></script>
<script type="text/javascript" src="<?php printf(SITE_ADDRESS); ?>js/jagex/jagex_form-0.js"></script>
<script src="<?php printf(SITE_ADDRESS); ?>js/community-50.js" type="text/javascript"></script>


<div id="banner_Container">
    <div id="headerMask"></div>
    <div id="banner">
        <div id="bannerReel">
            <a href="<?php printf(SITE_ADDRESS); ?>forums/"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/banners/firemaker_en.jpg" alt=""
                    title=""></a>
            <a target="_blank" href="/donate"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/banners/loyalty.jpg" alt="" title=""></a>
        </div>

        <div id="PlayNowWrapper">
            <a id="PlayNowWrapperPlayFree" href="<?php printf(SITE_ADDRESS); ?>play" class="GameLink HoverImgJs">
            </a>

            <div id="SN_links" style="display: block; ">

            </div>
        </div>
    </div>
    <div id="bannerBorder"></div>
    <div id="ctaOuterContainer">
        <div id="bannerSwitcher">
            <a id="bannerPrevious" class="HoverImg" href="?banner=4">
                <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/prev-button.png" alt="" title=""/>
            </a>
            <a id="bannerNext" class="HoverImg" href="?banner=1">
                <img src="<?php printf(SITE_ADDRESS); ?>img/main/community/next-button.png" alt="" title=""/>
            </a>
        </div>
        <div id="ctaInnerContainer">
            <a class="ctaHover" id="ctaMembers" href="/donate"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-members-hover.jpg"/></a>
            <a class="ctaHover" id="ctaClans" href="<?php printf(SITE_ADDRESS); ?>hiscores"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-clans-hover.jpg"/></a>
            <a class="ctaHover" id="ctaGrandExchange" href="/motivote"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-grand-exchange-hover.jpg"/></a>
            <a class="ctaHover" id="ctaMembership" href="/forum"><img
                    src="<?php printf(SITE_ADDRESS); ?>img/main/community/cta-membership-hover.jpg"/></a>
        </div>
    </div>
</div>
