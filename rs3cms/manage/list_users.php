<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'Users Listing');
define('PAGE_TAB', 3);

require_once("header.php");

$keyd = $_GET['key'];
$show = 50;
$page = 1;
if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}
$start_from = ($page - 1) * $show;

if (isset($_GET['key'])) {
    $key = $_GET['key'];
    $user_id = agent::id_from_name($key);

    if (isset($_GET['inactive'])) {
        $total_results = dbevaluate("SELECT COUNT(id) FROM characters WHERE id='$user_id' AND last_signin IS NULL;");
        $users = dbquery("SELECT * FROM characters WHERE id='$user_id' AND last_signin IS NULL ORDER BY id LIMIT $start_from, $show;");
    } else {
        $total_results = dbevaluate("SELECT COUNT(id) FROM characters WHERE id='$user_id';");
        $users = dbquery("SELECT * FROM characters WHERE id='$user_id' ORDER BY id LIMIT $start_from, $show;");
    }
} else {
    if (isset($_GET['inactive'])) {
        $total_results = dbevaluate("SELECT COUNT(id) FROM characters WHERE last_signin IS NULL;");
        $users = dbquery("SELECT * FROM characters WHERE last_signin IS NULL ORDER BY id LIMIT $start_from, $show;");
    } else {
        $total_results = dbevaluate("SELECT COUNT(id) FROM characters;");
        $users = dbquery("SELECT * FROM characters ORDER BY id LIMIT $start_from, $show;");
    }
}
$total_pages = ceil($total_results / $show);
?>

<h1>Users Listing</h1>
<hr>
<p>A listing of all users currently registered are shown here.</p>

<form method="get">
    <fieldset class="display-options" style="float: left">
        Search by name:
        <input type="text" name="key" value="<?php
        if (isset($key)) {
            printf($key);

        }
        ?>"/>&nbsp;
        <input type="submit" class="button2" value="Search"/>
    </fieldset>
</form>

<div class="pagination" style="float: right; margin: 15px 0 2px 0">
    <?php
    if ($total_pages > 1) {
        $base_url = basename($_SERVER['PHP_SELF']) . "?";
        if (isset($key)) {
            $base_url .= "key=$key&";
        }
        printf("Page $page of $total_pages &bull;");

        $adjacents = 3;
        $sp = " <span>";

        $previous_page = $page - 1;
        if ($page > 1) {
            $sp .= "&nbsp;&nbsp;<a href='" . $base_url . "page=" . $previous_page . "'>Previous</a>";
        }

        if ($total_pages < (7 + ($adjacents * 2))) {
            for ($i = 1; $i <= $total_pages; $i++) {
                if ($i == $page) {
                    $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                } else {
                    $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                }
            }
        } elseif ($total_pages > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($i = 1; $i < 4 + ($adjacents * 2); $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
                $sp .= "...";
                $sp .= "<a href='" . $base_url . "page=$total_pages'>$total_pages</a>";
            } elseif ($total_pages - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $sp .= "<a href='" . $base_url . "page=1'>1</a>";
                $sp .= "<a href='" . $base_url . "page=2'>2</a>";
                $sp .= "...";
                for ($i = $page - $adjacents; $i <= $page + $adjacents; $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
                $sp .= "...";
                $sp .= "<a href='" . $base_url . "page=$total_pages'>$total_pages</a>";
            } else {
                $sp .= "<a href='" . $base_url . "page=1'>1</a>";
                $sp .= "...";
                for ($i = $total_pages - (2 + ($adjacents * 2)); $i <= $total_pages; $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
            }
        }

        $next_page = $page + 1;
        if ($next_page <= $total_pages) {
            $sp .= "&nbsp;&nbsp;<a href='" . $base_url . "page=" . $next_page . "'>Next</a></span>";
        }
        printf($sp);
    }
    ?>
</div>

<table cellspacing="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>Email</th>
        <th>Username</th>
        <th>Online</th>
        <th>Register Date</th>
        <?php if (!isset($_GET['inactive'])) { ?>
            <th>Last Signin</th><?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php
    while ($user = mysql_fetch_assoc($users)) {
        printf("<tr>");
        printf("<td><a href=\"view_user.php?id=" . $user['id'] . "\"><img src='images/edit.png'></a> <b>" . $user['id'] . "</b></td>");
        printf("<td>" . $user['email'] . "</td>");
        printf("<td>" . $user['username'] . "</td>");
        printf("<td>" . online_status($user['online']) . "</td>");
        printf("<td>" . $user['register_date'] . "</td>");

        if (!isset($_GET['inactive'])) {
            printf("<td>" . $user['last_signin'] . "</td>");
        }
        printf("</tr>");
    }
    ?>
    </tbody>

</table>

<?php require_once("footer.php"); ?>
