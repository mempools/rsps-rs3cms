<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("manageglobal.php");

define('WINDOW_TITLE', 'Whois Lookup');
define('PAGE_TAB', 4);

if (isset($_GET['ip'])) {
    $ip_address = filter_for_input($_GET['id']);
    $q1 = dbquery("SELECT * FROM characters WHERE registered_ip = $ip_address;");
}

require_once("header.php");
?>
    <h1>IP Whois Lookup</h1>
    <p>This tool allows you to look up ip address of users.</p>


<?php require_once("footer.php"); ?>