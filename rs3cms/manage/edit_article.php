<?php

/*
 *     jWeb
 *     Copyright (C) 2011 Jolt Environment Team
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('AWINDOW_TITLE', 'Edit Article');
define('PAGE_TAB', 5);

if (!$agent->has_permission("web_admin")) {
    header("Location: index.php");
}

require_once("header.php");

if (isset($_GET['delete'])) {
    $id = $_GET['delete'];
    dbquery("DELETE FROM web_news WHERE id='$id';");
    header("location: view_articles.php");
}

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $article_id = $_GET['id'];

    if (!DEMO_MODE && isset($_POST['submit'])) {
        $a_id = $_POST['a_id'];
        $title = $_POST['title'];
        $description = filter_for_input($_POST['description']);
        $story = filter_for_input($_POST['story']);

        dbquery("UPDATE web_news SET web_news.title='$title',web_news.`desc`='$description',web_news.story='$story' WHERE id='$a_id';");
        mng_success("Successfully edited article.");
    }

    $article_qry = dbquery("SELECT * FROM web_news WHERE id='$article_id' LIMIT 1;");

    if (mysql_num_rows($article_qry) > 0) {
        $article_vars = mysql_fetch_assoc($article_qry);
        ?>
        <h2>Editing news article...</h2>
        <form method="post" action="edit_article.php?id=<?php printf($article_id); ?>">
            <input type="hidden" name="a_id" value="<?php printf($article_id); ?>"/>
            <fieldset>
                <dl>
                    <dt>
                        <label>Author:</label><br/>
                        <span>The name of the author who created the article.</span>
                    </dt>
                    <dd><strong><?php printf(agent::username_from_id($article_vars['author_id'])); ?></strong></dd>
                </dl>


                <dl>
                    <dt>
                        <label>Date:</label><br/>
                        <span>The date the article was created.</span>
                    </dt>
                    <dd><strong><?php printf(simpledate($article_vars['date'])); ?></strong></dd>
                </dl>

                <dl>
                    <dt>
                        <label for="title">Title:</label>
                        <span>A title shown on the news heading on front page.</span>
                    </dt>
                    <dd><input id="title" type="text" size="20" maxlength="255"
                               name="title" value="<?php printf($article_vars['title']); ?>"/>
                    </dd>
                </dl>

                <dl>
                    <dt>
                        <label for="description">Description:</label><br/>
                        <span>A brief summerization of what the article is about.</span>
                    </dt>
                    <dd>
                        <textarea id="description" name="description" rows="5"
                                  cols="45"><?php printf($article_vars['desc']); ?></textarea>
                    </dd>
                </dl>

                <dl>
                    <dt>
                        <label for="story">Story:</label><br/>
                        <span>The full story of the article.</span>
                    </dt>
                    <dd>
                        <textarea id="story" name="story" rows="10"
                                  cols="45"><?php printf($article_vars['story']); ?></textarea>
                    </dd>
                </dl>
            </fieldset>

            <fieldset class="submit-buttons">
                <input class="button1" type="submit" id="submit" name="submit" value="  Save  "/>&nbsp;
            </fieldset>
        </form>
        <?php
    } else {
        die('<script type="text/javascript">top.location.href = \'view_articles.php\';</script>');
    }
} else {
    die('<script type="text/javascript">top.location.href = \'view_articles.php\';</script>');
}
?>



<?php require_once("footer.php"); ?>
