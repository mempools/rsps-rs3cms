<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'Offences');
define('PAGE_TAB', 2);

require_once("header.php");

if (isset($_GET['new'])) {
    mng_success("Successfully added new offence ban.");
}
?>
<h1>Active Offenses</h1>
<hr>
<p>A list of current offenses. This page allows you to see the characters banned,
    type of ban, how long they are banned for, why they are banned, and who banned them.

    <b>Note: some offences that have expired may show up as 'active' until the character logs in.</b></p><br/>

<!--<form method="get">
    <fieldset class="display-options" style="float: left">
	Search by name:
        <input type="text" name="key" value="" />&nbsp;
        <input type="submit" class="button2" value="Search" />
    </fieldset>
</form>-->

<table cellspacing="1">
    <thead>
    <tr>
        <th>Character Id</th>
        <th>Username</th>
        <th>Type</th>
        <th>Date</th>
        <th>Expire Date</th>
        <th>Appealed</th>
        <th>Manage</th>
    </tr>
    </thead>
    <tbody>
    <?php

    if (isset($_GET['inactive'])) {
        $logs = dbquery("SELECT * FROM offences WHERE expired = '1' ORDER BY id DESC;");
    } else if (isset($_GET['appealed'])) {
        $logs = dbquery("SELECT * FROM offences WHERE expired = '0' AND appeal_data != '' ORDER BY id DESC;");
    } else {
        $logs = dbquery("SELECT * FROM offences WHERE expired = '0' ORDER BY id DESC;");
    }

    if (mysql_num_rows($logs) > 0) {
        while ($log = mysql_fetch_assoc($logs)) {
            $appealed = ($log['appeal_data'] != "" ? "Yes" : "No");
            if ($log['appeal_status'] == 1) {
                $appealed = "Yes, denied";
            }

            $expire_date = $log['expire_date'];
            if (time() > strtotime($log['expire_date'])) {
                $expire_date .= " (expired)";
            }


            echo "<tr>
                <td>" . $log['character_id'] . "</td>
                <td>" . agent::username_from_id($log['character_id']) . "</td>
                <td>" . ban_type($log['type']) . "</td>
                <td>" . $log['date'] . "</td>
                <td>" . $expire_date . "</td>
                <td>" . $appealed . "</td>
                <td><a href='manage_offence.php?id=" . $log['id'] . "'><img src='images/icon_edit.gif' alt='Edit' /></a></td>
                </tr>";
        }
    } else {
        echo "<tr><td colspan='7' style='text-align: center;'>No offences found.</td></tr>";
    }
    ?>
    </tbody>
</table>

<?php require_once("footer.php"); ?>
