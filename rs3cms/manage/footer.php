<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
</div>
</div>
<span class="corners-bottom"><span></span></span>
<div class="clear"></div>
</div>
</div>
</div>

<div id="page-footer">
    Powered by jWeb &copy; <a href="http://ajravindiran.com/jolt/">Jolt Environment</a><br/>
    Design by <a href="http://www.phpbb.com/">phpBB</a>&reg; Forum Software &copy; phpBB Group

    <?php
    $time = microtime();
    $time = explode(" ", $time);
    $time = $time[1] + $time[0];
    $endtime = $time;
    $totaltime = ($endtime - $begintime);
    echo '<br />Page created in ' . $totaltime . ' seconds.';
    ?>
</div>
</div>

</body>
</html>