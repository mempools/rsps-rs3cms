<?php

/*
 *     jPanel
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define("WINDOW_TITLE", "Dashboard");
define("PAGE_TAB", 1);

require_once("header.php");

$stat['server_version'] = dbevaluate("SELECT server_version FROM versioning;");
$stat['website_version'] = dbevaluate("SELECT website_version FROM versioning;");
$stat['database_version'] = dbevaluate("SELECT database_version FROM versioning;");
$stat['client_version'] = dbevaluate("SELECT client_version FROM versioning;");
$stat['user_count'] = dbevaluate("SELECT COUNT(id) FROM characters;");
$stat['users_online'] = dbevaluate("SELECT COUNT(id) FROM characters WHERE online > 0;");
$stat['mysql_version'] = dbevaluate("SELECT version();");
$stat['worlds_count'] = dbevaluate("SELECT COUNT(id) FROM worlds");
$stat['worlds_online'] = dbevaluate("SELECT COUNT(id) FROM worlds WHERE online='1';");
$result = dbquery("SHOW TABLE STATUS");
$db_size = 0;
while ($row = mysql_fetch_array($result)) {
    $db_size += $row["Data_length"] + $row["Index_length"];
}

$db_size = formatsize($db_size);

/**
 * Check if any notes are trying to be added.
 */
if (!DEMO_MODE && isset($_POST['add_note'])) {
    $note = filter_for_input($_POST['add_note']);

    if (strlen($note) < 3 || strlen($note) > 255) {
        mng_error("Unable to add note.");
    } else {
        dbquery("INSERT INTO web_acp_notes (user_id,note_date,note_message) VALUES ('" . $agent->master_id . "', NOW(), '" . $note . "')");
        add_log($agent->master_id, $agent->ip_address, "Added a note.");
        mng_success("Successfully added note.");
    }
}

/**
 * Attempts to delete the specified note id.
 */
if (isset($_GET['delete_note'])) {
    $uid = dbevaluate("SELECT user_id FROM web_acp_notes WHERE id = '" . $_GET['delete_note'] . "' LIMIT 1;");

    if ($agent->has_permission("web_admin") || $uid == $agent->master_id) {
        dbquery("DELETE FROM web_acp_notes WHERE id = '" . $_GET['delete_note'] . "';");
        add_log($agent->master_id, $agent->ip_address, "Deleted a note.");
        mng_success("Successfully deleted note.");
    } else {
        mng_error("You don't have rights to do that.");
    }
}

?>
<h1>Dashboard</h1>
<hr>
<p>Welcome to the backend of <?php printf(SITE_NAME) ?>. Both moderators and administrators can manage a variety of
    things here.</p><br/>

<h2>Server statistics</h2>
<table cellspacing="1">
    <col class="col1"/>
    <col class="col2"/>
    <col class="col1"/>
    <col class="col2"/>
    <thead>
    <tr>
        <th>Statistic</th>
        <th>Value</th>
        <th>Statistic</th>
        <th>Value</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Registered users:</td>
        <td><strong><?php echo $stat['user_count']; ?></strong></td>
        <td>Server version:</td>
        <td><strong><?php echo $stat['server_version']; ?></strong></td>
    </tr>
    <tr>
        <td>Users online:</td>
        <td><strong><?php echo $stat['users_online']; ?></strong></td>
        <td>Website version:</td>
        <td><strong><?php echo $stat['website_version']; ?></strong></td>
    </tr>
    <tr>
        <td>Worlds online:</td>
        <td><strong><?php echo $stat['worlds_online'] . " of " . $stat['worlds_count']; ?></strong></td>
        <td>Database version:</td>
        <td><strong><?php echo $stat['database_version']; ?></strong></td>
    </tr>
    <tr>
        <td>PHP version:</td>
        <td><strong><?php echo phpversion(); ?></strong></td>
        <td>Client version:</td>
        <td><strong><?php echo $stat['client_version']; ?></strong></td>
    </tr>
    <tr>
        <td>Database version:</td>
        <td><strong><?php echo "MySQL " . $stat['mysql_version']; ?></strong></td>
        <td>Database size:</td>
        <td><strong><?php echo $db_size; ?></strong></td>
    </tr>
    </tbody>
</table>
<br/>

<h2>Staff Notes</h2>
<p>Notes and general chatting between staff can be done with this.</p>
<div style="float: right;"><a href="staff_notes.php">&raquo; View all staff notes</a></div>

<table cellspacing="1">
    <thead>
    <tr>
        <th width="15%">Username</th>
        <th width="20%">Date</th>
        <th width="65%">Message</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $notes = dbquery("SELECT * FROM web_acp_notes ORDER BY id DESC LIMIT 10");
    $notebool = true;

    if (mysql_num_rows($notes) > 0) {
        while ($note = mysql_fetch_assoc($notes)) {
            $deletable = false;
            if ($agent->has_permission("web_admin") || $note['user'] == $agent->agent) {
                $deletable = true;
            }

            echo "
                <tr>
                    <td>";

            if ($deletable) {
                echo "<a href='dashboard.php?delete_note=" . $note['id'] . "'><img src='./images/icon_delete.gif' /></a>";
            }

            echo " <a href='viewuser.php?id=" . $note['user_id'] . "'><strong>" . agent::username_from_id($note['user_id']) . "</strong></a></td>
                    <td style='text-align: center;'>" . $note['note_date'] . "</td>
                    <td style='WORD-BREAK:BREAK-ALL;'>" . filter_for_outout($note['note_message'], true) . "</td>
                </tr>
                ";
            $notebool = !$notebool;
        }
    } else {
        echo "<tr><td colspan='5' style='text-align: center;'>No notes have been created.</td></tr>";
    }
    ?>
    <tr>
        <form method="post" action="dashboard.php">
            <td colspan='5' style='text-align: center;'>
                <input id="note" name="add_note" size="50" maxlength="255"/>
                <input class="button1" type="submit" id="submit" value="Post Note"/>
            </td>
        </form>
    </tr>
    </tbody>
</table>
<br/>

<?php if ($agent->has_permission("web_admin")) { ?>
    <h2>Administration Logs</h2>
    <p>This gives an overview of the last five actions carried out by staff.</p>
    <div style="float: right;"><a href="staff_logs.php">&raquo; View administration logs</a></div>

    <table cellspacing="1">
        <thead>
        <tr>
            <th>Username</th>
            <th>User IP</th>
            <th>Time</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $logs = dbquery("SELECT * FROM web_acp_logs ORDER BY id DESC LIMIT 5");

        if (mysql_num_rows($logs) > 0) {
            while ($log = mysql_fetch_assoc($logs)) {
                echo "            <tr>
                <td><strong><a href='viewuser.php?id=" . $log['user_id'] . "'>" . agent::username_from_id($log['user_id']) . "</a></strong></td>
                <td style='text-align: center;'>" . $log['user_ip'] . "</td>
                <td style='text-align: center;'>" . $log['log_time'] . "</td>
                <td><strong>" . $log['log_message'] . "</strong></td>
            </tr>";
            }
        } else {
            echo "<tr><td colspan='5' style='text-align: center;'>No logs have been created.</td></tr>";
        }
        ?>
        </tbody>
    </table>

<?php }
include_once("footer.php"); ?>

