<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'Manage Offence');
define('PAGE_TAB', 2);

require_once("header.php");

if (isset($_GET['id']) && is_numeric($_GET['id'])) {
    $offence_id = $_GET['id'];

    if (isset($_GET['appeal_denied'])) {
        $denied = $_GET['appeal_denied'];
        $q = dbquery("SELECT character_id,moderator_id FROM offences WHERE id='$offence_id' LIMIT 1;");
        if (mysql_num_rows($q) > 0) {
            $data = mysql_fetch_assoc($q);
            if ($agent->has_permission("web_admin") || $agent->master_id == $data['moderator_id']) {
                if ($denied == "true") {
                    dbquery("UPDATE offences SET appeal_status ='1' WHERE id='$offence_id';");
                    mng_success("The offence's appeal has been denied.");
                    add_log($agent->master_id, $agent->ip_address, "Denied offence appeal (id=$offence_id).");
                } else if ($denied == "false") {
                    dbquery("UPDATE offences SET expired = '1' WHERE id='$offence_id';");
                    mng_success("The ban's appeal has been approved and removed.");
                    add_log($agent->master_id, $agent->ip_address, "Accepted offence appeal (id=$offence_id).");
                    die;
                }
            } else {
                mng_error("You don't have permissions to do that.");
            }
        }
    }
} else {
    die('<script type="text/javascript">top.location.href = \'offences.php\';</script>');
}

$offence_qry = dbquery("SELECT * FROM offences WHERE id='$offence_id' LIMIT 1;");
if (mysql_num_rows($offence_qry) > 0) {
    $offence_vars = mysql_fetch_assoc($offence_qry);
    ?>

    <form method="post" action="manage_offence.php?id=<?php echo $offence_id; ?>">
        <input type="hidden" name="o_id" value="<?php echo $offence_id; ?>"/>
        <fieldset>
            <dl>
                <dt><label>Name:</label><br/></dt>
                <dd>
                    <strong>
                        <a href='viewuser.php?id=<?php echo $offence_vars['character_id']; ?>'>
                            <?php echo agent::username_from_id($offence_vars['character_id']); ?>
                        </a>
                    </strong>
                </dd>
            </dl>

            <dl>
                <dt>
                    <label>Type:</label><br/>
                    <span>Type of ban against the user.</span>
                </dt>
                <dd><strong><?php echo ban_type($offence_vars['type']); ?></strong></dd>
            </dl>

            <dl>
                <dt>
                    <label>Date:</label><br/>
                    <span>The date the user was banned.</span>
                </dt>
                <dd><strong><?php echo $offence_vars['date']; ?></strong></dd>
            </dl>

            <dl>
                <dt>
                    <label>Expire Date:</label><br/>
                    <span>The date the user will be unbanned.</span>
                </dt>
                <dd><strong><?php echo $offence_vars['expire_date']; ?></strong></dd>
            </dl>

            <dl>
                <dt>
                    <label>Moderator:</label><br/>
                    <span>The moderator who banned the user.</span>
                </dt>
                <dd>
                    <strong>
                        <a href='view_user.php?id=<?php echo $offence_vars['moderator_id']; ?>'>
                            <?php echo agent::username_from_id($offence_vars['moderator_id']); ?>
                        </a>
                    </strong>
                </dd>
            </dl>

            <dl>
                <dt>
                    <label for="reason">Reason:</label><br/>
                    <span>The reason why the user is banned.</span>
                </dt>
                <dd>
                    <textarea id="reason" name="reason" rows="3" cols="45"
                              disabled><?php echo $offence_vars['reason']; ?></textarea>
                </dd>
            </dl>

            <dl>
                <dt>
                    <label for="data">Appeal Data:</label><br/>
                    <span>A message by the user in attempt to appeal the ban.</span>
                </dt>
                <dd>
                    <textarea id="data" name="data" rows="3" cols="45"
                              disabled><?php echo $offence_vars['appeal_data']; ?></textarea>
                </dd>
            </dl>

            <dl>
                <dt>
                    <label>Appeal Status:</label><br/>
                    <span>The status of the appealed data.</span>
                </dt>
                <dd><strong><?php
                        $appealed = ($offence_vars['appeal_data'] != "" ? "Yes" : "No");
                        if ($offence_vars['appeal_status'] == 1) {
                            $appealed = "Yes, denied";
                        }
                        echo $appealed;
                        ?></strong></dd>
            </dl>

            <dl>
                <dt>
                    <label>Expired:</label><br/>
                    <span>Whether the ban has expired.</span>
                </dt>
                <dd><strong><?php echo($offence_vars['expired'] == 1 ? "Yes" : "No"); ?></strong></dd>
            </dl>

            <p class="quick">
                <input class="button1" value="Deny Appeal"
                       onclick="parent.location='manage_offence.php?id=<?php echo $offence_id; ?>&appeal_denied=true'"/>
                <input class="button1" value="Approve Appeal"
                       onclick="parent.location='manage_offence.php?id=<?php echo $offence_id; ?>&appeal_denied=false'"/>
                <input class="button1" value="Go Back" onclick="parent.location='offences.php'"/>
            </p>
        </fieldset>
    </form>

    <?php
} else {
    acp_error("Offence not found.");
}
require_once("footer.php");
?>
