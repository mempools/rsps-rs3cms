<?php
/*
 *     jWeb
 *     Copyright (C) 2011 Jolt Environment Team
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


require_once("manageglobal.php");

define('WINDOW_TITLE', 'Add Article');
define('PAGE_TAB', 5);

if (!$agent->has_permission("web_admin")) {
    header("Location: index.php");
}

require_once("header.php");

if (!DEMO_MODE && isset($_POST['submit'])) {
    $date = time();
    $uid = $agent->master_id;
    $title = $_POST['title'];
    $category_id = $_POST['category'];
    $desc = filter_for_input($_POST['desc']);
    $story = filter_for_input($_POST['story']);
    $seo_url = seo_url($title);

    $type = filter_for_input($_POST['type']);

    switch ($type) {
        case 1:
            $youtube_url = filter_for_input($_POST['youtube_url']);
            break;
        case 2:
            $image_url = $_POST['image_url'];
            break;
        default:
            $youtube_url = '';
            $image_url = '';
            break;
    }

    if ($category_id < 1 || $category_id > 4) {
        mng_error("Invalid category.");
    } else if (strlen($title) == 0 || strlen($desc) == 0 || strlen($story) == 0) {
        mng_error("You cannot leave any fields blank.");
    } else {
        dbquery("INSERT INTO web_news (date,author_id,category_id,title,web_news.desc,story,seo_url, youtube_url, image_url)
            VALUES ('$date','$uid','$category_id','$title','$desc','$story','$seo_url', '$youtube_url', '$image_url');");
        mng_success("Successfully created a new article.");
        add_log($agent->master_id, $agent->ip_address, "Added a new article [$title]");
    }
}
?>
<h1>Add Article</h1>
<hr>
<p>This page allows you to add a new article which is shown on the front-end website.</p><br/>

<form method="post" action="add_article.php">
    <fieldset>
        <dl>
            <dt>
                <label for="title">Title:</label><br/>
                <span>A title shown on the news heading on front page.</span>
            </dt>
            <dd><input id="title" type="text" size="20" maxlength="255" name="title" value=""/></dd>
        </dl>

        <dl>
            <dt>
                <label for="category">Category:</label><br/>
                <span>Which category to be enlisted.</span>
            </dt>
            <dd>
                <select id="category" name="category">
                    <option class="sep" value="0">Select</option>
                    <option value='1'>Game Updates</option>
                    <option value='2'>Website</option>
                    <option value='3'>Customer Support</option>
                    <option value='4'>Technical</option>
                </select>

            </dd>
        </dl>

        <!--        <dl>-->
        <!--            <dt>-->
        <!--                <label for="type">Type:</label><br/>-->
        <!--                <span>The type of the article.</span>-->
        <!--            </dt>-->
        <!--            <dd>-->
        <!--                <select id="type" name="type">-->
        <!--                    <option class="sep" value="0">Text</option>-->
        <!--                    <option value='1'>YouTube Video</option>-->
        <!--                    <option value='2'>Image Url</option>-->
        <!--                </select>-->
        <!--            </dd>-->
        <!---->
        <!--        </dl>-->

        <!--        <dl>-->
        <!--            <dt>-->
        <!--                <label for="youtube_url">YouTube Video: (OPTIONAL)</label><br/>-->
        <!--                <span>If the article type is YouTube, a video will be featured on the post.</span>-->
        <!--                <span>Input the video ID: e.g. watch?v=<b>fCQcFM4xrb0</b> <--</span>-->
        <!--            </dt>-->
        <!--            <dd><input id="youtube_url" type="text" size="20" maxlength="255" name="youtube_url" value=""/></dd>-->
        <!--        </dl>-->
        <!--        <dl>-->
        <!--            <dt>-->
        <!--                <label for="image_url">Image Url: (OPTIONAL)</label><br/>-->
        <!--                <span>If the article type is an Image, a image will be featured on the post.</span>-->
        <!--            </dt>-->
        <!--            <dd><input id="image_url" type="text" size="20" maxlength="255" name="image_url" value=""/></dd>-->
        <!--        </dl>-->

        <dl>
            <dt>
                <label for="desc">Description:</label><br/>
                <span>A brief summerization of what the article is about.</span>
            </dt>
            <dd>
                <textarea id="description" name="desc" rows="5" cols="45"></textarea>
            </dd>
        </dl>

        <dl>
            <dt>
                <label for="story">Story:</label><br/>
                <span>The full story of the article.</span>
            </dt>
            <dd>
                <textarea id="story" name="story" rows="10" cols="45"></textarea>
            </dd>
        </dl>
    </fieldset>

    <fieldset class="submit-buttons">
        <input class="button1" type="submit" id="submit" name="submit" value="  Submit Article  "/>&nbsp;
    </fieldset>
</form>

<?php require_once("footer.php"); ?>
