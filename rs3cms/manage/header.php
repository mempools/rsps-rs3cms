<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-gb" xml:lang="en-gb">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Style-Type" content="text/css"/>
    <meta http-equiv="Content-Language" content="en-gb"/>
    <meta http-equiv="imagetoolbar" content="no"/>

    <title><?php echo SITE_NAME . " - " . WINDOW_TITLE
        //        if (defined('WINDOW_TITLE')); ?></title>

    <link href="style/admin.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico"/>
</head>

<body class="ltr">

<div id="wrap">
    <div id="page-body">

        <?php if ($agent->has_permission("web_mod || web_admin")) { ?>
            <div id="tabs">
                <ul>
                    <li <?php if (PAGE_TAB == 1) echo "id='activetab'"; ?>><a
                            href="dashboard.php"><span>Dashboard</span></a></li>
                    <li <?php if (PAGE_TAB == 2) echo "id='activetab'"; ?>><a
                            href="moderation.php"><span>Moderation</span></a></li>
                    <li <?php if (PAGE_TAB == 3) echo "id='activetab'"; ?>><a href="users.php"><span>Users</span></a>
                    </li>
                    <?php if ($agent->has_permission("web_admin")) { ?>
                        <li <?php if (PAGE_TAB == 4) echo "id='activetab'"; ?>><a href="server.php"><span>Server</span></a>
                        </li>
                        <li <?php if (PAGE_TAB == 5) echo "id='activetab'"; ?>><a
                                href="website.php"><span>Website</span></a></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

        <div id="acp">
            <div class="panel">
                <span class="corners-top"><span></span></span>

                <div id="content">

                    <div id="menu">
                        <p>You are logged in as: <strong><?php echo $agent->email; ?></strong></p>
                        <ul>

                            <ul>
                                <li class="header">Quick access</li>
                                <li><a href="dashboard.php"><span>Dashboard</span></a></li>
                                <li><a href="<?php printf(SITE_ADDRESS); ?>home"><span>Home</span></a></li>
                                <li><a href="<?php printf(SITE_ADDRESS); ?>logout"><span>Logout</span></a></li>
                            </ul>

                            <?php if (PAGE_TAB == 1) { ?>

                            <?php } else if (PAGE_TAB == 2) { ?>
                                <ul>
                                    <li class="header">Offense Management</li>
                                    <li><a href="offences.php"><span>Active offences</span></a></li>
                                    <li><a href="offences.php?inactive"><span>Inactive offences</span></a></li>
                                    <li><a href="add_offence.php"><span>Add offence</span></a></li>
                                    <li><a href="offences.php?appealed"><span>View appeals</span></a></li>
                                </ul>

                                <ul>
                                    <li class="header">Logs</li>
                                    <li><a href="staff_notes.php"><span>Staff notes</span></a></li>
                                    <?php if ($agent->has_permission("web_admin")) { ?>
                                        <li><a href="staff_logs.php"><span>Staff Logs</span></a></li>
                                    <?php } ?>
                                </ul>
                            <?php } else if (PAGE_TAB == 3) { ?>
                                <ul>
                                    <li class="header">Users</li>
                                    <li><a href="users.php"><span>Find a user</span></a></li>
                                    <li><a href="list_users.php"><span>Users listing</span></a></li>
                                    <li><a href="list_users.php?inactive"><span>Inactive users</span></a></li>
                                </ul>
                            <?php } else if (PAGE_TAB == 4) { ?>
                                <ul>
                                    <li class="header">Logs</li>
                                    <li><a href="connection_logs.php"><span>Connection Logs</span></a></li>
                                    <li><a href="login_attempts.php"><span>Login Attempts</span></a></li>
                                </ul>

                                <ul>
                                    <li class="header">Maintenance</li>
                                    <li><a href="servers_status.php"><span>Servers Status</span></a></li>
                                </ul>
                            <?php } else if (PAGE_TAB == 5) { ?>
                                <ul>
                                    <li class="header">Core</li>
                                    <li><a href="edit_core.php"><span>Main Configuration</span></a></li>
                                </ul>
                                <ul>
                                    <li class="header">Articles</li>
                                    <li><a href="view_articles.php"><span>View Articles</span></a></li>
                                    <li><a href="add_article.php"><span>Add Article</span></a></li>
                                </ul>
                            <?php } ?>
                        </ul>
                    </div>

                    <div id="main">

                        <a name="maincontent"></a>
