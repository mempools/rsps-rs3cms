<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('WINDOW_TITLE', 'Editing User');
define('PAGE_TAB', 3);

require_once("manageglobal.php");

if (!$agent->has_permission("system_admin")) {
    header("Location: ../index.php");
}

require_once("header.php");

if (isset($_GET['id'])) {
    $user_id = $_GET['id'];
    if (is_numeric($user_id)) {
        $base_query = dbquery("SELECT * FROM characters WHERE id = '$user_id';");
        if (mysql_num_rows($base_query) > 0) {
            $base_data = mysql_fetch_assoc($base_query);
            echo $base_data;
        }
    }
}
if (isset($base_data)) {
    ?>
    <h1>User administration :: <?php printf($base_data['username']); ?></h1>
    <p>Here you can change your users information and certain specific options.</p>

    <form method="get" action="<?php printf($_SERVER['REQUEST_URI']); ?>">
        <input type="hidden" name="id" value="<?php printf($user_id); ?>"/>
        <fieldset class="quick">
            <select name="form" onchange="if (this.options[this.selectedIndex].value != '') this.form.submit();">
                <option class="sep" value="">Select form</option>
                <option value="overview">Overview</option>
                <option value="preferences">Preferences</option>
            </select> <input class="button2" type="submit" value="Go"/>
        </fieldset>
    </form>
    <?php
    if (isset($_GET['form'])) {
        $form = $_GET['form'];
        if ($form == "overview") {

            if (!DEMO_MODE && isset($_POST['update'])) {
                $email = $_POST['email'];
                $new_password = $_POST['password'];
                $username = $_POST['username'];
                $coord_x = $_POST['coord_x'];
                $coord_y = $_POST['coord_y'];
                $coord_z = $_POST['coord_z'];

                if ($email != $base_data['email']) {
                    if ($new_password != "") {
                        if (check_email($email)) {
                            $q = dbquery("SELECT id FROM characters WHERE email='$email';");
                            if (mysql_num_rows($q) > 0) {
                                $error = "Duplicate email.";
                            }
                        } else {
                            $error = "Invalid email.";
                        }
                    } else {
                        $error = "Password must also be changed to change email.";
                    }
                }

                if ($username != $base_data['username']) {
                    if (preg_match("/^[A-Za-z0-9_]{1,12}+$/", $username)) {
                        $q = dbquery("SELECT id FROM characters WHERE username='$username';");
                        if (mysql_num_rows($q) > 0) {
                            $error = "Duplicate username.";
                        }
                    } else {
                        $error = "Invalid username.";
                    }
                }

                if (!isset($error)) {
                    if ($new_password != "") {
                        $hashed = sha1($new_password);
                    }

                    $update_query = "UPDATE characters SET ";

                    if (isset($hashed)) {
                        $safe_email = filter_for_input($email);
                        $update_query .= "email='$safe_email',password='$hashed',";
                    }

                    $update_query .= "username='$username',coord_x='$coord_x',coord_y='$coord_y',coord_z='$coord_z' WHERE id='$user_id';";
                    dbquery($update_query);
                    mng_success("Successfully edited user.");

                    $base_query = dbquery("SELECT * FROM characters WHERE id = '$user_id';");
                    if (mysql_num_rows($base_query) > 0) {
                        $base_data = mysql_fetch_assoc($base_query);
                    }
                } else {
                    mng_error($error);
                }
            }

            mng_notice("Note: you can only change the email if you also change the password.<br/>This is due to a security feature where the email and passwords are hashed together.")
            ?>
            <form method="post" action="<?php printf($_SERVER['REQUEST_URI']); ?>">
                <fieldset>
                    <dl>
                        <dt><label>Master ID</label></dt>
                        <dd><strong><?php printf($base_data['id']); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Email</label></dt>
                        <dd><input type="text" id="email" name="email" value="<?php printf($base_data['email']); ?>"/>
                        </dd>
                    </dl>
                    <dl>
                        <dt><label>New Password</label></dt>
                        <dd><input type="password" id="password" name="password" value="" maxlength="20"/></dd>
                    </dl>
                    <dl>
                        <dt><label>Username</label></dt>
                        <dd><input type="text" id="username" name="username" maxlength="12"
                                   value="<?php printf($base_data['username']); ?>"/></dd>
                    </dl>
                    <dl>
                        <dt><label>Online Status</label></dt>
                        <dd><strong><?php printf(online_status($base_data['online'])); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Registered</label></dt>
                        <dd><strong><?php printf($base_data['register_date']); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Registered from IP</label></dt>
                        <dd><strong><?php printf($base_data['register_ip']); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Last signin</label></dt>
                        <dd><strong><?php printf($base_data['last_signin']); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Last signin IP</label></dt>
                        <dd><strong><?php printf($base_data['last_ip']); ?></strong></dd>
                    </dl>
                    <dl>
                        <dt><label>Location</label></dt>
                        <dd>
                            x: <input type="text" id="coord_x" name="coord_x"
                                      value="<?php printf($base_data['coord_x']); ?>"/>
                            y: <input type="text" id="coord_y" name="coord_y"
                                      value="<?php printf($base_data['coord_y']); ?>"/>
                            z: <input type="text" id="coord_z" name="coord_z"
                                      value="<?php printf($base_data['coord_z']); ?>"/>
                        </dd>
                    </dl>

                    <p class="quick">
                        <input class="button1" type="submit" name="update" value="Submit"/>
                    </p>
                </fieldset>
            </form>
            <?php
        } else if ($form == "preferences") {
            if (!DEMO_MODE && isset($_POST['update'])) {
                if (count($_POST) > 1) {
                    $tmp_q = dbquery("SELECT * FROM characters_preferences WHERE master_id = $user_id;");
                    if (mysql_num_rows($tmp_q) > 0) {
                        $tmp_udata = mysql_fetch_assoc($tmp_q);
                        $update_query = "UPDATE characters_preferences SET ";
                        $count = 0;
                        foreach ($_POST as $key => $value) {
                            if (array_key_exists($key, $tmp_udata)) {
                                if ($key == "master_id") {
                                    continue;
                                }
                                if (++$count > 1) {
                                    $update_query .= ",";
                                }
                                $safe_value = filter_for_input($value);
                                $update_query .= "$key='$safe_value'";
                            }
                        }
                        $update_query .= " WHERE master_id=$user_id;";
                        dbquery($update_query);
                        mng_success("Yay!");
                    }
                }
            }

            $q = dbquery("SELECT * FROM characters_preferences WHERE master_id = $user_id;");
            if (mysql_num_rows($q) > 0) {
                $udata = mysql_fetch_assoc($q);
                ?>
                <form method="post" action="<?php printf($_SERVER['REQUEST_URI']); ?>">
                    <fieldset>
                        <?php
                        foreach ($udata as $ukey => $uval) {
                            if ($ukey == "master_id") {
                                continue;
                            }

                            printf('
                    <dl>
                        <dt><label>%s</label></dt>
                        <dd><dd><input type="text" id="%s" name="%s" value="%s" /></dd></dd>
                    </dl>', $ukey, $ukey, $ukey, $uval);
                        }
                        ?>

                        <p class="quick">
                            <input class="button1" type="submit" name="update" value="Submit"/>
                        </p>
                    </fieldset>
                </form>

                <?php
            }
        } else if ($form == "statistics") {

        } else if ($form == "permissions") {

        } else {
            mng_error("Form does not exit.");
        }
    } else {
        mng_notice("Please choose a form.");
    }
}
?>


