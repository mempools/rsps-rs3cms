<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
define("WINDOW_TITLE", "Manage Users");
define("PAGE_TAB", 3);

require_once("manageglobal.php");
require_once("header.php");
?>

    <h1>User Management</h1>

    <p>Here you can view and manage registered users.</p>

    <form id="select_user" method="get" action="view_user.php">

        <fieldset>
            <legend>Select user</legend>
            <dl>
                <dt><label for="username">Find a member:</label></dt>
                <dd><input class="text medium" type="text" id="name" name="name"/></dd>
                <dd>[ <a href="list_users.php">List of users</a> ]</dd>
            </dl>

            <p class="quick">
                <input type="submit" value="Submit" class="button1"/>
            </p>
        </fieldset>

    </form>

<?php require_once("footer.php"); ?>