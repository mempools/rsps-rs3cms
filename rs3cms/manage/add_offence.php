<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("manageglobal.php");

define('WINDOW_TITLE', 'Add Offence');
define('PAGE_TAB', 2);

require_once("header.php");

if (!DEMO_MODE && isset($_POST['submit'])) {
    if ($agent->has_permission("web_admin")) {
        $name = $_POST['name'];
        $uid = $agent::id_from_name($name);
        $mod_id = $agent->master_id;
        $reason = $_POST['reason'];
        $type = $_POST['ban_type'];
        $length = $_POST['ban_length'];

        $date_time = date_create();
        $now = dbdate_format($date_time);
        $expire = dbdate_format($date_time->add(new DateInterval("P" . $length . "D")));

        if ($uid != -1) {
//            dbquery("INSERT INTO offences (character_id, type, date, expire_date ,moderator_id, reason)
//                VALUES ('$uid','$type','$now','$expire','$mod_id', '$reason');");

            dbquery("INSERT INTO offences (character_id, type, date, expire_date, moderator_id, reason, appeal_status, appeal_data, expired)
 VALUES ('$uid', '$type', '$now', '$expire', '$mod_id', '$reason', '0', '', '0');");

            add_log($mod_id, $agent->ip_address, "Added new ban.<br />» $name");

            $usql = dbquery("SELECT * FROM characters WHERE username = '$name' LIMIT 1;");
            if (mysql_num_rows($usql) > 0) {
                $user_data = mysql_fetch_array($usql);

                // check if character is online, if so, kick from server.
                if ($user_data['online'] > 0) {
                    $rd = rmd_send("world" . $user_data['online'] . ":kick \"" . $name . "\"", MASTER_SERVER_IP, REMOTE_SERVER_PORT);
                }
            }

            die('<script type="text/javascript">top.location.href = \'offences.php?new=true\';</script>');
        } else {
            mng_error("No character found with given username.");
        }
    } else {
        mng_error("You don't have permissions to do that.");
    }
}
?>
<h1>Add Offence</h1>
<hr>
<p>This page allows staff to ban users who have broken a rule.</p><br/>

<form method="post" action="add_offence.php">
    <fieldset>
        <dl>
            <dt>
                <label for="name">Username (or email):</label><br/>
                <span>The name or email of the user to ban.</span>
            </dt>
            <dd><input id="name" type="text" size="20"
                       name="name" value="<?php if (isset($name)) {
                    echo $name;
                } ?>"/>
            </dd>
        </dl>

        <dl>
            <dt>
                <label for="reason">Reason:</label><br/>
                <span>A brief description of why the user is being banned.</span>
            </dt>
            <dd>
                <textarea id="reason" name="reason" rows="5" cols="45"><?php if (isset($reason)) {
                        echo $reason;
                    } ?></textarea>
            </dd>
        </dl>

        <dl>
            <dt>
                <label for="ban_type">Offense type:</label><br/>
                <span>The type of ban to put in this user..</span>
            </dt>
            <dd>
                <select id="ban_type" name="ban_type">
                    <option value="1" <?php if (isset($type)) {
                        if ($type == 1) echo "selected";
                    } ?>>Mute
                    </option>
                    <option value="2" <?php if (isset($type)) {
                        if ($type == 2) echo "selected";
                    } ?>>Ban
                    </option>
                </select>
            </dd>
        </dl>

        <dl>
            <dt>
                <label for="ban_length">Ban length:</label><br/>
                <span>How long the user is banned for.</span>
            </dt>
            <dd>
                <select id="ban_length" name="ban_length">
                    <option value="1" <?php if (isset($length)) {
                        if ($length == 1) echo "selected";
                    } ?>>1 Day
                    </option>
                    <option value="3" <?php if (isset($length)) {
                        if ($length == 3) echo "selected";
                    } ?>>3 Days
                    </option>
                    <option value="5" <?php if (isset($length)) {
                        if ($length == 5) echo "selected";
                    } ?>>5 Days
                    </option>
                    <option value="7" <?php if (isset($length)) {
                        if ($length == 7) echo "selected";
                    } ?>>1 Week
                    </option>
                    <option value="14" <?php if (isset($length)) {
                        if ($length == 14) echo "selected";
                    } ?>>2 Weeks
                    </option>
                    <option value="21" <?php if (isset($length)) {
                        if ($length == 21) echo "selected";
                    } ?>>3 Weeks
                    </option>
                    <option value="30" <?php if (isset($length)) {
                        if ($length == 30) echo "selected";
                    } ?>>1 Month
                    </option>
                    <option value="60" <?php if (isset($length)) {
                        if ($length == 60) echo "selected";
                    } ?>>2 Months
                    </option>
                    <option value="90" <?php if (isset($length)) {
                        if ($length == 90) echo "selected";
                    } ?>>3 Months
                    </option>
                    <option value="180" <?php if (isset($length)) {
                        if ($length == 180) echo "selected";
                    } ?>>6 Months
                    </option>
                    <option value="365" <?php if (isset($length)) {
                        if ($length == 365) echo "selected";
                    } ?>>1 Year
                    </option>
                    <option value="830" <?php if (isset($length)) {
                        if ($length == 830) echo "selected";
                    } ?>>2 Years
                    </option>
                </select>
            </dd>
        </dl>
    </fieldset>

    <fieldset class="submit-buttons">
        <input class="button1" type="submit" id="submit" name="submit" value="Submit"/>&nbsp;
    </fieldset>
</form>

<?php require_once("footer.php"); ?>

