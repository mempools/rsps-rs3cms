<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'Servers Status');
define('PAGE_TAB', 4);

if (!$agent->has_permission("web_admin")) {
    header("Location: index.php");
}

require_once("header.php");
?>

<h1>Servers Status</h1>
<hr>
<p>You can view all servers' status here. Note that you can only view the some information if the server is online.</p>
<br/>

<h3>Master Server</h3>
<table cellspacing="1">
    <thead>
    <tr>
        <th>Address</th>
        <th>Worlds Online</th>
        <th>Startup Time</th>
        <th>Memory Usage</th>
        <th>CPU Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?php printf(MASTER_SERVER_IP); ?></td>

        <?php
        $rd = rmd_send("get_server_info", MASTER_SERVER_IP, REMOTE_SERVER_PORT);
        if ($rd) {
            $data = explode(",", $rd);

            printf("<td>%d</td><td>%s</td><td>%s</td><td>%.2lf</td>", $data[0], $data[1], number_format($data[2]) . " KB", $data[3]);
        } else {
            printf("<td colspan='4'><center><font color=red>Server not online or not responding.</font></center></td>");
        }
        ?>
    </tr>
    </tbody>
</table>


<h3>Game Servers</h3>
<table cellspacing="1">
    <thead>
    <tr>
        <th width="5%">World</th>
        <th width="20%">Name</th>
        <th width="15%">Address</th>
        <th width="20%">Startup Time</th>
        <th width="20%">Memory Usage</th>
        <th width="20%">CPU Usage</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $count = 0;
    $q = dbquery("SELECT * FROM worlds");
    if (mysql_num_rows($q) > 0) {
        while ($q_data = mysql_fetch_assoc($q)) {
            $world_id = $q_data['id'];

            $rd = rmd_send("world$world_id:get_server_info", MASTER_SERVER_IP, REMOTE_SERVER_PORT);
            if ($rd) {
                $data = explode(",", $rd);
                printf("<td>%d</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%.2lf</td>", $world_id, $q_data['name'], $q_data['ip'], $data[0], number_format($data[1]) . " KB", $data[2]);
                $count++;
            }
        }
    }

    // this means no servers are online.
    if ($count == 0) {
        printf("<td colspan='6'><center><font color=red>No game servers online (or not responding).</font></center></td>");
    }
    ?>
    </tbody>
</table>

<?php require_once("footer.php"); ?>
