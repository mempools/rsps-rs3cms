<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'Staff Notes');
define('PAGE_TAB', 2);

require_once("header.php");

$show = 50;
$page = 1;
if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
}
$start_from = ($page - 1) * $show;

if (isset($_GET['key'])) {
    $key = $_GET['key'];
    $user_id = agent::id_from_name($key);
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_acp_notes WHERE user_id='$user_id';");
    $notes = dbquery("SELECT * FROM web_acp_notes WHERE user_id='$user_id' ORDER BY id DESC LIMIT $start_from, $show");
} else {
    $total_results = dbevaluate("SELECT COUNT(id) FROM web_acp_notes;");
    $notes = dbquery("SELECT * FROM web_acp_notes ORDER BY id DESC LIMIT $start_from, $show");
}
$total_pages = ceil($total_results / $show);
?>

<h1>Staff Notes</h1>
<hr>
<p>Notes that staff members have posted can be seen here. Please do not share notes outside of staff hands!</p>

<form method="get">
    <fieldset class="display-options" style="float: left">
        Search by name:
        <input type="text" name="key" value="<?php
        if (isset($key)) {
            printf($key);
        }
        ?>"/>&nbsp;
        <input type="submit" class="button2" value="Search"/>
    </fieldset>
</form>

<div class="pagination" style="float: right; margin: 15px 0 2px 0">
    <?php
    if ($total_pages > 1) {
        $base_url = basename($_SERVER['PHP_SELF']) . "?";
        if (isset($key)) {
            $base_url .= "key=$key&";
        }

        printf("Page $page of $total_pages &bull;");

        $adjacents = 3;
        $sp = " <span>";

        $previous_page = $page - 1;
        if ($page > 1) {
            $sp .= "&nbsp;&nbsp;<a href='" . $base_url . "page=" . $previous_page . "'>Previous</a>";
        }

        if ($total_pages < (7 + ($adjacents * 2))) {
            for ($i = 1; $i <= $total_pages; $i++) {
                if ($i == $page) {
                    $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                } else {
                    $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                }
            }
        } elseif ($total_pages > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($i = 1; $i < 4 + ($adjacents * 2); $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
                $sp .= "...";
                $sp .= "<a href='" . $base_url . "page=$total_pages'>$total_pages</a>";
            } elseif ($total_pages - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $sp .= "<a href='" . $base_url . "page=1'>1</a>";
                $sp .= "<a href='" . $base_url . "page=2'>2</a>";
                $sp .= "...";
                for ($i = $page - $adjacents; $i <= $page + $adjacents; $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
                $sp .= "...";
                $sp .= "<a href='" . $base_url . "page=$total_pages'>$total_pages</a>";
            } else {
                $sp .= "<a href='" . $base_url . "page=1'>1</a>";
                $sp .= "...";
                for ($i = $total_pages - (2 + ($adjacents * 2)); $i <= $total_pages; $i++) {
                    if ($i == $page) {
                        $sp .= "<strong>$page</strong><span class='page-sep'>,</span>";
                    } else {
                        $sp .= "<a href='" . $base_url . "page=$i'>$i</a>";
                    }
                }
            }
        }

        $next_page = $page + 1;
        if ($next_page <= $total_pages) {
            $sp .= "&nbsp;&nbsp;<a href='" . $base_url . "page=" . $next_page . "'>Next</a></span>";
        }
        printf($sp);
    }
    ?>
</div>

<table cellspacing="1">
    <thead>
    <tr>
        <th width="15%">Username</th>
        <th width="20%">Date</th>
        <th width="65%">Message</th>
    </tr>
    </thead>
    <tbody>
    <?php
    while ($note = mysql_fetch_assoc($notes)) {
        printf("<tr>");
        printf("<td><strong><a href=\"view_user.php?id=%d\">%s</strong></td>", $note['user_id'], agent::username_from_id($note['user_id']));
        printf("<td style=\"text-align: center;\">%s</td>", $note['note_date']);
        printf("<td style=\"WORD-BREAK:BREAK-ALL;\">%s</td>", filter_for_outout($note['note_message']));
        printf("</tr>");
    }
    ?>
    </tbody>

</table>

<?php require_once("footer.php"); ?>

