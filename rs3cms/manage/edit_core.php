<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('WINDOW_TITLE', 'Editing Core');
define('PAGE_TAB', 5);

require_once("manageglobal.php");

if (!$agent->has_permission("system_admin")) {
    header("Location: ../index.php");
}

require_once("header.php");
$conf = get_configs();
//$base_data = dbquery("SELECT * FROM configurations ORDER BY name;");
//while ($base_query = mysql_fetch_assoc($base_data)) {
//    printf("set_config(\"".$base_query['name']."\", $".$base_query['name'].");<br>");
//}
?>
<h1>Core CMS Configuration</h1>
<!--    <p>Here you can change your users information and certain specific options.</p>-->
<?php

if (isset($_POST['update'])) {
    $new_theme = $_POST['new_theme'];
    $debug_mode = $_POST['debug_mode'];
    $email = $_POST['email'];
    $paypal_currency = $_POST['paypal_currency'];
    $paypal_email = $_POST['paypal_email'];
    $recaptcha_key = $_POST['recaptcha_key'];
    $recaptcha_secret = $_POST['recaptcha_secret'];
    $remote_ip = $_POST['remote_ip'];
    $remote_port = $_POST['remote_port'];
    $server_desc = $_POST['server_desc'];
    $server_motto = $_POST['server_motto'];
    $server_name = $_POST['server_name'];
    $session_check = $_POST['session_check'];
    $site_address = $_POST['site_address'];
    $twitter_url = $_POST['twitter_url'];
    $twitter_user = $_POST['twitter_user'];
    $twitter_widget = $_POST['twitter_widget'];
    $url_donate = $_POST['url_donate'];
    $url_forums = $_POST['url_forums'];
    $url_support = $_POST['url_support'];
    $whitelist_ip = $_POST['whitelist_ip'];
    $youtube_channel = $_POST['youtube_channel'];
    $youtube_video = $_POST['youtube_video'];

    set_config("new_theme", $new_theme);
    set_config("debug_mode", $debug_mode);
    set_config("email", $email);
    set_config("paypal_currency", $paypal_currency);
    set_config("paypal_email", $paypal_email);
    set_config("recaptcha_key", $recaptcha_key);
    set_config("recaptcha_secret", $recaptcha_secret);
    set_config("server_desc", $server_desc);
    set_config("server_motto", $server_motto);
    set_config("server_name", $server_name);
    set_config("session_check", $session_check);
    set_config("site_address", $site_address);
    set_config("twitter_url", $twitter_url);
    set_config("twitter_user", $twitter_user);
    set_config("twitter_widget", $twitter_widget);
    set_config("url_donate", $url_donate);
    set_config("url_forums", $url_forums);
    set_config("url_support", $url_support);
    set_config("whitelist_ip", $whitelist_ip);
    set_config("youtube_channel", $youtube_channel);
    set_config("youtube_video", $youtube_video);

    mng_success("Successfully change made.");
}

mng_notice("NOTE: Always keep session checking ENABLED.");
?>
<form method="post" action="<?php printf($_SERVER['REQUEST_URI']); ?>">
    <fieldset>
        <br>
        <!--        <dl>-->
        <!--            <dt><label>CMS Theme</label></dt>-->
        <!--            <dd><input type="radio" name="new_theme"-->
        <!--                    -->
        <?php //if (isset($new_theme) && $new_theme=="rs3") echo "checked";?><!-- value="rs3">Rs3-->
        <!--                <input type="radio" name="new_theme"-->
        <!--                    -->
        <?php //if (isset($new_theme) && $new_theme=="eoc") echo "checked";?><!-- value="eoc">EOC-->
        <!--            </dd>-->
        <!--        </dl>-->
        <dl>
            <dt><label>Site Address</label></dt>
            <dd><input type="text" id="site_address" name="site_address"
                       value="<?php printf($conf['site_address']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Session Checking</label></dt>
            <dd><input type="text" id="session_check" name="session_check"
                       value="<?php printf($conf['session_check']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Debug Mode</label></dt>
            <dd><input type="text" id="debug_mode" name="debug_mode"
                       value="<?php printf($conf['debug_mode']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Whitelisted IPs</label></dt>
            <dd><input type="text" id="whitelist_ip" name="whitelist_ip"
                       value="<?php printf($conf['whitelist_ip']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Links</label></dt>
            <dd>
                Donate URL: <input type="text" id="url_donate" name="url_donate"
                                   value="<?php printf($conf['url_donate']); ?>"/><br>
                Forums URL: <input type="text" id="url_forums" name="url_forums"
                                   value="<?php printf($conf['url_forums']); ?>"/><br>
                Support URL: <input type="text" id="url_support" name="url_support"
                                    value="<?php printf($conf['url_support']); ?>"/>
            </dd>
        </dl>
        <hr>
        <dl>
            <dt><label>Recaptcha</label></dt>
            <dd>
                Public Key: <input type="text" id="recaptcha_key" name="recaptcha_key"
                                   value="<?php printf($conf['recaptcha_key']); ?>"/><br>
                Secret: <input type="text" id="recaptcha_secret" name="recaptcha_secret"
                               value="<?php printf($conf['recaptcha_secret']); ?>"/>
            </dd>
        </dl>
        <hr>
        <dl>
            <dt><label>Server Name</label></dt>
            <dd><input type="text" id="server_name" name="server_name"
                       value="<?php printf($conf['server_name']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Server Motto</label></dt>
            <dd><input type="text" id="server_motto" name="server_motto"
                       value="<?php printf($conf['server_motto']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Server Description</label></dt>
            <dd><input type="text" id="server_motto" name="server_desc"
                       value="<?php printf($conf['server_desc']); ?>"/>
            </dd>
        </dl>
        <hr>

        <dl>
            <dt><label>PayPal</label></dt>
            <dd>
                Email: <input type="text" id="paypal_email" name="paypal_email"
                              value="<?php printf($conf['paypal_email']); ?>"/><br>
                Currency: <input type="text" id="paypal_currency" name="paypal_currency"
                                 value="<?php printf($conf['paypal_currency']); ?>"/>
            </dd>
        </dl>

        <hr>
        <dl>
            <dt><label>Youtube</label></dt>
            <dd>
                Channel: <input type="text" id="youtube_channel" name="youtube_channel"
                                value="<?php printf($conf['youtube_channel']); ?>"/><br>
                Featured Video (Displays on home page): <input type="text" id="youtube_video"
                                                               name="youtube_video"
                                                               value="<?php printf($conf['youtube_video']); ?>"/>
            </dd>
        </dl>

        <dl>
            <dt><label>Twitter</label></dt>
            <dd>
                Twitter Username: <input type="text" id="twitter_user" name="twitter_user"
                                         value="<?php printf($conf['twitter_user']); ?>"/><br>
                Twitter URL: <input type="text" id="twitter_url" name="twitter_url"
                                    value="<?php printf($conf['twitter_url']); ?>"/><br>
                Twitter Widget: <input type="text" id="twitter_widget" name="twitter_widget"
                                       value="<?php printf($conf['twitter_widget']); ?>"/>
            </dd>
        </dl>
        <dl>
            <dt><label>Email</label></dt>
            <dd><input type="text" id="email" name="email"
                       value="<?php printf($conf['email']); ?>"/>
            </dd>
        </dl
        <hr>
        <br>
        <p class="quick">
            <input class="button1" type="submit" name="update" value="Submit"/>
        </p>
    </fieldset>
</form>

