<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('WINDOW_TITLE', 'User Overview');
define('PAGE_TAB', 3);

require_once("manageglobal.php");
require_once("header.php");

if (isset($_GET['id'])) {
    $user_id = $_GET['id'];
    if (is_numeric($user_id)) {
        $uquery = dbquery("SELECT * FROM characters WHERE id = '$user_id';");
    }
} else if (isset($_GET['email'])) {
    $user_email = filter_for_input($_GET['email']);
    $uquery = dbquery("SELECT * FROM characters WHERE email = '$user_email';");
} else if (isset($_GET['name'])) {
    $user_name = filter_for_input($_GET['name']);
    $uquery = dbquery("SELECT * FROM characters WHERE username = '$user_name';");
}

if (isset($uquery) && mysql_num_rows($uquery) > 0) {
    $udata = mysql_fetch_assoc($uquery);
    ?>

    <h1>User Overview :: <?php printf($udata['username']); ?></h1>
    <p>You can view basic information about a user here.</p>

    <fieldset>
        <dl>
            <dt><label>Master ID</label></dt>
            <dd><strong><?php printf($udata['id']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Email</label></dt>
            <dd><strong><?php printf($udata['email']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Username</label></dt>
            <dd><strong><?php printf($udata['username']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Online Status</label></dt>
            <dd><strong><?php printf(online_status($udata['online'])); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Registered</label></dt>
            <dd><strong><?php printf($udata['register_date']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Registered from IP</label></dt>
            <dd><strong><?php printf($udata['register_ip']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Last signin</label></dt>
            <dd><strong><?php printf($udata['last_signin']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Last signin IP</label></dt>
            <dd><strong><?php printf($udata['last_ip']); ?></strong></dd>
        </dl>
        <dl>
            <dt><label>Location</label></dt>
            <dd><strong><?php printf("%d,%d,%d", $udata['coord_x'], $udata['coord_y'], $udata['coord_z']); ?></strong>
            </dd>
        </dl>

        <?php if ($agent->has_permission("system_admin")) { ?>
            <p class="quick">
                <input class="button1" type="submit" value=" Edit User "
                       onclick="parent.location='edit_user.php?id=<?php printf($udata['id']); ?>&form=overview'"/>
            </p>
        <?php } ?>
    </fieldset>
    <?php
} else {
    mng_error("No user found.");
}
require_once("footer.php");
?>
