<?php

/*
 *     jWeb
 *     Copyright (C) 2011 Jolt Environment Team
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("manageglobal.php");

define('WINDOW_TITLE', 'View Articles');
define('PAGE_TAB', 5);

if (!$agent->has_permission("web_admin")) {
    header("Location: index.php");
}

require_once("header.php");
?>
<h1>News Articles</h1>
<hr>
<p>All published news articles are shown here. Allows you to see who made the article,
    when it was made, and an option to edit the article.</p><br/>

<script type="text/javascript">
    function show_confirm(id) {
        var r = confirm("Are you sure you want to delete this?");
        if (r == true) {
            window.location = "edit_article.php?delete=" + id;
        }
    }
</script>

<table cellspacing="1">
    <thead>
    <tr>
        <th width="10%">Date</th>
        <th width="10%">Author</th>
        <th width="20%">Category</th>
        <th width="55%">Title</th>
        <th width="5%">Options</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if (isset($_GET['category']) && is_numeric($_GET['category'])) {
        $cid = $_GET['category'];
        $articles = dbquery("SELECT * FROM web_news WHERE category_id = '$cid' ORDER BY id DESC;");
    } else if (isset($_GET['author'])) {
        $author = $_GET['author'];
        if (!is_numeric($author)) {
            $author = filter_for_input(agent::id_from_name($author));
        }
        $articles = dbquery("SELECT * FROM web_news WHERE author_id = '$author' ORDER BY id DESC;");
    } else if (isset($_GET['featured'])) {
        $articles = dbquery("SELECT * FROM web_news WHERE featured = '1' ORDER BY id DESC;");
    } else {
        $articles = dbquery("SELECT * FROM web_news ORDER BY id DESC;");
    }

    if (mysql_num_rows($articles) > 0) {
        while ($article = mysql_fetch_assoc($articles)) {
            $id = $article['id'];
            $date = simpledate($article['date']);
            $author = agent::username_from_id($article['author_id']);
            $title = $article['title'];
            $category_id = $article['category_id'];
            $category_name = news_category($category_id);

            printf("
                <tr>
                    <td>$date</td>
                    <td>$author</td>
                    <td>$category_name</td>
                    <td><strong><a href='../viewnews.php?article=$id' target='_blank'>$title</a></strong></td>
                    <td>
                        <a href='#' onclick='show_confirm($id)'><img src='images/icon_delete.gif' /></a>
                        <a href='edit_article.php?id=$id'><img src='images/icon_edit.gif' /></a>
                    </td>
                </tr>");
        }
    } else {
        echo "<tr><td colspan='5' style='text-align: center;'>No worlds have been created.</td></tr>";
    }
    ?>
    </tbody>
</table>

<?php require_once("footer.php"); ?>
