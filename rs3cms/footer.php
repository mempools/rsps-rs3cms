<?php
/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div id="Footer" class="Kingthings">
    <ul class="Main">
        <li>
            <a href="#">Account</a>
            <ul class="Sub">
                <li><a href="<?php printf(SITE_ADDRESS); ?>account_settings">Profile &amp; Settings</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Support</a>
            <ul class="Sub">
                <li><a href="<?php printf(BUGREPORT_URL); ?>">Submit a Bug Report</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Game</a>
            <ul class="Sub">
                <li><a href="#">Downloads</a></li>
            </ul>
        </li>
        <li>
            <a href="#">Community</a>
            <ul class="Sub">
                <li><a href="<?php printf(FORUMS_URL); ?>">Forums</a></li>
            </ul>
        </li>
    </ul>
    <h2 id="RuneScapeLogo">
        <a href="<?php printf(SITE_ADDRESS); ?>"><img src="<?php printf(SITE_ADDRESS); ?>img/global/logos/runescape.png"
                                                      alt="RuneScape" title=""/></a>
    </h2>

    <div class="FooterBottom">

        <a href="http://jagex.com/" id="JagexLogo">
            <img src="<?php printf(SITE_ADDRESS); ?>img/global/logos/jagex.png" alt="Jagex" title=""/>
        </a>

        <p>
            Images on this website are copyright &copy; 1999 - 2016 Jagex Ltd<br/>
            RuneScape content and materials are trademarks and copyrights of Jagex or its licensors. All rights
            reserved. This site is not affiliated with Jagex.<br/>
            You can play RuneScape <a href="http://runescape.com/">here.</a>.
        </p>
    </div>
</div>
</div>
</div>
</div>
</body>
</html>