<?php

/*
 *     jWeb
 *     Copyright (c) Jolt Environment
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once("global.php");
if (!$agent->logged_in) {
    exit(header("Location: home"));
}
if (isset($_GET['email'])) {
    $new_email = $_GET['email'];

    if (strcasecmp($new_email, $agent->username) == 0) {
        exit("OK");
    }

    $qs = "SELECT email FROM characters WHERE characters.email = '$new_email';";

    if (filter_var($new_email, FILTER_VALIDATE_EMAIL)) {
        $q = dbquery("SELECT email FROM characters WHERE characters.email = '$new_email';");
        if (mysql_num_rows($q) > 0) {
            echo $qs . "</br>";
            exit("NOK");
        }
    } else {
        echo $qs . "</br>";
        exit("NONAME");
    }
    echo $qs . "</br>";
    exit("OK");
}

echo $q;
?>
